<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Emailtemp 
{
 	public function att_attribute_email_template($title, $contents, $vlink, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            <a href="'.$vlink.'"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; "> Visit Website</button></a>
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function account_creation_template($title, $contents, $vlink, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            <a href="'.$vlink.'"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; "> Visit Website</button></a>
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function account_creation_with_social_media_template($title, $contents, $vlink, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            <a href="'.$vlink.'"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; "> Visit Website</button></a>
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function general_email_template($title, $contents, $vlink, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function payment_template_callback($title, $contents, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price)
	{
	
	$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Memorial</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <style type="text/css">

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: none;
            -webkit-text-resize: 100%;
            text-resize: 100%;
        }

        a {
            outline: none;
            color: #40aceb;
            text-decoration: underline;
        }

        a:hover {
            text-decoration: none !important;
        }

        .nav a:hover {
            text-decoration: underline !important;
        }

        .title a:hover {
            text-decoration: underline !important;
        }

        .title-2 a:hover {
            text-decoration: underline !important;
        }

        .btn:hover {
            opacity: 0.8;
        }

        .btn
        a:hover {
            text-decoration: none !important;
        }

        .btn {
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }

        table td {
            border-collapse: collapse !important;
        }

        .ExternalClass, .ExternalClass a, .ExternalClass span,
        .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
            line-height: inherit;
        }

        @media only screen and
        (max-width: 500px) {
            table[class="flexible"] {
                width: 100% !important;
                margin: 0px !important;
            }

            table[class="center"] {
                float: none !important;
                margin: 0 auto !important;
            }

            *[class="hide"] {
                display: none !important;
                width: 0 !important;
                height: 0 !important;
                padding: 0 !important;
                font-size: 0 !important;
                line-height: 0 !important;
            }

            td[class="img-flex"] img {
                width: 100% !important;
                height: auto !important;
            }

            td[class="aligncenter"] {
                text-align: center !important;
            }

            th[class="flex"] {
                display: block !important;
                width: 100% !important;
            }

            td[class="wrapper"] {
                padding: 0 !important;
            }
            p[class="descr"]{
                    margin: 5px 36px !important;
            }

            

            td[class="nav"] {
                padding: 20px 0 0 !important;
                text-align: center !important;
            }

            td[class="h-auto"] {
                height: auto !important;
            }

            td[class="description"] {
                padding: 30px 20px !important;
            }

            td[class="i-120"] img {
                width: 120px !important;
                height: auto !important;
            }

            td[class="footer"] {
                padding: 5px 20px 20px !important;
            }

            td[class="footer"] td[class="aligncenter"] {
                line-height: 25px !important;
                padding: 20px 0 0 !important;
            }

            tr[class="table-holder"] {
                display: table !important;
                width: 100% !important;
            }

            th[class="thead"] {
                display: table-header-group !important;
                width: 100% !important;
            }

            th[class="tfoot"] {
                display: table-footer-group !important;
                width: 100% !important;
            }
        }
     
  .row {justify-content: space-between;}    

.row {
  display: flex;
  flex-direction: row;
}
.row-vertical {
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-between;
}
.row-vertical > .col {
  flex: 0;
}
.row > .col {
  flex: 1;
}
.row > .col.half {
  flex-basis: 50%;
}
.rTableRow{
 display: table-row;
}

.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid #582236;
}
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
    
<body style="margin:0; padding:0;" bgcolor="#eaeced">

<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
    <!-- fix for gmail -->
    <tr>
        <td class="hide">
            <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                <tr>
                    <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td class="wrapper" style="padding:0 10px;">
            <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                        <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            
                                            <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                  <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                            </td>
                                        </tr>
            
			
													<tr>
														<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
															<h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
															<p style="color: #582236" class="descr">Here is your payment receipt.</p>
															 <h3 class="text-left" style="color: #582236;font-size: 18px;"><b>Payment Receipt Summary</b></h3>
																<table class="table" style="border: 1px solid #582236; width: 100% ">
																	<thead>
																		<tr style="border-bottom: 2px solid #582236 ">
																			<td style="color: #582236"><strong> In Memory Of </strong></td>
																			<td class="text-center" style="color: #582236 "><strong>Package</strong></td>
																			<td class="text-right" style="color: #582236"><strong>Total</strong></td>
																		</tr>
																	</thead>
																	<tbody>
																		<!-- foreach ($order->lineItems as $line) or some such thing here -->
																		<tr>
																			<td style="color:#582236 ">'.$memorial_name.'</td>
																			<td class="text-center" style="color:#582236 ">'.$pakage.'</td>
																			<td class="text-right" style="color:#582236 ">'.$pakage_price.' KES</td>
																		</tr>
																	  
																		<tr>
																			<td class="thick-line" style="color:#582236 "></td>
																			<td class="thick-line text-center" style="color:#582236 "><strong>Subtotal</strong></td>
																			<td class="thick-line text-right" style="color:#582236 ">'.$pakage_price.' KES</td>
																		</tr>
																		<tr>
																			<td class="no-line" style="color:#582236 "></td>
																			<td class="no-line text-center" style="color:#582236 "><strong>Discount</strong></td>
																			<td class="no-line text-right" style="color:#582236 ">'.($pakage_price - $price).' KES</td>
																		</tr>
																		<tr>
																			<td class="no-line" style="color:#582236 "></td>
																			<td class="no-line text-center" style="color:#582236 "><strong>Total</strong></td>
																			<td class="no-line text-right" style="color:#582236 ">'.$price.' KES</td>
																		</tr>
																	</tbody>
																</table>
															<p class="text-center descr" style="color:#582236" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center" style="color:#582236">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
															
														   

														</td>
													</tr>
													
													
                                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="28"></td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
                     <!-- module 7 -->
                                <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                            <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="footer" style="padding:0 0 10px;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr class="table-holder">
                                                                <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">

                                                                        <tr>
                                                                            <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                 If you believe that the email doesn’t belong to you please respond to this email
                                                                                immediately.
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                              Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                <!--<a href="#">Unsubscribe</a>-->
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

           
        </td>
    </tr>

    <!-- fix for gmail -->

    <tr>
        <td style="line-height:0;">
            <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp;
            </div>
        </td>
    </tr>

</table>
    
</body>
</html>';	
 		return $html;
	}
	
	public function payment_email_upgrade($title, $contents, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price){
		
		$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Memorial</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <style type="text/css">

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: none;
            -webkit-text-resize: 100%;
            text-resize: 100%;
        }

        a {
            outline: none;
            color: #40aceb;
            text-decoration: underline;
        }

        a:hover {
            text-decoration: none !important;
        }

        .nav a:hover {
            text-decoration: underline !important;
        }

        .title a:hover {
            text-decoration: underline !important;
        }

        .title-2 a:hover {
            text-decoration: underline !important;
        }

        .btn:hover {
            opacity: 0.8;
        }

        .btn
        a:hover {
            text-decoration: none !important;
        }

        .btn {
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }

        table td {
            border-collapse: collapse !important;
        }

        .ExternalClass, .ExternalClass a, .ExternalClass span,
        .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
            line-height: inherit;
        }

        @media only screen and
        (max-width: 500px) {
            table[class="flexible"] {
                width: 100% !important;
                margin: 0px !important;
            }

            table[class="center"] {
                float: none !important;
                margin: 0 auto !important;
            }

            *[class="hide"] {
                display: none !important;
                width: 0 !important;
                height: 0 !important;
                padding: 0 !important;
                font-size: 0 !important;
                line-height: 0 !important;
            }

            td[class="img-flex"] img {
                width: 100% !important;
                height: auto !important;
            }

            td[class="aligncenter"] {
                text-align: center !important;
            }

            th[class="flex"] {
                display: block !important;
                width: 100% !important;
            }

            td[class="wrapper"] {
                padding: 0 !important;
            }
            p[class="descr"]{
                    margin: 5px 36px !important;
            }

            

            td[class="nav"] {
                padding: 20px 0 0 !important;
                text-align: center !important;
            }

            td[class="h-auto"] {
                height: auto !important;
            }

            td[class="description"] {
                padding: 30px 20px !important;
            }

            td[class="i-120"] img {
                width: 120px !important;
                height: auto !important;
            }

            td[class="footer"] {
                padding: 5px 20px 20px !important;
            }

            td[class="footer"] td[class="aligncenter"] {
                line-height: 25px !important;
                padding: 20px 0 0 !important;
            }

            tr[class="table-holder"] {
                display: table !important;
                width: 100% !important;
            }

            th[class="thead"] {
                display: table-header-group !important;
                width: 100% !important;
            }

            th[class="tfoot"] {
                display: table-footer-group !important;
                width: 100% !important;
            }
        }
     
  .row {justify-content: space-between;}    

.row {
  display: flex;
  flex-direction: row;
}
.row-vertical {
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-between;
}
.row-vertical > .col {
  flex: 0;
}
.row > .col {
  flex: 1;
}
.row > .col.half {
  flex-basis: 50%;
}
.rTableRow{
 display: table-row;
}

.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid #582236;
}
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
    
<body style="margin:0; padding:0;" bgcolor="#eaeced">

<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
    <!-- fix for gmail -->
    <tr>
        <td class="hide">
            <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                <tr>
                    <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td class="wrapper" style="padding:0 10px;">
            <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                        <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            
                                            <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                  <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                            </td>
                                        </tr>
            
			
													<tr>
														<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
															<h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
															<p style="color: #582236" class="descr">Here is your payment receipt.</p>
															 <h3 class="text-left" style="color: #582236;font-size: 18px;"><b>Payment Receipt Summary</b></h3>
																<table class="table" style="border: 1px solid #582236; width: 100%">
																	<thead>
																		<tr style="border-bottom: 2px solid #582236 ">
																			<td style="color: #582236"><strong>Upgradation of Memorial</strong></td>
																			<td class="text-center" style="color: #582236 "><strong>Package</strong></td>
																			<td class="text-right" style="color: #582236"><strong>Total</strong></td>
																		</tr>
																	</thead>
																	<tbody>
																		<!-- foreach ($order->lineItems as $line) or some such thing here -->
																		<tr>
																			<td style="color:#582236 ">'.$memorial_name.'</td>
																			<td class="text-center" style="color:#582236 ">'.$pakage.'</td>
																			<td class="text-right" style="color:#582236 ">'.$pakage_price.' KES</td>
																		</tr>
																	  
																		<tr>
																			<td class="thick-line" style="color:#582236 "></td>
																			<td class="thick-line text-center" style="color:#582236 "><strong>Subtotal</strong></td>
																			<td class="thick-line text-right" style="color:#582236 ">'.$pakage_price.' KES</td>
																		</tr>
																		<tr>
																			<td class="no-line" style="color:#582236 "></td>
																			<td class="no-line text-center" style="color:#582236 "><strong>Discount</strong></td>
																			<td class="no-line text-right" style="color:#582236 ">'.($pakage_price - $price).' KES</td>
																		</tr>
																		<tr>
																			<td class="no-line" style="color:#582236 "></td>
																			<td class="no-line text-center" style="color:#582236 "><strong>Total</strong></td>
																			<td class="no-line text-right" style="color:#582236 ">'.$price.' KES</td>
																		</tr>
																	</tbody>
																</table>
															<p class="text-center descr" style="color:#582236" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center" style="color:#582236">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
															
														   

														</td>
													</tr>
													
													
                                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="28"></td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
                     <!-- module 7 -->
                                <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                            <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="footer" style="padding:0 0 10px;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr class="table-holder">
                                                                <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">

                                                                        <tr>
                                                                            <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                 If you believe that the email doesn’t belong to you please respond to this email
                                                                                immediately.
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                              Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                <!--<a href="#">Unsubscribe</a>-->
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

           
        </td>
    </tr>

    <!-- fix for gmail -->

    <tr>
        <td style="line-height:0;">
            <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp;
            </div>
        </td>
    </tr>

</table>
    
</body>
</html>';
	
	return $html;
		
	}
	
	public function payment_email_template($title, $contents, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function anniversary_email_template($content, $to_name, $vlink)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                 
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b>Dear '.$to_name.',</b> </h2>
                                                            
															'.$content.'
															
															<p class="text-center descr" style="color:#582236" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>															
                                                            <p class="text-center" style="color:#582236">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                            
                                                            <a href="'.$vlink.'"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; "> Visit Memorial Website</button></a>
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function post_summary_email_template()
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<title>Memorial</title>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0" />
						<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
						<style type="text/css">
					
							* {
								-ms-text-size-adjust: 100%;
								-webkit-text-size-adjust: none;
								-webkit-text-resize: 100%;
								text-resize: 100%;
							}
					
							a {
								outline: none;
								color: #40aceb;
								text-decoration: underline;
							}
					
							a:hover {
								text-decoration: none !important;
							}
					
							.nav a:hover {
								text-decoration: underline !important;
							}
					
							.title a:hover {
								text-decoration: underline !important;
							}
					
							.title-2 a:hover {
								text-decoration: underline !important;
							}
					
							.btn:hover {
								opacity: 0.8;
							}
					
							.btn
							a:hover {
								text-decoration: none !important;
							}
					
							.btn {
								-webkit-transition: all 0.3s ease;
								-moz-transition: all 0.3s ease;
								-ms-transition: all 0.3s ease;
								transition: all 0.3s ease;
							}
					
							table td {
								border-collapse: collapse !important;
							}
					
							.ExternalClass, .ExternalClass a, .ExternalClass span,
							.ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
								line-height: inherit;
							}
					
							@media only screen and
							(max-width: 500px) {
								table[class="flexible"] {
									width: 100% !important;
									margin: 0px !important;
								}
					
								table[class="center"] {
									float: none !important;
									margin: 0 auto !important;
								}
					
								*[class="hide"] {
									display: none !important;
									width: 0 !important;
									height: 0 !important;
									padding: 0 !important;
									font-size: 0 !important;
									line-height: 0 !important;
								}
					
								td[class="img-flex"] img {
									width: 100% !important;
									height: auto !important;
								}
					
								td[class="aligncenter"] {
									text-align: center !important;
								}
					
								th[class="flex"] {
									display: block !important;
									width: 100% !important;
								}
					
								td[class="wrapper"] {
									padding: 0 !important;
								}
								p[class="descr"]{
										margin: 5px 36px !important;
								}
					
								
					
								td[class="nav"] {
									padding: 20px 0 0 !important;
									text-align: center !important;
								}
					
								td[class="h-auto"] {
									height: auto !important;
								}
					
								td[class="description"] {
									padding: 30px 20px !important;
								}
					
								td[class="i-120"] img {
									width: 120px !important;
									height: auto !important;
								}
					
								td[class="footer"] {
									padding: 5px 20px 20px !important;
								}
					
								td[class="footer"] td[class="aligncenter"] {
									line-height: 25px !important;
									padding: 20px 0 0 !important;
								}
					
								tr[class="table-holder"] {
									display: table !important;
									width: 100% !important;
								}
					
								th[class="thead"] {
									display: table-header-group !important;
									width: 100% !important;
								}
					
								th[class="tfoot"] {
									display: table-footer-group !important;
									width: 100% !important;
								}
							}
						 
					  .row {justify-content: space-between;}    
					
					.row {
					  display: flex;
					  flex-direction: row;
					}
					.row-vertical {
					  display: flex;
					  height: 100%;
					  flex-direction: column;
					  justify-content: space-between;
					}
					.row-vertical > .col {
					  flex: 0;
					}
					.row > .col {
					  flex: 1;
					}
					.row > .col.half {
					  flex-basis: 50%;
					}
					.rTableRow{
					 display: table-row;
					}
					.table-header{
						background: #fbad18;
						padding: 10px;
						color: #000;
						width: 100%;
						font-size: 14px
						
					}
					.table-row{
						padding: 10px;
						border-bottom: #d7d7d7 solid 1px;
					
						width: 100%;
					}
					.rTableCell{
						display: table-cell;
						padding: 10px 10px;
						color: #000000;
							}
					.rTableHead{
							padding: 0px 6px;
							}
							.rTable{
								width: 100%;
								display: table;
							}
						</style>
					
						<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
					
					</head>
						
					<body style="margin:0; padding:0;" bgcolor="#eaeced">
					
					<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
						<!-- fix for gmail -->
						<tr>
							<td class="hide">
								<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
									<tr>
										<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					
						<tr>
							<td class="wrapper" style="padding:0 10px;">
								<table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td data-bgcolor="bg-module" bgcolor="#eaeced">
											<table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
												<tr>
													<td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr>
																
																<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
																	  <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
																</td>
															</tr>
					
															<tr>
																<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
																	<h2 style="color: #582236"><b>Dear Doe,</b> </h2>
																	<p style="color: #582236" class="descr">Please find a summary of posts shared on <a href="#" style="color: #582236;text-decoration: underline"><b>Johns Online Memorial</b></a> since your last visit:</p>
																	<p><a href="#"><button class="btn btn-danger round btn-glow px-5 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#eaeced;color: #582236;border: 1px solid ; #582236; border-radius: 30px;height: 25px;padding-top: 3px">2 new tributes</button></a>
																		<a href="#"><button class="btn btn-danger round btn-glow px-5 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#eaeced;color: #582236;border: 1px solid ; #582236; border-radius: 30px;height: 25px;padding-top: 3px">View</button></a></p>
																	<p class="text-center descr" style="color:#582236" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
																	<p class="text-center" style="color:#582236">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
																	
																	<a href="#"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; ">See Online Memorial</button></a>
					
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td height="28"></td>
												</tr>
											</table>
										</td>
					
									</tr>
								</table>
										 <!-- module 7 -->
													<table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td data-bgcolor="bg-module" bgcolor="#eaeced">
																<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
																	<tr>
																		<td class="footer" style="padding:0 0 10px;">
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr class="table-holder">
																					<th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
																						<table width="100%" cellpadding="0" cellspacing="0">
					
																							<tr>
																								<td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
																									 If you believe that the email doesn not belong to you please respond to this email
																									immediately.
																								</td>
																							</tr>
					
																							<tr>
																								<td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
																								  Safiri Salama  © 2019  &nbsp; All Rights Reserved.
																									<!--<a href="#">Unsubscribe</a>-->
																								</td>
																							</tr>
																						</table>
					
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
					
							   
							</td>
						</tr>
					
						<!-- fix for gmail -->
					
						<tr>
							<td style="line-height:0;">
								<div style="display:none; white-space:nowrap; font:15px/1px courier;">
									&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
									&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
									&nbsp; &nbsp; &nbsp;
								</div>
							</td>
						</tr>
					
					</table>
						
					</body>
					</html>';	
 		return $html;
	}
	
	public function suggestion_email_template()
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<title>Memorial</title>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0" />
						<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
						<style type="text/css">
					
							* {
								-ms-text-size-adjust: 100%;
								-webkit-text-size-adjust: none;
								-webkit-text-resize: 100%;
								text-resize: 100%;
							}
					
							a {
								outline: none;
								color: #40aceb;
								text-decoration: underline;
							}
					
							a:hover {
								text-decoration: none !important;
							}
					
							.nav a:hover {
								text-decoration: underline !important;
							}
					
							.title a:hover {
								text-decoration: underline !important;
							}
					
							.title-2 a:hover {
								text-decoration: underline !important;
							}
					
							.btn:hover {
								opacity: 0.8;
							}
					
							.btn
							a:hover {
								text-decoration: none !important;
							}
					
							.btn {
								-webkit-transition: all 0.3s ease;
								-moz-transition: all 0.3s ease;
								-ms-transition: all 0.3s ease;
								transition: all 0.3s ease;
							}
					
							table td {
								border-collapse: collapse !important;
							}
					
							.ExternalClass, .ExternalClass a, .ExternalClass span,
							.ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
								line-height: inherit;
							}
					
							@media only screen and
							(max-width: 500px) {
								table[class="flexible"] {
									width: 100% !important;
									margin: 0px !important;
								}
					
								table[class="center"] {
									float: none !important;
									margin: 0 auto !important;
								}
					
								*[class="hide"] {
									display: none !important;
									width: 0 !important;
									height: 0 !important;
									padding: 0 !important;
									font-size: 0 !important;
									line-height: 0 !important;
								}
					
								td[class="img-flex"] img {
									width: 100% !important;
									height: auto !important;
								}
					
								td[class="aligncenter"] {
									text-align: center !important;
								}
					
								th[class="flex"] {
									display: block !important;
									width: 100% !important;
								}
					
								td[class="wrapper"] {
									padding: 0 !important;
								}
								p[class="descr"]{
										margin: 5px 36px !important;
								}
					
								
					
								td[class="nav"] {
									padding: 20px 0 0 !important;
									text-align: center !important;
								}
					
								td[class="h-auto"] {
									height: auto !important;
								}
					
								td[class="description"] {
									padding: 30px 20px !important;
								}
					
								td[class="i-120"] img {
									width: 120px !important;
									height: auto !important;
								}
					
								td[class="footer"] {
									padding: 5px 20px 20px !important;
								}
					
								td[class="footer"] td[class="aligncenter"] {
									line-height: 25px !important;
									padding: 20px 0 0 !important;
								}
					
								tr[class="table-holder"] {
									display: table !important;
									width: 100% !important;
								}
					
								th[class="thead"] {
									display: table-header-group !important;
									width: 100% !important;
								}
					
								th[class="tfoot"] {
									display: table-footer-group !important;
									width: 100% !important;
								}
							}
						 
					  .row {justify-content: space-between;}    
					
					.row {
					  display: flex;
					  flex-direction: row;
					}
					.row-vertical {
					  display: flex;
					  height: 100%;
					  flex-direction: column;
					  justify-content: space-between;
					}
					.row-vertical > .col {
					  flex: 0;
					}
					.row > .col {
					  flex: 1;
					}
					.row > .col.half {
					  flex-basis: 50%;
					}
					.rTableRow{
					 display: table-row;
					}
					.table-header{
						background: #fbad18;
						padding: 10px;
						color: #000;
						width: 100%;
						font-size: 14px
						
					}
					.table-row{
						padding: 10px;
						border-bottom: #d7d7d7 solid 1px;
					
						width: 100%;
					}
					.rTableCell{
						display: table-cell;
						padding: 10px 10px;
						color: #000000;
							}
					.rTableHead{
							padding: 0px 6px;
							}
							.rTable{
								width: 100%;
								display: table;
							}
							#more {display: none;}
						</style>
					
						<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
					
					</head>
						
					<body style="margin:0; padding:0;" bgcolor="#eaeced">
					
					<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
						<!-- fix for gmail -->
						<tr>
							<td class="hide">
								<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
									<tr>
										<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					
						<tr>
							<td class="wrapper" style="padding:0 10px;">
								<table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td data-bgcolor="bg-module" bgcolor="#eaeced">
											<table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
												<tr>
													<td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr>
																
																<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
																	  <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
																</td>
															</tr>
					
															<tr>
																<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
																	<h2 style="color: #582236"><b>Dear Doe,</b> </h2>
																	<p style="color: #582236" class="descr">It is our hope that the online memorial that you recently created for John is helping you remember and celeberate a very special life. </p>
																	<p style="color: #582236" class="descr">At this time, we would like to suggest some of the ways to further enhance his website.  </p>
																	<p><a href="#"><button class="btn btn-danger round btn-glow px-5 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#eaeced;color: #582236;border: 1px solid ; #582236; border-radius: 30px;height: 25px;padding-top: 3px">Invite Johns Family Members</button></a>
																	</p>
																	<p><a href="#"><button class="btn btn-danger round btn-glow px-5 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#eaeced;color: #582236;border: 1px solid ; #582236; border-radius: 30px;height: 25px;padding-top: 3px">Add a cover photo</button></a>
																	</p>
																	<p><a style="color:#582236; text-decoration: underline"><b>Go to Website ></b></a></p>
																	<p class="text-center descr" style="color:#582236" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
																	<p class="text-center" style="color:#582236">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
																	
																	<a href="#"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; ">See Online Memorial</button></a>
					
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td height="28"></td>
												</tr>
											</table>
										</td>
					
									</tr>
								</table>
										 <!-- module 7 -->
													<table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td data-bgcolor="bg-module" bgcolor="#eaeced">
																<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
																	<tr>
																		<td class="footer" style="padding:0 0 10px;">
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr class="table-holder">
																					<th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
																						<table width="100%" cellpadding="0" cellspacing="0">
					
																							<tr>
																								<td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
																									 If you believe that the email doesn not belong to you please respond to this email
																									immediately.
																								</td>
																							</tr>
					
																							<tr>
																								<td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
																								  Safiri Salama  © 2019  &nbsp; All Rights Reserved.
																									<!--<a href="#">Unsubscribe</a>-->
																								</td>
																							</tr>
																						</table>
					
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
					
							   
							</td>
						</tr>
					
						<!-- fix for gmail -->
					
						<tr>
							<td style="line-height:0;">
								<div style="display:none; white-space:nowrap; font:15px/1px courier;">
									&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
									&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
									&nbsp; &nbsp; &nbsp;
								</div>
							</td>
						</tr>
					
					</table>
						
						<script>
						
						function myFunction() {
					  var dots = document.getElementById("dots");
					  var moreText = document.getElementById("more");
					  var btnText = document.getElementById("myBtn");
					
					  if (dots.style.display === "none") {
						dots.style.display = "inline";
						btnText.innerHTML = "Read more"; 
						moreText.style.display = "none";
					  } else {
						dots.style.display = "none";
						btnText.innerHTML = "Read less"; 
						moreText.style.display = "inline";
					  }
					}
						</script>
					</body>
					</html>';	
 		return $html;
	}
	
	public function resendinvoice($contents, $to_name){
		
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
		
	}
	
	public function payment_renewal_template($contents, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function payment_upgrade_template($contents, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	public function memorial_reminder_template($contents, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html; 
	}
	
	public function memorial_admin_msg_template($contents, $to_name)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
         <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Memorial</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
                <style type="text/css">
            
                    * {
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: none;
                        -webkit-text-resize: 100%;
                        text-resize: 100%;
                    }
            
                    a {
                        outline: none;
                        color: #40aceb;
                        text-decoration: underline;
                    }
            
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .nav a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title a:hover {
                        text-decoration: underline !important;
                    }
            
                    .title-2 a:hover {
                        text-decoration: underline !important;
                    }
            
                    .btn:hover {
                        opacity: 0.8;
                    }
            
                    .btn
                    a:hover {
                        text-decoration: none !important;
                    }
            
                    .btn {
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }
            
                    table td {
                        border-collapse: collapse !important;
                    }
            
                    .ExternalClass, .ExternalClass a, .ExternalClass span,
                    .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
                        line-height: inherit;
                    }
            
                    @media only screen and
                    (max-width: 500px) {
                        table[class="flexible"] {
                            width: 100% !important;
                            margin: 0px !important;
                        }
            
                        table[class="center"] {
                            float: none !important;
                            margin: 0 auto !important;
                        }
            
                        *[class="hide"] {
                            display: none !important;
                            width: 0 !important;
                            height: 0 !important;
                            padding: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        td[class="img-flex"] img {
                            width: 100% !important;
                            height: auto !important;
                        }
            
                        td[class="aligncenter"] {
                            text-align: center !important;
                        }
            
                        th[class="flex"] {
                            display: block !important;
                            width: 100% !important;
                        }
            
                        td[class="wrapper"] {
                            padding: 0 !important;
                        }
                        p[class="descr"]{
                                margin: 5px 36px !important;
                        }
            
                        
            
                        td[class="nav"] {
                            padding: 20px 0 0 !important;
                            text-align: center !important;
                        }
            
                        td[class="h-auto"] {
                            height: auto !important;
                        }
            
                        td[class="description"] {
                            padding: 30px 20px !important;
                        }
            
                        td[class="i-120"] img {
                            width: 120px !important;
                            height: auto !important;
                        }
            
                        td[class="footer"] {
                            padding: 5px 20px 20px !important;
                        }
            
                        td[class="footer"] td[class="aligncenter"] {
                            line-height: 25px !important;
                            padding: 20px 0 0 !important;
                        }
            
                        tr[class="table-holder"] {
                            display: table !important;
                            width: 100% !important;
                        }
            
                        th[class="thead"] {
                            display: table-header-group !important;
                            width: 100% !important;
                        }
            
                        th[class="tfoot"] {
                            display: table-footer-group !important;
                            width: 100% !important;
                        }
                    }
                  
              .row {justify-content: space-between;}    
            
            .row {
              display: flex;
              flex-direction: row;
            }
            .row-vertical {
              display: flex;
              height: 100%;
              flex-direction: column;
              justify-content: space-between;
            }
            .row-vertical > .col {
              flex: 0;
            }
            .row > .col {
              flex: 1;
            }
            .row > .col.half {
              flex-basis: 50%;
            }
            .rTableRow{
             display: table-row;
            }
            .table-header{
                background: #fbad18;
                padding: 10px;
                color: #000;
                width: 100%;
                font-size: 14px
                
            }
            .table-row{
                padding: 10px;
                border-bottom: #d7d7d7 solid 1px;
            
                width: 100%;
            }
            .rTableCell{
                display: table-cell;
                padding: 10px 10px;
                color: #000000;
                    }
            .rTableHead{
                    padding: 0px 6px;
                    }
                    .rTable{
                        width: 100%;
                        display: table;
                    }
                </style>
            
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                    <td class="hide">
                        <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                            <tr>
                                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
                <tr>
                    <td class="wrapper" style="padding:0 10px;">
                        <table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                    <table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        
                                                        <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
                                                              <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
                                                        </td>
                                                    </tr>
            
                                                    <tr>
                                                        <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
                                                            <h2 style="color: #582236"><b> Dear '.$to_name.',</b> </h2>
                                                            <p class="descr">'.$contents.'</p>
															
															<p class="text-center descr" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
															<p class="text-center">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
                                                             
                                                            
            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="28"></td>
                                        </tr>
                                    </table>
                                </td>
            
                            </tr>
                        </table>
                                 <!-- module 7 -->
                                            <table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td data-bgcolor="bg-module" bgcolor="#eaeced">
                                                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="footer" style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="table-holder">
                                                                            <th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
            
                                                                                    <tr>
                                                                                        <td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                             If you believe that the email doesnt belong to you please respond to this email
                                                                                            immediately.
                                                                                        </td>
                                                                                    </tr>
            
                                                                                    <tr>
                                                                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
                                                                                          Safiri Salama  © 2019  &nbsp; All Rights Reserved.
                                                                                            <!--<a href="#">Unsubscribe</a>-->
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
            
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
            
                       
                    </td>
                </tr>
            
                <!-- fix for gmail -->
            
                <tr>
                    <td style="line-height:0;">
                        <div style="display:none; white-space:nowrap; font:15px/1px courier;">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp;
                        </div>
                    </td>
                </tr>
            
            </table>
                
            </body>
        </html>';	
 		return $html;
	}
	
	
	public function memorial_invitation_template($title, $to_name, $memo_link, $memory_of, $new_user_text, $accept_link, $reject_link)
	{
		$html =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>Memorial</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
			<style type="text/css">

				* {
					-ms-text-size-adjust: 100%;
					-webkit-text-size-adjust: none;
					-webkit-text-resize: 100%;
					text-resize: 100%;
				}

				a {
					outline: none;
					color: #40aceb;
					text-decoration: underline;
				}

				a:hover {
					text-decoration: none !important;
				}

				.nav a:hover {
					text-decoration: underline !important;
				}

				.title a:hover {
					text-decoration: underline !important;
				}

				.title-2 a:hover {
					text-decoration: underline !important;
				}

				.btn:hover {
					opacity: 0.8;
				}

				.btn
				a:hover {
					text-decoration: none !important;
				}

				.btn {
					-webkit-transition: all 0.3s ease;
					-moz-transition: all 0.3s ease;
					-ms-transition: all 0.3s ease;
					transition: all 0.3s ease;
				}

				table td {
					border-collapse: collapse !important;
				}

				.ExternalClass, .ExternalClass a, .ExternalClass span,
				.ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
					line-height: inherit;
				}

				@media only screen and
				(max-width: 500px) {
					table[class="flexible"] {
						width: 100% !important;
						margin: 0px !important;
					}

					table[class="center"] {
						float: none !important;
						margin: 0 auto !important;
					}

					*[class="hide"] {
						display: none !important;
						width: 0 !important;
						height: 0 !important;
						padding: 0 !important;
						font-size: 0 !important;
						line-height: 0 !important;
					}

					td[class="img-flex"] img {
						width: 100% !important;
						height: auto !important;
					}

					td[class="aligncenter"] {
						text-align: center !important;
					}

					th[class="flex"] {
						display: block !important;
						width: 100% !important;
					}

					td[class="wrapper"] {
						padding: 0 !important;
					}
					p[class="descr"]{
							margin: 5px 36px !important;
					}

					

					td[class="nav"] {
						padding: 20px 0 0 !important;
						text-align: center !important;
					}

					td[class="h-auto"] {
						height: auto !important;
					}

					td[class="description"] {
						padding: 30px 20px !important;
					}

					td[class="i-120"] img {
						width: 120px !important;
						height: auto !important;
					}

					td[class="footer"] {
						padding: 5px 20px 20px !important;
					}

					td[class="footer"] td[class="aligncenter"] {
						line-height: 25px !important;
						padding: 20px 0 0 !important;
					}

					tr[class="table-holder"] {
						display: table !important;
						width: 100% !important;
					}

					th[class="thead"] {
						display: table-header-group !important;
						width: 100% !important;
					}

					th[class="tfoot"] {
						display: table-footer-group !important;
						width: 100% !important;
					}
				}
			 
		  .row {justify-content: space-between;}    

		.row {
		  display: flex;
		  flex-direction: row;
		}
		.row-vertical {
		  display: flex;
		  height: 100%;
		  flex-direction: column;
		  justify-content: space-between;
		}
		.row-vertical > .col {
		  flex: 0;
		}
		.row > .col {
		  flex: 1;
		}
		.row > .col.half {
		  flex-basis: 50%;
		}
		.rTableRow{
		 display: table-row;
		}
		.table-header{
			background: #fbad18;
			padding: 10px;
			color: #000;
			width: 100%;
			font-size: 14px
			
		}
		.table-row{
			padding: 10px;
			border-bottom: #d7d7d7 solid 1px;

			width: 100%;
		}
		.rTableCell{
			display: table-cell;
			padding: 10px 10px;
			color: #000000;
				}
		.rTableHead{
				padding: 0px 6px;
				}
				.rTable{
					width: 100%;
					display: table;
				}
			</style>

			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		</head>
			
		<body style="margin:0; padding:0;" bgcolor="#eaeced">

		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
			<!-- fix for gmail -->
			<tr>
				<td class="hide">
					<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
						<tr>
							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td class="wrapper" style="padding:0 10px;">
					<table data-module="module-6" data-thumb="thumbnails/06.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="600" align="center" style="margin:30px auto 0; border-radius: 10px; overflow: hidden;" cellpadding="0" cellspacing="0">
									<tr>
										<td style="border-radius: 10px; overflow: hidden;" align="center" class="holder" bgcolor="#f9f9f9">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													
													<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:20px/23px Arial, Helvetica, sans-serif; color:#292c34; padding:10px; background:#fff;font-weight: bold;color: #fff; text-align: center">
														 
														  <div><img src="'.base_url("assets/img/logo.png").'" style="width: 30%;"></div>
													</td>
												</tr>

												<tr>
													<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:20px;">
														
														
														<h2 style="color: #582236"><b>Dear '.$to_name.',</b> </h2>
														<p style="color: #582236" class="descr">You have been invited to manage memorial page.<a href="'.$memo_link.'" style="color: #582236;text-decoration: underline"><b>'.strtoupper($memory_of).'</b></a>. '.$new_user_text.'</p>
														<p class="text-left"><a style="text-decoration:none; cursor:pointer" href="'.$accept_link.'"><button style="cursor: pointer; background-color: #794e5e; text-decoration:none; color:#f9f9f9; display:block; padding:9px 8px; border: none;" class="btn btn-danger">Accept Invitation</button></a>  </p>
														<p class="text-left"><a style="text-decoration:none; cursor:pointer" href="'.$reject_link.'"><button style="cursor: pointer; background-color: #794e5e; text-decoration:none; color:#f9f9f9; display:block; padding:9px 8px; border: none;margin-bottom:10px" class="btn btn-danger">Reject</button></a></p>
														<p class="text-center descr" style="color:#582236" >If you have any question or need any assistance with the website, please reply to this email, or contact us at <a href="#" style="color: #582236;text-decoration: underline">Support@safirisalama.com</a></p>
														<p class="text-center" style="color:#582236">Kind Regards,<br>Safiri Salama Team<br><a href="#" style="color: #582236;text-decoration: underline"><b>Support@safirisalama.com</b></a></p>
														
														<a style="cursor:pointer" href="'.$memo_link.'"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false" style="background-color:#582236;color: #fff;border: none; ">See Online Memorial</button></a>

														
														
														
														
														
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td height="28"></td>
									</tr>
								</table>
							</td>

						</tr>
					</table>
							 <!-- module 7 -->
										<table data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td data-bgcolor="bg-module" bgcolor="#eaeced">
													<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
														<tr>
															<td class="footer" style="padding:0 0 10px;">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr class="table-holder">
																		<th class="tfoot" width="400" align="center" style="vertical-align:top; padding:0;">
																			<table width="100%" cellpadding="0" cellspacing="0">

																				<tr>
																					<td style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
																						 If you believe that the email doesn’t belong to you please respond to this email
																						immediately.
																					</td>
																				</tr>

																				<tr>
																					<td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px; text-align: center;">
																					  Safiri Salama  © 2019  &nbsp; All Rights Reserved.
																						<!--<a href="#">Unsubscribe</a>-->
																					</td>
																				</tr>
																			</table>

																		</th>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>

				   
				</td>
			</tr>

			<!-- fix for gmail -->

			<tr>
				<td style="line-height:0;">
					<div style="display:none; white-space:nowrap; font:15px/1px courier;">
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						&nbsp; &nbsp; &nbsp;
					</div>
				</td>
			</tr>

		</table>
			
		</body>
		</html>';	
 		return $html;
	}
}
?>