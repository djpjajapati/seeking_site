<?php $this->load->view('include/header'); ?>    
<style>
.cat {
    display: block;
    width: 100%;
    text-overflow: ellipsis;
	height: 75px;
}

</style>
<section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Featured Memorials</h1>
            </div>
        </div>
    </div>
</section>

<section class="welcome_main">
    <div class="container">
        <div class="row">
			<div class="col-md-12 mt-5">
				<h2>Latest Featured Memorials</h2>
			</div>
			
			<?php foreach($memorials as $memo){ ?>
			
			
			<?php $full_name = $memo->fname.' '.$memo->lname; ?>
			
				<div class="col-lg-3 col-md-4 mt-3">
					<div class="mem_box" style="cursor: pointer;" onClick="view_detail('<?php echo $memo->ID; ?>', '<?php echo $memo->fname; ?>')">
						<div class="media">
						<img width="150" height="150" src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>" class="mr-3 wp-post-image" alt="<?php echo $full_name; ?>">								
						<div class="media-body">
						   <h4><a class="elipses cat" href="<?php echo base_url(); ?>featured/detail/<?php echo $memo->ID.'/'.$memo->fname; ?>"><?php echo ucwords($full_name); ?></a></h4>
							<p><?php echo date('Y', strtotime($memo->dob)).'-'.date('Y', strtotime($memo->dod)); ?></p>
						  </div>

						</div>

						 <a href="<?php echo base_url(); ?>featured/detail/<?php echo $memo->ID.'/'.$memo->fname; ?>" class="memorial_link">View Featured Memorial</a>
					</div>
				</div>
			
			<?php } ?>
			
			
			<!--<div class="col-lg-3 col-md-4 mt-3">
				<div class="mem_box">
					<div class="media">
					<img width="150" height="150" src="img/kofi-annan-1-150x150.png" class="mr-3 wp-post-image" alt="">								
					<div class="media-body">
					   <h4><a href="featured-memorial-detail.html">Kofi Annan</a></h4>
						<p>1960-2018</p>
					  </div>

					</div>

					 <a href="featured-memorial-detail.html" class="memorial_link">view featured memorial</a>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4 mt-3">
				<div class="mem_box">
					<div class="media">
					<img width="150" height="150" src="img/kofi-annan-1-150x150.png" class="mr-3 wp-post-image" alt="">								
					<div class="media-body">
					   <h4><a href="featured-memorial-detail.html">Kofi Annan</a></h4>
						<p>1960-2018</p>
					  </div>

					</div>

					 <a href="featured-memorial-detail.html" class="memorial_link">view featured memorial</a>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4 mt-3">
				<div class="mem_box">
					<div class="media">
					<img width="150" height="150" src="img/kofi-annan-1-150x150.png" class="mr-3 wp-post-image" alt="">								
					<div class="media-body">
					   <h4><a href="featured-memorial-detail.html">Kofi Annan</a></h4>
						<p>1960-2018</p>
					  </div>

					</div>

					 <a href="featured-memorial-detail.html" class="memorial_link">view featured memorial</a>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4 mt-3">
				<div class="mem_box">
					<div class="media">
					<img width="150" height="150" src="img/kofi-annan-1-150x150.png" class="mr-3 wp-post-image" alt="">								
					<div class="media-body">
					   <h4><a href="featured-memorial-detail.html">Kofi Annan</a></h4>
						<p>1960-2018</p>
					  </div>

					</div>

					 <a href="featured-memorial-detail.html" class="memorial_link">view featured memorial</a>
				</div>
			</div>-->
			
        </div>
    </div>
</section>

<script>

function view_detail(id, name){
		location.href="<?php echo base_url(); ?>featured/detail/"+id+"/"+name;
	}
//featured/detail/4/Burton%20Frederick
$(function() {
    $('span.cat').each(function(i) {
        var element = $(this)
                        .clone()
                        .css({display: 'inline', width: 'auto', visibility: 'hidden'})
                        .appendTo('body');
        element.remove();
    });
});

</script>


</script>

<?php $this->load->view('include/footer'); ?>    
