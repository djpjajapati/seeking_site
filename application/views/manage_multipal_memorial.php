<?php $this->load->view("include/header.php"); ?>


<section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Manage Memorials</h1>
				
				<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-block  alert-success">
						<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
						<?php echo $this->session->flashdata('success'); ?>
					</div>
				<?php }else{ ?>
				
					<?php if($user->EmailVerification != 1){ ?>
						<div class="alert alert-block  alert-danger">
								Please verify your email address. 
								<a href="<?php echo base_url(); ?>login/verify_eamil/<?php echo $user->email.'/'.$user->EmailVerification; ?>">
								Click Here to resend verification email.</a>
						</div>
					
					<?php } ?>
					
				<?php } ?>
				
            </div>
        </div>
    </div>
</section>

<section class="welcome_main">
    <div class="container">
        <div class="row">
            	
			<?php $i=1; foreach($my_memorials as $memorial){ ?>
			
				<div class="col-md-4" style="margin-bottom: 50px;">
                	<div class="acc_box mar_class p-0">
                    <div class="media border-bottom p-4 mb-1">
					  <img style="width: 20%; border-radius: 100%;" src="<?php echo base_url(); ?>uploads/<?php echo $memorial->cropped_image; ?>" alt="<?php echo $memorial->fname.' '.$memorial->lname; ?>">
                      <div class="media-body">
                        
						
						<h4 class="pb-1"><a target="_blank" href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>"><?php echo $memorial->fname.' '.$memorial->lname; ?></a></h4>
						  
						  
						<?php if($memorial->pakage == 'BASIC'){ ?>
						  
							<a href="javascript:;" class="badge badge-active p-2 text-white">
								<i class="fa fa-star"></i> BASIC
							</a>
						<?php }else{ ?>
						  
							<a href="javascript:;" class="badge badge-warning p-2 text-white">
								<i class="fa fa-star"></i> PREMIUM
							</a>
						
						<?php } ?>
						  
                      </div>
                    </div>
						
						<div class="list p-4 pt-0">
							<!--<h2>Memorial Name</h2>	-->	
							
							<?php $days_left = floor((strtotime($memorial->createdAt." +1 years") - time())/60/60/24); if($days_left < 0) $days_left = 0; ?>
							
							<p class="mb-2">Created on <span class="float-right font-weight-bold"><?php echo date('d.m.Y', strtotime($memorial->createdAt)); ?> </span></p>
							<p class="mb-2">Date of Expiry <span class="float-right font-weight-bold"><?php echo date('d.m.Y', strtotime($memorial->createdAt." +1 years")); ?></span></p>
							<p class="mb-2">Number of days left <span class="float-right font-weight-bold"><?php echo $days_left; ?> </span></p>
							<p class="mb-2">Page visitor <span class="float-right font-weight-bold"><?php echo $memorial->views; ?></span></p>
						
						</div>
						
						<div class="text-center">
							<?php //$noti_cnt = 0; 
							
							$noti_cnt = $this->db->query("select count(*) as cnt from memo_tribute where memo_id = '$memorial->ID' and notification_status = 0")->row()->cnt;
							
							?>
							<a href="#" style="color: #582236;"><span class="noti-badge"><?php echo $noti_cnt; ?></span> 
							
								<?php if($noti_cnt == 0){ echo 'New Notifications'; }else { echo 'Notifications'; } ?> 
							
							
							</a>
						
						</div>
					
                    <div class="p_class_new pl-4 pr-4 pb-4">
                    <a href="javascript:;" class="red_btn next_btn btn_start mt-3">Renew Subscription</a>
					<a href="<?php echo base_url(); ?>memorial/manage_memorial/<?php echo $memorial->ID; ?>" class="red_btn next_btn btn_start mt-3">Setting/Edit</a>
					
					<?php if($memorial->status == 'Active'){ ?>
					
							<a href="<?php echo base_url(); ?>memorial/update_status/<?php echo $memorial->ID; ?>/Inactive" class="red_btn next_btn btn_start custom-active mt-3">Deactivate</a>
					
					<?php }else{ ?>
					
							<a href="<?php echo base_url(); ?>memorial/update_status/<?php echo $memorial->ID; ?>/Active" class="red_btn next_btn btn_start custom-active mt-3">Activate</a>
					
					<?php } ?>
					
					
					
					
                    <div class="clearfix"></div>
                   </div>
					
                </div>
                </div>
								
			<?php $i++; } ?>
			
			
			<?php $j=1; foreach($my_memorials1 as $memorial){ ?>
			
				<div class="col-md-4" style="margin-bottom: 50px;">
                	<div class="acc_box mar_class p-0">
                    <div class="media border-bottom p-4 mb-1">
					  <img style="width: 20%; border-radius: 100%;" src="<?php echo base_url(); ?>uploads/<?php echo $memorial->cropped_image; ?>" alt="<?php echo $memorial->fname.' '.$memorial->lname; ?>">
                      <div class="media-body">
                        
						<h4 class="pb-1"><?php echo $memorial->fname.' '.$memorial->lname; ?></h4>
						  
						  
						<?php if($memorial->pakage == 'BASIC'){ ?>
						  
							<a href="javascript:;" class="badge badge-active p-2 text-white">
								<i class="fa fa-star"></i> BASIC
							</a>
							  
						<?php }else{ ?>
						  
							<a href="javascript:;" class="badge badge-warning p-2 text-white">
								<i class="fa fa-star"></i> PREMIUM
							</a>
						
						<?php } ?>
						  
                      </div>
                    </div>
						
						<div class="list p-4 pt-0">
							<!--<h2>Memorial Name</h2>	-->	
							<p class="mb-2">Created on <span class="float-right font-weight-bold"><?php echo date('d.m.Y', strtotime($memorial->createdAt)); ?> </span></p>
							<p class="mb-2">Date of Expiry <span class="float-right font-weight-bold"><?php echo date('d.m.Y', strtotime($memorial->createdAt." +1 years")); ?></span></p>
							<p class="mb-2">Number of days left <span class="float-right font-weight-bold"><?php echo floor((strtotime($memorial->createdAt." +1 years") - time())/60/60/24); ?> </span></p>
							<p class="mb-2">Page visitor <span class="float-right font-weight-bold"><?php echo $memorial->views; ?></span></p>
						
						</div>
						
						<div class="text-center">
							<?php //$noti_cnt = 0; 
							
							$noti_cnt = $this->db->query("select count(*) as cnt from memo_tribute where memo_id = '$memorial->ID' and notification_status = 0")->row()->cnt;
							
							?>
							<a href="#" style="color: #582236;"><span class="noti-badge"><?php echo $noti_cnt; ?></span> 
								
								<?php if($noti_cnt == 0){ echo 'New Notifications'; }else{ echo 'Notifications'; } ?> 
								
							</a>
						
						</div>
					
                    <div class="p_class_new pl-4 pr-4 pb-4">
                    <a href="javascript:;" class="red_btn next_btn btn_start mt-3">Renew Subscription</a>
					<a href="<?php echo base_url(); ?>memorial/manage_memorial/<?php echo $memorial->ID; ?>" class="red_btn next_btn btn_start mt-3">Setting/Edit</a>
					
					<?php if($memorial->status == 'Active'){ ?>
					
							<a href="<?php echo base_url(); ?>memorial/update_status/<?php echo $memorial->ID; ?>/Inactive" class="red_btn next_btn btn_start custom-active mt-3">Deactivate</a>
					
					<?php }else{ ?>
					
							<a href="<?php echo base_url(); ?>memorial/update_status/<?php echo $memorial->ID; ?>/Active" class="red_btn next_btn btn_start custom-active mt-3">Activate</a>
					
					<?php } ?>
					
					
					
					
                    <div class="clearfix"></div>
                   </div>
					
                </div>
                </div>
								
			<?php $j++; } ?>
			
	
        </div>
    </div>
</section>

<?php $this->load->view("include/footer"); ?>

