<?php $this->load->view("include/header.php"); ?>

<img class="img-fluid w-100 mx-auto" src="<?php echo base_url(); ?>assets/img/header_bg-about.png" alt="img">

<section class="main_sec">
    <div class="container">
        <div class="videoh_box">
            <div class="row">
                <div class="col-md-12">
                    <iframe
                    src="https://www.youtube.com/embed/IR1G7vESWts">
                    </iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="p_text">Our Online Memorials offer a number of special features<span> to help you memorialize a loved one.</span> <br>Please review some of them below.</p>
            </div>
        </div>
        <div class="row">
                <div class="col-md-4 pad_right25">
                    <div class="red_box">
                        <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img3.png">
                        <h4>Share Your Memories </h4>
                        <h5>With a slide of a mock template of a memorial page)</h5>
                        <p>Share stories, tributes, photos, music and videos celebrating life of a family member or friend who passed away.</p>
                    </div>
                </div>
                <div class="col-md-4 pad20">
                    <div class="red_box">
                        <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img4.png">
                        <h4>Personalize Your Memorial </h4>
                        <h5>(with the address safirisalama.com/name-of- your-beloved)</h5>
                        <p>Select one of dozens available designs, personalize URL, privacy settings and features available to visitors.</p>
                    </div>
                </div>
                <div class="col-md-4 pad_left25">
                    <div class="red_box">
                        <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img5.png">
                        <h4>Invite And Collaborate</h4>
                        
                        <p>nvite family, friends, colleagues to contribute to the website. Online memorial allows multiple guests to contribute. It is a great way to support one another in a difficult time.</p>
                    </div>
                </div>
        </div>
        <div class="row">
                <div class="col-md-4 pad_right25">
                    <div class="red_box">
                        <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img6.png">
                        <h4>Control Your Privacy</h4>
                        <p>We offer multiple privacy and notification options. Choose to allow access only to selected guests, keep the memorial completely private, or enable everyone to view and contribute. Premium memorials include access control settings for individual guests, allowing you to review their activity, change access and notification preferences.</p>
                    </div>
                </div>
                <div class="col-md-4 pad20">
                    <div class="red_box">
                        <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img7.png">
                        <h4>Always accessible Always online</h4>
                        <p>Because the memorial website is web-based, you and your guests have instant access to it wherever they are in the world. Come back, read, and share memories from anywhere, any time. We take great care to keep all the information safe and secure. And are committed to preserving all the content for future generations to come.</p>
                    </div>
                </div>
                <div class="col-md-4 pad_right25">
                    <div class="red_box">
                        <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img8.png">
                        <h4>Choose Your Theme</h4>
                        <p>Choose one of our available designs and start adding content to your Online Memorial in minutes.</p>
                    </div>
                </div>
        </div>
        <div class="owl_class">
          <div class="row">
              <div class="col-md-12">
                  <div class="owl-carousel owl-theme">
                    
				<?php foreach($testimonials as $t){ ?>					
					  
					 <div class="item">
                        <div class="item-box">
						
						<?php 
							
							if(strlen(trim($t->user_pic)) > 1){ 
									$src = base_url().'uploads/'.$t->user_pic;
							}else{
									$src = base_url().'assets/img/default.png';
							} ?>
						
                            <img class="img-fluid rounded imgr" alt="<?php echo $t->name; ?>" src="<?php echo $src; ?>">
                            <h3><?php echo $t->name; ?></h3>
                            <p> <?php echo $t->message; ?> </p>
                            <!--<ul class="list-inline">
                                
								<li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li class="list-inline-item"><a target="_blank" href="https://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" ><i class="fa fa-twitter" aria-hidden="true"></i></a></li>	
							
							</ul>-->
                        </div>
                      </div>
					  
				<?php } ?> 
					  <!--<div class="item">
                        <div class="item-box">
                            <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img9.png">
                            <h3>John Doe</h3>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem </p>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-box">
                            <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img9.png">
                            <h3>John Doe</h3>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem </p>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-box">
                            <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img9.png">
                            <h3>John Doe</h3>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem </p>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-box">
                            <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img9.png">
                            <h3>John Doe</h3>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem </p>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-box">
                            <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img9.png">
                            <h3>John Doe</h3>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem </p>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-box">
                            <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img9.png">
                            <h3>John Doe</h3>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem </p>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="list-inline-item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                      </div>-->              
                  </div>
                  <a href="<?php echo base_url(); ?>contact/Testimonials" class="testimonial_btn">See all Testimonials</a>
              </div>
          </div>
        </div>
        <div class="counte_Class counte_Class1">
            <div class="row">
                <div class="col-md-12">
                    <div id="projectFacts" class="sectionClass">
                        <div class="fullWidth eight columns">
                            <div class="projectFactsWrap ">
                                <!--<div class="item wow fadeInUpBig animated animated" data-number="12" style="visibility: visible;">
                                            <p id="number1" class="number number_class">17</p>
                                            <p>Years of Experience</p>
                                </div>
                                <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                                            
                                            <p id="number2" class="number">1267</p>
                                            <p>Satisfied Customers</p>
                                </div>
                                <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                                            
                                            <p id="number3" class="number">278</p>
                                            <p>Tributes Written</p>
                                </div>
                                <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                                            
                                            <p id="number4" class="number">350</p>
                                            <p>Memorials Created</p>
                                </div>-->
								
								
								 <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_visitor; ?>" style="visibility: visible;">
                                            
									<p id="number32" class="number"><?php echo $total_visitor; ?></p>
									<p>Our Visitors</p>
								</div>
								
								<div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_memorial; ?>" style="visibility: visible;">
									
									<p id="number32" class="number"><?php echo $total_memorial; ?></p>
									<p>Memorials Created</p>
								</div>
								
								<div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_tributes; ?>" style="visibility: visible;">
									
									<p id="number33" class="number"><?php echo $total_tributes; ?></p>
									<p>Tributes Written</p>
								</div>
								
								<div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_picture_uploaded; ?>" style="visibility: visible;">
									
									<p id="number34" class="number"><?php echo $total_picture_uploaded; ?></p>
									<p>Pictures Uploaded</p>
								</div>
								
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mem_sec">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-lg-5">
        <h2>Create a Memorial</h2>
        <p>Bring all the warm memories of your loved one together in a Memorial Website</p>
        <a href="<?php echo base_url(); ?>memorial/create_memorial" class="red_btn">CREATE MEMORIAL</a>
      </div>
    </div>
  </div>
</section>





<?php $this->load->view("include/footer.php"); ?>



