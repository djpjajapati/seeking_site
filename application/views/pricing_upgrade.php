<?php $this->load->view("include/header.php"); ?>

<section class="Plans_head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Plans and Features</h1>
            </div>
        </div>
    </div>
</section>

<section class="price_main">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-4 col-md-12">
                <div class="row">
                   
				<?php $i=1; foreach($pakages as $p){ ?>
				   
					<div class="col-md-6">
                        <div class="price_box green">
                            <h2 id="title_<?php echo $p->ID; ?>"><?php echo $p->title; ?></h2>
                            <div class="inner_price">
                                <h3><span id="price_<?php echo $p->ID; ?>"><?php echo $p->price; ?></span>.<span>00</span> <span style="font-size: 20px;"><?php echo $p->currency; ?></span></h3>
                                <ul class="list-unstyled">
                                    
									<!--<li>Personalized Online Memorial</li>
                                    <li>Update</li>
                                    <li>Photo gallery - <?php //echo $p->photo_limit; ?> Pictures</li>
									<?php //if($i > 1){ ?>
                                    <li>Video gallery - <?php //echo $p->video_limit; ?> Videos</li>
									<li>Manage together  </li>
									<?php //} ?>
                                    <li>Personalized web address</li>
                                    <li>Share your memories</li>
                                    <li>Control privacy</li>
                                    <li>Background music - <?php //echo $p->audio_limit; ?> Song</li>
									<li>Multiple Administrators - <?php //echo $p->page_managers; ?> Admin </li>-->
									
									
									
									
									
									
									<li>Customized Design Templates</li>
                                    <li>Notice Board</li>
                                    <li>Personalized Web Address</li>
                                    <li>Email Notifications</li>
                                    <li>Funeral Pin Location</li>
                                    <li>Social Media Integration</li>
                                    <li>Privacy Settings</li>
                                    <li>No Advertising</li>
                                    <li>Access Memorial 24/7/365</li>
                                    <li>Unlimited Contributors</li>
                                    <li>Background Music</li>
                                    <li>Photo Gallery - <?php echo $p->photo_limit; ?> Pictures</li>
									<?php //if($i > 1){ ?>
                                    <li>Video Gallery - <?php echo $p->video_limit; ?> Videos</li>
									
									<?php //} ?>
									<li>Audio Clips - <?php echo $p->audio_limit; ?> </li>
									<li>Multiple Administrators - <?php echo $p->page_managers; ?> Admin </li>
									
									
									
									
									
									
									
									
                                </ul>
                            </div>
                            <div class="bottom_price">
                                <h4><?php echo $p->annual_maintenance_fee; ?>.00 <span style="font-size: 20px;"><?php echo $p->currency; ?></span> </h4>
                                <p>Annual Maintenance Fees (From 2nd Year)</p>
								
									<a href="<?php echo base_url(); ?>memorial/proceed_to_upgrade/<?php echo $memo_id.'/'.$p->ID; ?>"  class="purchase">Upgrade</a>
								
                            </div>
                        </div>
                    </div>
					
				<?php $i++; } ?>
				
					
                </div>
            </div>
        </div>
    </div>
</section>






<?php $this->load->view("include/footer"); ?>

