<?php $this->load->view("include/header.php"); ?>

<style>
	.file-wrapper {
		cursor: pointer;
		display: inline-block;
		overflow: hidden;
		position: relative;
	}

	.file-wrapper input {
		cursor: pointer;
		font-size: 100px;
		filter: alpha(opacity=1);
		-moz-opacity: 0.01;
		opacity: 0.01;
		position: absolute;
		right: 0;
		top: 0;
	}

	.file-wrapper .button {
		width: auto;
		height: 35px;
		line-height: 33px;
		border: #582236 solid 1px !important;
		color: #582236 !important;
		background: none !important;
		font-size: 14px !important;
		box-shadow: none;
		padding: 0 20px;
	}

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/phoneinput/css/intlTelInput.css">
<section class="testi_sec testi_sec1">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Create a Memorial</h1>
			</div>
		</div>
		<div class="search_box">
			<div class="row">
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="nav-item"><a onclick="playAudio()" class="nav-link active firtab" data-toggle="tab">
								<div class="media create_box">
									<h2>1</h2>
									<div class="media-body">
										<h4>Select a Layout</h4>
									</div>
								</div>
							</a></li>
						<li class="nav-item"><a class="nav-link sectab" data-toggle="tab">
								<div class="media create_box">
									<h2>2</h2>
									<div class="media-body">
										<h4>Personal Details</h4>
									</div>
								</div>
							</a>

						</li>
						<li class="nav-item"><a class="nav-link thirdtab" data-toggle="tab">
								<div class="media create_box">
									<h2>3</h2>
									<div class="media-body">
										<h4>Account Details</h4>
									</div>
								</div>
							</a></li>
						<li class="nav-item"><a class="nav-link forttab" data-toggle="tab">
								<div class="media create_box">
									<h2>4</h2>
									<div class="media-body">
										<h4>Privacy Options</h4>
									</div>
								</div>
							</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="create_main">
	<div class="container" style="height:100% !important;">
		<div class="row" style="height:100% !important;">
			<div class="col-md-12">
				<div class="panel-body">
					<form class="form-horizontal" action="<?php echo base_url(); ?>memorial/add_memo" id="contact_form1"
						  method="post"
						  enctype="multipart/form-data">
						<div class="tab-content">

							<?php if ($this->session->flashdata('error')) { ?>
								<div class="alert alert-block  alert-danger">
									<button data-dismiss="alert" class="close close-sm" type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong></strong> <?php echo $this->session->flashdata('error'); ?>
								</div>

							<?php } ?>

							<div class="tab-pane active" id="home">
								<div class="row">
									<div class="col-md-5">
										<h4>Choose a template:</h4>
										<div class="temp_block">

											<?php $i = 1;
											foreach ($memorial_templates as $temp) { ?>

												<div class="temp_box">
													<div class="media">
														<img class=""
															 src="<?php echo base_url(); ?>assets/img/<?php echo $temp->image; ?>"
															 alt="img" style="height:177px">
														<div class="media-body">
															<h5><?php echo $temp->title; ?></h5>
															<h6><?php if ($i > 13) echo '(Under 10)'; ?></h6>
															<a class="red_btn preview_btn recid"
															   href="javascript:;"
															   onclick="update_preview('<?php echo base_url(); ?>assets/img/<?php echo $temp->image; ?>', 'prev', '<?php echo $temp->ID; ?>')">preview</a>
															<a class="red_btn select_btn recid"
															   href="javascript:;"
															   onclick="update_preview('<?php echo base_url(); ?>assets/img/<?php echo $temp->image; ?>', 'select', '<?php echo $temp->ID; ?>')">select</a>
														</div>
													</div>
												</div>

												<?php $i++;
											} ?>

										</div>
									</div>

									<script>

                                        function update_preview(src, act, id) {

                                            $("#preview_image").attr("src", src);
                                            if (act == 'select') {
                                                $("#memorial_template").val(id);
                                            }

                                        }


									</script>

									<div class="col-md-7 pad_left40">
										<h4>Your choice of template:</h4>

										<div id="test"><img id="preview_image" class="img-fluid feature_img" alt="img"
															src="<?php echo base_url(); ?>assets/img/<?php echo $memorial_templates[0]->image; ?>">
										</div>

										<div class="space20"></div>
										<h4>Select Background Song:</h4>
										<select class="form-control" id="selection" name="songna" style="height: auto">
											<option selected value="select">- Select background music -</option>
											<?php $j = 1;
											foreach ($memorial_music as $music) { ?>

												<option
													value="<?php echo $music->file; ?>" <?php if ($j == 1) echo ''; ?> <?php if ($pending_memo) {
													if ($pending_memo->memorial_music == $music->file) echo 'selected';
												} ?>><?php echo $music->title; ?></option>

												<!--<option value="img/Kalimba.mp3">kalima</option>
													<option value="img/Maid-with-the-Flaxen-Hair.mp3">main with flaxen</option>
													<option value="img/Joan-Osborne-What-If-God-Was-One-Of-Us.mp3">What if God was one of us</option>-->

												<?php $j++;
											} ?>

										</select>
										<audio id="player" controls>
											<source id="mp3_src"
													src="<?php echo base_url(); ?>uploads/music/<?php echo $memorial_music[0]->file; ?>"
													type="audio/mp3"/>
										</audio>

										<div class="themeidint">
											<input type="hidden" class="themeid" name="themeid"
												   value="501">
										</div>

									</div>
								</div>
								<div class="space10"></div>
								<!--<a href="#" class="red_btn prev_btn prevtab"><i class="fa fa-angle-left" aria-hidden="true"></i>PREVIOUS</a>-->
								<a href="#" id="first_next_btn"
								   class="red_btn next_btn nexttab firsttab btn <?php if (!$pending_memo) echo 'disabled'; ?>"
								   style="padding:0px" onclick="pauseAudio()">NEXT<i
										class="fa fa-angle-right" aria-hidden="true"></i></a></div>
							<div class="tab-pane fade create_main-tab" id="menu1">
								<h4>This memorial is dedicated to:</h4>
								<div class="row">
									<div class="col-md-8">
										<div class="row">
											<div class="col-md-6">
												<input type="hidden" placeholder="" id="memorial_template"
													   value="<?php if ($pending_memo) echo $pending_memo->memorial_template; else echo $memorial_templates[0]->ID; ?>"
													   name="memorial_template">
												<input type="hidden" placeholder="" id="memorial_music"
													   value="<?php if ($pending_memo) echo $pending_memo->memorial_music; else echo $memorial_music[0]->file; ?>"
													   name="memorial_music">
												<input type="text" class="form-control names" placeholder="First Name"
													   id="usr"
													   value="<?php if ($_GET['fname']) echo $_GET['fname']; else echo $pending_memo->fname; ?>"
													   name="fname">
												<p class="imgmsg1" style="display:none;">This field is required</p>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control names" placeholder="Last Name"
													   id="lname"
													   value="<?php if ($_GET['lname']) echo $_GET['lname']; else echo $pending_memo->lname; ?>"
													   name="lname">
												<p class="imgmsg2" style="display:none;">This field is required</p>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control names" placeholder="Middle Name"
													   name="mname" value="<?php echo $pending_memo->mname; ?>"
													   id="mname">
												<p class="imgmsg3" style="display:none;">This field is required</p>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="Nick Name"
													   name="nname" value="<?php echo $pending_memo->mname; ?>"
													   id="niname">
												<p class="imgmsg4" style="display:none;">This field is required</p>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<select class="form-control CountryOrigin valid" name="countryorigin1"
														id="countryorigin1">

													<?php foreach ($countries as $con) { ?>

														<option
															value="<?php echo $con->name; ?>" <?php if ($pending_memo) {
															if ($pending_memo->countryorigin1 == $con->name) echo 'selected';
														} else {
															if ($con->name == 'Kenya') echo 'selected';
														} ?>><?php echo $con->name; ?></option>

													<?php } ?>

													<!--<option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
												<option value="Wallis and Futana Islands">Wallis and Futuna
													Islands
												</option>
												<option value="Western Sahara">Western Sahara</option>
												<option value="Yemen">Yemen</option>
												<option value="Yugoslavia">Yugoslavia</option>
												<option value="Zambia">Zambia</option>
												<option value="Zimbabwe">Zimbabwe</option>-->

												</select>
												<!--<p class="imgmsg3" style="display:none;">This field is required</p>-->
											</div>
											<div class="col-md-6">
												<input type="text" id="citytown" class="form-control"
													   placeholder="City/Town" pattern="[A-Za-z]"
													   value="<?php echo $pending_memo->citytown; ?>" name="citytown">
												<p class="imgmsgaa" style="display:none;">This field is required</p>
											</div>

										</div>
									</div>

									<!--<script src="<?php echo base_url(); ?>assets/crop/jquery.min.js"></script>
								<script src="<?php echo base_url(); ?>assets/crop/bootstrap.min.js"></script>
								<link rel="stylesheet" href="<?php echo base_url(); ?>assets/crop/bootstrap.min.css" />-->
									<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>
									<link rel="stylesheet" href="<?php echo base_url(); ?>assets/crop/croppie.css"/>

									<div style="text-align: center" class="nospanstuff col-md-4">
										<div class="row">
											<div class="col-md-5">

												<div id="uploaded_image" class="thumbnail">

													<?php $src = base_url() . "assets/img/default.png";
													if ($pending_memo) $src = base_url() . "uploads/" . $pending_memo->cropped_image; ?>

													<img src="<?php echo $src; ?>" width="130" height="130" alt=""/>
												</div>

												<!--<input id="cropped_image" name="cropped_image" type="text" value="nelson-mandela-blog1.png" />-->
												<input id="cropped_image" name="cropped_image" type="hidden"
													   value="<?php echo $pending_memo->cropped_image; ?>"/>
												<p class="cropped_image_msg" style="display:none; margin-top: 5px;">This
													field is required</p>
											</div>

											<div class="col-md-7 text-left">

												<!--<p>Upload Deceased Picture</p>-->
												<p style="font-size: 14px;">Upload an image of your loved one</p>
												<span class="file-wrapper" style="margin: 20px">
											  <input type="file" name="image" id="image"/>
											  <span class="button red_btn">Choose a File</span>
											</span>
												<!--
											<div class="add-photo_new m-0">
											<a href="" style=" cursor: pointer !important;" class="red_btn btn-choose mt-0">Choose File</a>
											<input type="file" name="image" id="image" style=" cursor: pointer !important;">
										  </div>
-->
												<!--<p class="mt-2" style="line-height: normal; color: #582236; font-size: 12px;">Click on the image and drag around to adjust it.</p>-->
												<p class="mt-2"
												   style="line-height: normal; color: #582236; font-size: 12px;"></p>

												<!--<a href="" class="red_btn tribute_box-btn m-0 mt-2"><i class="fa fa-upload"></i> Upload</a>-->
											</div>

										</div>
									</div>


									<!--  Crop Modal Start Here   -->
									<div id="uploadimageModal" class="modal" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;
													</button>

												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12 text-center">
															<div id="image_demo" style="width:100%;height: auto;"></div>

															<p class="col-md-12"
															   style="line-height: normal; color: #582236;">Click on the
																image and drag around to adjust it.</p>


														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="red_btn crop_image">Upload</button>
													<!--<button id="uploadimageModal_btn" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
												</div>
											</div>
										</div>
									</div>
									<!--  Crop Modal End Here   -->


									<div class="col-md-12">
										<div class="row">
											<div class="col-md-4">
												<input type="text" class="form-control" placeholder="Place of Death"
													   pattern="[A-Za-z]" name="placeofdeath" id="placeofdeath"
													   value="<?php echo $pending_memo->placeofdeath; ?>">
												<p class="imgmsgaa21" style="display:none;">This field is required</p>
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-7 pad_right0">
														<p>Date of Birth (Date/Month/Year)</p>
														<p class="dateval" style="display:none">Date of death is before
															date of birth</p>
													</div>
													<div class="col-md-5">
														<input class="form-control date_form" readonly
															   type="text"
															   name="birthdate" id="birthdatecheck"
															   value="<?php if ($pending_memo->birthdate) echo date('m/d/Y', strtotime($pending_memo->birthdate)); else echo '01/01/2001'; ?>">
														<p class="dateval1" style="display:none">This field is
															required</p>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-7 pad_right0">
														<p>Date of Death (Date/Month/Year)</p>
													</div>
													<div class="col-md-5">
														<input class="form-control date_form" type="text"
															   max="2019-04-12" readonly
															   name="deathdate" id="dathdatecheck"
															   value="<?php if ($pending_memo->deathdate) {
																   echo date('m/d/Y', strtotime($pending_memo->deathdate));
															   } else {
																   echo date('m/d/Y');
															   } ?>">
														<p class="dateval2" style="display:none">This field is
															required</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--col-md-3-->
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-3 pad_right0">
												<p>Gender</p>
											</div>
											<div class="col-md-9 pad_right0">
												<div class="radio_class radio_class-tab">
													<div class="radio radio-inline">
														<input type="radio" id="Radio8" value="Male" name="gender"
															<?php if ($pending_memo->gender != 'Female') echo 'checked="checked"'; ?>>
														<label for="Radio8"> Male </label>
													</div>
													<div class="radio radio-inline">
														<input type="radio" id="Radio9" value="Female"
															   name="gender" <?php if ($pending_memo->gender == 'Female') echo 'checked="checked"'; ?>>
														<label for="Radio9"> Female </label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<p class="d-inline-block pr-2">Choose Memorial web address: </p>
										<p class="d-inline-block" class="weburl"
										   style="color: #582236;"><?php echo base_url(); ?>In_Memory_Of/</p>
									</div>
									<!--col-md-3-->
									<div class="col-md-2">
										<input type="text" class="form-control" onkeyup="check_address(this.value)"
											   value="<?php echo $pending_memo->webaddress; ?>" id="weaddress"
											   name="webaddress">
										<p class="imgmsg5" style="display:none;">This field is required</p>
										<p class="addnotok" style="display:none;">This Address Already Exist</p>
									</div>
								</div>


								<h4>This memorial is Managed by:</h4>
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-5 pad_right0">
												<p>The deceased person is your...</p>
											</div>
											<div class="col-md-7 pad_left0">
												<select class="form-control" id="sel1" name="relationname">
													<optgroup>
														<option value="Friend">Friend</option>
														<option value="Boss">Boss</option>
														<option value="Neighbour ">Neighbour</option>
														<option value="Colleague">Colleague</option>
														<option value="Teacher">Teacher</option>
														<option value="Worker">Worker</option>
													</optgroup>

													<optgroup label="_______________________________________________">

														<option value="" disabled></option>
														<option value="Grandfather">Grandfather</option>
														<option value="Grandmother">Grandmother</option>
														<option value="Great-aunt">Great-aunt</option>
														<option value="Uncle">Uncle</option>
														<option value="Aunt">Aunt</option>
														<option value="Father">Father</option>
														<option value="Mother">Mother</option>
														<option value="Uncle (Husband of Aunt)">Uncle (Husband of
															Aunt)
														</option>
														<option value="Sister">Sister</option>
														<option value="Brother-in-law">Brother-in-law</option>
														<option value="Brother">Brother</option>
														<option value="Sister-in-law">Sister-in-law</option>
														<option value="Husband">Husband</option>
														<option value="Wife">Wife</option>
														<option value="Cousin">Cousin</option>
														<option value="Cousin’s wife">Cousin’s wife</option>
														<option value="Cousin">Cousin</option>
														<option value="Cousin’s husband">Cousin’s husband</option>
														<option value="Nephew">Nephew</option>
														<option value="Niece">Niece</option>
														<option value="Son">Son</option>
														<option value="Daughter-in-law">Daughter-in-law</option>
														<option value="Daughter">Daughter</option>
														<option value="Son-in-law">Son-in-law</option>
														<!--<option value="First cousin once removed">First cousin once removed</option>-->
														<option value="Grandson">Grandson</option>
														<option value="Granddaughter">Granddaughter</option>


													</optgroup>
													<optgroup label="_________________________________________________">
														<option value="" disabled></option>
														<option value="Other">Other</option>
														<option value="" disabled></option>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<input type="text" class="form-control" placeholder="First Name" name="rfname"
											   value="<?php if ($this->session->userdata('user')) echo $this->session->userdata('user')->fname; ?>"
											   id="rfname">
										<p class="imgmsg8" style="display:none;">This field is required</p>
									</div>
									<div class="col-md-3">
										<input type="text" class="form-control" placeholder="Surname"
											   value="<?php if ($this->session->userdata('user')) echo $this->session->userdata('user')->lname; ?>"
											   name="rsurname"
											   id="rsurname">
										<p class="imgmsg9" style="display:none;">This field is required</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<input type="tel" class="form-control rnum"
											   placeholder="Phone Number" name="rphone"
											   value="<?php echo $pending_memo->rphone; ?>" id="rphone">
										<p class="imgmsg7" style="display:none; color: red">This field is required</p>
									</div>

									<script src="<?php echo base_url(); ?>assets/phoneinput/js/intlTelInput.js">
									</script>
									<script src="https://unpkg.com/imask"></script>
									<script>
                                        var input = document.querySelector("#rphone");
                                        window.intlTelInput(input, {});
                                        var element = document.getElementById('rphone');
                                        var maskOptions = {
                                            mask: '(000)000-0000'
                                        };
                                        var mask = IMask(element, maskOptions);

									</script>

									<div class="col-md-3">

										<select class="form-control CountryOrigin valid" name="countryorigin2"
												id="countryorigin2">

											<?php foreach ($countries as $con) { ?>
												<option
													value="<?php echo $con->name; ?>" <?php if ($con->name == 'Kenya') echo 'selected'; ?>><?php echo $con->name; ?></option>
											<?php } ?>

										</select>
										<p class="imgmsg10" style="display:none;">This field is required</p>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" name="raddress" id="raddress"
											   value="<?php echo $pending_memo->raddress; ?>"
											   placeholder="Physical Address">
										<p class="imgmsg10" style="display:none;">This field is required</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<input type="Email" class="form-control" placeholder="Email Address"
											   value="<?php if ($this->session->userdata('user')) echo $this->session->userdata('user')->email; ?>"
											   name="remailadd" id="remail">
										<p class="imgmsg11" style="display:none;">This field is required - e.g abc@gmail
											.com</p>
										<p id="valid"></p>
										<input type="hidden" value="yes" class="emailval">
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-5 pad_right0">
												<p>Current town</p>
											</div>
											<div class="col-md-7 pad_left0">
												<input type="text" class="form-control"
													   placeholder="Your Current town" name="rpnm"
													   value="<?php echo $pending_memo->current_town; ?>"
													   id="rpnm">
												<p class="imgmsg12" style="display:none;">This field is required</p>
											</div>
										</div>
									</div>
								</div>
								<div class="space10"></div>
								<a href="#" class="red_btn prev_btn prevtab" onclick="playAudio()"><i
										class="fa fa-angle-left"
										aria-hidden="true"></i>PREVIOUS</a>

								<button type="button" name="submit" class="red_btn" id="info" style="display:block">NEXT
									&nbsp;&nbsp; <i
										class="fa fa-angle-right" aria-hidden="true"></i></button>
								<a class="red_btn next_btn abc" style="display:none">NEXT<i
										class="fa fa-angle-right" aria-hidden="true"></i></a>
								<a class="red_btn next_btn nexttab abc" id="sec" onclick="pauseAudio()"
								   style="display:none">NEXT<i
										class="fa fa-angle-right" aria-hidden="true"></i></a></div>
							<div class="tab-pane fade" id="menu2">
								<!--<p class="signtitle">Sign in to your account</p>-->
								<div class="row">

									<div class="col-md-3">

										<?php if (!$this->session->userdata('user')) { ?>

											<!--<h4>Sing in With your social Account:</h4>
										<a href="#" class="facebook_btn btn_layout"><i
													class="fa fa-facebook-official"
													aria-hidden="true"></i>Sign up
											with Facebook</a> <a href="#" class="google_btn btn_layout"><i
													class="fa fa-google" aria-hidden="true"></i>Sign up
											with Google</a>-->

										<?php } ?>

									</div>
									<div class="col-md-4">
										<h4>Your account details:</h4>
										<!--<em>Already have an account? <a href="#" class="switch-to-login from-register">Login here.</a></em>-->

										<?php if ($this->session->userdata('user')) { ?>

											<input type="text" class="form-control" placeholder="First Name"
												   name="accountname" id="ac_fname" value="<?php echo $user->fname; ?>"
												   disabled>
											<p class="imgmsg13" style="display:none;">This field is required</p>
											<input type="text" class="form-control" placeholder="Last Name"
												   name="accountlname" id="ac_lname" value="<?php echo $user->lname; ?>"
												   disabled>
											<p class="imgmsg14" style="display:none;">This field is required</p>
											<input type="text" class="form-control" placeholder="Email"
												   name="acountemail"
												   id="acemail" value="<?php echo $user->email; ?>" disabled>
											<p class="imgmsg15" style="display:none;">This field is required Like -
												abc@gmail.com</p>
											<!--<p class="imgmsgemailsas" style="display:none;"></p>
											<input type="password" class="form-control" placeholder="Create password"
												   name="accountpassword"
												   id="password" value="<?php echo $user->password; ?>" disabled>
											<p class="imgmsg16" style="display:none;">This field is required</p>
											<input type="password" class="form-control" placeholder="Confirm password"
												   name="accountconpassword" id="confirm_password" value="<?php echo $user->password; ?>" disabled>-->


											<span id='message'></span>

										<?php } else { ?>

											<input type="text" class="form-control" placeholder="First Name"
												   name="accountname" id="acn">
											<p class="imgmsg13" style="display:none;">This field is required</p>
											<input type="text" class="form-control" placeholder="Last Name"
												   name="accountlname" id="aclna">
											<p class="imgmsg14" style="display:none;">This field is required</p>
											<input type="text" class="form-control" placeholder="Email"
												   name="acountemail"
												   id="acemail">
											<p class="imgmsg15" style="display:none;">This field is required Like -
												abc@gmail.com</p>
											<p class="imgmsgemailsas" style="display:none;"></p>
											<input type="password" class="form-control" placeholder="Create password"
												   name="accountpassword"
												   id="password">
											<p class="imgmsg16" style="display:none;">This field is required</p>
											<p class="imgmsglength" style="display:none; color: red">Password must be
												atleast 6 characters</p>

											<div id="confirm_password_field">

												<input type="password" class="form-control"
													   placeholder="Confirm password"
													   name="accountconpassword" id="confirm_password">

												<span id='message'></span>
											</div>


											<p class="login_error_message" style="display:none; color: red">Incorrect
												email or password</p>
											<p class="login_success_message" style="display:none; color: green">Login
												successful, Please click next to proceed</p>
											<input type="button" style="display: none" id="do_login_btn"
												   onclick="do_login()" class="btn_start" value="Login"/>


											<p id="go_to_login">Already have an account:
												<button class="btn_start" style="cursor: pointer" onclick="login_now()"
														type="submit">LOGIN
												</button>
											</p>
											<p id="go_to_signup" style="display: none;">Create a new account
												<button class="btn_start" style="cursor: pointer" onclick="signup_now()"
														type="submit">REGISTER
												</button>
											</p>

										<?php } ?>

										<input id="createdby" type="hidden"
											   value="<?php if ($this->session->userdata('user')) echo $this->session->userdata('user')->ID; ?>"
											   name="createdby"/>

										<!-- <p class="logindata" data-toggle="modal" data-target="#myModal2">already have account click here for login</p>-->
									</div>


									<script>

                                        function login_now() {

                                            $("#inforeg").hide();

                                            //hide error message
                                            $(".imgmsg13").hide();
                                            $(".imgmsg14").hide();
                                            $(".imgmsg15").hide();
                                            $(".imgmsg16").hide();
                                            $(".imgmsgemailsas").hide();
                                            $(".imgmsglength").hide();
                                            $(".login_error_message").hide();

                                            //hide confirm password
                                            $("#acn").hide();
                                            $("#aclna").hide();
                                            $("#confirm_password_field").hide();
                                            $("#password").attr("placeholder", "Enter password here");
                                            $("#do_login_btn").show();

                                            $("#go_to_login").hide();
                                            $("#go_to_signup").show();
                                        }

                                        function signup_now() {

                                            $("#inforeg").show();

                                            //hide error message
                                            $(".imgmsg13").hide();
                                            $(".imgmsg14").hide();
                                            $(".imgmsg15").hide();
                                            $(".imgmsg16").hide();
                                            $(".imgmsgemailsas").hide();
                                            $(".imgmsglength").hide();
                                            $(".login_error_message").hide();

                                            //hide confirm password


                                            $("#acn").show();
                                            $("#aclna").show();
                                            $("#confirm_password_field").show();
                                            $("#password").attr("placeholder", "Confirm password");
                                            $("#do_login_btn").hide();

                                            $("#go_to_login").show();
                                            $("#go_to_signup").hide();
                                        }


                                        function do_login() {

                                            var email = $("#acemail").val();
                                            var password = $("#password").val();

                                            $.ajax({
                                                url: "<?php echo base_url(); ?>login/do_login_existing_account",
                                                type: "POST",
                                                data: {"email": email, "password": password},
                                                success: function (resp) {
                                                    if (resp == 'error') {
                                                        $('.login_error_message').show();
                                                        $(".login_success_message").hide();
                                                    } else {
                                                        $('#createdby').val(resp);
                                                        $("#do_login_btn").hide();

                                                        $("#imgmsg14").hide();
                                                        $("#go_to_signup").hide();

                                                        $('.login_error_message').hide();
                                                        $(".login_success_message").show();

                                                        $("#inforeg").show();

                                                    }

                                                }
                                            });

                                        }


									</script>


								</div>
								<div class="space80"></div>
								<a href="#" class="red_btn prev_btn prevtab prevtodata"><i class="fa fa-angle-left"
																						   aria-hidden="true"></i>PREVIOUS</a>

								<button type="button" name="submit" class="red_btn" id="inforeg"
										style="display:block">
									NEXT &nbsp;&nbsp; <i
										class="fa fa-angle-right" aria-hidden="true"></i></button>
								<!-- <a class="red_btn next_btn nexttab" id="info">NEXT<i class="fa fa-angle-right" aria-hidden="true"></i></a>-->
								<a class="red_btn next_btn" id="inforegsed" style="display:none">NEXT<i
										class="fa fa-angle-right"
										aria-hidden="true"></i></a> <a class="red_btn next_btn nexttab"
																	   id="sec1"
																	   style="display:none">NEXT<i
										class="fa fa-angle-right"
										aria-hidden="true"></i></a>

								<!-- <a href="#" class="red_btn next_btn nexttab">NEXT<i class="fa fa-angle-right" aria-hidden="true"></i></a>-->
							</div>
							<div class="tab-pane" id="menu3">
								<div class="row">
									<div class="col-md-4">
										<h4>Would you like to share your memorial with others, or keep it private? <i>(This
												can be changed
												later.)</i></h4>
										<div class="radio_class">
											<div class="radio radio-inline">
												<input type="radio" id="inlineRadio1" value="private"
													   name="visibility">
												<label for="inlineRadio1"> Keep it private </label>
											</div>
											<div class="radio radio-inline">
												<input type="radio" id="inlineRadio2" value="public" name="visibility"
													   checked="">
												<label for="inlineRadio2"> Make it public </label>
											</div>
										</div>
										<h4>Accept/Agree Guidelines . You can read <a
												href="../term-and-condition/index.html"
												target="blank"> Terms and
												Conditions here.</a></h4>
										<div class="radio_class">
											<div class="radio radio-inline">
												<input type="radio" id="inlineRadio3" value="option1"
													   name="radioInline1" checked="">
												<label for="inlineRadio3"> I Agree </label>
											</div>
										</div>
										<div class="space120"></div>
										<input type="hidden" name="plane_id" value="">
										<input type="hidden" name="action" value="ajax_addnewmamorial">


										<!--<input type="submit" id="addmoamoial" class="red_btn red_btn1"
										   name="subbtnlastfirst"
										   value="Proceed to Payment">-->

										<input type="submit" id="addmoamoial" onclick="pauseAudio()"
											   class="red_btn red_btn1"
											   name="subbtnlastfirst"
											   value="Proceed to Payment">

										<!-- <a href="" class="red_btn red_btn1">create memorial</a>-->
										<a href="#menu2" onclick="go_to_account_form()" class="go_back">or go back and
											edit information</a>
									</div>
									<div class="col-md-8">
										<div class="pull-right">
											<h4>Memorial Preview</h4>

											<script>
                                                function go_to_account_form() {
                                                    $("#go_to_account_form").click();
                                                }
											</script>

											<div id="test2" class="template-layout crafts-template">

												<!--												<img class="img-fluid feature_img" alt="img" src="<?php echo base_url(); ?>assets/img/MOCKUPS-13-min.png">-->


												<section id="background_head_img" class="tribute_head"
														 style="background: url(<?php echo base_url(); ?>assets/img/flag_bg.png) #FFF no-repeat;">
													<div class="container" style="height: 100% !important;">
														<div class="row" style="height: 100% !important;">
															<div class="col-md-4 col-4">
																<div id="preview_src" class="preview_image"
																	 style="background: url(<?php echo base_url(); ?>assets/img/flag_bg.png) no-repeat;">
																	<!--<img id="preview_src1" class="" alt="img" src="<?php echo base_url(); ?>assets/img/img10.png">-->
																</div>


															</div>
															<div class="col-md-8 col-8">
																<p>In loving memory of...</p>
																<h1 id="memory_name">John Doe</h1>
																<ul class="nav nav-tabs d-none d-md-flex">
																	<li class="nav-item">
																		<a class="nav-link active" data-toggle="tab"
																		   href="">ABOUT</a>
																	</li>
																	<li class="nav-item">
																		<a class="nav-link" data-toggle="tab" href="">NOTICES</a>
																	</li>
																	<li class="nav-item">
																		<a class="nav-link" data-toggle="tab" href="">GALLERY</a>
																	</li>
																	<li class="nav-item">
																		<a class="nav-link" data-toggle="tab" href="">TRIBUTES</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
												</section>

												<section class="tribute_main">
													<div class="container">
														<div class="row">
															<div class="col-md-8 col-12 d-md-none d-block">
																<div class="tribute_ul_new">
																	<ul class="nav nav-tabs">
																		<li class="nav-item">
																			<a class="nav-link active" data-toggle="tab"
																			   href="">ABOUT</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link" data-toggle="tab"
																			   href="">NOTICES</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link" data-toggle="tab"
																			   href="">GALLERY</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link" data-toggle="tab"
																			   href="">TRIBUTES</a>
																		</li>
																	</ul>
																</div>
																<div class="tab-content">
																	<div class="tab-pane active" id="home1">
																		<div class="tribute_box">
																			<h3><img
																					src="<?php echo base_url(); ?>assets/img/quote1.png"
																					class="d-inline-block" alt=""/>
																				Lorem ipsum dolor sit amet, consectetur
																				adipisicing elit, sed do eiusmod tempor
																				incididunt. <img
																					src="<?php echo base_url(); ?>assets/img/quote2.png"
																					class="d-inline-block" alt=""/></h3>
																			<span class="edit_class"><i
																					class="fa fa-pencil"
																					aria-hidden="true"></i>Edit</span>
																		</div>
																		<div class="tribute_box">
																			<ul class="tri_list_box">
																				<li>56 Years Old</li>
																				<li>Born on March 3, 1962 in Nairoby,
																					Kenya
																				</li>
																				<li>Passed a way on June 22, 2018 in
																					Nakuru, Kenya
																				</li>
																			</ul>
																			<span class="edit_class"><i
																					class="fa fa-pencil"
																					aria-hidden="true"></i>Edit</span>
																		</div>
																		<div class="tribute_box">
																			<p>Lorem ipsum dolor sit amet, consectetur
																				adipisicing elit, sed do eiusmod tempor
																				incididunt ut labore et dolore magna
																				aliqua. Ut enim ad minim veniam, quis
																				nostrud exercitation ullamco laboris
																				nisi ut aliquip ex ea commodo
																				consequat. </p>

																			<p>Duis aute irure dolor in reprehenderit in
																				voluptate velit esse cillum dolore eu
																				fugiat nulla pariatur. Excepteur sint
																				occaecat cupidatat non proident, sunt in
																				culpa qui officia deserunt mollit anim
																				id est laborum. Sed ut perspiciatis unde
																				omnis iste natus error sit voluptatem
																				accusantium doloremque laudantium, totam
																				rem aperiam, eaque ipsa quae ab illo
																				inventore veritatis et quasi architecto
																				beatae vitae dicta sunt explicabo. Nemo
																				enim ipsam voluptatem quia voluptas sit
																				aspernatur aut odit aut fugit, sed quia
																				consequuntur magni dolores eos qui
																				ratione voluptatem sequi nesciunt. </p>

																			<p>Neque porro quisquam est, qui dolorem
																				ipsum quia dolor sit amet, consectetur,
																				adipisci velit, sed quia non numquam
																				eius modi tempora incidunt ut labore et
																				dolore magnam aliquam quaerat
																				voluptatem.</p>
																			<span class="edit_class"><i
																					class="fa fa-pencil"
																					aria-hidden="true"></i>Edit</span>
																		</div>
																		<a href="" class="add_story">Leave a Tribute</a>
																	</div>
																	<div class="tab-pane fade tab_notice" id="menu4">
																		<div class="notice_main">
																			<div class="form-group">
																				<label for="txt1">Funeral Notice
																					Title</label>
																				<input type="text" class="form-control"
																					   id="txt1"
																					   placeholder="ie Details for Todd Msekema’s Funeral">
																			</div>
																			<div class="form-group">
																				<label for="txt1">Location and Date of
																					Funeral Service</label>
																				<input type="text" class="form-control"
																					   id="txt1"
																					   placeholder="25 May 2018, Sixtine Chapel">
																			</div>
																			<div class="form-group">
																				<label for="txt1">Funeral Date</label>
																				<input type="text" class="form-control"
																					   id="txt1"
																					   placeholder="25 May 2018">
																			</div>
																			<div class="form-group">
																				<label for="txt1">Address of Funeral
																					Location</label>
																				<input type="text" class="form-control"
																					   id="txt1"
																					   placeholder="828 Union Street, Seattle, Washington">
																			</div>
																			<div class="form-group">
																				<label for="txt1">Time of
																					Funeral:</label>
																				<input type="text" class="form-control"
																					   id="txt1" placeholder="2:00 PM">
																			</div>
																			<div class="form-group">
																				<label for="txt1">Funeral committee
																					venue (Place Building, Road)</label>
																				<input type="text" class="form-control"
																					   id="txt1" placeholder="2:00 PM">
																			</div>
																			<div class="form-group">
																				<label for="txt1">Funeral Committee
																					Contact Person & Tel. Number</label>
																				<input type="text" class="form-control"
																					   id="txt1"
																					   placeholder="John Malkovic, 822-1125-2813">
																			</div>
																			<div class="map_class">
																				<iframe
																					src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50470.02846874147!2d-122.47261937941492!3d37.75776267828958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1539413862187"
																					width="600" height="450"
																					frameborder="0" style="border:0"
																					allowfullscreen></iframe>
																			</div>
																			<ul class="list-inline notice_list">
																				<li class="list-inline-item">Notice:
																					dawdaoiajdawda
																				</li>
																				<li class="list-inline-item">Posted: 10
																					hours ago
																				</li>
																				<li class="list-inline-item">Posted by:
																					admin123
																				</li>
																			</ul>
																		</div>
																		<div class="notice_sub">
																			<div class="info_new">
																				<h5>PERSONAL INFORMATION</h5>
																				<span>Notices Board For:</span>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="First Name">
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Last Name">
																						</div>
																					</div>
																				</div>
																				<span>Date of Birth:</span>
																				<div class="row">
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Date">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Month">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Year">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Place">
																						</div>
																					</div>
																				</div>
																				<span>Date of Death:</span>
																				<div class="row">
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Date">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Month">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Year">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Place">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>FUNERAL SERVICE DETAILS</h5>
																				<span>Church/Mosque/Temple:</span>
																				<div class="row">
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Location">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Town">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Date">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Time">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>BURIAL DETAILS</h5>
																				<span>Location:</span>
																				<div class="row">
																					<div class="col-md-3">
																						<div class="form-group">
																							<select class="form-control"
																									id="sel1">
																								<option>County</option>
																								<option>2</option>
																								<option>3</option>
																								<option>4</option>
																							</select>
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Town/Centre">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Date">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Time">
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Area/Location">
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="If Public Cemetery">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>CREMATION DETAILS</h5>
																				<div class="row">
																					<div class="col-md-5">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Town">
																						</div>
																					</div>
																					<div class="col-md-5">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Crematorium">
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Time">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>Funeral Committee Contact
																					Person</h5>
																				<div class="row">
																					<div class="col-md-5">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="1st Name">
																						</div>
																					</div>
																					<div class="col-md-5">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="2nd Name">
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Tel No">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>Funeral Committee Contact
																					Person</h5>
																				<div class="row">
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Town">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Location">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Building">
																						</div>
																					</div>
																					<div class="col-md-3">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Meeting Dates">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>Burial/Funeral/Interment Location
																					Google Map</h5>
																				<div class="row">
																					<div class="col-md-12">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Add Location">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="info_new">
																				<h5>POST FUNERAL NOTICES</h5>
																				<div class="row">
																					<div class="col-md-5">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Notice/Subject">
																						</div>
																					</div>
																					<div class="col-md-5">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="When Notice Was Posted">
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="form-group">
																							<input type="text"
																								   class="form-control"
																								   id="txt1"
																								   placeholder="Posted By">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="tribute_list1">
																				<a href="" class="add_story ylw_btn">Share</a>
																				<a href="" class="add_story">POST
																					NOTICES</a>
																			</div>
																		</div>
																		<!-- <div class="tribute_list1">
                      <a href="" class="add_story ylw_btn">Share</a>
                      <a href="" class="add_story">POST NOTICES</a>
                    </div> -->
																	</div>
																	<div class="tab-pane gal_img fade" id="menu5">
																		<!-- <div class="gal_main">
                      <div class="row">
                        <div class="col-md-4">
                          <h4>Photos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">

                          <h4>Videos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Videos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Audio</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Audio</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                      </div>
                    </div> -->
																		<div class="gal_sub">
																			<ul class="nav nav-tabs">
																				<li class="nav-item">
																					<a class="nav-link active"
																					   data-toggle="tab"
																					   href="#photoTab1">Photos</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#videoTab1">VIDEOS</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#audioTab1">AUDIO</a>
																				</li>
																			</ul>
																			<!-- Tab panes -->
																			<div class="tab-content">
																				<div class="tab-pane active"
																					 id="photoTab1">
																					<div class="gal_box">
																						<div class="row">
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="tab-pane fade"
																					 id="videoTab1">
																					<div class="gal_box">
																						<div class="row">
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="tab-pane fade"
																					 id="audioTab1">
																					<div class="gal_box">
																						<div class="row">
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3 col-4">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="space20"></div>
																			<div class="row">
																				<div class="col-md-4"></div>
																				<div class="col-md-4"></div>
																				<div class="col-md-4">
																					<div class="add_gal">
																						<span class="red_btn"><i
																								class="fa fa-plus-circle"
																								aria-hidden="true"></i>Add Photos</span>
																						<input id="selectfile"
																							   type="file" name="">
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="tab-pane fade" id="menu6">
																		<div class="main_tri">
																			<div class="featured_box">
																				<div class="media">
																					<img class=""
																						 src="<?php echo base_url(); ?>assets/img/img12.png"
																						 alt="mage">
																					<div class="media-body">
																						<h5>Audrey <i>lit a Candle</i>
																						</h5>
																						<h6>July 1, 2018, 01.11 pm</h6>
																						<img class="img-fluid img13"
																							 alt="img"
																							 src="<?php echo base_url(); ?>assets/img/img13.png">
																					</div>
																				</div>
																			</div>
																			<div class="featured_box">
																				<div class="media">
																					<img class=""
																						 src="<?php echo base_url(); ?>assets/img/img12.png"
																						 alt="mage">
																					<div class="media-body">
																						<h5>Audrey <i>left a Tribute</i>
																						</h5>
																						<h6>July 1, 2018, 01.11 pm</h6>
																						<span class="span_share"><i
																								class="fa fa-share-alt"
																								aria-hidden="true"></i>Share</span>
																						<p>Lorem ipsum dolor sit amet,
																							consectetur adipisicing
																							elit, sed do eiusmod tempor
																							incididunt ut labore et
																							dolore magna aliqua. Ut enim
																							ad minim veniam, quis
																							nostrud exercitation ullamco
																							laboris nisi ut aliquip ex
																							ea commodo consequat. Duis
																							aute irure dolor in
																							reprehenderit in voluptate
																							velit esse cillum dolore eu
																							fugiat</p>
																					</div>
																				</div>
																			</div>
																			<div class="featured_box">
																				<div class="media">
																					<img class=""
																						 src="<?php echo base_url(); ?>assets/img/img12.png"
																						 alt="mage">
																					<div class="media-body">
																						<h5>Audrey <i>lit a Candle</i>
																						</h5>
																						<h6>July 1, 2018, 01.11 pm</h6>
																						<span
																							class="span_share">View</span>
																					</div>
																				</div>
																			</div>
																			<ul class="list-inline tribute_list">
																				<li class="list-inline-item"><a
																						class="red_btn lea_tri">Leave a
																						Tribute</a></li>
																				<li class="list-inline-item"><a href=""
																												class="red_btn">Light
																						a Candle</a></li>
																				<li class="list-inline-item"><a href=""
																												class="red_btn">Leave
																						a Note</a></li>
																			</ul>
																		</div>
																		<div class="leave_tribute">
																			<span class="tri_span"><img
																					class="img-fluid" alt="img"
																					src="<?php echo base_url(); ?>assets/img/pencil.png">Leave a Tribute</span>
																			<ul class="nav nav-tabs" role="tablist">
																				<li class="nav-item">
																					<a class="nav-link active"
																					   data-toggle="tab"
																					   href="#tab4"><img
																							class="img-fluid active_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
																						<img
																							class="img-fluid default_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
																					</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#tab5"><img
																							class="img-fluid default_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/flower.png">
																						<img
																							class="img-fluid active_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/flower1.png">
																					</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#tab6"><img
																							class="img-fluid default_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/feather.png">
																						<img
																							class="img-fluid active_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/feather1.png">
																					</a>
																				</li>
																			</ul>
																			<div class="tab-content">
																				<div id="tab4" class="tab-pane active">
																					<textarea class="form-control"
																							  rows="8" id="comment"
																							  placeholder="Add your tribute text here"></textarea>
																				</div>
																				<div id="tab5" class="tab-pane fade">
																					<textarea class="form-control"
																							  rows="8" id="comment"
																							  placeholder="Add your tribute text here"></textarea>
																				</div>
																				<div id="tab6" class="tab-pane fade">
																					<textarea class="form-control"
																							  rows="8" id="comment"
																							  placeholder="Add your tribute text here"></textarea>
																				</div>
																			</div>
																			<ul class="list-inline leave_ul">
																				<li class="list-inline-item">by John
																					Nyongessa
																				</li>
																				<li class="list-inline-item pull-right">
																					<a href="javascript:;"
																					   class="red_btn">Leave a
																						Tribute</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div class="col-md-4 col-12">
																<div class="space70"></div>
																<div class="space30 d-block d-md-none"></div>
																<div class="tribute_box-left tribute_box-new">
																	<div class="prehead_left">
																		<a href="javascript:;" class="">Invite Family
																			and Friends to this memorial:</a>
																		<a href="javascript:;"
																		   class="red_btn tribute_box-btn"><span>+</span>Invite
																			Now</a>
																	</div>
																</div>
																<div class="tribute_box-left">
																	<div class="prehead_left">
																		<a href="">Share with a friend</a>
																		<ul class="list-inline">
																			<li class="list-inline-item"><a
																					href="javascript:;"><i
																						class="fa fa-facebook"
																						aria-hidden="true"></i></a></li>
																			<li class="list-inline-item"><a
																					href="javascript:;"><i
																						class="fa fa-instagram"
																						aria-hidden="true"></i></a></li>
																			<li class="list-inline-item"><a
																					href="javascript:;"><i
																						class="fa fa-twitter"
																						aria-hidden="true"></i></a></li>
																		</ul>
																	</div>
																</div>
																<div class="tribute_box-left tribute_box-new1">
																	<div class="prehead_left">
																		<a href="javascript:;" class="tribute_box-text">Sign
																			up for Notifications</a>
																		<a href="javascript:;"
																		   class="red_btn tribute_box-btn tribute_box-btn1">Sign
																			Up</a>
																		<div class="clearfix"></div>
																	</div>
																</div>

																<div id="accordion">

																	<div class="tribute_box-left tribute_box-new">
																		<div class="prehead_left">
																			<a href="javascript:;" class="">Add
																				Picture</a>
																			<div class="add-photo_new">
																				<a href="javascript:;"
																				   class="red_btn tribute_box-btn"><span>+</span>Add
																					Picture</a>
																				<input type="file" name="">
																			</div>
																		</div>
																	</div>


																	<div class="card card1">
																		<div class="card-header">
																			<a class="collapsed card-link card-link1"
																			   data-toggle="collapse"
																			   href="javascript:;">
																				Important Notifiactions<span><i
																						class="fa fa-exclamation"
																						aria-hidden="true"></i></span>
																			</a>
																		</div>
																		<div id="collapseThree1" class="collapse"
																			 data-parent="#accordion">
																			<div class="card-body">
																				Lorem ipsum..
																			</div>
																		</div>
																	</div>

																	<div class="card">
																		<div class="card-header">
																			<a class="collapsed card-link activity"
																			   data-toggle="collapse"
																			   href="javascript:;">
																				Page Activity<span><i
																						class="fa fa-users"
																						aria-hidden="true"></i></span>
																			</a>
																		</div>
																		<div id="collapseFour" class="collapse show"
																			 data-parent="javascript:;">
																			<div class="card-body">
																				<ul class="list-unstyled list_page-activity">
																					<li>Marcela laid a flower<span>2 hours ago</span>
																					</li>
																					<li>Angel left a note<span>2 hours ago</span>
																					</li>
																					<li>Michael lit a candle<span>3 hours ago</span>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																	<div class="card">
																		<div class="card-header">
																			<a class="collapsed card-link activity"
																			   data-toggle="collapse"
																			   href="javascript:;">
																				Page Views <span>85</span>
																			</a>
																		</div>
																		<div id="collapseFive" class="collapse"
																			 data-parent="#accordion">
																			<div class="card-body">
																				Lorem ipsum..
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-8 col-12 d-md-block d-none">

																<div class="tab-content">
																	<div class="tab-pane active" id="home">
																		<div class="tribute_box">
																			<h3><img
																					src="<?php echo base_url(); ?>assets/img/quote1.png"
																					class="d-inline-block" alt=""/>
																				Lorem ipsum dolor sit amet, consectetur
																				adipisicing elit, sed do eiusmod tempor
																				incididunt. <img
																					src="<?php echo base_url(); ?>assets/img/quote2.png"
																					class="d-inline-block" alt=""/></h3>
																			<span class="edit_class"><i
																					class="fa fa-pencil"
																					aria-hidden="true"></i>Edit</span>
																		</div>
																		<div class="tribute_box">
																			<ul class="tri_list_box">
																				<li>56 Years Old</li>
																				<li>Born on March 3, 1962 in Nairoby,
																					Kenya
																				</li>
																				<li>Passed a way on June 22, 2018 in
																					Nakuru, Kenya
																				</li>
																			</ul>
																			<span class="edit_class"><i
																					class="fa fa-pencil"
																					aria-hidden="true"></i>Edit</span>
																		</div>
																		<div class="tribute_box">
																			<p>Lorem ipsum dolor sit amet, consectetur
																				adipisicing elit, sed do eiusmod tempor
																				incididunt ut labore et dolore magna
																				aliqua. Ut enim ad minim veniam, quis
																				nostrud exercitation ullamco laboris
																				nisi ut aliquip ex ea commodo
																				consequat. </p>

																			<p>Duis aute irure dolor in reprehenderit in
																				voluptate velit esse cillum dolore eu
																				fugiat nulla pariatur. Excepteur sint
																				occaecat cupidatat non proident, sunt in
																				culpa qui officia deserunt mollit anim
																				id est laborum. Sed ut perspiciatis unde
																				omnis iste natus error sit voluptatem
																				accusantium doloremque laudantium, totam
																				rem aperiam, eaque ipsa quae ab illo
																				inventore veritatis et quasi architecto
																				beatae vitae dicta sunt explicabo. Nemo
																				enim ipsam voluptatem quia voluptas sit
																				aspernatur aut odit aut fugit, sed quia
																				consequuntur magni dolores eos qui
																				ratione voluptatem sequi nesciunt. </p>

																			<p>Neque porro quisquam est, qui dolorem
																				ipsum quia dolor sit amet, consectetur,
																				adipisci velit, sed quia non numquam
																				eius modi tempora incidunt ut labore et
																				dolore magnam aliquam quaerat
																				voluptatem.</p>
																			<span class="edit_class"><i
																					class="fa fa-pencil"
																					aria-hidden="true"></i>Edit</span>
																		</div>
																		<a href="javascript:;" class="add_story">Leave a
																			Tribute</a>
																	</div>
																	<div class="tab-pane fade tab_notice" id="menu1">
																		<div class="notice-form">
																			<div class="form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text1">Notice Board
																						For</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text1"
																						   placeholder="First Name, Last Name">
																				</div>
																			</div>
																			<div class="form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text2">Date of
																						Birth</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text2"
																						   placeholder="Date, Month, Year, Place">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text3">Date of
																						Death</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text3"
																						   placeholder="Date, Month, Year, Place">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text4">Church/Mosque/Temple</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text4"
																						   placeholder="Location, Town, Date, Time">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text5">Country</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text5"
																						   placeholder="Country, Public Cemetery, Date, Time">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text5">County</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text5"
																						   placeholder="County">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text5">Town</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text5"
																						   placeholder="Town/Centre">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text6">Cremation
																						Details</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text6"
																						   placeholder="Location, Crematorium, Time">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text7">Funeral Committee
																						Contact Person</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text7"
																						   placeholder="1st Name, 2nd Name, Phones">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text8">Funeral Committee
																						Meetings Venue</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text8"
																						   placeholder="Town , Location, Buidling, Meeting Dates">
																				</div>
																			</div>
																			<div
																				class="form-group form-group row m-0 mb-2">
																				<div class="col-md-6">
																					<label for="text9">Burial/Funeral/Interment
																						Location Google Map</label>
																				</div>
																				<div class="col-md-6">
																					<input type="text"
																						   class="form-control"
																						   id="text9" placeholder="">
																				</div>
																			</div>


																			<div class="map_class">
																				<iframe
																					src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50470.02846874147!2d-122.47261937941492!3d37.75776267828958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1539413862187"
																					width="600" height="450"
																					frameborder="0" style="border:0"
																					allowfullscreen></iframe>
																			</div>
																			<ul class="list-inline notice_list">
																				<li class="list-inline-item">Notice:
																					dawdaoiajdawda
																				</li>
																				<li class="list-inline-item">Posted: 10
																					hours ago
																				</li>
																				<li class="list-inline-item">Posted by:
																					admin123
																				</li>
																			</ul>
																		</div>


																		<!--  -->
																	</div>
																	<div class="tab-pane gal_img fade" id="menu2">
																		<!-- <div class="gal_main">
                      <div class="row">
                        <div class="col-md-4">
                          <h4>Photos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Videos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Videos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Audio</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Audio</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                      </div>
                    </div> -->
																		<!-- <div class="space30"></div> -->
																		<div class="gal_sub">
																			<ul class="nav nav-tabs">
																				<li class="nav-item">
																					<a class="nav-link active"
																					   data-toggle="tab"
																					   href="#photoTab">Photos</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#videoTab">VIDEOS</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#audioTab">AUDIO</a>
																				</li>
																			</ul>
																			<!-- Tab panes -->
																			<div class="tab-content">
																				<div class="tab-pane active"
																					 id="photoTab">
																					<div class="gal_box">
																						<div class="row">
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>

																						</div>
																					</div>
																				</div>
																				<div class="tab-pane fade"
																					 id="videoTab">
																					<div class="gal_box">
																						<div class="row">
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="tab-pane fade"
																					 id="audioTab">
																					<div class="gal_box">
																						<div class="row">
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																							<div class="col-md-3">
																								<img class="img-fluid"
																									 alt="img"
																									 src="<?php echo base_url(); ?>assets/img/img23.jpg">
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="space20"></div>
																			<div class="row">
																				<div class="col-md-4"></div>
																				<div class="col-md-4"></div>
																				<div class="col-md-4">
																					<div class="add_gal">
																						<span class="red_btn"><i
																								class="fa fa-plus-circle"
																								aria-hidden="true"></i>Add Photos</span>
																						<input id="selectfile"
																							   type="file" name="">
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="tab-pane fade" id="menu3">
																		<div class="main_tri">
																			<div class="featured_box">
																				<div class="media">
																					<img class=""
																						 src="<?php echo base_url(); ?>assets/img/img12.png"
																						 alt="mage">
																					<div class="media-body">
																						<h5>Audrey <i>lit a Candle</i>
																						</h5>
																						<h6>July 1, 2018, 01.11 pm</h6>
																						<img class="img-fluid img13"
																							 alt="img"
																							 src="<?php echo base_url(); ?>assets/img/img13.png">
																					</div>
																				</div>
																			</div>
																			<div class="featured_box">
																				<div class="media">
																					<img class=""
																						 src="<?php echo base_url(); ?>assets/img/img12.png"
																						 alt="mage">
																					<div class="media-body">
																						<h5>Audrey <i>left a Tribute</i>
																						</h5>
																						<h6>July 1, 2018, 01.11 pm</h6>
																						<span class="span_share"><i
																								class="fa fa-share-alt"
																								aria-hidden="true"></i>Share</span>
																						<p>Lorem ipsum dolor sit amet,
																							consectetur adipisicing
																							elit, sed do eiusmod tempor
																							incididunt ut labore et
																							dolore magna aliqua. Ut enim
																							ad minim veniam, quis
																							nostrud exercitation ullamco
																							laboris nisi ut aliquip ex
																							ea commodo consequat. Duis
																							aute irure dolor in
																							reprehenderit in voluptate
																							velit esse cillum dolore eu
																							fugiat</p>
																					</div>
																				</div>
																			</div>
																			<div class="featured_box">
																				<div class="media">
																					<img class=""
																						 src="<?php echo base_url(); ?>assets/img/img12.png"
																						 alt="mage">
																					<div class="media-body">
																						<h5>Audrey <i>lit a Candle</i>
																						</h5>
																						<h6>July 1, 2018, 01.11 pm</h6>
																						<span
																							class="span_share">View</span>
																					</div>
																				</div>
																			</div>

																		</div>
																		<ul class="list-inline tribute_list">
																			<li class="list-inline-item"><a
																					class="red_btn lea_tri">Leave a
																					Tribute</a></li>
																			<li class="list-inline-item"><a href=""
																											class="red_btn">Light
																					a Candle</a></li>
																			<li class="list-inline-item"><a href=""
																											class="red_btn">Leave
																					a Note</a></li>
																		</ul>
																		<div class="leave_tribute">
																			<span class="tri_span"><img
																					class="img-fluid" alt="img"
																					src="<?php echo base_url(); ?>assets/img/pencil.png">Leave a Tribute</span>
																			<ul class="nav nav-tabs" role="tablist">
																				<li class="nav-item">
																					<a class="nav-link active"
																					   data-toggle="tab"
																					   href="#tab1"><img
																							class="img-fluid active_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
																						<img
																							class="img-fluid default_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
																					</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#tab2"><img
																							class="img-fluid default_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/flower.png">
																						<img
																							class="img-fluid active_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/flower1.png">
																					</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link"
																					   data-toggle="tab"
																					   href="#tab3"><img
																							class="img-fluid default_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/feather.png">
																						<img
																							class="img-fluid active_img"
																							alt="img"
																							src="<?php echo base_url(); ?>assets/img/feather1.png">
																					</a>
																				</li>
																			</ul>
																			<div class="tab-content">
																				<div id="tab1" class="tab-pane active">
																					<br>
																					<textarea class="form-control"
																							  rows="8" id="comment"
																							  placeholder="Add your tribute text here"></textarea>
																				</div>
																				<div id="tab2" class="tab-pane fade">
																					<br>
																					<textarea class="form-control"
																							  rows="8" id="comment"
																							  placeholder="Add your tribute text here"></textarea>
																				</div>
																				<div id="tab3" class="tab-pane fade">
																					<br>
																					<textarea class="form-control"
																							  rows="8" id="comment"
																							  placeholder="Add your tribute text here"></textarea>
																				</div>
																			</div>
																			<ul class="list-inline leave_ul">
																				<li class="list-inline-item">by John
																					Nyongessa
																				</li>
																				<li class="list-inline-item pull-right">
																					<a href="" class="red_btn">Leave a
																						Tribute</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</section>

											</div>


										</div>
									</div>
								</div>
								<div class="space30"></div>
								<a href="#" id="go_to_account_form" class="red_btn prev_btn prevtab"><i
										class="fa fa-angle-left"
										aria-hidden="true"></i>PREVIOUS</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<style>
	#test img.img-fluid.feature_img {
		box-shadow: 0px 3px 9px 3px #ccc;
	}

	p#valid {
		color: red;
		margin-top: -31px;
	}

	p.signtitle {
		margin-top: 30px;
		font-size: 23px;
		text-decoration: underline;
	}

	div#testlast img {
		/*
		height: 376px;
		width: 72%;
*/
		box-shadow: 1px 1px 12px 4px #ccc;
	}

	input.red_btn.red_btn1 {
		border: none;
	}

	button#info {
		float: right;
		max-width: 165px;
		height: 40px;
		font-size: 16px;
		font-family: 'SF-Pro-Display-Bold';
		line-height: 40px;
		border: none;
	}

	label.error {
		color: red;
		margin-top: -31px;
		position: absolute;
		/* float: right; */
	}

	a#info1 {
		color: #fff;
	}

	p.dateval2 {
		color: red;
		margin-top: -21px;
	}

	a#sec, a#inforegsed, a#sec1 {
		color: #fff;
	}

	p.cropped_image_msg {
		color: red;
		margin-top: -12px;
	}

	p.dateval1 {
		color: red;
		margin-top: -19px;
	}

	p.dateval {
		color: red;
		margin-top: -12px;
	}

	button#inforeg {
		float: right;
		max-width: 165px;
		height: 40px;
		font-size: 16px;
		font-family: 'SF-Pro-Display-Bold';
		line-height: 40px;
		border: none;
	}

	p.imgmsg {
		color: red;
	}

	p.weburl {
		font-size: 15px;
	}

	.purchase_modal.modal {
		z-index: 9999;
	}

	p.imgmsg1, p.imgmsg2, .imgmsgupload, .imgmsgemailsas, .imgmsgaa21, .imgmsgaa, p.imgmsg3, p.imgmsg4, p.imgmsg5, p.imgmsg8, p.imgmsg9, p.imgmsg10, p.imgmsg11, p.imgmsg12, p.imgmsg13, p.imgmsg14, p.imgmsg15, p.imgmsg16 {
		color: red !important;
		margin-top: -23px;
	}

	p.imgmsg6 {
		color: red;
		margin-top: 0px;
	}

	p.addnotok {
		color: red;
		margin-top: 0px;
	}

	a.red_btn.next_btn.nexttab {
		color: #fff;
	}

	.red_btn {
		cursor: pointer;
	}
</style>


<?php $this->load->view("include/footer_menu.php"); ?>


<!-- javascript libraries -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://unpkg.com/sweetalert%402.1.2/dist/sweetalert.min.js"></script>
<script type="text/javascript">


    $(function () {

        var pending_memo = '<?php echo count($pending_memo); ?>';
        if (pending_memo > 0) {
            //$(".firsttab").click();
        }
    });


    $(function () {

        pauseAudio();

        $("#datepicker").datepicker({
            autoclose: true,
            todayHighlight: true
        }).datepicker('update', new Date());
    });
    $(function () {
        $("#datepicker1").datepicker({
            autoclose: true,
            todayHighlight: true
        }).datepicker('update', new Date());
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    $('.nexttab').click(function () {
        $(this).parents('.tab-pane').removeClass('show active');
        $(this).parents('.tab-pane').next().addClass('show active');
        $(this).parents('.create_main').siblings('.testi_sec').find('a.active').parents('li').next('li').find('.nav-link').trigger('click');
    });
    $('.prevtab').click(function () {
        $(this).parents('.tab-pane').removeClass('show active');
        $(this).parents('.tab-pane').prev().addClass('show active');
        $(this).parents('.create_main').siblings('.testi_sec').find('a.active').parents('li').prev('li').find('.nav-link').trigger('click');
    });
</script>

<script type="text/javascript">
    jQuery('.select_btn').on('click', function (e1) {
        e1.preventDefault();
        var idthm = jQuery(this).attr('href');
        jQuery(".themeidint").html('');
        $('.themeidint').html('<input type="hidden" class="themeid" name="themeid" value="' + idthm + '">');
        $('.select_btn').css("background", "#582236");
        $(this).css("background", "green");
        $("html, body").animate({scrollTop: $(document).height()}, "slow");
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    $("#contact_form1").validate({
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            deathdate: {
                required: true
            },
            birthdate: {
                required: true
            },
            rphone: {
                required: true
            },
            /* mname: {
			required: true
		},
		nname: {
			required: true
		}, */
            rfname: {
                required: true
            },
            rsurname: {
                required: true
            },
            radd: {
                required: true
            },
            webaddress: {
                required: true
            },
            rsurname: {
                required: true
            },
            florida: {
                required: true
            },
            accountname: {
                required: true
            },
            accountlname: {
                required: true
            },
            acountemail: {
                required: true
            },
            accountpassword: {
                required: true
            },
        },
        messages: {
            fname: {
                fname: ''
            },
            lname: {
                required: ''
            },
            deathdate: {
                required: ''
            },
            birthdate: {
                required: ''
            },
            radd: {
                required: ''
            },
            /* mname: {
			required: ''
		},
		niname: {
			required: ''
		}, */
            rphone: {
                required: ''
            },
            rfname: {
                required: ''
            },
            rsurname: {
                required: ''
            },
            florida: {
                required: ''
            },
            accountname: {
                required: ''
            },
            accountlname: {
                required: ''
            },
            acountemail: {
                required: ''
            },
            accountpassword: {
                required: ''
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>
<script type="text/javascript">
    $('.navbar-nav li').addClass('nav-item');
    $('.navbar-nav li a').addClass('nav-link');
</script>
<script type="text/javascript">
    $(".rnum").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script>
    function AvoidSpace(event) {
        var k = event ? event.which : window.event.keyCode;
        if (k == 32) return false;
    }

    $('#info').click(function () {
        var cropped_image = $("#cropped_image").val();
        var fname = $("#usr").val();
        var laname = $("#lname").val();
        var mname = $("#mname").val();
        var niname = $("#niname").val();
        var towncity = $("#citytown").val();
        var placeofdeath = $("#placeofdeath").val();
        var birthdatecheck = $("#birthdatecheck").val();
        var dathdatecheck = $("#dathdatecheck").val();
        var weaddress = $("#weaddress").val();
        //var addressallok = $(".addressallok").val();
        var rphone = $("#rphone").val();
        var rfname = $("#rfname").val();
        var rsurname = $("#rsurname").val();
        var radd = $("#radd").val();
        var remail = $("#remail").val();
        var rpnm = $("#rpnm").val();
        //var imgtemid = $(".imgtemid").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var valeemail = $(".emailval").val();

        if (birthdatecheck == "") {
            $(".dateval1").css("display", "block")
        } else {
            $(".dateval1").css("display", "none")
        }
        if (dathdatecheck == "") {
            $(".dateval2").css("display", "block")
        } else {
            $(".dateval2").css("display", "none")
        }


        var GivenDate = dathdatecheck;
        var CurrentDate = new Date();
        GivenDate = new Date(GivenDate);

        birthdatecheck = new Date(birthdatecheck);
        dathdatecheck = new Date(dathdatecheck);

        if (GivenDate > CurrentDate) {
            $(".dateval2").text("Date of Death Is greter then today");
            $(".dateval2").show();
        } else if (birthdatecheck < dathdatecheck) {
            $(".dateval").css("display", "none")
            $(".dateval1").css("display", "none")
            $(".dateval2").css("display", "none")
        } else if (birthdatecheck == "") {
            $(".dateval1").css("display", "block")
        } else {
            $(".dateval1").css("display", "none")
            $(".dateval").css("display", "block")
        }


        if (placeofdeath == "") {
            $(".imgmsgaa21").css("display", "block")
        } else {
            $(".imgmsgaa21").css("display", "none")
        }
        /* if (imgtemid == "") {
		$(".imgmsgupload").css("display", "block")
	} else {
		$(".imgmsgupload").css("display", "none")
	} */
        /* if (addressallok == "") {
		$(".addnotok").css("display", "block")
	} else {
		$(".addnotok").css("display", "none")
	} */
        if (towncity == "") {
            $(".imgmsgaa").css("display", "block")
        } else {
            $(".imgmsgaa").css("display", "none")
        }
        if (fname == "") {
            $(".imgmsg1").css("display", "block");
        } else {
            $(".imgmsg1").css("display", "none")
        }
        if (laname == "") {
            $(".imgmsg2").css("display", "block");
        } else {
            $(".imgmsg2").css("display", "none")
        }
        if (mname == "") {
            //$(".imgmsg3").css("display", "block");
        } else {
            //$(".imgmsg3").css("display", "none")
        }
        if (niname == "") {
            //$(".imgmsg4").css("display", "block");
        } else {
            //$(".imgmsg4").css("display", "none")
        }
        if (weaddress == "") {
            $(".imgmsg5").css("display", "block");
        } else {
            $(".imgmsg5").css("display", "none")
        }
        if (rphone == "") {
            $(".imgmsg7").css("display", "block");
        } else {
            $(".imgmsg7").css("display", "none")
        }
        if (rfname == "") {
            $(".imgmsg8").css("display", "block");
        } else {
            $(".imgmsg8").css("display", "none")
        }
        if (rsurname == "") {
            $(".imgmsg9").css("display", "block");
        } else {
            $(".imgmsg9").css("display", "none")
        }
        if (radd == "") {
            $(".imgmsg10").css("display", "block");
        } else {
            $(".imgmsg10").css("display", "none")
        }
        if (remail == "") {
            $(".imgmsg11").css("display", "block");
        } else {
            $(".imgmsg11").css("display", "none")
        }
        if (rpnm == "") {
            $(".imgmsg12").css("display", "block");
        } else {
            $(".imgmsg12").css("display", "none")
        }

        if (cropped_image == "") {
            $(".cropped_image_msg").css("display", "block");
        } else {
            $(".cropped_image_msg").css("display", "none")
        }

        var weaddress_taken = "NO";
        $.ajax({
            url: "<?php echo base_url(); ?>memorial/check_address",
            type: "POST",
            data: {"webaddress": weaddress},
            success: function (resp) {
                //var response = eval("(" + data + ")");
                if (resp > 0) {
                    weaddress_taken = "YES";
                    $(".addnotok").text("Name already taken");
                    $(".addnotok").show();
                    return false;
                } else {
                    weaddress_taken = "NO";
                    $(".addnotok").hide();

                    if (weaddress_taken == 'YES' || cropped_image == "" || rphone == "" || placeofdeath == "" || fname == "" || laname == "" || rfname == "" || rsurname == "" || radd == "" || remail == "" || rpnm == "" || birthdatecheck == "" || dathdatecheck == "" || weaddress == "" || birthdatecheck > dathdatecheck || valeemail == "not"
                    ) {
                        $("#info").css("display", "block");
                        $("#info1").removeClass("nexttab");
                    } else if (weaddress_taken == 'NO' || cropped_image !== "" || rphone != "" || fname != "" || placeofdeath != "" || laname != "" || rfname != "" || rsurname != "" || radd != "" || remail != "" || rpnm != "" || birthdatecheck != "" || dathdatecheck != "" || weaddress != "" || birthdatecheck < dathdatecheck || valeemail == " ") {
                        //swal("Done!", "All data filled Sucessfully!", "success");
                        $("#info").css("display", "none");
                        $("#sec").css("display", "block");
                        $("#sec").click();
                    }

                }
            }
        });


    });
    $("#usr").keyup(function () {
        var input = $(this);
        if (input.val() != "") {
            $(".imgmsg1").css("display", "none");
        }
    });


    $('#weaddress').keyup(function () {

        var yourInput = $(this).val();

        if (yourInput.match(/\s/g)) {
            $(this).val(yourInput.replace(/\s/g, ''));
        }

        re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        var isSplChar = re.test(yourInput);
        if (isSplChar) {
            var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            $(this).val(no_spl_char);
        }

    });

    $(".names").keyup(function () {

        var fname = $("#usr").val();
        var lname = $("#lname").val();
        var mname = $("#mname").val();

        $.ajax({
            url: "<?php echo base_url(); ?>memorial/get_unique_address",
            type: "POST",
            data: {"fname": fname, "mname": mname, "lname": lname},
            success: function (resp) {
                $("#weaddress").val(resp);
            }
        });


    });

    /* $('#inforeg').click(function () {
	if ($("input").val() != "") {
		$("#inforeg").css("display", "block");
		$(".imgmsg1").css("display", "block");
	} else if (("input").val() != "") {
		$("#inforegsed").addClass('nexttab');
	}
}); */
    $('#inforeg').click(function () {

        var createdby = $("#createdby").val();
        //alert(createdby);

        if (createdby > 0) {
            $("#inforeg").hide();
            $("#sec1").show();
            $('#sec1').click();
        } else {

            var acn = $("#acn").val();
            var aclna = $("#aclna").val();
            var acemail = $("#acemail").val();
            var password = $("#password").val();
            if (acn == "") {
                $(".imgmsg13").css("display", "block");
            }
            if (aclna == "") {
                $(".imgmsg14").css("display", "block");
            }
            if (acemail == "") {
                $(".imgmsg15").css("display", "block");
            }
            if (password == "") {
                $(".imgmsg16").css("display", "block");
            }

            if (password.length < 6) {
                $(".imgmsglength").css("display", "block");
            } else {
                $(".imgmsglength").css("display", "none");
            }

            if (acn == "" || aclna == "" || acemail == "" || password == "") {
                $("#inforegsed").css("display", "none");
                $("#info1").removeClass("nexttab");
            } else if (acn != "" || aclna != "" || acemail != "" || password != "") {
                $("#inforegsed").css("display", "none");
            }
            $("#acn").keyup(function () {
                var input = $(this);
                if (input.val() != "") {
                    $(".imgmsg13").css("display", "none");
                }
            });
            $("#aclna").keyup(function () {
                var input = $(this);
                if (input.val() != "") {
                    $(".imgmsg14").css("display", "none");
                }
            });
            $("#acemail").keyup(function () {
                var input = $(this);
                if (input.val() != "") {
                    $(".imgmsg15").css("display", "none");
                }
            });
            $("#password").keyup(function () {
                var input = $(this);
                if (input.val() != "") {
                    $(".imgmsg16").css("display", "none");
                }
            });

            /* if ($("input").val() != "") {
			$("#inforeg").css("display", "block");
			$(".imgmsg1").css("display", "block");
		} else if (("input").val() != "") {
			$("#inforegsed").addClass('nexttab');
		} */


            if (acn != "" && aclna != "" && acemail != "" && password != "" && password.length >= 6 && password == $('#confirm_password').val()) {

                //alert("send ajax");
                $.ajax({
                    url: "<?php echo base_url(); ?>login/create_account_with_memorial",
                    type: "POST",
                    data: {"fname": acn, "lname": aclna, "email": acemail, "password": password},
                    success: function (resp) {
                        console.log(resp);
                        if (resp == 'aleady_exists') {
                            //alert("already exists");
                            $(".imgmsg15").html("Already register with this email, Please <a style='cursor: pointer;' onclick='login_now()' class='red_btn tribute_box-btn tribute_box-btn1'>login</a>");
                            $(".imgmsg15").show();
                            //$(".go_to_account_form").click();
                        } else if (resp == 'error') {
                            $(".imgmsg15").html("Something went wrong");
                            $(".imgmsg15").show();
                            return false;
                        } else {
                            $(".imgmsg15").hide();
                            $("#createdby").val(resp);

                            $("#inforeg").hide();
                            $("#sec1").show();
                            $('#sec1').click();
                        }
                    }
                });

            } else {
                // do somthing
            }

        }


    });


    var validateEmail = function (elementValue) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    }
    $('#acn, #aclna, #acemail,#password,#confirm_password').on('keyup', function () {
        $val = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(acemail.value);
        if ($val == false) {
            $('.imgmsgemailsas').show();
            $('.imgmsgemailsas').html("Invalid email address");
        } else {
            $('.imgmsgemailsas').hide();
        }

        /* if ($('#acn').val() != "" && $('#aclna').val() != "" && $('#acemail').val() != "" && $('#password').val() != "" && $('#password').val() == $('#confirm_password').val() && $val == true) {
		$("#sec1").css("display", "block");
		$("#inforeg").css("display", "none");
	} */
    });
    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
        } else
            $('#message').html('Not Matching').css('color', 'red');
    });
    $(document).ready(function () {
        $('#selection').on('change', function () {
            if ($(this).val() != 'select') {
                $("#first_next_btn").removeClass('disabled');
            } else {
                $("#first_next_btn").addClass('disabled');
            }
            var sourceUrl = '<?php echo base_url() . "uploads/music/"; ?>' + $(this).val();
            $('#memorial_music').val($(this).val());
            change(sourceUrl);
            sessionStorage.setItem("song", sourceUrl);
        });
    });

    function change(sourceUrl) {
        var audio = document.getElementById("player");
        var source = document.getElementById("mp3_src");
        audio.pause();
        if (sourceUrl) {
            source.src = sourceUrl;
            audio.load();
            audio.play();
        }
    }

    $('.firsttab').click(function () {	/* var songona = $("#selection").val();	alert(songona); 		if(songona === 'select'){		return false;	} */
        $(".firtab").attr("href", "#home");
        $(".sectab").attr("href", "#menu1");
    });
    $('.abc').click(function () {
        $(".sectab").attr("href", "#menu1");
        $(".thirdtab").attr("href", "#menu2");
    });
    $('#sec1').click(function () {


        /* Store info for later use if user does not create memorial */


        var memorial_template = $("#memorial_template").val();
        var memorial_music = $("#memorial_music").val();

        var fname = $("#usr").val();
        var lname = $("#lname").val();
        var mname = $("#mname").val();
        var nname = $("#niname").val();

        var countryorigin1 = $("#countryorigin1").val();
        var citytown = $("#citytown").val();
        var cropped_image = $("#cropped_image").val();

        var placeofdeath = $("#placeofdeath").val();
        var birthdate = $("#birthdatecheck").val();
        var deathdate = $("#dathdatecheck").val();

        var gender = $("input[name='gender']:checked").val();
        var webaddress = $("#weaddress").val();

        var relationname = $("#sel1").val();
        var rfname = $("#rfname").val();
        var rsurname = $("#rsurname").val();

        var rphone = $("#rphone").val();
        var countryorigin2 = $("#countryorigin2").val();
        var raddress = $("#raddress").val();

        var remailadd = $("#remail").val();
        var rpnm = $("#rpnm").val();


        var createdby = $("#createdby").val();

        $.ajax({
            url: "<?php echo base_url(); ?>memorial/save_memo_info_if_left_in_the_way",
            type: "POST",
            data: {
                "memorial_template": memorial_template,
                "memorial_music": memorial_music,
                "fname": fname,
                "lname": lname,
                "mname": mname,
                "nname": nname,
                "countryorigin1": countryorigin1,
                "citytown": citytown,
                "cropped_image": cropped_image,
                "placeofdeath": placeofdeath,
                "birthdate": birthdate,
                "deathdate": deathdate,
                "gender": gender,
                "webaddress": webaddress,
                "relationname": relationname,
                "rfname": rfname,
                "rsurname": rsurname,
                "rphone": rphone,
                "countryorigin2": countryorigin2,
                "raddress": raddress,
                "remailadd": remailadd,
                "rpnm": rpnm,
                "createdby": createdby
            },
            success: function (resp) {

            }
        });

        /* End section //remove this info from db if memorial created (detected by unique webaddress) */


        var template_id = $("#memorial_template").val();

        var createdby = $("#createdby").val();
        if (createdby > 0) {

            $(".thirdtab").attr("href", "#menu2");
            $(".forttab").attr("href", "#menu3");

            $.ajax({
                url: "<?php echo base_url(); ?>memorial/get_template",
                type: "POST",
                data: {"template_id": template_id},
                success: function (resp) {
                    var template = JSON.parse(resp);
                    $('.crafts-template').removeClass('crafts-template').addClass(template.class);
                    $('#background_head_img').css('background', 'url(<?php echo base_url(); ?>assets/img/' + template.background + ') no-repeat');
                    //$("#preview_src").attr('src', '<?php echo base_url(); ?>uploads/'+$('#cropped_image').val());
                    $('#preview_src').css('background', 'url(<?php echo base_url(); ?>uploads/' + $('#cropped_image').val() + ') center no-repeat');

                    var name = $("#usr").val() + ' ' + $("#lname").val();
                    $("#memory_name").text("");
                    $("#memory_name").text(name);
                    //$("#loving_memory").text("");


                    console.log(template);
                    //alert(template.class);
                    //$('#uploadimageModal').modal('hide');
                    //$('#uploaded_image').html(resp);
                }
            });

        }

    });
</script>

<script>
    $("#birthdatecheck").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/m/yy',
        autoclose: true,
        endDate: "today",
        yearRange: "-119:+0",
        maxDate: '0'
    });


    $("#birthdatecheck1").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/m/yy',
        autoclose: true,
        endDate: "today",
        yearRange: "-119:+0",
        maxDate: '0'
    });

    $("#dathdatecheck").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/m/yy',
        autoclose: true,
        endDate: "today",
        yearRange: "-119:+0",
        maxDate: '0'
    });
    var x = document.getElementById("player");

    function pauseAudio() {
        x.pause();
    }

    function playAudio() {
        x.play();
    }

    var uploadCrop;
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 376,
            height: 478,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 500
        }
    });
    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
                console.log('jQuery bind complete');
            });
        }
        $('.upload-result').show();
        $("#upload-demo").show();
        $("#placehold").hide();
        reader.readAsDataURL(this.files[0]);
    });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $.ajax({
                url: "",
                type: "POST",
                data: {action: "ajax_uploadimageresule", "entryimage": resp},
                success: function (data) {
                    console.log(data);
                    var response = eval("(" + data + ")");
                    $('#upload-demo-i').attr('src', resp);
                    $('.profile_imageurl').val(response.message);
                    $('.imgtemid').val(response.status);
                    $('.hidethisclass').hide();
                    $('#upload-demo-i2').show();
                }
            });
        });
    });

    $('.prevtodata').click(function () {
        $("#info").css("display", "block");
        $("#sec").css("display", "none");
    });
    $('#button_facebook').click(function () {
    });
    $('input[name="radioInline8"]').on('change', function () {
        sessionStorage.setItem('gender', $(this).val());
    });
    $('#sel1').on('change', function () {
        change($(this).val());
        sessionStorage.setItem("relaion", $(this).val());
    });
    $('.firsttab').click(function () {
        sessionStorage.setItem("themeid", $('.themeid').val());
    });
    $('.abc').click(function () {
        sessionStorage.setItem("fname", $('#usr').val());
        sessionStorage.setItem("lname", $('#lname').val());
        sessionStorage.setItem("mname", $('#mname').val());
        sessionStorage.setItem("niname", $('#niname').val());
        sessionStorage.setItem("niname", $('#niname').val());
        sessionStorage.setItem("dod", $('#dathdatecheck').val());
        sessionStorage.setItem("dob", $('#birthdatecheck').val());
        sessionStorage.setItem("rphone", $('#rphone').val());
        sessionStorage.setItem("rfname", $('#rfname').val());
        sessionStorage.setItem("rsurname", $('#rsurname').val());
        sessionStorage.setItem("radd", $('#radd').val());
        sessionStorage.setItem("remail", $('#remail').val());
        sessionStorage.setItem("rpnm", $('#rpnm').val());
    });
    window.onload = function () {
        var themeid = sessionStorage.getItem("themeid");
        if (themeid !== null) $('.themeid').val(themeid);
        //var song = sessionStorage.getItem("song");
        //if (song !== null) $('#selection').val(song);
        var fname = sessionStorage.getItem("fname");
        if (fname !== null) $('#usr').val(fname);
        var lname = sessionStorage.getItem("lname");
        if (lname !== null) $('#lname').val(lname);
        var mname = sessionStorage.getItem("mname");
        if (mname !== null) $('#mname').val(mname);
        var niname = sessionStorage.getItem("niname");
        if (niname !== null) $('#niname').val(niname);
        var niname = sessionStorage.getItem("niname");
        if (niname !== null) $('#niname').val(niname);
        var dod = sessionStorage.getItem("dod");
        if (dod !== null) $('#dathdatecheck').val(dod);
        var dob = sessionStorage.getItem("dob");
        if (dob !== null) $('#birthdatecheck').val(dob);
        var dob = sessionStorage.getItem("dob");
        if (dob !== null) $('#birthdatecheck').val(dob);
        var relaion = sessionStorage.getItem("relaion");
        if (relaion !== null) $('#sel1').val(relaion);
        var rphone = sessionStorage.getItem("rphone");
        if (rphone !== null) $('#rphone').val(rphone);
        var rfname = sessionStorage.getItem("rfname");
        if (rfname !== null) $('#rfname').val(rfname);
        var rsurname = sessionStorage.getItem("rsurname");
        if (rsurname !== null) $('#rsurname').val(rsurname);
        var radd = sessionStorage.getItem("radd");
        if (radd !== null) $('#radd').val(radd);
        var remail = sessionStorage.getItem("remail");
        if (remail !== null) $('#remail').val(remail);
        var rpnm = sessionStorage.getItem("rpnm");
        if (rpnm !== null) $('#rpnm').val(rpnm);
        var radios = document.getElementsByName("radioInline8");
        var val = sessionStorage.getItem('radioInline8');
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].value == val) {
                radios[i].checked = true;
            }
        }
    }
</script>
<script src='<?php echo base_url(); ?>assets/js/jquery-input-file-text.js'></script>
<script>


    function check_address(address) {

        var address = $("#weaddress").val();
        if (address != "") {

            $(".imgmsg5").css("display", "none");

            $.ajax({
                url: "<?php echo base_url(); ?>memorial/check_address",
                type: "POST",
                data: {"webaddress": address},
                success: function (resp) {
                    //var response = eval("(" + data + ")");
                    if (resp == 0) {
                        //alert("does not");
                        $('#alladdressok').attr('value', "yes");
                        $(".addnotok").hide();
                    } else {
                        //alert("exists");
                        $('#alladdressok').attr('value', "");
                        $(".addnotok").text("Name already taken");
                        $(".addnotok").show();
                        return false;
                    }

                }
            });
        }
    }

    $(document).ready(function () {

        /* $('#weaddress').on('keyup', function () {

	}); */

        $(".preview_btn").mouseover(function () {
            $(this).click();
        });

        $('#videoUploadFile').inputFileText({text: 'select image'});
    });

    function validateEmail(emailField) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField.value) == false) {
            $(".imgmsg19").css("display", "block");
            return false;
            $("#info").css("display", "block");
            $("#sec").css("display", "none");
        }
        return true;
    }

    $('#remail').on('keyup', function () {

        if ($('#remail').val() != '') {
            $('#valid').show();
            $('.imgmsg11').hide();
            var valid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value) && this.value.length;
            $('#valid').html((valid ? '' : ' Please enter a valid email address'));
            $('.emailval').val(valid ? '' : 'not');
        } else {

            $('.imgmsg11').show();
            $('#valid').hide();
        }


    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#videoUploadFile").change(function () {
        readURL(this);
    });
</script>


<script>
    var validaaaa = "";
    $(document).ready(function () {

        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
                width: 280,
                height: 280,
                type: 'square' //circle
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        var v1 = "ss";

        $('#image').on('change', function (e) {

            // check width and height of image
            var _URL = window.URL || window.webkitURL;
            var file, img;
            if (file = this.files[0]) {
                img = new Image();
                img.onload = function () {

                    if (this.width < 120 || this.height < 120) {

                        $("#err_text").text("Image is too small, Please try a larger image. Min dimension: 120px");
                        $("#ErrorModal").modal('show');

                    } else if (this.width > 24000 || this.height > 24000) {

                        $("#err_text").text("Image is too large, Please try a smaller image. Max dimension: 25000px");
                        $("#ErrorModal").modal('show');

                    } else {
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            $image_crop.croppie('bind', {
                                url: event.target.result
                            }).then(function () {
                                console.log('jQuery bind complete');
                            });
                        }

                        reader.readAsDataURL(file);
                        $('#uploadimageModal').modal('show');
                    }

                };

                img.src = _URL.createObjectURL(file);
            }


            // check for image size
            //alert(this.files[0].clientWidth);
            /* var size = this.files[0].size;
										if(size < 8164){
											$("#err_text").text("Image is too small, Please try a larger image. Min dimension: 120px");
											$("#ErrorModal").modal('show');
											return false;
										}
										if(size > 5554349){
											$("#err_text").text("Image is too large, Please try a smaller image. Max dimension: 25000px");
											$("#ErrorModal").modal('show');
											return false;
										} */
            // End check for image size


            //alert(validate);


            /* if(validaaaa == "success"){
											reader.readAsDataURL(this.files[0]);
											$('#uploadimageModal').modal('show');
										}else{
											//alert("error");
											$("#uploadimageModal").hide();
											return false;
										} */


        });


        /* if(v1=='error'){
									 $("#uploadimageModal").hide();
											return false;
								  } */

        $('.crop_image').click(function (event) {
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (response) {
                $.ajax({
                    url: "<?php echo base_url(); ?>memorial/upload_crop_image",
                    type: "POST",
                    data: {"image": response},
                    success: function (img) {
                        $('#uploadimageModal').modal('hide');
                        //$('#uploaded_image').html(data.text);
                        $('#uploaded_image').html('<img src="<?php echo base_url(); ?>uploads/' + img + '" width="130" height="130" class="thumbnail" />');
                        $('#cropped_image').val(img);
                    }
                });
            })
        });

    });
</script>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/croppie/croppie.js"></script>

<!---- Success Modal -->
<div class="modal fade" id="SuccessModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 style="line-height: 1; color: black" class="modal-title">Success!</h4>
			</div>
			<div class="modal-body">
				<p style="color: green" id="suc_text"> Record updated Successfully! </p>
			</div>
			<div style="padding: 15px; text-align: right">
				<button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Success Modal -->  <!---- Error Modal -->
<div class="modal fade" id="ErrorModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 style="line-height: 1; color: black" class="modal-title">Error!</h4>
			</div>
			<div class="modal-body">
				<p style="color: red; font-size: 15px;" id="err_text"> Something went wrong! </p>
			</div>
			<div style="padding: 15px; text-align: right">
				<button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Error Modal -->


</body>
</html>
