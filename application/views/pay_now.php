<?php $this->load->view("include/header.php"); ?>


<section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Pay Now</h1>
            </div>
        </div>
    </div>
</section>

<section class="welcome_main">
    <div class="container">
        <div class="row">
            	
			
			
			
			
			
			<?php 
/*
This is a sample PHP script of how you would ideally integrate with iPay Payments Gateway and also handling the 
callback from iPay and doing the IPN check

----------------------------------------------------------------------------------------------------
            ************(A.) INTEGRATING WITH iPAY ***********************************************
----------------------------------------------------------------------------------------------------
*/
//Data needed by iPay a fair share of it obtained from the user from a form e.g email, number etc...
$fields = array("live"=> "0",
					"oid"=> time().rand(),
					"inv"=> "112020102292999",
					"ttl"=> "2",
					"tel"=> "256712375678",
					"eml"=> "james.noah1172@gmail.com",
					"vid"=> "gt14",
					"curr"=> "KES",
					"p1"=> "airtel",
					"p2"=> "020102292998",
					"p3"=> "",
					"p4"=> "900",
					"cbk"=> 'http://goalwebs.com/memorial/ipay-response.php',
					"cst"=> "1",
					"crl"=> "0"
                );
/*
----------------------------------------------------------------------------------------------------
************(b.) GENERATING THE HASH PARAMETER FROM THE DATASTRING *********************************
----------------------------------------------------------------------------------------------------

The datastring IS concatenated from the data above
*/
$datastring =  $fields['live'].$fields['oid'].$fields['inv'].$fields['ttl'].$fields['tel'].$fields['eml'].$fields['vid'].$fields['curr'].$fields['p1'].$fields['p2'].$fields['p3'].$fields['p4'].$fields['cbk'].$fields['cst'].$fields['crl'];
$hashkey ="5d14Gtplreih6eiuf";//use "demo" for testing where vid also is set to "demo"

/********************************************************************************************************
* Generating the HashString sample
*/
$generated_hash = hash_hmac('sha1',$datastring , $hashkey);

?>
<!--    Generate the form BELOW   -->
   <form method="post" action="https://payments.ipayafrica.com/v3/ke">

<?php 
 foreach ($fields as $key => $value) {
       
     echo  '<input type="text" name="'.$key.'" value="'.$value.'">';
 }
?>
<input name="hsh" type="text" value="<?php echo $generated_hash ?>" ></td>
<br>
<button type="submit">&nbsp;Submit&nbsp;</button>
</form>
			
			
			
			
			
			
			
			
			
			
             
        </div>
    </div>
</section>

<?php $this->load->view("include/footer"); ?>

