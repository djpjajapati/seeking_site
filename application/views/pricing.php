<?php $this->load->view("include/header.php"); ?>

<section class="Plans_head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Plans and Features</h1>
            </div>
        </div>
    </div>
</section>

<section class="price_main">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-12">
                <div class="row">
                   
				<?php $i=1; foreach($pakages as $p){ ?>
				   
					<div class="col-md-6">
                        <div class="price_box <?php if($i == 1) echo 'blue'; else echo 'green'; ?>">
                            <h2 id="title_<?php echo $p->ID; ?>"><?php echo $p->title; ?></h2>
                            <div class="inner_price">
                                <h3><span id="price_<?php echo $p->ID; ?>"><?php echo $p->price; ?></span>.<span>00</span> <span style="font-size: 20px;"><?php echo $p->currency; ?></span></h3>
                                <ul class="list-unstyled">
                                    <li>Customized Design Templates</li>
                                    <li>Notice Board</li>
                                    <li>Personalized Web Address</li>
                                    <li>Email Notifications</li>
                                    <li>Funeral Pin Location</li>
                                    <li>Social Media Integration</li>
                                    <li>Privacy Settings</li>
                                    <li>No Advertising</li>
                                    <li>Access Memorial 24/7/365</li>
                                    <li>Unlimited Contributors</li>
                                    <li>Background Music</li>
                                    <li>Photo Gallery - <?php echo $p->photo_limit; ?> Pictures</li>
									<?php //if($i > 1){ ?>
                                    <li>Video Gallery - <?php echo $p->video_limit; ?> Videos</li>
									
									<?php //} ?>
                                    <li>Audio Clips - <?php echo $p->audio_limit; ?> </li>
									<li>Multiple Administrators - <?php echo $p->page_managers; ?> Admin </li>
                                </ul>
                            </div>
                            <div class="bottom_price">
                                <h4><?php echo $p->annual_maintenance_fee; ?>.00 <span style="font-size: 20px;"><?php echo $p->currency; ?></span> </h4>
                                <p>Annual Maintenance Fees (From 2nd Year)</p>
								
								<?php if($user && $is_memo == 1){ ?>
								
									<a href="<?php echo base_url(); ?>memorial/proceed_to_payment/<?php echo $p->ID; ?>"  class="purchase">Purchase</a>
								
								<?php }else{ ?>
								
									<a href="<?php echo base_url(); ?>memorial/create_memorial?pakage_id=<?php echo $p->ID; ?>" class="purchase">Purchase</a>
								
								<?php } ?>
								
                            </div>
                        </div>
                    </div>
					
				<?php $i++; } ?>
				
					
                </div>
            </div>
        </div>
    </div>
</section>






<?php $this->load->view("include/footer"); ?>

