<?php $this->load->view("include/header.php"); ?>

<section class="tribute_head" style="background: url('<?php echo base_url(); ?>assets/img/<?php echo $memo->background; ?>') no-repeat"> 
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-4">
				<div class="preview_src">
					<img class="img-fluid w-100 img10" alt="img" src="<?php echo base_url().'uploads/'.$memo->cropped_image; ?>">
				</div>
            </div>
            <div class="col-md-8 col-8">
                <p>In loving memory of...</p>
                <h1><?php echo $memo->fname.' '.$memo->lname; ?></h1>
                <ul class="nav nav-tabs d-none d-md-flex">
                  <li class="nav-item">
                    <a id="about_tab" class="about_tab nav-link active" data-toggle="tab" href="#home">ABOUT</a>
                  </li>
                  <li class="nav-item">
                    <a id="notice_tab" class="notice_tab nav-link" data-toggle="tab" href="#menu1">NOTICES</a>
                  </li>
                  <li class="nav-item">
                    <a id="gallary_tab" class="gallary_tab nav-link" data-toggle="tab" href="#menu2">GALLERY</a>
                  </li>
                  <li class="nav-item">
                    <a id="tribute_tab" class="tribute_tab nav-link" data-toggle="tab" href="#menu3">TRIBUTES</a>
                  </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="tribute_main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 d-md-none d-block">
              <div class="tribute_ul_new">
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="about_tab nav-link active" data-toggle="tab" href="#home1">ABOUT</a>
                  </li>
                  <li class="nav-item">
                    <a class="notice_tab nav-link" data-toggle="tab" href="#menu4">NOTICES</a>
                  </li>
                  <li class="nav-item">
                    <a class="gallary_tab nav-link" data-toggle="tab" href="#menu5">GALLERY</a>
                  </li>
                  <li class="nav-item">
                    <a class="tribute_tab nav-link" data-toggle="tab" href="#menu6">TRIBUTES</a>
                  </li>
                </ul>
              </div>
                <div class="tab-content">
                  <div class="tab-pane active" id="home1">
                      <div class="tribute_box">
                        <h3><img src="<?php echo base_url(); ?>assets/img/quote1.png" class="d-inline-block" alt=""/> <p><?php echo $memo->about; ?> </p> <img src="<?php echo base_url(); ?>assets/img/quote2.png" class="d-inline-block" alt=""/></h3>
                        <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <div class="tribute_box">
                        <ul class="tri_list_box">
                          <li>56 Years Old  <?php echo $memo->birthdate; ?></li>
                          <li>Born on March 3, 1962 |<?php echo $memo->birthdate; ?>| in |<?php echo $memo->citytown; ?>|  Nairoby, |<?php echo $memo->countryorigin1; ?>|Kenya</li>
                          <li>Passed a way on June 22, 2018 |<?php echo $memo->deathdate; ?>| in Nakuru, Kenya</li>
                        </ul>
                        <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <div class="tribute_box">
                            <p class=""><?php echo $memo->description; ?> </p>
                            <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <a href="" class="add_story">Add a story</a>
                  </div>
                  <div class="tab-pane fade tab_notice" id="menu4">
                    <div class="notice_main">
                      <div class="form-group">
                        <label for="txt1">Funeral Notice Title</label>
                        <input type="text" class="form-control" id="txt1" placeholder="ie Details for Todd Msekema’s Funeral">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Location and Date of Funeral Service</label>
                        <input type="text" class="form-control" id="txt1" placeholder="25 May 2018, Sixtine Chapel">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Funeral Date</label>
                        <input type="text" class="form-control" id="txt1" placeholder="25 May 2018">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Address of Funeral Location</label>
                        <input type="text" class="form-control" id="txt1" placeholder="828 Union Street, Seattle, Washington">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Time of Funeral:</label>
                        <input type="text" class="form-control" id="txt1" placeholder="2:00 PM">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Funeral committee venue (Place Building, Road)</label>
                        <input type="text" class="form-control" id="txt1" placeholder="2:00 PM">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Funeral Committee Contact Person & Tel. Number</label>
                        <input type="text" class="form-control" id="txt1" placeholder="John Malkovic, 822-1125-2813">
                      </div>
                      <div class="map_class">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50470.02846874147!2d-122.47261937941492!3d37.75776267828958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1539413862187" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                      <ul class="list-inline notice_list">
                        <li class="list-inline-item">Notice: dawdaoiajdawda</li>
                        <li class="list-inline-item">Posted:  10 hours ago</li>
                        <li class="list-inline-item">Posted by:  admin123</li>
                      </ul>
                    </div>
                    <div class="notice_sub">
                      <div class="info_new">
                        <h5>PERSONAL INFORMATION</h5>
                        <span>Notices Board For:</span>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="First Name">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Last Name">
                            </div>
                          </div>
                        </div>
                        <span>Date of Birth:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Month">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Year">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Place">
                            </div>
                          </div>
                        </div>
                        <span>Date of Death:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Month">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Year">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Place">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>FUNERAL SERVICE DETAILS</h5>
                        <span>Church/Mosque/Temple:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Location">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Time">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>BURIAL DETAILS</h5>
                        <span>Location:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" id="sel1">
                                <option>County</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town/Centre">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Time">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Area/Location">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="If Public Cemetery">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>CREMATION DETAILS</h5>
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Crematorium">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Time">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>Funeral Committee Contact Person</h5>
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="1st Name">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="2nd Name">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Tel No">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>Funeral Committee Contact Person</h5>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Location">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Building">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Meeting Dates">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>Burial/Funeral/Interment Location Google Map</h5>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Add Location">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>POST FUNERAL NOTICES</h5>
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Notice/Subject">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="When Notice Was Posted">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Posted By">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tribute_list1">
                        <a href="" class="add_story ylw_btn">Share</a>
                        <a href="" class="add_story">POST NOTICES</a>
                      </div>
                    </div>
                    <!-- <div class="tribute_list1">
                      <a href="" class="add_story ylw_btn">Share</a>
                      <a href="" class="add_story">POST NOTICES</a>
                    </div> -->
                  </div>
                  <div class="tab-pane gal_img fade" id="menu5">
                    <!-- <div class="gal_main">
                      <div class="row">
                        <div class="col-md-4">
                          <h4>Photos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Videos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Videos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Audio</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Audio</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <div class="gal_sub">
                      <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="p_gallery nav-link active" data-toggle="tab" href="#photoTab1">Photos</a>
                        </li>
                        <li class="nav-item">
                          <a class="v_gallery nav-link" data-toggle="tab" href="#videoTab1">VIDEOS</a>
                        </li>
                        <li class="nav-item">
                          <a class="a_gallery nav-link" data-toggle="tab" href="#audioTab1">AUDIO</a>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="photoTab1">
                          <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="videoTab1">
                            <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="audioTab1">
                          <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="space20"></div>
                      <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name="">
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="tab-pane fade" id="menu6">
                     <div class="main_tri">
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>left a Tribute</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</span>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</p>
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share">View</span>
                            </div>
                          </div>
                      </div>
                      <ul class="list-inline tribute_list">
                           <li class="list-inline-item"><a class="red_btn lea_tri">Leave a Tribute</a></li>
                           <li class="list-inline-item"><a href="" class="red_btn">Light a Candle</a></li>
                           <li class="list-inline-item"><a href="" class="red_btn">Leave a Note</a></li>
                      </ul>
                    </div>
                     <div class="leave_tribute">
                        <span class="tri_span"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/pencil.png">Leave a Tribute</span>
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab4"><img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
                            <img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab5"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab6"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather1.png">
                            </a>
                          </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab4" class="tab-pane active">
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab5" class="tab-pane fade">
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab6" class="tab-pane fade">
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                        </div>
                        <ul class="list-inline leave_ul">
                          <li class="list-inline-item">by John Nyongessa</li>
                          <li class="list-inline-item pull-right">
                            <a href="" class="red_btn">Leave a Tribute</a>
                          </li>
                        </ul>
                     </div>
                  </div>
                </div>

            </div>
            <div class="col-md-4 col-12">
                <div class="space70"></div>
                <div class="space30 d-block d-md-none"></div>
                
				<?php if($is_page_admin){ ?>
				
				<div class="tribute_box-left tribute_box-new">
                    <div class="prehead_left">
                        <a href="" class="">Invite Family and Friends to this memorial:</a>
                        <a href="javascript:;" onclick="invite_modal()" class="red_btn tribute_box-btn"><span>+</span>Invite Now</a>
                    </div>
                </div>
				
				<?php } ?>
				
                <div class="tribute_box-left">
                    <div class="prehead_left">
                        <a href="">Share with a friend</a>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>In_Memory_Of/<?php echo $memo->webaddress; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <!--<li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
                            <li class="list-inline-item"><a target="_blank" href="https://twitter.com/share?url=<?php echo base_url(); ?>In_Memory_Of/<?php echo $memo->webaddress; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            
                        </ul>
                    </div>
                </div>
				
                <div class="tribute_box-left tribute_box-new1">
                    <div class="prehead_left">
                        <a href="javascript:;" class="tribute_box-text">Sign up for Notifications</a>
                        <a href="javascript:;" onclick="open_sign_up_model()" class="red_btn tribute_box-btn tribute_box-btn1">Sign Up</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
								
                <div id="accordion">
                    
					<?php if($is_page_admin){ ?>
					
					<form id="add_pic_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_pic" >
					
                    <div class="tribute_box-left tribute_box-new">
                      <div class="prehead_left">
                          <a href="" class="">Add Picture</a>
                          <div >
						  <?php if(count($pictures) > 0){ ?>
							<img class="img-fluid" src="<?php echo base_url(); ?>uploads/<?php echo $pictures[0]->pic; ?>" />
						  <?php } ?>
						  </div>
						  
						  
						 <div class="row" style="margin-top: 20px"> 
						 
						 <div class="col-md-12">
								<?php if($this->session->flashdata('error')){ ?>
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button>
											<?php echo $this->session->flashdata('error'); ?>
										</div>
								<?php } ?>
						 </div>
						 
							  <div class="col-md-6">
								<input type="file" id="upload_picture" name="pic" required>
								<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
								<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">
							  </div>
							  
							  <div class="col-md-6">
								  <div class="add-photo_new">
									<a href="javascript:;" onclick="add_pic()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>
									
								  </div>
							  </div>
						  </div>
						  
						  
                      </div>
                    </div>
					
					<script>
					
					function add_pic(){
						var pic = $("#upload_picture").val();
						if(pic == ''){
							$("#err_text").text("Please select image to upload");
							$("#ErrorModal").modal("show");
						}else{
							
							var memo_id = '<?php echo $memo->ID; ?>';
							var name_limit = 'photo_limit';
							
							$.ajax({
								url:"<?php echo base_url(); ?>memorial/is_pakage_expired",
								type: "POST",
								data:{"memo_id": memo_id, "name_limit": name_limit},
								success:function(resp)
								{
									if(resp == 'yes'){
										$("#err_text").text("Limit Exceeded"); 
										$("#ErrorModal").modal("show"); 
										
									}else{
										$("#add_pic_form").submit();
									}
								}
							}); 
							
							
						}
						
						
					}
					
					</script>
					
                    
					
				</form>
                 
				<?php } ?>

					
                    <div class="card card1">
                        <div class="card-header">
                              <a class="collapsed card-link card-link1" data-toggle="collapse" href="#collapseThree1">
                                <i class="red_clr">2 New </i>  Important Notifications<span><i class="fa fa-exclamation" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseThree1" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Lorem ipsum..
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFour">
                                Page Activity<span><i class="fa fa-users" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseFour" class="collapse show" data-parent="#accordion">
                            <div class="card-body">
                                <ul class="list-unstyled list_page-activity">
                                  <?php foreach($tributes_activity as $t){ ?>
								  
									<li><?php echo $t->by.' '.$t->d_action; ?><span><?php echo $t->timeago; ?></span></li>
								  
								  <?php } ?>
								  
								  
								  <!--<li>Marcela laid a flower<span>2 hours ago</span></li>
                                  <li>Angel left a note<span>2 hours ago</span></li>
                                  <li>Michael lit a candle<span>3 hours ago</span></li>-->
								  
								  
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFive">
                                Page Views   <span><?php echo $memo->views; ?></span>
                              </a>
                        </div>
                        <!--<div id="collapseFive" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Lorem ipsum..
                            </div>
                        </div>-->
                    </div>
                </div>
				
            </div>
            <div class="col-md-8 col-12 d-md-block d-none">
               
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                      <div class="tribute_box">
                        <h3>
							<img src="<?php echo base_url(); ?>assets/img/quote1.png" class="d-inline-block" alt=""/> 
							<span class="memo_about"><?php echo $memo->about; ?></span> 
							<img src="<?php echo base_url(); ?>assets/img/quote2.png" class="d-inline-block" alt=""/>
							</h3>
							
							<?php if($is_page_admin){ ?>
							
									<span class="edit_class" onclick="edit_memo('about');"><i class="fa fa-pencil" aria-hidden="true"></i>Add Quote</span>
							
							<?php } ?>
							
                        
                      </div>
                      <div class="tribute_box">
                        <ul class="tri_list_box">
												
						<span class="memo_dates">
						
						<?php 	$d1 = new DateTime($memo->birthdate);
								$d2 = new DateTime($memo->deathdate);
								$diff = $d2->diff($d1); 
						?>
						
                          <li><?php echo $diff->y; ?> Years Old </li>
                          <li>Born on <?php echo date('l', strtotime($memo->birthdate)).', '.date("F", strtotime($memo->birthdate)).' '.date("d", strtotime($memo->birthdate)); ?> <?php echo date("Y",strtotime($memo->birthdate)); ?> in <?php echo $memo->citytown; ?>, <?php echo $memo->countryorigin1; ?></li>
                          <li>Passed a way on <?php echo date('l', strtotime($memo->deathdate)).', '.date("F", strtotime($memo->deathdate)).' '.date("d", strtotime($memo->deathdate)); ?> <?php echo date("Y",strtotime($memo->deathdate)); ?> in <?php echo $memo->citytown; ?>, <?php echo $memo->countryorigin1; ?></li>
                        
						</span>
						
						</ul>
						
						<?php if($is_page_admin){ ?>
						
							<span class="edit_class" onclick="edit_memo('dates')"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
							
						<?php } ?>
                      </div>
                      <div class="tribute_box">
                            <p class="memo_desc"><?php echo $memo->description; ?></p>
							
							<?php if($is_page_admin){ ?>
							
								<span class="edit_class" onclick="edit_memo('desc');"><i class="fa fa-pencil" aria-hidden="true"></i>Add Description</span>
								
							<?php } ?>
                      </div>
                      <!--<a href="#" class="add_story">Leave a Tribute</a>-->
                      <?php if($is_page_admin){ ?>
					  <a href="javascript:;" onclick="go_to_tribute_tab()" class="add_story">Leave a Tribute</a>
					  <?php } ?>
                    </div>
					
					
					
					<script>
					
					function go_to_tribute_tab(){
						
						$("#tribute_tab").click();
						
					}
					
					function edit_memo(action){
						
						 $("#action").val(action);
						
						 $("#about_sec,#desc_sec,#dates_sec").hide(); 
						 
						 if(action == 'dates'){
							 $('#dates_sec').show();
						 }else if(action == 'desc'){
							 $('#desc_sec').show();
						 }else{
							 $('#about_sec').show();
						 }
						 
						 var memo_id = <?php echo $memo->ID; ?>;
						 var about = $(".memo_about").text();
						 var description = $(".memo_desc").text();
						 
						 $("#edit_about").val(about);
						 $("#edit_desc").val(description);
						 
						 $("#MemoEditModal").modal("show"); 
												
					}
					
					</script>
					
					
					
					<!---- Modal -->
					  <div class="modal fade" id="MemoEditModal" role="dialog">
						<div class="modal-dialog">
						
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <h4 style="line-height: 1; color: black" class="modal-title">Edit Memorial</h4>
							</div>
							<div class="modal-body">
							
							<div class="alert alert-success" id="edit_memo_message" style="display: none">
							  <strong>Success!</strong> Record Updated successfully.
							</div>
							
							<input type="hidden" id="action" type="text" />
							
							<div id="about_sec">
								<label> About </label>
								<textarea id="edit_about" class="form-control" name="about"></textarea>
								<p id="edit_about_error" style="display: none; color: red;">This field is required</p>
							</div>
							
							<div id="desc_sec">							
								<label> Description </label>
								<textarea id="edit_desc" class="form-control" name="description"></textarea>
								<p id="edit_desc_error" style="display: none; color: red;">This field is required</p>
							</div>
							
							<div id="dates_sec">							
								<label> Dates Section </label>
								
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-7 pad_right0">
											<label> Date of Birth </label>
											
										</div>
										<div class="col-md-5">
											<input class="form-control date_form" id="birthdate" name="birthdate"  max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d", strtotime($memo->birthdate)); ?>" type="date"  />
										
										</div>
									</div>
								</div>
								
								<br>
								
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-7 pad_right0">
											<label> Date of Death </label>
										</div>
										<div class="col-md-5">
											<input class="form-control date_form" id="deathdate" name="deathdate" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d", strtotime($memo->deathdate)); ?>" type="date" />
											
										</div>
									</div>
								</div>
								
								</br>
								
								<p id="edit_dates_error" style="display: none; color: red; text-align: center">Date of Death must be greater that Date of birth</p>
								
								
							</div>
	
							<div></div>
							
							
							</div>
							
							<div style="padding: 15px; text-align: right">
								<button type="button" class="btn btn-primary btn-small-edit" onclick="update_memo()" style="background: #582236; border-color: #582236;">Update</button>
								<button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>
							</div>
						  </div>
						  
						</div>
					  </div>
					 <!-- End Success Modal -->
					 
					 
					 <script>
					 
					 function update_memo(){
						 
						 var memo_id = <?php echo $memo->ID; ?>;
						 var about = $("#edit_about").val();
						 var desc = $("#edit_desc").val();
						 var action = $("#action").val();
						 
						 var birthdate = $("#birthdate").val();
						 var deathdate = $("#deathdate").val();
						 
						 if(action == 'dates'){
							
							birthdatecheck = new Date(birthdate);
							dathdatecheck = new Date(deathdate);
							
							if (birthdatecheck > dathdatecheck) {
								$("#edit_dates_error").show();
								return false;
							}else{
								$("#edit_dates_error").hide();
							}
							 
						 }
						 
						 
						 $.ajax({
								url:"<?php echo base_url(); ?>memorial/update_memo",
								type: "POST",
								data:{"memo_id": memo_id, "about": about,"desc": desc, "action": action, "birthdate": birthdate, "deathdate": deathdate},
								success:function(resp)
								{
									if(action == 'about'){
										$(".memo_about").text(resp);
									}else if(action == 'desc'){
										$(".memo_desc").text(resp);
									}if(action == 'dates'){
										$(".memo_dates").html("");
										$(".memo_dates").html(resp);
									}
									
									$("#MemoEditModal").modal("hide");
									
								}
							});
					 }
					 
					 </script>
					
					
					
					
					
                  <div class="tab-pane fade tab_notice" id="menu1">
                    <div class="notice-form">
                      <div class="form-group row m-0 mb-2">
						  <div class="col-md-6">
							<label for="text1">Notice Board For</label>
						  </div>
						  <div class="col-md-6">
                        	<input type="text" value="<?php if($notice->notice_for == ""){ echo $memo->fname.' '.$memo->lname; }else{ echo $notice->notice_for; } ?>" class="form-control notice_frm" id="notice_for" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="First Name, Last Name">
						  </div>
                      </div>
                      <div class="form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text2">Date of Birth</label>
						  </div>
						  <div class="col-md-6">
                        	<input type="text" value="<?php if($notice->dob == ""){ echo $memo->birthdate; }else{ echo $notice->dob; } ?>" class="form-control notice_frm" id="dob" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Date, Month, Year, Place">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text3">Date of Death</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php if($notice->dod == ""){ echo $memo->deathdate; }else{ echo $notice->dod; } ?>"class="form-control notice_frm" id="dod" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Date, Month, Year, Place">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text4">Church/Mosque/Temple</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->house_of_worship; ?>" class="form-control notice_frm" id="house_of_worship" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Location, Town, Date, Time">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text5">Country</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->country; ?>" class="form-control notice_frm" id="country" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Country, Public Cemetery, Date, Time">
						  </div>
                      </div>
					
						<div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text5">Town</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->town; ?>" class="form-control notice_frm" id="town" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Town/Centre">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text6">Cremation Details</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->cremation_detail; ?>" class="form-control notice_frm" id="cremation_detail" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Location, Crematorium, Time">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
                        <div class="col-md-6">
						  <label for="text7">Funeral Committee Contact Person</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->contact_person; ?>" class="form-control notice_frm" id="contact_person" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="1st Name, 2nd Name, Phones">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text8">Funeral Committee Meetings Venue</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->meeting_venue; ?>" class="form-control notice_frm" id="meeting_venue" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Town , Location, Buidling, Meeting Dates">
						  </div>
                      </div>
                      
					  
					  <style type="text/css">
							#map {
								width: 100%;
								height: 400px;
							}
							.mapControls {
								margin-top: 10px;
								border: 1px solid transparent;
								border-radius: 2px 0 0 2px;
								box-sizing: border-box;
								-moz-box-sizing: border-box;
								height: 32px;
								outline: none;
								box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
							}
							#google_map_loc {
								background-color: #fff;
								font-family: Roboto;
								font-size: 15px;
								font-weight: 300;
								margin-left: 12px;
								padding: 0 11px 0 13px;
								text-overflow: ellipsis;
								width: 50%;
							}
							#google_map_loc:focus {
								border-color: #4d90fe;
							}
						</style>
						
					  
					  
					  
					  
					  <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <!--<label for="text9">Burial/Funeral/Interment Location Google Map</label>-->
							  </div>
						  <div class="col-md-6">
                        <input type="text" value="<?php echo $notice->google_map_loc; ?>" class="form-control notice_frm" id="google_map_loc" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Burial/Funeral/Interment Location Google Map">
						  </div>
                      </div>
					  
					  <div id="map" class="map_class">
                        
                      </div>
					  
					  <script>
						function initMap() {
							var map = new google.maps.Map(document.getElementById('map'), {
							  center: {lat: 22.3038945, lng: 70.80215989999999},
							  zoom: 13
							});
							var input = document.getElementById('google_map_loc');
							map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
						   
							var autocomplete = new google.maps.places.Autocomplete(input);
							autocomplete.bindTo('bounds', map);
						  
							var infowindow = new google.maps.InfoWindow();
							var marker = new google.maps.Marker({
								map: map,
								anchorPoint: new google.maps.Point(0, -29)
							});
						  
							autocomplete.addListener('place_changed', function() {
								infowindow.close();
								marker.setVisible(false);
								var place = autocomplete.getPlace();
							
								/* If the place has a geometry, then present it on a map. */
								if (place.geometry.viewport) {
									map.fitBounds(place.geometry.viewport);
								} else {
									map.setCenter(place.geometry.location);
									map.setZoom(17);
								}
								marker.setIcon(({
									url: place.icon,
									size: new google.maps.Size(71, 71),
									origin: new google.maps.Point(0, 0),
									anchor: new google.maps.Point(17, 34),
									scaledSize: new google.maps.Size(35, 35)
								}));
								marker.setPosition(place.geometry.location);
								marker.setVisible(true);
							  
								var address = '';
								if (place.address_components) {
									address = [
									  (place.address_components[0] && place.address_components[0].short_name || ''),
									  (place.address_components[1] && place.address_components[1].short_name || ''),
									  (place.address_components[2] && place.address_components[2].short_name || '')
									].join(' ');
								}
							  
								infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
								infowindow.open(map, marker);
								
								/* Location details */
								document.getElementById('google_map_loc').value = place.formatted_address;
								document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
								document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
							});
						}
						</script>
						<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWpO4jMKxJzeU0hOBmPgGwbQ4jRtxTEjA&libraries=places&callback=initMap" async defer></script>-->
						<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&libraries=places&callback=initMap"></script>
											  
						
						
						
                     
                      <ul class="list-inline notice_list">
                        <li class="list-inline-item">Notice: Last Updated</li>
                        <li class="list-inline-item">Posted:  <?php echo $notice->timeago; ?> ago</li>
                        <li class="list-inline-item">Posted by:  <?php echo $notice->updated_by; ?></li>
                      </ul>
                    </div>
					
                    <!--  -->
                  </div>
                  <div class="tab-pane gal_img fade" id="menu2">
                    <!-- <div class="gal_main">
                      <div class="row">
                        <div class="col-md-4">
                          <h4>Photos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Videos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Videos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Audio</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Audio</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <!-- <div class="space30"></div> -->
                    <div class="gal_sub">
                      <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="p_gallery nav-link active" data-toggle="tab" href="#photoTab">Photos</a>
                        </li>
                        <li class="nav-item">
                          <a class="v_gallery nav-link" data-toggle="tab" href="#videoTab">VIDEOS</a>
                        </li>
                        <li class="nav-item">
                          <a class="a_gallery nav-link" data-toggle="tab" href="#audioTab">AUDIO</a>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="photoTab">
                          <div class="gal_box">
                            <div class="row">
							
							
							<?php foreach($pictures as $p){ ?>
							
								<div class="col-md-3" id="p_<?php echo $p->ID; ?>">
									<?php if($is_page_admin){ ?>
										<button class="remove_btn" style="position: absolute;cursor: pointer;" onclick="delete_pic('<?php echo $p->ID; ?>', '<?php echo $p->pic; ?>')">X</button>
									<?php } ?>
									<img class="img-fluid" alt="img" src="<?php echo base_url(); ?>uploads/<?php echo $p->pic; ?>">
								  </div>
							
							<?php } ?>
							
                              
                            </div>
                          </div>
						  
						  
						  <script>
						  
						  function delete_pic(id, pic){
							  
							  if(confirm("Are you sure?")){
								  $.ajax({
											url:"<?php echo base_url(); ?>memorial/delete_pic",
											type: "POST",
											data:{"id": id, "pic": pic},
											success:function(resp)
											{
												if(resp == 'success'){
													$('#p_'+id).remove();
												}
												
											}
									}); 
							  }
						  }
						  
						  </script>
						  
						  
						  <form id="add_pic_form1" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_pic_gallary">
					
							  <div class="prehead_left">
								 
								  <div class="row"> 
								  
									 <?php if($is_page_admin){ ?>
									 
										 <div class="col-md-7">
										 
										 <?php if($this->session->flashdata('error_pic')){ ?>
													<div class="alert alert-danger display-hide">
														<button class="close" data-close="alert"></button>
														<?php echo $this->session->flashdata('error_pic'); ?> 
													</div>
											<?php } ?>
										 
										 </div>
											
										  <div class="col-md-3">
										  
											<input type="file" id="upload_picture1" name="pic" required>
											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
											<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">
										  
										  </div>
											  
										  <div class="col-md-2">
											  <div class="add-photo_new">
												<a href="javascript:;" onclick="add_pic1()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>
												
											  </div>
										  </div>
										  
										  <?php } ?>
										  
								 </div>
							</div>
					
							<script>
							
							function add_pic1(){
								var pic = $("#upload_picture1").val();
								if(pic == ''){
									$("#err_text").text("Please select image to upload");
									$("#ErrorModal").modal("show");
								}else{
									
									var memo_id = '<?php echo $memo->ID; ?>';
									var name_limit = 'photo_limit';
									
									$.ajax({
										url:"<?php echo base_url(); ?>memorial/is_pakage_expired",
										type: "POST",
										data:{"memo_id": memo_id, "name_limit": name_limit},
										success:function(resp)
										{
											if(resp == 'yes'){
												$("#err_text").text("Limit Exceeded"); 
												$("#ErrorModal").modal("show"); 
												return false;
											}else{
												$("#add_pic_form1").submit();
											}
										}
									}); 
									
								}
								
								
							}
							
							</script>
							
						</form>
						  
						  
						  
                        </div>
                        <div class="tab-pane fade" id="videoTab">
                            <div class="gal_box">
                            <div class="row">
							
							<?php foreach($videos as $v){ ?>
							
								<div class="col-md-3" id="v_<?php echo $v->ID; ?>">
									
									<?php if($is_page_admin){ ?>
										<button class="remove_btn" style="position: absolute;cursor: pointer; margin-top: -18px;" onclick="delete_video('<?php echo $v->ID; ?>', '<?php echo $v->video; ?>')">X</button>
									<?php } ?>
									<video width="160" height="120" controls>
									  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $v->video; ?>" type="video/mp4">
									  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $v->video; ?>" type="video/mp4">
									  Your browser does not support the video tag.
									</video>
									
								  </div>
							
							<?php } ?>
							
                              
							  <!--<div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>-->
                            </div>
                          </div>
						  
						  <script>
						  
						  function delete_video(id, vid){
							  
							  if(confirm("Are you sure?")){
								  $.ajax({
											url:"<?php echo base_url(); ?>memorial/delete_vid",
											type: "POST",
											data:{"id": id, "vid": vid},
											success:function(resp)
											{
												if(resp == 'success'){
													$('#v_'+id).remove();
												}
												
											}
									}); 
							  }
						  }
						  
						  </script>
						  
						  
						  
						  <form id="add_video_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_video">
					
							  <div class="prehead_left">
								 
								 <?php if($is_page_admin){ ?>
								 
								  <div class="row"> 

										 <div class="col-md-7">
										 
											<?php if($this->session->flashdata('error_video')){ ?>
													<div class="alert alert-danger display-hide">
														<button class="close" data-close="alert"></button>
														<?php echo $this->session->flashdata('error_video'); ?>
													</div>
											<?php } ?>
										 
										 
										 </div>
											
										  <div class="col-md-3">
																			  
											<input type="file" id="upload_video" name="video" required>
											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
											<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">
										  </div>
											  
										  <div class="col-md-2">
											  <div class="add-photo_new">
												<a href="javascript:;" onclick="add_video()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>
												
											  </div>
										  </div>
								 </div>
								 
								 <?php } ?>
								 
							</div>
					
							<script>
							
							function add_video(){
								var pic = $("#upload_video").val();
								if(pic == ''){
									$("#err_text").text("Please select video to upload");
									$("#ErrorModal").modal("show");
								}else{
									
									var memo_id = '<?php echo $memo->ID; ?>';
									var name_limit = 'video_limit';
									
									$.ajax({
										url:"<?php echo base_url(); ?>memorial/is_pakage_expired",
										type: "POST",
										data:{"memo_id": memo_id, "name_limit": name_limit},
										success:function(resp)
										{
											if(resp == 'yes'){
												$("#err_text").text("Limit Exceeded"); 
												$("#ErrorModal").modal("show"); 
												return false;
											}else{
												$("#add_video_form").submit();
											}
										}
									}); 
									
									
								}
								
								
							}
							
							</script>
							
						</form>
						  
						  
						  
                        </div>
                        <div class="tab-pane fade" id="audioTab">
                          <div class="gal_box">
                            <div class="row">
                              
							  <?php $a_counter=0; foreach($audios as $a){ ?>
									
									<div id="a_<?php echo $a->ID; ?>">
									<?php if($is_page_admin){ ?>
										<button class="remove_btn" style="position: absolute;cursor: pointer; margin-top: -30px;" onclick="delete_audio('<?php echo $a->ID; ?>', '<?php echo $a->audio; ?>')">X</button>
									<?php } ?>
									<audio id="player" controls="controls" <?php if($a_counter == 0) echo 'autoplay'; ?>>
										<source id="mp3_src" src="<?php echo base_url(); ?>uploads/music/<?php echo $a->audio; ?>" type="audio/mp3"/>
									</audio>
									
									</div>
							  
							  <?php $a_counter++; } ?>
							  
							  <!--<div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>-->
                            </div>
                          </div>
						  
						  <script>
						  
						  function delete_audio(id, aud){
							  
							  if(confirm("Are you sure?")){
								  $.ajax({
											url:"<?php echo base_url(); ?>memorial/delete_audio",
											type: "POST",
											data:{"id": id, "aud": aud},
											success:function(resp)
											{
												if(resp == 'success'){
													$('#a_'+id).remove();
												}
												
											}
									}); 
							  }
						  }
						  
						  </script>
						  
						  <form id="add_music_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_music">
					
							  <div class="prehead_left">
								 
								  <div class="row"> 
									<?php if($is_page_admin){ ?>
										 <div class="col-md-7">
										 
											<?php if($this->session->flashdata('error_music')){ ?>
													<div class="alert alert-danger display-hide">
														<button class="close" data-close="alert"></button>
														<?php echo $this->session->flashdata('error_music'); ?>
													</div>
											<?php } ?>
										 
										 
										 </div>
											
										  <div class="col-md-3">
										  									  
											<input type="file" id="upload_music" name="audio" required>
											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
											<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">
										  </div>
											  
										  <div class="col-md-2">
											  <div class="add-photo_new">
												<a href="javascript:;" onclick="add_music()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>
												
											  </div>
										  </div>
										 
									<?php } ?>
										  
								 </div>
							</div>
					
							<script>
							
							function add_music(){
								var pic = $("#upload_music").val();
								if(pic == ''){
									$("#err_text").text("Please select music to upload");
									$("#ErrorModal").modal("show");
								}else{
									
									var memo_id = '<?php echo $memo->ID; ?>';
									var name_limit = 'audio_limit';
									
									$.ajax({
										url:"<?php echo base_url(); ?>memorial/is_pakage_expired",
										type: "POST",
										data:{"memo_id": memo_id, "name_limit": name_limit},
										success:function(resp)
										{
											if(resp == 'yes'){
												$("#err_text").text("Limit Exceeded"); 
												$("#ErrorModal").modal("show"); 
												return false;
											}else{
												$("#add_music_form").submit();
											}
										}
									}); 
				
								}
								
								
							}
							
							</script>
							
						</form>
						  
						  
						  
						  
						  
						  
                        </div>
                      </div>
                      <div class="space20"></div>
                      <div class="row">
                        <div class="col-md-4"></div>
                        
                        <div class="col-md-8">
                          <div class="">
                            <!--<span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name="">-->
							
							
												
							
							
							
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  
				  
				  <div class="tab-pane fade" id="menu3">
                    <div class="main_tri" id="tribute_div">
                      
					  <?php foreach($tributes as $t){ ?>
					  
					  <?php

					  $t_img = 'feather1.png';
					  if($t->action == "candle"){
						  $t_img = 'christmas-candle.png';
					  }elseif($t->action == 'tribute'){
						  $t_img = 'flower.png';
					  }

					  ?>
					  
					  
						  <div class="featured_box">
							  <div class="media">
							  
							  <img class="" src="<?php echo base_url(); ?>assets/img/<?php echo $t_img; ?>" alt="mage">
							  
								<div class="media-body">
								  <h5><?php echo $t->by; ?> <i><?php echo $t->d_action; ?></i></h5>
								  <p><?php echo $t->comment; ?></p>								  
								  </br>
								  <h6><?php echo date('l', strtotime($t->createdAt)).', '.date("F", strtotime($t->createdAt)).' '.date("d", strtotime($t->createdAt)); ?> <?php echo date("Y",strtotime($t->createdAt)); ?> <?php echo date("h:i:a",strtotime($t->createdAt)); ?> </h6>
								  <?php //if($t->action == "candle"){ ?>
									<!--<img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">-->
								  <?php //} ?>
								</div>
							  </div>
						  </div>
						  
					  <?php } ?>
					  
					  
					  <!--<div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>left a Tribute</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</span>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</p>
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share">View</span>
                            </div>
                          </div>
                      </div> -->
                      
                    </div>
					
					
					
					
					  <ul class="list-inline tribute_list">
							<?php if($this->session->userdata('user')){ ?>
								<li class="list-inline-item"><a onclick="leave_tribute('ttab1')" href="javascript:;" class="red_btn">Light a Candle</a></li>
								<li class="list-inline-item"><a onclick="leave_tribute('ttab2')" href="javascript:;" class="red_btn lea_tri">Leave a Tribute</a></li>
								<li class="list-inline-item"><a onclick="leave_tribute('ttab3')" href="javascript:;" class="red_btn">Leave a Note</a></li>
						    <?php }else{ ?>
							
								<li class="list-inline-item"><a href="<?php echo base_url(); ?>login" class="red_btn">Light a Candle</a></li>
								<li class="list-inline-item"><a href="<?php echo base_url(); ?>login" class="red_btn lea_tri">Leave a Tribute</a></li>
								<li class="list-inline-item"><a href="<?php echo base_url(); ?>login" class="red_btn">Leave a Note</a></li>
								
							<?php } ?>
                      </ul>
                    <div class="leave_tribute">
                        <span class="tri_span"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/pencil.png">Leave a Tribute</span>
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" id="ttab1" onclick="update_tribute(this.id)" href="#tab1"><img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
                            <img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="ttab2" onclick="update_tribute(this.id)" href="#tab2"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="ttab3" onclick="update_tribute(this.id)" href="#tab3"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather1.png">
                            </a>
                          </li>
                        </ul>
                        <div class="tab-content">
							<input type="hidden" id="leave_tab" />
                            <div id="tab1" class="tab-pane active"><br>
                              <textarea class="form-control" rows="8" id="comment1" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab2" class="tab-pane fade"><br>
                              <textarea class="form-control" rows="8" id="comment2" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab3" class="tab-pane fade"><br>
                              <textarea class="form-control" rows="8" id="comment3" placeholder="Add your tribute text here"></textarea>
                            </div>
                        </div>
                        <ul class="list-inline leave_ul">
                          <li class="list-inline-item">by <?php echo $this->session->userdata('user')->fname.' '.$this->session->userdata('user')->lname; ?></li>
                          <li class="list-inline-item pull-right">
                            <a href="javscript:;" onclick="add_tribute()" class="red_btn">Leave a Tribute</a>
                          </li>
                        </ul>
                    </div>
					
				
					
                  </div>
				  
				  <script>
				  
				  function leave_tribute(tab){
			
					  if(tab == 'ttab1'){
						  $("#leave_tab").val('candle');
					  }else if(tab == 'ttab2'){
						  $("#leave_tab").val('tribute');
					  }else{
						  $("#leave_tab").val('note');
					  }
					  
					  $(".leave_tribute").show();
					  $('#'+tab).click();					  
				  }
				  
				  function update_tribute(tab){
					  if(tab == 'ttab1'){
						  $("#leave_tab").val('candle');
					  }else if(tab == 'ttab2'){
						  $("#leave_tab").val('tribute');
					  }else{
						  $("#leave_tab").val('note');
					  }
				  }
				  
				  function add_tribute(){
					  
					  var memo_id = '<?php echo $memo->ID; ?>';
					  var memo_created_id = '<?php echo $memo->createdby; ?>';
					  var login_user = '<?php if($this->session->userdata('user')) echo $this->session->userdata('user')->ID;  ?>';
					  var action = $("#leave_tab").val();
					  var comment = '';
					  if(action == 'candle'){
						  comment = $("#comment1").val();
					  }else if(action == 'tribute'){
						  comment = $("#comment2").val();
					  }else if(action == 'note'){
						  comment = $("#comment3").val();
					  }else{
						  
					  }
					  
					  $.ajax({
						url:"<?php echo base_url(); ?>memorial/add_tribute",
						type: "POST",
						data:{"memo_id": memo_id, "action": action, "comment": comment, "memo_created_id": memo_created_id},
						success:function(resp)
						{
							if(resp == 'block'){
								$("#err_text").text("You are block from this memorial!");
								$("#ErrorModal").modal("show");
								return false;
							}else{
								$(".leave_tribute").hide();
								$("#tribute_div").html("");
								$("#tribute_div").html(resp);
							}
							
							if(login_user != memo_created_id){
								$("#suc_text").text("Tribute will be posted after the approval from page owner!");
								$("#SuccessModal").modal("show");
							}
							
							
						}
					}); 
					  
				  }
				  
				  
				  </script>
				  
				  
                </div>
                
            </div>
        </div>
    </div>
</section>

<!---- Modal -->
  <div class="modal fade" id="SignupModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Signup For Notification</h4>
        </div>
        <div class="modal-body">
		
		<div class="alert alert-success" id="notification_email_msg1" style="display: none">
		  <strong></strong> <span id="noti_msg"> You have successfully signup for notification. </span>
		</div>
		
		<Label> Email Address</label>
		  <input type="text" id="notification_email" class="form-control" />
		  <p id="email_error1" style="display: none; color: red;">This field is required</p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" onclick="sign_up_notification()" style="background: #582236; border-color: #582236;">Invite</button>
            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Success Modal -->
 
 
 <script>
				
		function open_sign_up_model(){	
				$("#SignupModal").modal("show");
		}
		
		function sign_up_notification(){
			 
			 var memo_id = '<?php echo $memo->ID; ?>';
			 var email = $("#notification_email").val();
			 
			 if(email == ""){
				 $("#email_error1").show();
			 }else{
				 $("#email_error1").hide();
			 }
			 
			 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			 if(!regex.test(email)) {
				$("#email_error1").text("Invalid email address!");
				$("#email_error1").show();
				return false;
			 }else{
				 $("#email_error1").hide();
			 }
			 
			 
			 $.ajax({
				url:"<?php echo base_url(); ?>login/sign_up_notification",
				type: "POST",
				data:{"memo_id": memo_id, "email": email},
				success:function(resp)
				{
					
					$("#noti_msg").text(resp);
					$("#notification_email_msg1").show();
					
				}
			}); 
			 
		 }
				
</script>
 

<!---- Modal -->
  <div class="modal fade" id="InviteModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Family OR Friend Email</h4>
        </div>
        <div class="modal-body">
		
		<div class="alert alert-success" id="sent_email_msg" style="display: none">
		  <strong>Success!</strong> Invitation Email sent successfully.
		</div>
		
		<div class="alert alert-danger" id="sent_email_err_msg" style="display: none">
		  <strong></strong> Invitation Already Sent.
		</div>
		
		<Label> Email Address</label>
		  <input type="text" id="invited_email" class="form-control" />
		  <p id="email_error" style="display: none; color: red;">This field is required</p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" onclick="send_invitation()" style="background: #582236; border-color: #582236;">Invite</button>
            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Success Modal -->
 
 
  <script>
 
 function invite_modal(){
	 
		var memo_id = '<?php echo $memo->ID; ?>';
		var name_limit = 'page_managers';
		
		$.ajax({
			url:"<?php echo base_url(); ?>memorial/is_pakage_expired",
			type: "POST",
			data:{"memo_id": memo_id, "name_limit": name_limit},
			success:function(resp)
			{
				if(resp == 'yes'){
					$("#err_text").text("Limit Exceeded"); 
					$("#ErrorModal").modal("show"); 
					
				}else{
					$("#invited_email").val("");
					$("#sent_email_msg").hide();
					$("#sent_email_err_msg").hide();
					$("#InviteModal").modal("show");
				}
			}
		}); 
 }
 
 function send_invitation(){
	 
	 var memo_id = '<?php echo $memo->ID; ?>';
	 var createdBy = '<?php echo $memo->createdby; ?>';
	 var webaddress = '<?php echo $memo->webaddress; ?>';
	 var memory_of = '<?php echo strtoupper($memo->fname).' '.strtoupper($memo->lname); ?>';  
	 var email = $("#invited_email").val();
	 
	 if(email == ""){
		 $("#email_error").show();
	 }else{
		 $("#email_error").hide();
	 }
	 
	 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	 if(!regex.test(email)) {
		$("#email_error").text("Invalid email address!");
		$("#email_error").show();
		return false;
	 }else{
		 $("#email_error").hide();
	 }
	 
	 
	 $.ajax({
		url:"<?php echo base_url(); ?>login/send_invitation",
		type: "POST",
		data:{"memo_id": memo_id, "email": email, "webaddress": webaddress, "memory_of": memory_of, "createdBy": createdBy},
		success:function(resp)
		{
			if(resp == 'success'){
				$("#sent_email_msg").show();
				$("#sent_email_err_msg").hide();
			}else{
				$("#sent_email_err_msg").show();
				$("#sent_email_msg").hide();
			}
		}
	}); 
	 
 }
 
 
 </script>
 
 

<?php $this->load->view("include/footer"); ?>


<script>

					
$(".notice_frm").focusout(function(){
	
		var notice_for = $("#notice_for").val();
		var dob = $("#dob").val();
		var dod = $("#dod").val();
		var house_of_worship = $("#house_of_worship").val();
		var country = $("#country").val();
		var town = $("#town").val();
		var cremation_detail = $("#cremation_detail").val();
		var contact_person = $("#contact_person").val();
		var meeting_venue = $("#meeting_venue").val();
		var google_map_loc = $("#google_map_loc").val();
		var memo_id = <?php echo $memo->ID; ?>;
		
		$.ajax({
			url:"<?php echo base_url(); ?>memorial/update_notices",
			type: "POST",
			data:{"memo_id": memo_id, "notice_for": notice_for, "dob": dob, "dod": dod, "house_of_worship": house_of_worship, 
			"country": country, "town": town, "cremation_detail": cremation_detail, "contact_person": contact_person,
			"meeting_venue": meeting_venue, "google_map_loc": google_map_loc},
			success:function(resp)
			{
				
				//$("#noti_msg").text(resp);
				//$("#notification_email_msg1").show();
				
			}
		});
	
});
					


$(document).ready(function(){
  
	var memo_id = '<?php echo $memo->ID; ?>';
	var views = '<?php echo $memo->views + 1; ?>';
	
	$.ajax({
		url:"<?php echo base_url(); ?>login/update_views",
		type: "POST",
		data:{"memo_id": memo_id, "views": views},
		success:function(resp)
		{
			///
		}
	});
  
});


function getUrlParameter(sParam) {

	var sPageURL = decodeURIComponent(window.location.search.substring(1)),

		sURLVariables = sPageURL.split('&'),

		sParameterName,

		i;



	for (i = 0; i < sURLVariables.length; i++) {

		sParameterName = sURLVariables[i].split('=');



		if (sParameterName[0] === sParam) {

			return sParameterName[1] === undefined ? true : sParameterName[1];

		}

	}

};



$(function(){

		var tab_index = getUrlParameter('tab');


		if(tab_index == 'a_gallery'){

			$(".gallary_tab").click();
			$(".a_gallery").click();

		}else if(tab_index == 'v_gallery'){

			$(".gallary_tab").click();
			$(".v_gallery").click();

		}else if(tab_index == 'p_gallery'){

			$(".gallary_tab").click();
			$(".p_gallery").click();

		}else{

			//$(".gallary_tab").click();
			//$(".p_gallery").click();

		}	

	});


</script>





