<?php $this->load->view("include/header.php"); ?>


<section class="package_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="media">
                    <img src="<?php echo base_url(); ?>assets/img/checkmark.png" alt="img" class="">
                    <div class="media-body">
                      <h1>Payment  Sucessful</h1>
                      <p>You have successfully made your payment. Confirmation email will be sent to you shortly.</p>      
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>

<section class="redirect_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>Redirecting you to the home page in 3 seconds...</p>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view("include/footer");?>

<script>

$(document).ready(function(){
	
	var redirect = '<?php echo $_GET['redirect']; ?>';
	var memo_id = '<?php echo $_GET['memo_id']; ?>';
	
	var location = '<?php echo base_url(); ?>memorial/' + redirect;
	if(memo_id > 0){
		location = '<?php echo base_url(); ?>memorial/' + redirect +'/'+memo_id;
	}
	
	setTimeout(function(){
				
		//window.location = '<?php echo base_url(); ?>memorial/' + redirect;
		window.location = location;
		
	}, 3000);
  
});

</script>