<?php $this->load->view("include/header.php"); ?>
				
<section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="log_box">
                    <h5>Please Reset your Password.</h5>
                    <div class="superadminform">
                        <form action="<?php echo base_url(); ?>login/change_password" class="space40 comment-form" name="login_form" id="login_form" method="post">
							
						   <?php if($this->session->flashdata('error')){ ?>
							 <div class="alert alert-block  alert-danger">
								  <button data-dismiss="alert" class="close close-sm" type="button">
									  <i class="fa fa-times"></i>
								  </button>
								  <strong></strong> <?php echo $this->session->flashdata('error'); ?>
							  </div>
							 
						   <?php } ?>
						   
						   <?php if($this->session->flashdata('success')){ ?>
							 <div class="alert alert-block  alert-success">
								  <button data-dismiss="alert" class="close close-sm" type="button">
									  <i class="fa fa-times"></i>
								  </button>
								  <strong></strong> <?php echo $this->session->flashdata('success'); ?>
							  </div>
							 
						   <?php } ?>
						   
						   <input type="hidden" name="ResetPassword" value="<?php echo $ResetPassword; ?>" >
							 
                            <div class="form-group">
								<input type="password" class="form-control" name="new_password"  id="n-pwd" required placeholder="New  Password">
                            </div> 
							
                            <div class="form-group">
								<input type="password" class="form-control" name="c-password" id="rt-pwd" required placeholder="Retype New  Password">
                            </div>
                           
                            <input type="submit" class="logimein log_btn" id="" name="subbtnlast" value="Reset">
                            <p class="errormsg"></p>
                        </form>
                        <!-- <a href="" class="log_btn">LOG IN</a>-->
                        <!--<p class="social_text">- or log in with social media -</p>

                           <div class="apsl-login-networks theme-4 clearfix text-center">
                   
							  <div class="social-networks">
									<a href="#" title="Login with facebook">
										<div class="apsl-icon-block icon-facebook">
											<i class="fa fa-facebook"></i>
											<span class="apsl-long-login-text">Login with facebook</span>
										</div>
									</a>
									<a href="#" title="Login with twitter">
										<div class="apsl-icon-block icon-twitter">
											<i class="fa fa-twitter"></i>
											<span class="apsl-long-login-text">Login with twitter</span>
										</div>
									</a>
											
									<a href="#" title="Login with google">
										<div class="apsl-icon-block icon-google">
											<i class="fa fa-google"></i>
											<span class="apsl-long-login-text">Login with google</span>
										</div>
									</a>
							</div>
						</div>-->
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
			
			
			
<!-- javascript libraries -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
        $(function () {
          $("#datepicker").datepicker({ 
                autoclose: true, 
                todayHighlight: true
          }).datepicker('update', new Date());
        });
        $(function () {
          $("#datepicker1").datepicker({ 
                autoclose: true, 
                todayHighlight: true
          }).datepicker('update', new Date());
        });
    </script>



</body>
</html>
