<?php $this->load->view("include/header.php"); ?>

<section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Welcome, <?php echo $this->session->userdata('user')->fname.' '.$this->session->userdata('user')->lname; ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="welcome_main">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="acc_box mar_class">
                    <h3>Page Managers</h3>
                    
					<?php foreach($page_admin as $admin){ ?>
						
						<div class="media" id="admin_<?php echo $admin->user_id; ?>">
						  <img src="<?php echo base_url(); ?>assets/img/Ellipse-2.png" alt="img">
						  <div class="media-body row m-0">
							
							  <div class="col-md-8 mm-mb">
								   <h4><?php echo $admin->name; ?></h4>
									<p>Page admin</p>
							 </div>
							  <div class="col-md-4 removebtn-mb p-0 text-right">
							  <a href="javascript:;" onclick="remove_admin('<?php echo $admin->user_id; ?>', '<?php echo $memo->ID; ?>')" class="btn custom-outline">Remove</a>
							  </div>
						  </div>
						</div>
					
					<?php } ?>
					
					<script>
					
					function remove_admin(user_id, memo_id){
						if(confirm("Are you sure?")){
							$.ajax({
								url: "<?php echo base_url(); ?>memorial/remove_admin",
								type: "POST",
								data: {"user_id":user_id, "memo_id":memo_id},
								success: function (resp) {
										if(resp == 'success'){
											$('#admin_'+user_id).remove();
										}else{
											$("#err_text").text(resp);
											$("#ErrorModal").modal('show');
										}
								}
							});
						}
					}
					
					</script>
					
					
                    <div class="p_class_new">
                    <a href="javascript:;" onclick="invite_modal()" class="red_btn next_btn btn_start" data-toggle="modal" data-target="#exampleModal">Invite a Manager</a>
                    <div class="clearfix"></div>
                   </div>
					
                </div>
                
				<?php if($is_page_super_admin){ ?>
				
				<div class="acc_box text-center">
                    <h3>Billing Details</h3>
                    <div class="package-box">
						
						<div class="row">
						
							<div class="col-md-6 d-flex align-items-center justify-content-center border-right">
								<div>
								<p>Package</p>
								<h2><?php echo $memo->pakage; ?></h2>
								<h6 class="invisible">11 months left</h6>
								<?php if($memo->pakage == 'BASIC'){ ?>
									<a href="<?php echo base_url(); ?>memorial/upgrade/<?php echo $memo->ID; ?>" class="btn custom-outline mt-4">Upgrade</a>
								<?php } ?>
								</div>
							</div>
							
							<div class="col-md-6 d-flex align-items-center justify-content-center">
								<div>
								<p>Due Date</p>
								<h2><?php echo date('d', strtotime($memo->createdAt." +1 years")).' '.date('M', strtotime($memo->createdAt." +1 years")); ?></h2>
								
								<?php $remaining_month = floor(floor((strtotime($memo->createdAt." +1 years") - time())/60/60/24)/30); if($remaining_month <= 0) $remaining_month = 0; ?>
								
								<h6><?php echo $remaining_month; ?> months left</h6>
									
								<?php if($remaining_month <= 1 ){ ?>
									<a href="<?php echo base_url(); ?>memorial/proceed_to_renew/<?php echo $memo->ID.'/'.$memo->plan_id; ?>" class="btn custom-outline mt-4">Renew</a>
								<?php } ?>
								</div>
							</div>
						
						</div>
					
					</div>
                </div>
				
				<?php } ?>
				
                <div class="acc_box">
                    <h3>Memorial Privacy</h3>
                    <div class="radio_class radio_class-new">
                        <div class="radio radio-inline mp-mb">
                            <input type="radio" id="inlineRadio5" value="public" name="visibility" <?php if($memo->visibility == 'public') echo 'checked' ?>>
                            <label for="inlineRadio5"> All visitors can view and contribute. </label>
                        </div>
                        <p>Recommended for most websites. This option facilitates collaboration.</p>
                        <div class="radio radio-inline mp-mb">
                            <input type="radio" id="inlineRadio6" value="private" name="visibility" <?php if($memo->visibility == 'private') echo 'checked' ?>>
                            <label for="inlineRadio6"> Visible only to me </label>
                        </div>
                        <p>This option allows for maximum privacy</p>
                    </div>
					
					<div class="radio_class radio_class-new">
					
						Memorial URL
						
						<p><?php echo base_url().'In_Memory_Of/'.$memo->webaddress; ?></p>
					
					</div>
					
					
                </div>
				
				<div>
					<a href="javascript:;" onclick="update_memorial()" class="red_btn next_btn save_new">Save</a>
				</div>
				
				
            </div>
            <div class="col-md-8">
                <h4 class="manage_class">Manage Page</h4>
				
				<?php if($this->session->flashdata('error')){ ?>
				  <div class="alert alert-block  alert-danger">
					  <button data-dismiss="alert" class="close close-sm" type="button">
						  <i class="fa fa-times"></i>
					  </button>
					  <strong></strong> <?php echo $this->session->flashdata('error'); ?>
				  </div>
				 
				<?php } ?>

				<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-block  alert-success">
						<button data-dismiss="alert" class="close close-sm" type="button">
							<i class="fa fa-times"></i>
						</button>
						<strong></strong> <?php echo $this->session->flashdata('success'); ?>
					</div>

				<?php } ?>
				
                <div class="row">
                    <div class="col-md-9">
                        <div id="test2" class="template-layout template-layout2 <?php echo $memo->tclass; ?>">
												
<!--												<img class="img-fluid feature_img" alt="img" src="<?php echo base_url(); ?>assets/img/MOCKUPS-13-min.png">-->

												
<section class="tribute_head" style="background: url(<?php echo base_url().'assets/img/'.$memo->background; ?>) no-repeat;"> 
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-4 pr-0">
			<div id="preview_src" class="preview_image" style="background: url(<?php echo base_url().'uploads/'.$memo->cropped_image; ?>) center center no-repeat;">
			</div>
            </div>
            <div class="col-md-8 col-8 mt-4 mt-md-0">
                <p>In loving memory of...</p>
                <h1><?php echo $memo->fname.' '.$memo->lname; ?></h1>
                <ul class="nav nav-tabs d-none d-md-flex">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="javascript:;">ABOUT</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="javascript:;">NOTICES</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="javascript:;">GALLERY</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="javascript:;">TRIBUTES</a>
                  </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="tribute_main tm-mb">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 d-md-none d-block pr-0 pl-0">
              <div class="tribute_ul_new">
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="javascript:;">ABOUT</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="javascript:;">NOTICES</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="javascript:;">GALLERY</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="javascript:;">TRIBUTES</a>
                  </li>
                </ul>
              </div>
                <div class="tab-content">
                  <div class="tab-pane active" id="home1">
                      <div class="tribute_box">
                        <h3><img src="<?php echo base_url(); ?>assets/img/quote1.png" class="d-inline-block" alt=""/> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. <img src="<?php echo base_url(); ?>assets/img/quote2.png" class="d-inline-block" alt=""/></h3>
                        <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <div class="tribute_box">
                        <ul class="tri_list_box">
                          <li>56 Years Old</li>
                          <li>Born on March 3, 1962 in Nairoby, Kenya</li>
                          <li>Passed a way on June 22, 2018 in Nakuru, Kenya</li>
                        </ul>
                        <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <div class="tribute_box">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>

                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <a href="" class="add_story">Add a story</a>
                  </div>
                  <div class="tab-pane fade tab_notice" id="menu4">
                    <div class="notice_main">
                      <div class="form-group">
                        <label for="txt1">Funeral Notice Title</label>
                        <input type="text" class="form-control" id="txt1" placeholder="ie Details for Todd Msekema’s Funeral">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Location and Date of Funeral Service</label>
                        <input type="text" class="form-control" id="txt1" placeholder="25 May 2018, Sixtine Chapel">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Funeral Date</label>
                        <input type="text" class="form-control" id="txt1" placeholder="25 May 2018">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Address of Funeral Location</label>
                        <input type="text" class="form-control" id="txt1" placeholder="828 Union Street, Seattle, Washington">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Time of Funeral:</label>
                        <input type="text" class="form-control" id="txt1" placeholder="2:00 PM">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Funeral committee venue (Place Building, Road)</label>
                        <input type="text" class="form-control" id="txt1" placeholder="2:00 PM">
                      </div>
                      <div class="form-group">
                        <label for="txt1">Funeral Committee Contact Person & Tel. Number</label>
                        <input type="text" class="form-control" id="txt1" placeholder="John Malkovic, 822-1125-2813">
                      </div>
                      <div class="map_class">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50470.02846874147!2d-122.47261937941492!3d37.75776267828958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1539413862187" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                      <ul class="list-inline notice_list">
                        <li class="list-inline-item">Notice: dawdaoiajdawda</li>
                        <li class="list-inline-item">Posted:  10 hours ago</li>
                        <li class="list-inline-item">Posted by:  admin123</li>
                      </ul>
                    </div>
                    <div class="notice_sub">
                      <div class="info_new">
                        <h5>PERSONAL INFORMATION</h5>
                        <span>Notices Board For:</span>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="First Name">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Last Name">
                            </div>
                          </div>
                        </div>
                        <span>Date of Birth:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Month">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Year">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Place">
                            </div>
                          </div>
                        </div>
                        <span>Date of Death:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Month">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Year">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Place">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>FUNERAL SERVICE DETAILS</h5>
                        <span>Church/Mosque/Temple:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Location">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Time">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>BURIAL DETAILS</h5>
                        <span>Location:</span>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" id="sel1">
                                <option>County</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town/Centre">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Date">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Time">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Area/Location">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="If Public Cemetery">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>CREMATION DETAILS</h5>
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Crematorium">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Time">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>Funeral Committee Contact Person</h5>
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="1st Name">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="2nd Name">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Tel No">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>Funeral Committee Contact Person</h5>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Town">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Location">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Building">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Meeting Dates">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>Burial/Funeral/Interment Location Google Map</h5>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Add Location">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="info_new">
                        <h5>POST FUNERAL NOTICES</h5>
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Notice/Subject">
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="When Notice Was Posted">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control" id="txt1" placeholder="Posted By">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tribute_list1">
                        <a href="" class="add_story ylw_btn">Share</a>
                        <a href="" class="add_story">POST NOTICES</a>
                      </div>
                    </div>
                    <!-- <div class="tribute_list1">
                      <a href="" class="add_story ylw_btn">Share</a>
                      <a href="" class="add_story">POST NOTICES</a>
                    </div> -->
                  </div>
                  <div class="tab-pane gal_img fade" id="menu5">
                    <!-- <div class="gal_main">
                      <div class="row">
                        <div class="col-md-4">
                          <h4>Photos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">

                          <h4>Videos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Videos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Audio</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Audio</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <div class="gal_sub">
                      <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#photoTab1">Photos</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#videoTab1">VIDEOS</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#audioTab1">AUDIO</a>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="photoTab1">
                          <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="videoTab1">
                            <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="audioTab1">
                          <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3 col-4">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="space20"></div>
                      <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name="">
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="tab-pane fade" id="menu6">
                     <div class="main_tri">
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>left a Tribute</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</span>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</p>
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share">View</span>
                            </div>
                          </div>
                      </div>
                      <ul class="list-inline tribute_list">
                           <li class="list-inline-item"><a class="red_btn lea_tri">Leave a Tribute</a></li>
                           <li class="list-inline-item"><a href="" class="red_btn">Light a Candle</a></li>
                           <li class="list-inline-item"><a href="" class="red_btn">Leave a Note</a></li>
                      </ul>
                    </div>
                     <div class="leave_tribute">
                        <span class="tri_span"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/pencil.png">Leave a Tribute</span>
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab4"><img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
                            <img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab5"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab6"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather1.png">
                            </a>
                          </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab4" class="tab-pane active">
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab5" class="tab-pane fade">
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab6" class="tab-pane fade">
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                        </div>
                        <ul class="list-inline leave_ul">
                          <li class="list-inline-item">by John Nyongessa</li>
                          <li class="list-inline-item pull-right">
                            <a href="" class="red_btn">Leave a Tribute</a>
                          </li>
                        </ul>
                     </div>
                  </div>
                </div>

            </div>
            <div class="col-md-4 col-12 pr-0">
                <div class="space70"></div>
                <div class="space30 d-block d-md-none"></div>
                <div class="tribute_box-left tribute_box-new">
                    <div class="prehead_left">
                        <a href="javascript:;" class="">Invite Family and Friends to this memorial:</a>
                        <a href="javascript:;" class="red_btn tribute_box-btn"><span>+</span>Invite Now</a>
                    </div>
                </div>
                <div class="tribute_box-left">
                    <div class="prehead_left">
                        <a href="">Share with a friend</a>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="javascript:;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:;"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="tribute_box-left tribute_box-new1">
                    <div class="prehead_left">
                        <a href="javascript:;" class="tribute_box-text">Sign up for Notifications</a>
                        <a href="javascript:;" class="red_btn tribute_box-btn tribute_box-btn1">Sign Up</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div id="accordion">
                    
                    <div class="tribute_box-left tribute_box-new">
                      <div class="prehead_left">
                          <a href="javascript:;" class="">Add Picture</a>
                          <div class="add-photo_new">
                            <a href="javascript:;" class="red_btn tribute_box-btn"><span>+</span>Add Picture</a>
                            
                          </div>
                      </div>
                    </div>
                    
                    
                    <div class="card card1">
                        <div class="card-header">
                              <a class="collapsed card-link card-link1" data-toggle="collapse" href="#collapseThree1">
                                Important Notifiactions<span><i class="fa fa-exclamation" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <!--<div id="collapseThree1" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Lorem ipsum..
                            </div>
                        </div>-->
                    </div>

                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFour">
                                Page Activity<span><i class="fa fa-users" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseFour" class="collapse show" data-parent="#accordion">
                            <div class="card-body">
                                <ul class="list-unstyled list_page-activity">
                                  <li>Laid a flower<span>2 hours ago</span></li>
                                  <li>Left a note<span>2 hours ago</span></li>
                                  <li>lit a candle<span>3 hours ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFive">
                                Page Views   <span>85</span>
                              </a>
                        </div>
                        <div id="collapseFive" class="collapse" data-parent="#accordion">
                            <!--<div class="card-body">
                                Lorem ipsum..
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-12 d-md-block d-none">
               
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                      <div class="tribute_box">
                        <h3><img src="<?php echo base_url(); ?>assets/img/quote1.png" class="d-inline-block" alt=""/> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. <img src="<?php echo base_url(); ?>assets/img/quote2.png" class="d-inline-block" alt=""/></h3>
                        <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <div class="tribute_box">
                        <ul class="tri_list_box">
						
						<?php 
								
								$d1 = new DateTime($memo->birthdate);
								$d2 = new DateTime($memo->deathdate);
								$diff = $d2->diff($d1); 
						  ?>
						
						
                          <li><?php echo $diff->y; ?> Years Old</li>
                          <li>Born on <?php echo date('l', strtotime($memo->birthdate)).', '.date("F", strtotime($memo->birthdate)).' '.date("d", strtotime($memo->birthdate)); ?> <?php echo date("Y",strtotime($memo->birthdate)); ?> in <?php echo $memo->citytown; ?>, <?php echo $memo->countryorigin1; ?></li>
                          <li>Passed a way on <?php echo date('l', strtotime($memo->deathdate)).', '.date("F", strtotime($memo->deathdate)).' '.date("d", strtotime($memo->deathdate)); ?> <?php echo date("Y",strtotime($memo->deathdate)); ?> in <?php echo $memo->citytown; ?>, <?php echo $memo->countryorigin1; ?></li>
                        </ul>
                        <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <div class="tribute_box">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>

                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            <span class="edit_class"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
                      </div>
                      <a href="javascript:;" class="add_story">Leave a Tribute</a>
                    </div>
                  <div class="tab-pane fade tab_notice" id="menu1">
                    <div class="notice-form">
                      <div class="form-group row m-0 mb-2">
						  <div class="col-md-6">
							<label for="text1">Notice Board For</label>
						  </div>
						  <div class="col-md-6">
                        	<input type="text" class="form-control" id="text1" placeholder="First Name, Last Name">
						  </div>
                      </div>
                      <div class="form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text2">Date of Birth</label>
						  </div>
						  <div class="col-md-6">
                        	<input type="text" class="form-control" id="text2" placeholder="Date, Month, Year, Place">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text3">Date of Death</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text3" placeholder="Date, Month, Year, Place">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text4">Church/Mosque/Temple</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text4" placeholder="Location, Town, Date, Time">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text5">Country</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text5" placeholder="Country, Public Cemetery, Date, Time">
						  </div>
                      </div>
						<div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text5">County</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text5" placeholder="County">
						  </div>
                      </div>
						<div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text5">Town</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text5" placeholder="Town/Centre">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text6">Cremation Details</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text6" placeholder="Location, Crematorium, Time">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
                        <div class="col-md-6">
						  <label for="text7">Funeral Committee Contact Person</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text7" placeholder="1st Name, 2nd Name, Phones">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text8">Funeral Committee Meetings Venue</label>
						  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text8" placeholder="Town , Location, Buidling, Meeting Dates">
						  </div>
                      </div>
                      <div class="form-group form-group row m-0 mb-2">
						  <div class="col-md-6">
                        <label for="text9">Burial/Funeral/Interment Location Google Map</label>
							  </div>
						  <div class="col-md-6">
                        <input type="text" class="form-control" id="text9" placeholder="">
						  </div>
                      </div>
						
						
                      <div class="map_class">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50470.02846874147!2d-122.47261937941492!3d37.75776267828958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1539413862187" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                      <ul class="list-inline notice_list">
                        <li class="list-inline-item">Notice: dawdaoiajdawda</li>
                        <li class="list-inline-item">Posted:  10 hours ago</li>
                        <li class="list-inline-item">Posted by:  admin123</li>
                      </ul>
                    </div>
                    

                    <!--  -->
                  </div>
                  <div class="tab-pane gal_img fade" id="menu2">
                    <!-- <div class="gal_main">
                      <div class="row">
                        <div class="col-md-4">
                          <h4>Photos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Videos</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Videos</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <h4>Audio</h4>
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img22.png">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Audio</span>
                            <input id="selectfile" type="file" name=""/>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <!-- <div class="space30"></div> -->
                    <div class="gal_sub">
                      <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#photoTab">Photos</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#videoTab">VIDEOS</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#audioTab">AUDIO</a>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="photoTab">
                          <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>

                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="videoTab">
                            <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="audioTab">
                          <div class="gal_box">
                            <div class="row">
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                              <div class="col-md-3">
                                <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/img23.jpg">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="space20"></div>
                      <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                          <div class="add_gal">
                            <span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>
                            <input id="selectfile" type="file" name="">
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="tab-pane fade" id="menu3">
                    <div class="main_tri">
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>left a Tribute</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</span>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</p>
                            </div>
                          </div>
                      </div>
                      <div class="featured_box">
                          <div class="media">
                            <img class="" src="<?php echo base_url(); ?>assets/img/img12.png" alt="mage">
                            <div class="media-body">
                              <h5>Audrey <i>lit a Candle</i></h5>
                              <h6>July 1, 2018, 01.11 pm</h6>
                              <span class="span_share">View</span>
                            </div>
                          </div>
                      </div>
                      
                    </div>
					  <ul class="list-inline tribute_list">
                           <li class="list-inline-item"><a class="red_btn lea_tri">Leave a Tribute</a></li>
                           <li class="list-inline-item"><a href="" class="red_btn">Light a Candle</a></li>
                           <li class="list-inline-item"><a href="" class="red_btn">Leave a Note</a></li>
                      </ul>
                    <div class="leave_tribute">
                        <span class="tri_span"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/pencil.png">Leave a Tribute</span>
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab1"><img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
                            <img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab2"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab3"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather1.png">
                            </a>
                          </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane active"><br>
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab2" class="tab-pane fade"><br>
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab3" class="tab-pane fade"><br>
                              <textarea class="form-control" rows="8" id="comment" placeholder="Add your tribute text here"></textarea>
                            </div>
                        </div>
                        <ul class="list-inline leave_ul">
                          <li class="list-inline-item">by John Nyongessa</li>
                          <li class="list-inline-item pull-right">
                            <a href="" class="red_btn">Leave a Tribute</a>
                          </li>
                        </ul>
                    </div>
                  </div>
                </div>
                
            </div>
        </div>
    </div>
</section>

                                            </div>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            <li>
								<select id="memorial_music" name="memorial_music" class="form-control setting_btn">
									
											<option value="<?php echo $memo->memorial_music; ?>" selected><?php echo $memo->memorial_music; ?></option>
											<?php foreach($audios as $a){ ?>
												<option value="<?php echo $a->file; ?>"><?php echo $a->title; ?></option>
											<?php } ?>
											
											
										<!--<option>Music type 1</option>
										<option>Music type 2</option>
										<option>Music type 3</option>-->
								</select>
							</li>
                            <li>
								<select id="memorial_template" name="memorial_template" class="form-control setting_btn">
								
									<?php foreach($memorial_templates as $template){ ?>
									
											<option value="<?php echo $template->ID; ?>" <?php if($template->ID == $memo->memorial_template) echo 'selected'; ?>><?php echo $template->title; ?></option>
									
									<?php } ?>
								
									<!--<option>Change Template</option>
									<option>Template type 1</option>
									<option>Template type 2</option>
									<option>Template type 3</option>-->
								</select>
							</li>
                            <li><a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memo->webaddress; ?>" class="setting_btn">Edit Information</a></li>
                            <li><a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memo->webaddress; ?>?tab=p_gallery" class="setting_btn">View Gallery</a></li>
                            
							<?php if($is_page_super_admin){ ?>
							<li><a href="javascript:;" onclick="delete_memorial()" class="setting_btn">Delete Memorial</a></li>
							<?php } ?> 
                            <li><a href="javascript:;" onclick="update_memorial()" class="red_btn next_btn save_new">Save</a></li>
                        </ul>
                    </div>
                </div>
				
				
				<script>
				
				
				function delete_memorial(){
					if(confirm("Are you sure?")){
						var memo_id = '<?php echo $memo->ID; ?>';
						$.ajax({
							url:"<?php echo base_url(); ?>memorial/delete_memorial",
							type: "POST",
							data:{"memo_id": memo_id},
							success:function(resp)
							{
								if(resp == 'success'){
									location.href="<?php echo base_url(); ?>memorial/mymemorials";
								}else{
									$("#err_text").text("Somthing went wrong!");
									$("#ErrorModal").modal('show');
								}
							}
						});
					} 
					
					
				}
				
				
				function update_memorial(){
					
					
						
						var memo_id = '<?php echo $memo->ID; ?>';
						//var memorial_music = $("input[name='memorial_music']:checked").val();
						var memorial_music = $('#memorial_music').val();
						//var memorial_template = $("input[name='memorial_template']:checked").val();
						var memorial_template = $("#memorial_template").val();
						var visibility = $("input[name='visibility']:checked").val();
						
						$.ajax({
							url:"<?php echo base_url(); ?>memorial/update_memo1",
							type: "POST",
							data:{"memo_id": memo_id, "memorial_music": memorial_music, "memorial_template": memorial_template, "visibility": visibility},
							success:function(resp)
							{
								if(resp == 'success'){
									location.href="<?php echo base_url(); ?>memorial/manage_memorial/<?php echo $memo->ID; ?>";
								}else{
									$("#err_text").text("Somthing went wrong!");
									$("#ErrorModal").modal('show');
								}
							}
						});
						
				}
				
				</script>
				
				
                <h4 class="manage_class">Manage Posts</h4>
				
				<?php foreach($pending_tributes as $t){ ?>
				<div id="tribute_<?php echo $t->ID; ?>">
                <h5><?php echo $t->by; ?> posted a tribute.</h5>
                <p class="main_new"><?php echo $t->comment; ?></p>
                <ul class="list-inline list_new">
                    <!--
                    <li class="list-inline-item"><a href="javascript:;" onclick="approve_tribute('<?php echo $t->ID; ?>')" class="red_btn next_btn save_new save_new1">Approve</a></li>
                    -->
                    <li class="list-inline-item"><a href="javascript:;" onclick="delete_tribute('<?php echo $t->ID; ?>')" class="red_text">Delete</a></li>
                    <li class="list-inline-item"><a href="javascript:;" onclick="block_user('<?php echo $t->ID; ?>', '<?php echo $memo->ID; ?>', '<?php echo $t->createdby; ?>')" class="red_text">Block <?php echo $t->by; ?></a></li>
                    <li class="list-inline-item"><a href="" class="red_text">Profanity:  <b style="color: red;"><?php if($t->profanity_status == 1) echo 'Yes'; ?></b></a></li>
                    
					<li class="list-inline-item"><a href="" class="red_text">Flag:  <b style="color: red;"><?php echo $t->flaggedcontent; ?></b></a></li>
                    					
                </ul>
				
				</div>
				
				<?php } ?>
				
            </div>
        </div>
    </div>
</section>


<script>

function block_user(tribute_id, memo_id, user_id){
	if(confirm("Block user from memorial! Are you sure?")){
		
		$.ajax({
			url:"<?php echo base_url(); ?>memorial/block_user",
			type: "POST",
			data:{"tribute_id":tribute_id, "memo_id": memo_id, "user_id": user_id},
			success:function(resp)
			{
				if(resp == 'success'){
					$("#tribute_"+tribute_id).remove();  // remove comment
					$("#admin_"+user_id).remove();  // remove if page admin
					
					$("#suc_text").text("User successfully blocked from memorial");
					$("#SuccessModal").modal('show');
				}
			}
		}); 
		
	}	
}

function approve_tribute(id){
	$.ajax({
		url:"<?php echo base_url(); ?>memorial/approve_tribute",
		type: "POST",
		data:{"id": id},
		success:function(resp)
		{
			if(resp == 'success'){
				$("#tribute_"+id).remove();
				$("#suc_text").text("Post approved");
				$("#SuccessModal").modal('show');
			}
		}
	}); 
}

function delete_tribute(id){
	if(confirm("Are you sure?")){
		$.ajax({
			url:"<?php echo base_url(); ?>memorial/delete_tribute",
			type: "POST",
			data:{"id": id},
			success:function(resp)
			{
				if(resp == 'success'){
					$("#tribute_"+id).remove();
					$("#suc_text").text("Post deleted!");
					$("#SuccessModal").modal('show');
				}
			}
		}); 
	}
}


</script>

<!-- Modal -->
<!--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Family OR Friend Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
		  <form>
			  <div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
				<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			  </div>
			</form>
		  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary popup-btn">Invite</button>
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>-->


<!---- Modal -->
  <div class="modal fade" id="InviteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Family OR Friend Email</h4>
        </div>
        <div class="modal-body">
		
		<div class="alert alert-success" id="sent_email_msg" style="display: none">
		  <strong>Success!</strong> Invitation Email sent successfully.
		</div>
		
		<div class="alert alert-danger" id="sent_email_err_msg" style="display: none">
		  <strong></strong> Invitation Already Sent.
		</div>
		
		<Label> Email Address</label>
		  <input type="text" id="invited_email" class="form-control" />
		  <p id="email_error" style="display: none; color: red;">This field is required</p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" onclick="send_invitation()" style="background: #582236; border-color: #582236;">Invite</button>
            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Success Modal -->

<script>

function invite_modal(){
	
		var memo_id = '<?php echo $memo->ID; ?>';
		var name_limit = 'page_managers';
		
		$.ajax({
			url:"<?php echo base_url(); ?>memorial/is_pakage_expired",
			type: "POST",
			data:{"memo_id": memo_id, "name_limit": name_limit},
			success:function(resp)
			{
				if(resp == 'yes'){
					$("#err_text").text("Limit Exceeded"); 
					$("#ErrorModal").modal("show"); 
					
				}else{
					$("#invited_email").val("");
					$("#sent_email_msg").hide();
					$("#sent_email_err_msg").hide();
					$("#InviteModal").modal("show");
				}
			}
		}); 
 }
 
 function send_invitation(){
	 
	 var memo_id = '<?php echo $memo->ID; ?>';
	 var createdBy = '<?php echo $memo->createdby; ?>';
	 var webaddress = '<?php echo $memo->webaddress; ?>';
	 var memory_of = '<?php echo strtoupper($memo->fname).' '.strtoupper($memo->lname); ?>';  
	 var email = $("#invited_email").val();
	 
	 if(email == ""){
		 $("#email_error").show();
	 }else{
		 $("#email_error").hide();
	 }
	 
	 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	 if(!regex.test(email)) {
		$("#email_error").text("Invalid email address!");
		$("#email_error").show();
		return false;
	 }else{
		 $("#email_error").hide();
	 }
	 
	 
	 $.ajax({
		url:"<?php echo base_url(); ?>login/send_invitation",
		type: "POST",
		data:{"memo_id": memo_id, "email": email, "webaddress": webaddress, "memory_of": memory_of, "createdBy": createdBy},
		success:function(resp)
		{
			if(resp == 'success'){
				$("#sent_email_msg").show();
				$("#sent_email_err_msg").hide();
			}else{
				$("#sent_email_err_msg").show();
				$("#sent_email_msg").hide();
			}
		}
	}); 
	 
 }

</script>



<?php $this->load->view("include/footer"); ?>

