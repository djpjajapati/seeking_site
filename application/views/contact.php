<?php $this->load->view("include/header.php"); ?>
<section class="contact_head">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="contact_box">
                    <div class="media">
                    <div class="img_class">
                      <img src="<?php echo base_url("assets/img/img17.png");?>" alt="img">
                    </div>
                      <div class="media-body">
                        <h4>Address</h4>
                        <p style="font-size: 12px;">Duplex Apartments,<br>
                            Lower Hill Road, Upperhill<br>
                            Nairobi</p>					
							
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="contact_box">
                    <div class="media">
                    <div class="img_class">
                      <img src="<?php echo base_url("assets/img/img18.png");?>" alt="img">
                    </div>
                      <div class="media-body">
                        <h4>Opening Hours</h4>
                        <p style="font-size: 12px;">Mon to Fri: 9.30am to 4.30pm<br>
                            Saturday: 9.00am to 12.30pm<br>
                            Sunday: Closed</p>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="contact_box">
                    <div class="media">
                    <div class="img_class">
                      <img src="<?php echo base_url("assets/img/img19.png");?>" alt="img">
                    </div>
                      <div class="media-body">
                        <h4>Email Address</h4>
                        <a style="font-size: 12px;" href="">support@safirisalama.com</a>
                        <!--<a style="font-size: 12px;" href="">support@safirisalama.co.ke</a>
                        <a style="font-size: 14px;" href="">Support@safirisalama.com</a>-->
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="contact_box">
                    <div class="media">
                    <div class="img_class">
                      <img src="<?php echo base_url("assets/img/img20.png");?>" alt="img">
                    </div>
                      <div class="media-body">
                        <h4>Phone Number</h4>
                        <p style="font-size: 14px;">Call customer service on</p>
                            <a style="font-size: 14px;" href="">(+254) 748 80 80 80</a>
                            
                      </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<section class="contact_space">
    <div class="container">
        <div class="search_box">
            <div class="row">
                <div class="col-md-12">
                	<div id="success_div" class="alert alert-success" style="display:none;"></div>
                    <div id="error_div" class="alert alert-danger" style="display:none;"></div>
                    <form name="form_data" id="form_data" action="" method="post">
                      <div class="radio_class">
                            <div class="radio radio-inline radio-inline1">
                                <input type="radio" id="inlineRadio1" name="type" value="contact" onclick="get_radio_button(this.value)" checked="">
                                <label for="inlineRadio1"> DROP US A LINE </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio2" name="type"  onclick="get_radio_button(this.value)" value="testimonial">
                                <label for="inlineRadio2"> Leave a testimonial </label>
                            </div>
                        </div>

					<div id="testimonial_div_login">
                      <div class="row">
                          <div class="col-md-4 pad6">
                              <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                          </div>
                          <div class="col-md-4 pad6">
                              <input type="text" name="email" id="email" class="form-control" placeholder="Your Email">
                          </div>
                          <div class="col-md-4 pad6" id="phone_div" style="display:none;">
                              <input type="text" name="phone" id="phone" class="form-control" placeholder="Your Phone No" onkeypress="return isNumberKey(event)">
                          </div>
                           <div class="col-md-4 pad6" id="rating_div" style="display:none;">
                               <select class="form-control" id="rating" name="rating">
                                    <option value="">Please Select Rating</option>
                                    <?php
                                    for($i=1;$i<=5;$i++)
                                    {
                                    ?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php
                                    }
                                    ?>
                              </select>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-8 pad6">
                              <textarea id="message" name="message" class="form-control" rows="3" placeholder="Your Message"></textarea>
                          </div>
                          <div class="col-md-4 pad6">
                              <!--<div class="captha">-->
                              	   <script src='https://www.google.com/recaptcha/api.js'></script>	
                                   <!--<div class="g-recaptcha" data-sitekey="6LeE6Z8UAAAAAOw71ct4NPT4xNssUtq64R5WpKnR"></div>-->
                                   <div class="g-recaptcha" data-sitekey="6LdjBaQUAAAAALbefVFHHI1aTvw-54pNDATL6wcW"></div>
                              <!--</div>-->
                          </div>
                      </div>
                      <button id="form_btn" name="form_btn" class="btn_start" type="submit" onclick="form_common_data_validation();">SEND MESSAGE</button>
                   				   
					</div>
					
					
					
						<span id="go_to_login" style="display: none"> Please <a href="<?php echo base_url(); ?>login?redirect=contact"> <b> login </b> </a> to leave a testimonial </span>
					
					

				   </form>
                </div>
            </div>
        </div>
    </div>
</section>


 <input type="hidden" id="login_id" value="<?php echo $login_id; ?>" />

<div class="contact_map">
<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50470.02846874147!2d-122.47261937941495!3d37.757762678289566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1539175243461"  frameborder="0" style="border:0" allowfullscreen></iframe>-->
<!--<iframe src="https://www.google.com/maps/place/Lower+Duplex+Apartments,+Upper+Hill+Rd,+Nairobi,+Kenya/@-1.3006333,36.8175185,17z/data=!3m1!4b1!4m5!3m4!1s0x182f10e3870e3dfd:0xf38558b9d21e2820!8m2!3d-1.3006333!4d36.8197072?hl=en-US"  frameborder="0" style="border:0" allowfullscreen></iframe>-->

<iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Lower%20Duplex%20Apartments%2C%20Upper%20Hill%20Rd%2C%20Nairobi%2C%20Kenya&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style>

</div>

<?php $this->load->view("include/footer");?>
<script type="text/javascript">
 	function get_radio_button(radioValue)
	{
 		$("#phone_div").hide();
		$("#rating_div").hide();
		if(radioValue=='contact')
		{
			$("#form_btn").html('SEND MESSAGE');
			$("#phone_div").show();
			
			
			$("#go_to_login").hide();
			$("#testimonial_div_login").show();
		}
		else if(radioValue=='testimonial')
		{			
					$("#form_btn").html('LEAVE A TESTIMONIAL');
					$("#rating_div").show();
					
					var login_id = $("#login_id").val();
					if(login_id <= 0){
						$("#go_to_login").show();
						$("#testimonial_div_login").hide();
					}
					
		}	
	}
	
	var radioValue = $("input[name='type']:checked").val();
	get_radio_button(radioValue);
	function form_common_data_validation()
	{
 		$('#form_btn').hide();
		$('#error_div').hide();
		$('#success_div').hide();
		$('#form_data').submit(function(e) 
		{
			e.preventDefault();
			var form = $(this);
			var formdata = false;
			if(window.FormData)
			{
				formdata = new FormData(form[0]);
			}
		
			var formAction = form.attr('action');
			$.ajax({
				type : 'POST',
				url : "<?php echo base_url('contact/Check');?>",
				cache : false,
				data : formdata ? formdata : form.serialize(),
				contentType : false,
				processData : false,
		
				success: function(data) 
				{
 					myarray = new Array();
					myarray = data.split('-SEPARATOR-');
 					if(data.search('done') != -1)
					{
						$('#error_div').html('');
						$('#error_div').hide();
						$('#success_div').fadeIn("slow");
						$('#form_data').each(function(){
							this.reset();
						});
						$('#success_div').html('Message sent successfully!');
 						 window.setTimeout( function(){
							 window.location = '<?php echo base_url('contact');?>';
						 }, 1000);
  					} 
					else 
					{
						$('#success_div').hide();
						$("#form_btn").show();
						$('#error_div').fadeIn("slow");
						$('#error_div').html('');
						$('#error_div').html(data);
						$('#form_data').removeAttr('onsubmit').submit(e);
					}
				}
			});
		});
	}
	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode != 46 && charCode > 31 
		&& (charCode < 48 || charCode > 57))
		return false;
		return true;
	}  
 </script> 

