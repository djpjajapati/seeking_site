<?php $this->load->view("include/header.php"); ?>

	
<section class="package_sec1 package_sec1-new">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Search Results</h1>
            </div>
        </div>
    </div>
</section>
<!--
<section class="search_margin">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="search_box search_ylw-new">
                    <div class="search_ylw-new1">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" class="form-inline" action="<?php echo base_url(); ?>memorial/gallery" >
                                    <label for="sel1" class="lab_text">View tributes in memory of:</label>
                                    <select name="keywords" class="form-control form-control-new" id="sel1">
                                        <option value="All">All Memorials</option>
										<?php foreach($memorials_list as $memo){ ?>
											<option value="<?php echo $memo->ID; ?>"><?php echo $memo->fname.' '.$memo->lname;; ?></option>
										<?php } ?>
                                    </select>
                                    <input type="submit" style="cursor: pointer;" class="red_btn red_btn-new" value="SEARCH" />
                                    <a href="" class="advance_Search">Advanced Search ›</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
-->
	
	<section class="search_margin">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="search_box search_ylw-new">
                    <div class="search_ylw-new1">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" class="form-inline" action="<?php echo base_url(); ?>memorial/gallery">
                                    <label for="sel1" class="lab_text">View tributes in memory of:</label>
                                    <select name="keywords" class="form-control form-control-new" id="sel1">
                                        <option value="All">All Memorials</option>
										<?php foreach($memorials_list as $memo){ ?>
											<option value="<?php echo $memo->ID; ?>"><?php echo $memo->fname.' '.$memo->lname;; ?></option>
										<?php } ?>
                                    </select>
                                     <input type="submit" class="red_btn red_btn-new" value="SEARCH" />
                                    <a href="" class="advance_Search" data-toggle="collapse" data-target="#advancesearch" aria-expanded="false">Advanced Search ›</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="search_ylw-new2">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="advancesearch" class="collapse">
                                    <p>*Fill in any field to narrow your search criteria.</p>
                                    <div class="row">
                                      <div class="col-md-6">
                                          <input id="fname" type="text" class="form-control" placeholder="First Name">
                                          <input id="lname" type="text" class="form-control" placeholder="Second Name">
                                      </div>
                                      <div class="col-md-6">
										  <input type="number" id="yborn" min="1900" max="<?php echo date('Y') ?>" step="1" class="form-control" value="" placeholder="Year Born">
										  <input type="number" id="ydeath" min="2000" max="<?php echo date('Y') ?>" step="1" class="form-control" value="" placeholder="Year Passed Away">
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3 col-lg-2 pad_right0">
                                                    <span class="span_text-new1">Gender</span>
                                                </div>
                                                <div class="col-md-9 col-lg-10 pad_left0">
                                                    <div class="radio_class">
                                                        <div class="radio radio-inline radio-inline-new">
                                                            <input type="radio" id="inlineRadio1" value="Male" name="gender">
                                                            <label for="inlineRadio1"> Male </label>
                                                        </div>
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="inlineRadio2" value="Female" name="gender">
                                                            <label for="inlineRadio2"> Female </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="list-inline reset_list">
                                                <li class="list-inline-item"><a href="javascript:;" onclick="clear_search()">Reset</a></li>
                                                <li class="list-inline-item"><a href="javascript:;" onclick="search_gallery('0')" class="red_btn">Apply FIlters</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="search_main">
    <div class="container">
        <div class="row">
		
			<div class="col-md-12" id="search_memo_list">
                <h3 class="results_class">Your search has returned  <span id="search_result_cnt"> <?php echo count($memorials); ?> </span>  results</h3>
                <div class="search_block-new">
                    
					<?php foreach($memorials as $memo){ ?>
					
						<div class="search_box-new">
							<div class="row">
								<div class="col-md-2 col-3">
									<img class="img-fluid w-100" alt="<?php echo $memo->fname.' '.$memo->lname; ?>" src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>">
								</div>
								<div class="col-md-10 col-9 pad_mob_left0">
									<h4><?php echo $memo->fname.' '.$memo->lname; ?> (<?php echo date('Y', strtotime($memo->birthdate)).'-'.date('Y', strtotime($memo->deathdate)); ?>)<span class="pull-right d-none d-md-block">Memorial created by <?php echo $memo->creator; ?> on <?php echo date('m-d-Y', strtotime($memo->createdAt)); ?></span></h4>
									<p><a target="_blank" href="<?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?>"><?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?></a></p>
									<span class="pull-right d-block d-md-none">Memorial created by <?php echo $memo->creator; ?> on <?php echo date('m-d-Y', strtotime($memo->createdAt)); ?></span>
									<div class="d-none d-md-block">
										<p class="new_text-search "><?php echo substr($memo->about, 0, 180); if(strlen($memo->about) > 180) echo '...'; ?></p>
										<a href="<?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?>" class="red_btn memorial_btn">VIEW MEMORIAL</a>
									</div>
								</div>
								<div class="col-12 d-block d-md-none">
									<p class="new_text-search"><?php echo substr($memo->about, 0, 180); if(strlen($memo->about) > 180) echo '...'; ?></p>
									<a href="<?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?>" class="red_btn memorial_btn">VIEW MEMORIAL</a>
								</div>
							</div>
						</div>
						
					<?php } ?>
					
					
                </div>
                <a href="javascript:;" onclick="search_gallery('<?php echo count($memorials); ?>')" class="setting_btn setting_btn1">Show More Memorials</a>
            
				<input id="current_limit" type="hidden" value="<?php if(count($memorials) < 4 ) echo '4'; else echo count($memorials); ?>" />
			
			</div>
			
			
        </div>
    </div>
</section>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>

function search_gallery(lm){
	
	var limit = $("#current_limit").val();
	
	if(lm > 0){
		limit = parseInt(parseInt(limit) + parseInt(4));
	}
	
	
	var fname = $("#fname").val();
	var lname = $("#lname").val();
	var yborn = $("#yborn").val();
	var ydeath = $("#ydeath").val();
	var gender = $("input[name='gender']:checked").val();
	
	$.ajax({
		url:"<?php echo base_url(); ?>memorial/search_gallery",
		type: "POST",
		data:{"fname": fname, "lname": lname, "yborn": yborn, "ydeath": ydeath, "gender": gender, "limit": limit},
		success:function(resp)
		{
			console.log(resp);
			$("#search_memo_list").html("");
			$("#search_memo_list").html(resp);
		}
	}); 
	
}

function clear_search(){
	
	$("#fname").val("");
	$("#lname").val("");
	$("#yborn").val("");
	$("#ydeath").val("");
	
	$('input:radio[name=gender]').each(function () { 
			$(this).prop('checked', false); 
	});
	
	search_gallery();
}

</script>


<?php $this->load->view("include/footer"); ?>

