<?php $this->load->view("include/header.php"); ?>

	
<section class="package_sec1 package_sec1-new">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Memorials</h1>
            </div>
        </div>
    </div>
</section>

<section class="search_margin pt-5 pb-3">
    <div class="container">
		<div class="search_box search_box_main search_box_main2">
				<div class="row">
					<div class="col-md-12">
						<form method="post" action="<?php echo base_url(); ?>memorial/search">
						  <div class="row ">
							  <div class="col-md-12">
								  <h3>Search for a memorial:</h3>
								  <div class="row pad_right5">
									  <div class="col-md-10 pad6">
										  <input id="keywords" type="text" name="last_name" value="<?php echo $last_name; ?>" class="form-control" placeholder="John Doe or Names">
									  </div>
									  <div class="col-md-2 pad6">
										  <button class="btn_start" type="submit">SEARCH</button>
									  </div>
								  </div>
							  </div>
							  
						  </div>
						</form>
					</div>
				</div>
			</div>
        <div class="row">
            <div class="col-md-10 m-auto">
				<div class="radio_class d-block text-center">
				
				<?php if(count($memorials) > 0){ ?>
				
					<span class="span_text-new1 d-inline-block mr-4">Sort by</span>
					<div class="radio radio-inline radio-inline-new">
						<input type="radio" id="inlineRadio1" value="dod" onclick="sort_by('dod', 'asc')" name="radioInline" >
						<label for="inlineRadio1"> Date of Death  
						<a href="javascript:;" onclick="sort_by('dod', 'desc')" class="ml-3" style="color: #582236;">
							<i class="fa fa-angle-up"></i>	
					    </a>
							<a href="javascript:;" onclick="sort_by('dod', 'asc')" class="ml-1" style=" color: #582236;">
							<i class="fa fa-angle-down"></i>	
					    </a>
						</label>
					</div>
					<div class="radio radio-inline">
						<input type="radio" id="inlineRadio2" value="gender" onclick="sort_by('gender', 'asc')" name="radioInline">
						<label for="inlineRadio2"> Gender 
						<a href="javascript:;" onclick="sort_by('gender', 'desc')" class="ml-3" style="color: #582236;">
							M
					    </a>
							<a href="javascript:;" onclick="sort_by('gender', 'asc')" class="ml-1" style="color: #582236;">
							F
					    </a>
						</label>
					</div>
					<div class="radio radio-inline">
						<input type="radio" id="inlineRadio3" value="fname" onclick="sort_by('fname', 'asc')" name="radioInline">
						<label for="inlineRadio3"> First Name 
						<a href="javascript:;" class="ml-3" onclick="sort_by('fname', 'desc')" style="color: #582236;">
							<i class="fa fa-angle-up"></i>	
					    </a>
							<a href="javascript:;" class="ml-1" onclick="sort_by('fname', 'asc')" style="color: #582236;">
							<i class="fa fa-angle-down"></i>	
					    </a>
						</label>
					</div>
					<div class="radio radio-inline">
						<input type="radio" id="inlineRadio4" value="lname" onclick="sort_by('lname', 'asc')" name="radioInline">
						<label for="inlineRadio4"> Last Name 
						<a href="javascript:;" class="ml-3" onclick="sort_by('lname', 'desc')" style="color: #582236;">
							<i class="fa fa-angle-up"></i>	
					    </a>
							<a href="javascript:;" class="ml-1" onclick="sort_by('lname', 'asc')" style="color: #582236;">
								<i class="fa fa-angle-down"></i>	
							</a>
						</label>
					</div>
					
				<?php } ?>
					
				</div>
			</div>
        </div>
    </div>
</section>
<section class="search_main">
    <div class="container">
		
        <div class="row" id="memo_list">
		
		<?php if(count($memorials) > 0){ foreach($memorials as $memorial){ 
					$full_name = $memorial->fname.' '.$memorial->lname;
		?>
				
				<div class="col-lg-3 col-md-4 mt-3">
					<div class="mem_box" style="cursor: pointer;" onClick="view_detail('<?php echo $memorial->webaddress; ?>')">
						
						<div class="media">
							
						<img width="150" height="150" src="<?php echo base_url(); ?>uploads/<?php echo $memorial->cropped_image; ?>" class="mr-3 wp-post-image" alt="">								
						<div class="media-body">
						   <h4><a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>"><?php if(strlen($full_name) > 11) echo substr($full_name, 0, 11); else echo $full_name; ?></a></h4>
							<p><?php echo date('Y', strtotime($memorial->birthdate)); ?>-<?php echo date('Y', strtotime($memorial->deathdate)); ?></p>
						  </div>
							
						</div>
						
						 <a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>" class="memorial_link">View Memorial</a>
					</div>
				</div>
				
		<?php } }else{ ?>
		
						<h3 style="margin-left: 50px;">No Records Found</h3>
		
		<?php } ?>
			
			<div class="clearfix"></div>
			
			
			
        </div>
		
		<?php if($total > 12){ ?>
		
		<a href="javascript:;" onclick="sort_by('show_more', 'show_more')" class="setting_btn setting_btn1">Show More Memorials</a>
		
		<?php } ?>
		
    </div>
</section>


<input type="hidden" id="current_sort" value=""  />
<input type="hidden" id="order" value=""  />
<input type="hidden" id="limit" value="<?php echo count($memorials); ?>"  />


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
	function view_detail(webaddress){
		location.href="<?php echo base_url(); ?>memorial/tribute/"+webaddress;
	}

function sort_by(sortby, order){
	
	var limit = $("#limit").val();
	var keywords = $("#keywords").val();
	
	//var order = 'desc';
	
	var current_sort = $("#current_sort").val();
	
	if(sortby == 'show_more'){
		sortby = $("#current_sort").val();
		limit = parseInt(parseInt(limit) + parseInt(12));
		$("#limit").val(limit);
		
		order = $('#order').val();
		if(order == '') order = 'desc';
		
	}
	
	$("#current_sort").val(sortby);
	$("#order").val(order);
	
	$.ajax({
		url:"<?php echo base_url(); ?>memorial/sort_by",
		type: "POST",
		data:{"sortby": sortby, "order": order, "keywords": keywords, "limit": limit},
		success:function(resp)
		{
			console.log(resp);
			$("#memo_list").html("");
			$("#memo_list").html(resp);
		}
	}); 
	
}

function show_more(){
	
	var limit = $("#limit").val();
	var keywords = $("#keywords").val();
	var order = 'desc';
	var current_sort = $("#current_sort").val();
	
	
	if(current_sort == sortby){
		order = $("#order").val();
		if(order == 'asc'){
			order = 'desc';
		}else{
			order = 'asc';
		}
	}
	
	
	$("#current_sort").val(sortby);
	$("#order").val(order);
	
	$.ajax({
		url:"<?php echo base_url(); ?>memorial/sort_by",
		type: "POST",
		data:{"sortby": sortby, "order": order, "keywords": keywords, "limit": limit},
		success:function(resp)
		{
			console.log(resp);
			$("#memo_list").html("");
			$("#memo_list").html(resp);
		}
	}); 
	
}

</script>


<?php $this->load->view("include/footer"); ?>

