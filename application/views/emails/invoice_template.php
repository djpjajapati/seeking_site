<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="custom.css" rel="stylesheet" id="bootstrap-css">

<style>

.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}

</style>


</head>
    
<body>
    
<div class="container" style="margin: 30px auto;max-width: 800px;">
    <div class="row">
        <div class="col-md-12">
    		<div class="invoice-title row">
                
    			<div class="col-md-4"><img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 65%;"></div>
                <div class="col-md-8">
                    <div class="col-md-12">
                    <h3 class="pull-right">Invoice #<?php echo $memo->ID.$user->ID ?></h3>
                    </div>
                    <br>
                    <div class="col-md-12">
                    <address class="pull-right">
    					<strong>Invoice Date:</strong>
    					<?php echo date('F', strtotime($memo->createdAt)); ?> <?php echo date('d', strtotime($memo->createdAt)); ?> , <?php echo date('Y', strtotime($memo->createdAt)); ?>
    				</address></div>
                </div>

    		</div>
    		<hr>
    		<div class="row">
                
    			<div class="col-md-6">
    				<address>
    				<strong>Invoice To:</strong><br>
    					<?php echo $user->fname.' '.$user.lname; ?><br>
    					<?php echo $user->email; ?><br >
    					<?php echo $memo->citytown; ?>
    					<?php echo $memo->countryorigin1; ?>
    				</address>
    			</div>
                <div class="col-xs-6 text-right">
    				<address>
        			<strong>Address:</strong><br>
    					<?php echo $memo->raddress; ?>
    					<?php echo $memo->countryorigin2; ?>
                    </address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Invoice Summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong> <?php if($payment->action == 'create') echo 'Creation of Memorial'; else echo 'Upgradation of Memorial'; ?> </strong></td>
        							<td class="text-center"><strong>Package</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<tr>
    								<td><?php echo $memo->fname.' '.$memo->lname; ?></td>
    								<td class="text-center"><?php echo $memo->pakage; ?></td>
    								<td class="text-right"><?php echo $memo->pakage_price; ?> KES</td>
    							</tr>
                              
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right"><?php echo $memo->pakage_price; ?> KES</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Discount</strong></td>
    								<td class="no-line text-right"><?php echo ($memo->pakage_price-$memo->amount_paid); ?> KES</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right"><?php echo $memo->amount_paid; ?> KES</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
    
</body>
</html>