<?php $this->load->view("include/header.php"); ?>

<section class="testi_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>
				<?php echo $page->title; ?>
				</h1>
            </div>
        </div>
    </div>
</section>
<section class="testi_main">
    <div class="container">
    		<div class="row">
    			<div class="col-lg-12 pl-md-5 ">
    				
					<?php echo $page->description; ?>
					
					<!--<p >Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
    				<p >Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				    </p>
                    <p >Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia magnam aliquam quaerat voluptatem.
				    </p>
                    <br>
                    <h4>Step 1</h4>
                    <p>Aliquip ex ea commodo consequat. Duis aute irure (  ) dolor in reprehenderit voluptate velit esse cillum dolore eu fugiat nulla pariatur.
				    </p>
                    <p>OVoluptatem accusantium doloremque (  ) laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				    </p>
                    <br>
                    <h4>Step 2</h4>
                     <p>Aliquip ex ea commodo consequat. Duis aute irure (  ) dolor in reprehenderit voluptate velit esse cillum dolore eu fugiat nulla pariatur.
				    </p>
                    <p>OVoluptatem accusantium doloremque (  ) laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				    </p>
                    <br>
                    <h4>Step 3</h4>
                      <p>Aliquip ex ea commodo consequat. Duis aute irure (  ) dolor in reprehenderit voluptate velit esse cillum dolore eu fugiat nulla pariatur.
				    </p>
                    <p>Ovoluptatem accusantium doloremque (  ) laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				    </p>-->
    			</div>
    		</div>
    </div>
</section>



<?php $this->load->view("include/footer"); ?>

