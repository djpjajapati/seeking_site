<?php $this->load->view("admin/include/header"); ?>
  
  
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
          <div class="row">
             <div class="col-md-6">
                <h2>View User Management</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/users"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
          </div>
        </div>
      </div>
      <div class="content-body">
           <section id="basic-tabs-components">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 ">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-content">
                  <div class="card-body">
                    <ul class="nav nav-tabs">
                      <li class="nav-item">
                        <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                        href="#tab1" aria-expanded="true">Personal Information</a>
                      </li>
                      <li class="nav-item">
                       <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2"
                        aria-expanded="false">Memorials</a>
                      </li> 
					  
					  <li class="nav-item">
                       <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3" href="#tab3"
                        aria-expanded="false">User Activities</a>
                      </li>
					  
					  
                    </ul>
                    <div class="tab-content px-1 pt-1">
                      <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
        <!-- Dropzone section start -->
        <section id="dropzone-examples">
                      <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form method="post" action="<?php echo base_url(); ?>admin/users/make_page_owner">
                      <div class="form-body">
                        
						<?php if($this->session->flashdata('error')){ ?>
							<div class="alert alert-block  alert-danger">
								<button data-dismiss="alert" class="close close-sm" style="font-size: medium;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
								<?php echo $this->session->flashdata('error'); ?>
							</div>
						<?php } ?>
						
						<?php if($this->session->flashdata('success')){ ?>
								<div class="alert alert-block  alert-success">
									<button data-dismiss="alert" class="close close-sm" style="font-size: medium;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
									<?php echo $this->session->flashdata('success'); ?>
								</div>
						<?php } ?>
						
						
                        <div class="row">
                            <div class="col-md-6">
                            <label>Make Page Owner Of</label>
                                  <select name="memo_id" class="form-control form-control-sm">
                                      
									  <?php foreach($memorials as $memo){ ?>
									  
											<option value="<?php echo $memo->ID; ?>"><?php echo $memo->fname.' '.$memo->lname; ?></option>
									  
									  <?php } ?>
									  
									  
									  <!--<option value="Select">Select</option>
                                      <option value="">Memorial 1</option>
                                      <option value="">Memorial 2</option>
                                      <option value="">Memorial 3</option>
                                      <option value="">Memorial 4</option>-->
									  
									  
									  
                                  </select>
                               
                             </div>
							 
                          <div class="col-md-6">
                              <label>Name</label>
                            <div class="form-group half-group">
                              <input type="text" class="form-control" value="<?php echo $user->fname.' '.$user->lname; ?>" readonly>
							 <input name="user_id" type="hidden" value="<?php echo $this->uri->segment('4'); ?>" />
						   </div>
                            </div>
                              <div class="col-md-6">
                            <label>Email</label>
                              <div class="form-group half-group">
                              <input type="text" class="form-control" value="<?php echo $user->email; ?>" readonly>
                            </div>
                             </div>
                            <div class="col-md-6">
                            <label>Page Owner URL</label>
                             <div class="form-group">
                                    <div class="input-group">
                                      <input type='text' class="form-control" value="<?php echo $user->page_url; ?>" readonly>
                                    </div>  
                                </div>
                          </div>
                            <div class="col-md-6">
                                <label>Package</label>
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?php echo $user->pakage; ?>" readonly>
                            </div>
                          </div>
                             <div class="col-md-6">
                                <label>Status</label>
                            <div class="form-group">
                              <input type="text" class="form-control" value="<?php echo $user->status; ?>" readonly>
                            </div>
                          </div>
                             <div class="col-md-12 text-right">
                        <button onchange="make_page_owner(this.value)" type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i>Update
                        </button>
                            </div>
                        </div> 
                      </div>
                       
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
         
        </section>
                        </div>
    
  
                <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
  
					<section id="horizontal">
					  <div class="row">
						<div class="col-12">
						  <div class="card">
							  <div class="card-header pb-md-0">
								  <div class="row">
								  <div class="col-md-6">
									  <!--<div class="col-md-3">
									  <select class="form-control" id="basicSelect">
										<option>Select</option>
										<option>Amsterdam</option>
									  </select>
									  </div>-->
								  </div>
								  <!--<div class="col-md-6 text-right">
									  <strong>Export As:</strong>
								  <button type="button" class="btn btn-dark square btn-min-width mr-1 mb-1">EXCEL</button>
								  </div>-->
								  </div>
							  </div>
							<div class="card-content collapse show">
							  <div class="card-body card-dashboard">
								<table class="table display nowrap table-striped table-bordered scroll-horizontal">
								  <thead>
									<tr>
									  <th>Page URL</th>
									  <th>Memorial Package</th>
									  <th>Expiry Date</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
									
									<?php foreach($user_memorials as $memo){ ?>
									
										<tr>
										  <td><?php echo $memo->page_url; ?></td>
										  <td><?php echo $memo->pakage; ?></td>
										  <td><?php echo date('d-m-Y', strtotime($memo->createdAt." +1 years")); ?></td>
										  
											<td>
											<a href="<?php echo base_url(); ?>admin/users/activities/<?php echo $memo->ID; ?>" class="btn btn-primary btn-xs"  title="View"><i style="cursor: pointer" class="fas fa-eye text-white"></i></a>
											 
											</td>
										</tr>
									
									<?php } ?>
									
									
									
									  
								  </tbody>
								</table>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					</section>
 
				</div>
				 
				 
				 <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
  
					<section id="horizontal">
					  <div class="row">
						<div class="col-12">
						  <div class="card">
							  <div class="card-header pb-md-0">
								  <div class="row">
								  <div class="col-md-6">
									  <!--<div class="col-md-3">
									  <select class="form-control" id="basicSelect">
										<option>Select</option>
										<option>Amsterdam</option>
									  </select>
									  </div>-->
								  </div>
								  <!--<div class="col-md-6 text-right">
									  <strong>Export As:</strong>
								  <button type="button" class="btn btn-dark square btn-min-width mr-1 mb-1">EXCEL</button>
								  </div>-->
								  </div>
							  </div>
							<div class="card-content collapse show">
							  <div class="card-body card-dashboard">
								<table class="table display nowrap table-striped table-bordered scroll-horizontal">
								  <thead>
									<tr>
									  <th>S.No</th>
									  <th style="width: 100%">Description</th>
									  <th>Created At</th>
									  <!--<th>Action</th>-->
									</tr>
								  </thead>
								  <tbody>
									
									<?php $c=1; foreach($user_activities as $act){ ?>
									
										<tr>
										  <td><?php echo $c; ?></td>
										  <td><?php echo $act->description; ?></td>
										  <td><?php echo date('d-m-Y', strtotime($act->createdAt)); ?></td>
										  
											<!--<td>
											<a href="<?php //echo base_url(); ?>admin/users/activities/<?php //echo $memo->ID; ?>" class="btn btn-primary btn-xs"  title="View"><i style="cursor: pointer" class="fas fa-eye text-white"></i></a>
											 
											</td>-->
										</tr>
									
									<?php $c++; } ?>
									
									
									
									  
								  </tbody>
								</table>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					</section>
 
				</div>
				 
				 
				 
				 
				 
				 
  
       </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
  
  
        </div>
      </div>
</div>

  
<?php $this->load->view('admin/include/footer'); ?>