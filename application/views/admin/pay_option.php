<?php $this->load->view("include/header.php"); ?>

<?php header('Access-Control-Allow-Origin: *'); ?>

<section class="package_sec1 package_sec1-new2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>You’re almost there...</h1>
                <p>Before completing your order please select the desired payment method.</p>
            </div>
        </div>
    </div>
</section>


<section class="payment_method">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pad_right9">
                <div class="payment_box-new payment_box-new1">
                    <h3>Your Selection:</h3>
                    <h4><?php echo $pakage->title; ?> PACKAGE</h4>
                    <h5><span id="price_to_pay"><?php echo $pakage->price; ?>.00</span></h5>
                    <a href="<?php echo base_url(); ?>memorial/pay_now" class="red_btn selectn_btn">Change Selection</a>
                </div>
            </div>
            <div class="col-md-8 pad_left9">
                <div class="payment_box-new">
                    <div class="payment-inner">
                      <h3>Your Payment Method:</h3>
                      <div class="row">
                          <div class="col-lg-2 col-md-4 col-6">
                            <div class="radio">
                                <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked="">
                                <label for="inlineRadio1"> Mpesa </label>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-6">
                              <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/mpesa.png">
                          </div>
                          <div class="col-lg-7 col-md-12 col-12">
                            <div class="form_country">
                             <input type="text" class="form-control" placeholder="Your Phone Number">
                             <span class="main_span"><img alt="img" src="<?php echo base_url(); ?>assets/img/Kenya-Emoji-Flag.png"><span class="inner_span">Kenya</span>  +254</span>
                            </div> 
                          </div>
                      </div>
                      <div class="space40"></div>
                      <div class="row">
                          <div class="col-md-6">
                              <p class="payment_text">Enter the Kenyan Telephone number you wish to make the Mpesa payment with. A notification will be sent to the number provided requesting authorization. Upon approval the payment will be made.</p>
                          </div>
                          <div class="col-md-6">
                              <ul class="list-inline">
                                  <li class="list-inline-item click_text">Click to send a verification code to your phone.</li>
                                  <li class="list-inline-item"><a href="" class="send_code">Send Code</a></li>
                              </ul>
                          </div>
                      </div>
                    </div>
                    <hr>
                    
					
					<div class="payment-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-7 pad_right0">
                                <div class="radio radio_pad">
                                    <input type="radio" id="inlineRadio2" value="option1" name="radioInline2" checked="">
                                    <label for="inlineRadio2"> Credit Card / Debit Card </label>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-5 pad_left0">
                                <ul class="list-inline list_pay">
                                    <li class="list-inline-item"><a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/visa.png"></a></li>
                                    <li class="list-inline-item"><a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/mastercard.png"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="space30"></div>
                        <div class="row">
                            <div class="col-lg-7 col-md-6">
                                <div class="input_text">
                                    <input type="text" name="fname" class="form-control" value="<?php echo $user->fname; ?>" placeholder="First Name">
                                    <input type="text" name="lname" class="form-control" value="<?php echo $user->lname; ?>" placeholder="Last Name">
                                    
									
									<span id="coupon_error" style="color: red; display: none">Sorry Coupon Code is not valid</span>
									</br>
									<span style="display: inline-flex;">
										<input type="text" name="coupon" id="coupon" class="form-control" value="" placeholder="Coupon Code"> 
										<a onclick="apply_coupon()" style="margin-left: 10px;" href="javascript:;" class="send_code pull-right"> Apply Coupon</a>
                                   </span>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <!--<div class="list-inline list_pay">
                                    <label for="sel1">Expiration</label>
                                    <select class="form-control select_class" id="sel1">
                                        <option>Month</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                    <select class="form-control" id="sel2">
                                        <option>Year</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>-->
                                <!--<a href="" class="send_code pull-right">Confirm Pay</a>-->
								
								<a style="margin-top: 100px;" href="javascript:;" onclick="open_modal('<?php echo $pakage->ID; ?>')" class="send_code">Confirm Pay</a>
								
								
								
								
                            </div>
                        </div>

                    </div>
					
					
                </div>
            </div>
        </div>
    </div>
</section>






<!-- Modal -->
<div class="modal purchase_modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Payment Confirmation</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>You have selcted:</p>
        <h4 id="title"></h4>
        <h3><span style="font-size: 56px;" id="price"><?php echo $pakage->price; ?></span>.00</h3>
		
		
		
		
		<form id="ipay_form" method="post" action="https://payments.ipayafrica.com/v3/ke">
		
		<input type="hidden" name="live" value="0">
		<input type="hidden" name="oid" value="<?php echo time().rand(0,9997); ?>">
		<input type="hidden" name="inv" value="<?php echo time().rand(0,1234); ?>">
		<input type="hidden" name="ttl" id="price1" value="<?php echo $pakage->price; ?>">
		<input type="hidden" name="tel" value="256712375678">
		<input type="hidden" name="eml" value="james.noah1172@gmail.com">
		<input type="hidden" name="vid" value="gt14">
		<input type="hidden" name="curr" value="KES">
		<input type="hidden" name="p1" id="plan_id" value="1">
		<input type="hidden" name="p2" value="<?php echo $user->ID; ?>">
		<input type="hidden" name="p3" value="<?php echo $user->fname; ?>">
		<input type="hidden" name="p4" value="<?php echo $user->lname; ?>">
		<input type="hidden" name="cbk" value="<?php echo base_url(); ?>memorial/payment_callback">
		<input type="hidden" name="cst" value="1">
		<input type="hidden" name="crl" value="0">
		<input name="hsh" type="hidden" id="hsh" value="">
		<br>
		
		<!--<button type="submit">&nbsp;Submit&nbsp;</button>-->
		<a href="javascript:;" data-dismiss="modal" class="cancel">cancel</a>
        <a href="javascript:;" onclick="generate_hash()" class="proceed blue_bg">proceed</a>
		
		</form>
		
      </div>

      <!-- Modal footer -->
    </div>
  </div>
</div>
<!-- Modal -->

<script>

	function generate_hash(){
		
		var live = $("input[name=live]").val();
		var oid = $("input[name=oid]").val();
		var inv = $("input[name=inv]").val();
		var ttl = $("input[name=ttl]").val();
		var tel = $("input[name=tel]").val();
		var eml = $("input[name=eml]").val();
		var vid = $("input[name=vid]").val();
		var curr = $("input[name=curr]").val();
		var p1 = $("input[name=p1]").val();
		var p2 = $("input[name=p2]").val();
		var p3 = $("input[name=p3]").val();
		var p4 = $("input[name=p4]").val();
		var cbk = $("input[name=cbk]").val();
		var cst = $("input[name=cst]").val();
		var crl = $("input[name=crl]").val();
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/generate_hash",
				type: "POST",
				data: {"live":live, "oid":oid, "inv":inv, "ttl":ttl, "tel":tel, "eml":eml, "vid":vid, "curr":curr, "p1":p1, "p2":p2, "p3":p3, "p4":p4, "cbk":cbk, "cst":cst, "crl":crl},
				success: function (resp) {
					console.log(resp);	
					$("#hsh").val(resp);
					if($("#hsh").val() != ''){
						$("#ipay_form").submit();
					}
				}
			});
	}
				
	function open_modal(id){
		
		//var title = '<?php echo $pakage->title; ?>'; //$("#title_"+id).text();
		//var price = '<?php echo $pakage->price; ?>'; //$("#price_"+id).text();
		//$("#plan_id").val(id);
		
		//$("#title").text(title);
		//$("#price").text(price);
		//$("#price1").val(price);
		$("#myModal").modal('show');
		
	}
	
</script>






<?php $this->load->view("include/footer"); ?>

