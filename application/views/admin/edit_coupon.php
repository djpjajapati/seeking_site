<?php $this->load->view("admin/include/header"); ?>
  
  
 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Edit Coupan</h3>
        </div>
        <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/coupon"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
      </div>
      <div class="content-body">
        <!-- Form repeater section start -->
        <section>
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <div class="">
                      <div >
                        <div>
                          <form method="post" action="<?php echo base_url(); ?>admin/coupon/do_edit_coupon" class="">
                              
                                <?php if($this->session->flashdata('error')){ ?>
                                    <div class="alert alert-block  alert-danger">
                                        <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php } ?>
                              
                              
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupan Code</label>
                              <br>
                              <input type="text" name="coupon_code" class="form-control" value="<?php echo $coupon->coupon_code; ?>" required placeholder="Enter Code">
                            </div>
                              <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupan Discount Percentage</label>
                              <br>
                              <input type="number" min="1" max="100" name="coupon_discount_percentage" value="<?php echo $coupon->coupon_discount_percentage; ?>" required class="form-control" placeholder="Enter Discount">
                            </div>
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Frequency</label>
                              <br>
                              
                                      <input type="number" name="frequency" min="1"  value="<?php echo $coupon->frequency; ?>" class="form-control" required  placeholder="Coupan usage time" />
                                    <input type="hidden" name="id" value="<?php echo $coupon->ID; ?>" />
                            </div>
                               <div class="form-group col-md-6">
                              <label for="bio" class="cursor-pointer">Expiry Date</label>
                              <br>
                                  <div class="form-group">
                                 
                                 
                                      <input type="date" name="expiry" value="<?php echo $coupon->expiry; ?>" class="form-control" required placeholder=" Pick-a-date"
                                      />
                                    
                                </div>

                            </div>
                            
                            <div class="form-group overflow-hidden">
                                <div class="col-12">
                                  <button type="submit" class="btn btn-primary">
                                    Update Coupon
                                  </button>
                                   
                                </div>
                          </div>
                            
                            
                          </form>
                          <hr>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
     
      </div>
    </div>
  </div>
  
  
  
<?php $this->load->view('admin/include/footer'); ?>