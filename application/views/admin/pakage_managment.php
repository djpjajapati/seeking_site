<?php $this->load->view("admin/include/header"); ?>
  
	<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
                 <div class="row">
             <div class="col-md-6">
                <h2>Package Management</h2>
             </div>
          </div>
      <div class="content-body">
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  
                  <?php if($this->session->flashdata('error')){ ?>
                        <div class="alert alert-block  alert-danger">
                            <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    <?php } ?>
                  
                  
                  <?php if($this->session->flashdata('success')){ ?>
                        <div class="alert alert-block  alert-success">
                            <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php } ?>
                  
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Package Name</th>
                          <th>Price</th>
                          <th>Images</th>
                          <th>Videos</th>
                          <th>Music</th>
                          <th>Admins</th>
                          <th>Maintenance Fee</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          
                        <?php foreach($pakages as $p){ ?>
                          
                        <tr id="p_<?php echo $p->ID; ?>">
                          <td><?php echo $p->title; ?></td>
                            <td><?php echo $p->price.' '.$p->currency; ?> </td>
                            <td><?php echo $p->photo_limit; ?></td>
                            <td><?php echo $p->video_limit; ?></td>
                            <td><?php echo $p->audio_limit; ?></td>
                            <td><?php echo $p->page_managers; ?></td>
                            <td><?php echo $p->annual_maintenance_fee; ?></td>
                            
                            
                          <td class="text-center">
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>admin/manage/edit_pakage/<?php echo $p->ID; ?>"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a onclick="delete_pakage('<?php echo $p->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                        
                        <?php } ?>
                           <!--<tr>
                          <td>Premium</td>
                            <td>2000 KSh</td>
                               <td>20</td>
                            <td>15</td>
                            <td>9</td>
                            <td>3</td>
                          <td class="text-center">
                             <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="edit-package-management.html"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>-->
                        
                        <script>
                            
                            function delete_pakage(id){
							
							if(confirm("Are you sure?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/manage/delete_pakage",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#p_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
						
					</script>
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Zero configuration table -->
            <!-- Modal -->
                         <!-- <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
                          aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit Package Management</label>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="#">
                                  <div class="modal-body">
                                    <label>Price </label>
                                    <div class="form-group">
                                      <input type="text" placeholder="Enter Price" class="form-control">
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                                    value="close">
                                    <input type="submit" class="btn btn-outline-primary btn-lg" value="Save">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>-->
      </div>
    </div>
  </div>
  </div>
					
<?php $this->load->view('admin/include/footer'); ?>