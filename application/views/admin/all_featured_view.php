<?php $this->load->view('admin/include/header'); ?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
  
 
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">

	  <section id="horizontal">
     
			    
				  <div class="card p-md-2 p-0">
				      <div class="row">
					   <div class="col-md-4">
					  <label><h2>Current Featured Memorial</h2><label>
					  </div>
					  
					   <div class="col-md-4">
					  <select id="current_featured" name="current_featured" class="form-control" style="margin-left: 10px; margin-right: 10px">
						<?php foreach($memorials as $m){ ?>
							<option value="<?php echo $m->ID; ?>" <?php if($m->current_featured > 0 ) echo 'selected'; ?>><?php echo $m->fname.' '.$m->lname; ?></option>
						<?php } ?>
					  </select>
					  </div>
					  
					   <div class="col-md-4">
						<button onclick="update_current_featured()" class="btn btn-success round btn-glow px-2 float-right" >Update </button>
					   </div>
					   </div>
				 
	  </div>
	  </section>
	  </div>
	  
          <h2>All Featured Memorial</h2>
       
	   
        
      <div class="content-body">

        <section id="horizontal">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">
				  
				  
				  
				  
				  </h4>
                
				
				
				<script>
				
				function update_current_featured(){
					
					var id = $("#current_featured").val();
					
					if(id > 0){
						
						$.ajax({
							url:"<?php echo base_url(); ?>admin/featured/update_current_featured",
							type: "POST",
							data:{"id": id},
							success:function(resp)
							{
								if(resp == 'success'){
									$("#suc_text").text("Recored Update Successfully!");
									$("#SuccessModal").modal('show');
								}else{
									$("#err_text").text("Somthing went wrong!");
									$("#ErrorModal").modal('show');
								}
							}
						});
					}
				}
				
				</script>
				
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table id="tab_customers" class="table display nowrap table-striped responsive">
                      <thead>
					  
						<tr>
                          <th>Name / Title</th>
                          <th>Memorial URL</th>
                          <th>Date of Birth</th>
                          <th>Date of Death</th>
                          <th>Tributes</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        
					  
					  <?php foreach($memorials as $m){ ?>
					  
								<tr id="m_<?php echo $m->ID; ?>">
								  <td><?php echo $m->fname.' '.$m->lname; ?></td>
								  <td><?php echo base_url().'featured/detail/'.$m->ID.'/'.$m->fname; ?></td>
								  <td><?php echo $m->dob; ?></td>
								  <td><?php echo $m->dod; ?></td>
								  <td><?php echo $m->tributes; ?></td>
								  
								  <td>
									 <a href="<?php echo base_url(); ?>admin/featured/edit_memo/<?php echo $m->ID; ?>" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
									 <a onclick="delete_memo('<?php echo $m->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
								  </td>
								</tr>
					  
					  <?php } ?>
					  
					  
					  <script>
					  
					  function delete_memo(id){
							
							if(confirm("Are you sure to delete this memorial?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/featured/delete_memo",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#m_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
					  
					  </script>
					  
                        <!--<tr>
                          <td>Emma Redshaw</td>
                          <td>www.memorial.com</td>
                          <td>12-09-2000</td>
                          <td>02-01-2018</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                           <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                            <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>www.memorial.com</td>
                          <td>12-09-2000</td>
                          <td>02-01-2018</td>
                          <td>20</td>
                          <td>
                            <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>www.memorial.com</td>
                          <td>12-09-2000</td>
                          <td>02-01-2018</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                           <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                            <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>www.memorial.com</td>
                          <td>12-09-2000</td>
                          <td>02-01-2018</td>
                          <td>20</td>
                          <td>
                              <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                              <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                         <td>www.memorial.com</td>
                          <td>02-10-1995</td>
                          <td>09-01-2017</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1990</td>
                          <td>05-06-2017</td>
                          <td>20</td>
                          <td>
                           <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                            <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                         <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                              <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                            <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>www.memorial.com</td>
                          <td>02-10-1998</td>
                          <td>12-01-2019</td>
                          <td>20</td>
                          <td>
                             <a class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>-->
        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       
      </div>
    </div>
  </div>
    </div>
  
  
  

<?php $this->load->view('admin/include/footer'); ?>

<script type="text/javascript">
        $(document).ready(function() {
			$('#tab_customers').DataTable( {
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					}
				]
				
				
				/* buttons: [
					'excelHtml5',
					'csvHtml5',
					'pdfHtml5'
				] */
			} );
		});
    </script>