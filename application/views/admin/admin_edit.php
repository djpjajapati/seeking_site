<?php $this->load->view('admin/include/header'); ?>

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Profile</h3>
          
        </div>
        <div class="content-header-right col-md-6 col-12">
        </div>
      </div>
      <div class="content-body">
        <section id="basic-form-layouts">
             
             
                <?php if($this->session->flashdata('error')){ ?>
                    <div class="col-12">
                        <div class="alert alert-block  alert-danger">
                            <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    </div>
                <?php } ?>
                
                
                <?php if($this->session->flashdata('success')){ ?>
                    <div class="col-12">
                        <div class="alert alert-block  alert-success">
                            <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    </div>
                <?php } ?>
             
             
          
          <div class="row match-height">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title form-section">Information</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form method="post" class="form" action="<?php echo base_url(); ?>admin/profile/update_code" enctype="multipart/form-data">
                    
                      <div class="form-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="userinput1">Fist Name </label>
                              
                              <input type="text" name="fname" class="form-control border-primary" value="<?php echo $user_data->fname; ?>" placeholder="First Name" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="userinput2">Last Name</label>
                              <input type="text" name="lname" class="form-control border-primary" value="<?php echo $user_data->lname; ?>" placeholder="Last Name" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="userinput3">Username</label>
                              <input type="text" name="admin_name" class="form-control border-primary" value="<?php echo $user_data->admin_name; ?>" placeholder="Username" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="userinput4">Email Address</label>
                             <input class="form-control border-primary" name="admin_email" type="email" value="<?php echo $user_data->admin_email; ?>" readonly>
                            </div>
                          </div>
                        </div>
                       
                  <h4 class="card-title form-section"><i class="fas fa-user"></i>Change Display Image</h4>

                <div class="card-content collapse show">
                  <div class="card-body">
                    <form class="form row">
                      <div class="form-group col-12 mb-2">
                       
                          <div>
                            <div class="row mb-1">
                              <div class="col-9 col-xl-10">
                                <label class="file center-block">
                                  <input name="pic" type="file" id="file">
                                  <span class="file-custom"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                     
                        <button type="submit" class="btn btn-primary float-right">
                         Update Profile
                        </button>
                      </div>
                    </form>
                    </div>
         
                        <h4 class="form-section card-title"><i class="fas fa-key"></i> Change Password </h4>
            
                         <form method="post" class="form" action="<?php echo base_url(); ?>admin/profile/change_password">
                
                        <?php if($this->session->flashdata('perror')){ ?>
                            <div class="col-12">
                                <div class="alert alert-block  alert-danger">
                                    <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                    <?php echo $this->session->flashdata('perror'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        
                        
                        <?php if($this->session->flashdata('psuccess')){ ?>
                            <div class="col-12">
                                <div class="alert alert-block  alert-success">
                                    <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                    <?php echo $this->session->flashdata('psuccess'); ?>
                                </div>
                            </div>
                        <?php } ?>
             
                
                        <div class="form-group">
                          <label>Old Password </label>
                          <input class="form-control border-primary" name="password" type="password" placeholder="Enter Old Password">
                        </div>
                          <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="userinput3">New Password</label>
                              <input type="password" name="new_password" class="form-control border-primary" placeholder="Enter New Password"
                              name="username">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="userinput4">Confirm New Password</label>
                             <input class="form-control border-primary" name="c-password" type="password" placeholder="Enter Confirm Password">
                            </div>
                          </div>
                        </div>
                          </div></div>
                      <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">
                          Update Password
                        </button>
                          </div>
                        
                    </form>
                  </div>
                </div>
              </div>
            </div>
              </div>
        </section>
      </div>
    </div>
  </div>
  

<?php $this->load->view('admin/include/footer'); ?>