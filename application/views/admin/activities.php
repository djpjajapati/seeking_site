<?php $this->load->view('admin/include/header'); ?>

 
   
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
                 <div class="row">
             <div class="col-md-6">
                <h2>Memorial Activities</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="javascript:window.history.go(-1);"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
          </div>
      <div class="content-body">
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Detail</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					  <?php foreach($activities as $act){ ?>
					  
							<tr id="act_<?php echo $act->ID; ?>">
							  <td><?php echo $act->description; ?></td>
								<td><?php echo date('d-m-Y', strtotime($act->createdAt)); ?></td>
							  <td class="text-center">
								 <a onclick="delete_activity('<?php echo $act->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
							  </td>
							</tr>
					  
					  <?php } ?>
					  
					  
					  <script>
					  
					  function delete_activity(id){
							
							if(confirm("Are you sure to delete this user?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/users/delete_activity",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#act_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
					  
					  
					  </script>
					  
					  
                        <!--<tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                            <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                             <tr>
                          <td>Uploade a new video to the memorial for John Doe.</td>
                            <td>20-June-2018</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>-->
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

       
      </div>
    </div>
  </div>
    </div>
  
  
  
  
  

<?php $this->load->view('admin/include/footer'); ?>