<?php 

	$sess_data=$this->session->userdata('admin');
	if(!isset($sess_data->admin_email))
	{
		redirect('admin/login');
	}      
	$this->admin_id = $sess_data->admin_id;       
	$this->admin_name = $sess_data->admin_name; 
	$admin = $this->db->query("select * from admin where admin_id = ".$sess_data->admin_id)->row();
	
	$notification_cnt = $this->db->query("select count(*) as cnt from activity_log where read_status = 0")->row()->cnt;
	$notification = $this->db->query("select * from activity_log where read_status = 0 order by ID desc limit 5")->result();
	
	$new_memorials = $this->db->query("select count(*) as cnt from memorials where read_status = 0")->row()->cnt;
	$used_coupon = $this->db->query("select count(*) as cnt from coupon_log where read_status = 0")->row()->cnt;
	$new_featured_memo_tribute = $this->db->query("select count(*) as cnt from featured_memo_tribute where read_status = 0")->row()->cnt;
	$new_testimonials = $this->db->query("select count(*) as cnt from testimonials where read_status = 0")->row()->cnt;

 ?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <title><?php echo $page_title; ?></title>

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  
  <!--<link href="maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.html"
  rel="stylesheet">-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/css/vendors.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/vendors/css/tables/datatable/datatables.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/css/app.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/css/core/menu/menu-types/vertical-menu-modern.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/css/core/colors/palette-gradient.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/vendors/css/extensions/sweetalert.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
  
  <!-- For add feature memorial -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/vendors/css/ui/prism.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/vendors/css/file-uploaders/dropzone.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/css/plugins/file-uploaders/dropzone.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/css/plugins/pickers/daterange/daterange.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/app-assets/vendors/css/pickers/pickadate/pickadate.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/dist/summernote.css">
  
  <!-- End add feature memorial -->
  
  </head>


<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
  <!-- fixed-top-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item mr-auto">
            <a class="navbar-brand" href="index.html">
              <h3 class="brand-text">Safiri Salama</h3>
            </a>
          </li>
          <li class="nav-item d-none d-md-block float-right"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse float-right" id="navbar-mobile">
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-user nav-item">
              <a style="cursor: pointer" class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="mr-1">Hello,
                  <span class="user-name text-bold-700"><?php echo $admin->admin_name; ?></span>
                </span>
                <span class="avatar avatar-online">
                  <img src="<?php echo base_url(); ?>uploads/<?php echo $admin->pic;  ?>" alt="avatar"><i></i></span>
              </a>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="<?php echo base_url(); ?>admin/profile"><i class="ft-user"></i> Edit Profile</a>
                <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url(); ?>admin/logout"><i class="ft-power"></i> Logout</a>
              </div>
            </li>
            
			
			<li class="dropdown dropdown-notification nav-item">
              <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i>
                <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow"><?php echo $notification_cnt; ?></span>
              </a>
              <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <h6 class="dropdown-header m-0">
                    <span class="grey darken-2">Notifications</span>
                  </h6>
                  <span class="notification-tag badge badge-default badge-danger float-right m-0"><?php echo $notification_cnt; ?> New</span>
                </li>
                <li class="scrollable-container media-list w-100">
                  
				  <?php foreach($notification as $noti){ ?>
				  
				   <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                      <div class="media-body">
                        <!--<h6 class="media-heading">You have new order!</h6>-->
                        <p class="notification-text font-small-3 text-muted"><?php echo $noti->description; ?></p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00"><?php echo $noti->createdAt; ?></time>
                        </small>
                      </div>
                    </div>
                  </a>
				  
				  <?php } ?>
				  
				  <!--<a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">You have new order!</h6>
                        <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading red darken-1">99% Server load</h6>
                        <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Five hour ago</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                        <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-check-circle icon-bg-circle bg-cyan"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">Complete the task</h6>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-file icon-bg-circle bg-teal"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">Generate monthly report</h6>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time>
                        </small>
                      </div>
                    </div>
                  </a>-->
                </li>
                <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="<?php echo base_url(); ?>admin/home/activities">Read all notifications</a></li>
              </ul>
            </li>
			
			
			
          </ul>
        </div>
      </div>
    </div>
  </nav>
     <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/home"><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
        </li>
        <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/users"><span class="menu-title" data-i18n="nav.dash.main">User Management</span></a>
        </li>
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/users/client_management"><span class="menu-title" data-i18n="nav.dash.main">Memorial Page Management</span><span class="badge badge badge-info badge-pill float-right"><?php echo $new_memorials; ?></span></a>
        </li>
        <!--<li class="nav-item has-sub"><a href="#"><span class="menu-title">Featured Memorial</span></a>-->
        <li class=" nav-item"><a href="#"><span class="menu-title">Featured Memorial</span><span class="badge badge badge-info badge-pill float-right"><?php echo $new_featured_memo_tribute; ?></span></a>
          <ul class="menu-content">
            <li><a class="menu-item" href="<?php echo base_url(); ?>admin/featured/add_featured_memorial">Add Featured Memorial</a>
            </li>
            <li><a class="menu-item" href="<?php echo base_url(); ?>admin/featured">All Featured Memorial</a>
            </li>
          </ul>
        </li>
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/coupon"><span class="menu-title" data-i18n="nav.dash.main">Coupon Management</span><span class="badge badge badge-info badge-pill float-right"><?php echo $used_coupon; ?></span></a>
        </li>
           <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/content_management"><span class="menu-title" data-i18n="nav.dash.main">Content Management</span></a>
        </li>
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/music"><span class="menu-title" data-i18n="nav.dash.main">Music Management</span></a>
        </li>
           <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/profanity"><span class="menu-title" data-i18n="nav.dash.main">Profanity Management</span></a>
        </li>
           <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/pakage"><span class="menu-title" data-i18n="nav.dash.main">Package Management</span></a>
        </li>
          <!--<li class=" nav-item"><a href="tributes-management.html"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Tributes Management</span><span class="badge badge badge-info badge-pill float-right mr-2">2</span></a>
        </li>-->
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/testimonials"><span class="menu-title" data-i18n="nav.dash.main">Testimonials Management</span><span class="badge badge badge-info badge-pill float-right"><?php echo $new_testimonials; ?></span></a>
        </li>

         <!-- <li class=" nav-item"><a href="data-management.html"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Data Management</span></a>
          <ul class="menu-content">
            <li><a class="menu-item" href="users.html">Users</a>
            </li>
            <li><a class="menu-item" href="memorials.html">Memorials</a>
            </li>
               <li><a class="menu-item" href="#">Invoices</a>
            </li>
              <li><a class="menu-item" href="pages.html">Pages</a>
            </li>
              <li><a class="menu-item" href="#">Notifications</a>
            </li>
          </ul>
        </li>-->
          <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.dash.main">Reports</span></a>
        </li>
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/invoices"><span class="menu-title" data-i18n="nav.dash.main">Invoices</span></a>
          </li>
          
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/manage/payments"><span class="menu-title" data-i18n="nav.dash.main">Payments</span></a>
        </li>
          <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.dash.main">Analytics</span></a>
        </li>
          
          <li class=" nav-item"><a href="<?php echo base_url(); ?>admin/profile"><span class="menu-title" data-i18n="nav.dash.main">Profile</span></a>
        </li>
      </ul>
    </div>
  </div>