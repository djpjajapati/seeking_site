
<!--<div id="confirmModal" class="swal-overlay swal-overlay--show-modal" style="display: none" tabindex="-1">
  <div class="swal-modal"><div class="swal-icon swal-icon--warning">
    <span class="swal-icon--warning__body">
      <span class="swal-icon--warning__dot"></span>
    </span>
  </div><div class="swal-title" style="">Are you sure?</div><div class="swal-text" style="">You want to delete it.</div><div class="swal-footer"><div class="swal-button-container">

    <button class="swal-button swal-button--cancel">Cancel</button>

    <div class="swal-button__loader">
      <div></div>
      <div></div>
      <div></div>
    </div>

  </div><div class="swal-button-container">

    <button class="swal-button swal-button--confirm">Delete</button>

    <div class="swal-button__loader">
      <div></div>
      <div></div>
      <div></div>
    </div>

  </div></div></div></div>-->




<!---- Success Modal -->
  <div class="modal fade" id="SuccessModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Success!</h4>
        </div>
        <div class="modal-body">
		  <p style="color: green" id="suc_text"> Record updated Successfully! </p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Success Modal -->
 
 <!---- Error Modal -->
  <div class="modal fade" id="ErrorModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Error!</h4>
        </div>
        <div class="modal-body">
		  <p style="color: red" id="err_text"> Something went wrong! </p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Error Modal -->



<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 All rights reserved. </span>
    </p>
  </footer>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/extensions/sweet-alerts.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/tables/datatables/datatable-basic.js"
  type="text/javascript"></script>
 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

</body>


</html>


<!--<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 All rights reserved. </span>

    </p>
  </footer>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/pages/dashboard-ecommerce.min.js" type="text/javascript"></script>

  

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>  
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/extensions/sweet-alerts.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
  
  
  
  <script>
  
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	});
	
</script>
  
</body>
</html>-->