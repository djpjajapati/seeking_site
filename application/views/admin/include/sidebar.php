	<!-- BEGIN SIDEBAR -->

	<div class="page-sidebar-wrapper">

		<div class="page-sidebar navbar-collapse collapse">

			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->

				<li class="sidebar-toggler-wrapper">

					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					
					<div class="sidebar-toggler">
					
					</div>

					<!-- END SIDEBAR TOGGLER BUTTON -->

				</li>

			<?php $sess_data=$this->session->userdata('admin'); ?>			

                </br>
				
				<li class="<?php if($this->uri->segment(2)=='users' || $this->uri->segment(2)=='') {?> active open<?php } ?>">
				
				
					
							<a href="<?=base_url()?>admin/users">
							<i class="icon-user"></i>
							<span class="title">Users</span></a>
						
					<!--<a href="javascript:;">
					<i class="icon-users"></i>
					<span class="title">Manage Users</span>
					<span class="arrow <?php if($this->uri->segment(2)=="users" || $this->uri->segment(3) == 'add') {?> open<?php } ?> "></span>
					</a>-->
					<!--<ul class="sub-menu">
						<li <?php if($this->uri->segment(4)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>member/add">
							<i class="icon-user"></i>
							Add User (Hotel)</a>
						</li>
                        	<li <?php if($this->uri->segment(4)=='' || $this->uri->segment(4)=='index'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>member">
							<i class="icon-users"></i>
							View Users (Hotel)</a>
						</li>
					
					</ul>-->
					
					<!--<ul class="sub-menu">
						<li <?php if($this->uri->segment(2) == 'users' && $this->uri->segment(3)==''){echo "class='active'";} ?>>
							<a href="<?=base_url()?>admin/users">
							<i class="icon-user"></i>
							Users</a>
						</li>
                        
						<li <?php if($this->uri->segment(2) == 'users' && $this->uri->segment(3)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>admin/users/add">
							<i class="icon-users"></i>
							Add User</a>
						</li>
					
					</ul>-->
					
				</li>
				
                <!--<li class="<?php if($this->uri->segment(2)=='' || $this->uri->segment(2)=='home') {?> active open<?php } ?>">
					<a href="<?=base_url()?>admin/home">
						<i class="icon-user"></i>
						<span class="title">Products</span>
					</a>
					
					<a href="javascript:;">
					<i class="icon-users"></i>
					<span class="title">Manage Product</span>
					<span class="arrow <?php if($this->uri->segment(2)=="home" || $this->uri->segment(3) == 'add') {?> open<?php } ?> "></span>
					</a>-->
					<!--<ul class="sub-menu">
						<li <?php if($this->uri->segment(4)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>member/add">
							<i class="icon-user"></i>
							Add User (Hotel)</a>
						</li>
                        	<li <?php if($this->uri->segment(4)=='' || $this->uri->segment(4)=='index'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>member">
							<i class="icon-users"></i>
							View Users (Hotel)</a>
						</li>
					
					</ul>-->
					
					<!--<ul class="sub-menu">
						<li <?php if($this->uri->segment(2) == 'home' && $this->uri->segment(3)==''){echo "class='active'";} ?>>
							<a href="<?=base_url()?>admin/home">
							<i class="icon-user"></i>
							products</a>
						</li>
                        
						<li <?php if($this->uri->segment(2) == 'home' && $this->uri->segment(3)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>admin/home/add">
							<i class="icon-users"></i>
							Add Product</a>
						</li>
					
					</ul>->
					
				</li>-->
				
				

                 <li class="<?php if($this->uri->segment(2)=="category") {?> active open<?php } ?>">
					
					<a href="<?=base_url()?>admin/category">
							<i class="icon-user"></i>
							<span class="title">Categories</span>
					</a>
					
				 </li>
				 
				 
				 
				 
				 <!--<li class="<?php if($this->uri->segment(2)=="features") {?> active open<?php } ?>">
					<a href="javascript:;">
					<i class="icon-users"></i>
					<span class="title">Features</span>
					<span class="arrow <?php if($this->uri->segment(2)=="features") {?> open<?php } ?> "></span>
					</a>
					<ul class="sub-menu">
						<li <?php if($this->uri->segment(2)=='features' && $this->uri->segment(3)==''){echo "class='active'";} ?>>
							<a href="<?=base_url()?>admin/features">
							<i class="icon-user"></i>
							Features</a>
						</li>
                        	<li <?php if($this->uri->segment(3)=='add_feature'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>admin/features/add_feature">
							<i class="icon-users"></i>
							Add Feature</a>
						</li>
					
					</ul>
				 </li>-->

                <!--<li class="<?php if($this->uri->segment(2)=="services") {?> active open<?php } ?>">
					<a href="javascript:;">
					<i class="icon-shield"></i>
					<span class="title">Services Management</span>
					<span class="arrow <?php if($this->uri->segment(2)=="services") {?> open<?php } ?> "></span>
					</a>
					<ul class="sub-menu">
						<li <?php if($this->uri->segment(3)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>services/add">
							<i class="icon-shield"></i>
							Add Service</a>
						</li>
                        	<li <?php if($this->uri->segment(3)=='' || $this->uri->segment(3)=='index'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>services">
							<i class="icon-shield"></i>
							View Services</a>
						</li>
					
					</ul>
				 </li>
               <li class="start <?php if($this->uri->segment(2)=="orders") {?> active <?php } ?>">
					<a href="<?=base_url()?>orders">
					<i class="fa fa-shopping-cart"></i>
					<span class="title">Order History </span>
                    <?php if($this->uri->segment(2)=="orders") {?>
                    <span class="selected"></span>
                    <?php } ?>				
					</a>				
				</li>
                <li class="<?php if($this->uri->segment(2)=="jobs") {?> active open<?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-tasks"></i>
					<span class="title">Jobs</span>
					<span class="arrow <?php if($this->uri->segment(2)=="jobs") {?> open<?php } ?> "></span>
					</a>
					<ul class="sub-menu">
					
                        	<li <?php if($this->uri->segment(3)=='' || $this->uri->segment(3)=='index'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>jobs">
							<i class="fa fa-tasks"></i>
							All Jobs</a>
						   </li>
                           <li <?php if($this->uri->segment(3)=='Pending'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>jobs/pending">
							<i class="fa fa-tasks"></i>
							Pending Jobs</a>
						   </li>
                           <li <?php if($this->uri->segment(3)=='inprogress'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>jobs/inprogress">
							<i class="fa fa-tasks"></i>
							InProgress Jobs</a>
						   </li>
                           <li <?php if($this->uri->segment(3)=='complete'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>jobs/complete">
							<i class="fa fa-tasks"></i>
							Completed Jobs</a>
						   </li>
					
					</ul>
				 </li>
                 
                  <li class="<?php if($this->uri->segment(2)=="transaction") {?> active open<?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-money"></i>
					<span class="title">Transaction History</span>
					<span class="arrow <?php if($this->uri->segment(2)=="transaction") {?> open<?php } ?> "></span>
					</a>
					<ul class="sub-menu">
						<li <?php if($this->uri->segment(3)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>transaction/members">
							<i class="fa fa-money"></i>
							Members</a>
						</li>
                        	<li <?php if($this->uri->segment(3)=='' || $this->uri->segment(3)=='index'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>transaction">
							<i class="fa fa-money"></i>
							Associate</a>
						</li>
					
					</ul>
				</li>
                  <li class="start <?php if($this->uri->segment(2)=="withdraw") {?> active <?php } ?>">
					<a href="<?=base_url()?>withdraw">
					<i class="fa fa-money"></i>
					<span class="title">WithDraw Requests </span>
                    <?php if($this->uri->segment(2)=="withdraw") {?>
                    <span class="selected"></span>
                    <?php } ?>				
					</a>				
				</li>
                  <li class="start <?php if($this->uri->segment(2)=="attorney") {?> active <?php } ?>">
					<a href="<?=base_url()?>attorney">
					<i class="icon-users"></i>
					<span class="title">Attorneys</span>
                    <?php if($this->uri->segment(2)=="attorney") {?>
                    <span class="selected"></span>
                    <?php } ?>				
					</a>				
				</li>
                <li class="<?php if($this->uri->segment(2)=="blog") {?> active open<?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-rss"></i>
					<span class="title">Blog Management</span>
					<span class="arrow <?php if($this->uri->segment(2)=="blog") {?> open<?php } ?> "></span>
					</a>
					<ul class="sub-menu">
						<li <?php if($this->uri->segment(3)=='add'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>blog/add">
							<i class="fa fa-rss"></i>
							Add Blog</a>
						</li>
                        	<li <?php if($this->uri->segment(3)=='' || $this->uri->segment(3)=='index'){echo "class='active'";} ?>>
							<a href="<?=base_url()?>blog">
							<i class="fa fa-rss"></i>
							View Blogs</a>
						</li>
					
					</ul>
				</li>-->
                
                
				</ul>

			<!-- END SIDEBAR MENU -->

		</div>

	</div>

	<!-- END SIDEBAR -->