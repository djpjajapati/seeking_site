<?php $this->load->view("admin/include/header"); ?>
  
	<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
          <div class="row">
             <div class="col-md-6">
                <h2>Edit Package Management</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/manage/pakage"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
          </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Dropzone section start -->
        <section id="dropzone-examples">
                      <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form method="post" action="<?php echo base_url(); ?>admin/manage/update_pakage">
                      <div class="form-body">
                        
                        <div class="row">
                            <div class="col-md-6">
                            <label>Package Name</label>
                                  <input type="text" name="title" class="form-control" value="<?php echo $pakage->title; ?>" required>
                                  <input type="hidden" name="id" value="<?php echo $pakage->ID; ?>" />
                             </div>
                          <div class="col-md-6">
                              <label>Price</label>
                            <div class="form-group half-group">
                              <input type="number" min="1" name="price" class="form-control" value="<?php echo $pakage->price; ?>" required>
                            </div>
                            </div>
                              <div class="col-md-6">
                            <label>Images</label>
                              <div class="form-group half-group">
                              <input type="number" name="photo_limit" min="0" class="form-control" min="1" value="<?php echo $pakage->photo_limit; ?>" required>
                            </div>
                             </div>
                            <div class="col-md-6">
                            <label>Videos</label>
                             <div class="form-group">
                                    <div class="input-group">
                                      <input type='number' name="video_limit" min="0" class="form-control" value="<?php echo $pakage->video_limit; ?>" >
                                    </div>  
                                </div>
                          </div>
                            <div class="col-md-6">
                                <label>Music</label>
                            <div class="form-group">
                              <input type="number"  min="0" name="audio_limit" class="form-control" value="<?php echo $pakage->audio_limit; ?>" >
                            </div>
                          </div>
                             <div class="col-md-6">
                                <label>Admins</label>
                            <div class="form-group">
                              <input type="text" name="page_managers" class="form-control" min="0" value="<?php echo $pakage->page_managers; ?>">
                            </div>
                          </div>
						  
						   <div class="col-md-6">
                                <label>Maintenance Fee</label>
                            <div class="form-group">
                              <input type="number" name="annual_maintenance_fee" class="form-control" min="0" value="<?php echo $pakage->annual_maintenance_fee; ?>">
                            </div>
                          </div>
						  
                            <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                            </div>

                        </div> 
                      </div>
                       
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
         
        </section>

      </div>
    </div>
  </div>
					
<?php $this->load->view('admin/include/footer'); ?>