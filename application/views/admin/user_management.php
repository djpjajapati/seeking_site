<?php $this->load->view("admin/include/header"); ?>
  
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
  
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">

          <h2>User Management</h2>
       
        
      <div class="content-body">

        <section id="horizontal">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table id="tab_customers" class="table display nowrap table-striped responsive">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <!--<th>Page Owner URL</th>
                          <th>Package Details</th>-->
                          <th>Status</th>
                          <th>Email Verification</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        
						<?php foreach($users as $user){ ?>
						
							<tr id="user_<?php echo $user->ID; ?>">
							  <td><?php echo $user->fname.' '.$user->lname; ?></td>
							  <td><?php echo $user->email; ?></td>
							  <!--<td><?php //echo $user->page_url; ?></td>
							  <td><?php //echo $user->pakage; ?></td>-->
							  <td><span id="status_<?php echo $user->ID; ?>"><?php echo $user->status; ?></td>
							  <td><?php if($user->EmailVerification == '1') echo 'Yes'; else echo 'No'; ?></td>
							  <td>
								<span id="block_<?php echo $user->ID; ?>">
								  <a onclick="block_user('<?php echo $user->ID; ?>', '<?php echo $user->status; ?>')" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas <?php if($user->status == 'Active') echo 'fa-user'; else echo 'fa-user-slash'; ?> text-white"></i></a>
								</span>
								 <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="<?php echo base_url(); ?>admin/users/view_user_management/<?php echo $user->ID; ?>"><i class="fas fa-eye text-white"></i></a>
								 <!--<a onclick="delete_user('<?php echo $user->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" id="cancel-button" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> -->
							  </td>
							</tr>
						
						
						<?php } ?>
						
						
						<script>
						
						function block_user(user_id, status){
							
							var new_status = 'Active';
							var new_class = 'fas fa-user text-white';
						
							if(status == 'Active'){
								new_status = 'Inactive';
								new_class = 'fas fa-user-slash text-white';
							}
							
							$.ajax({
								url:"<?php echo base_url(); ?>admin/users/update_user_status",
								type: "POST",
								data:{"user_id": user_id, 'new_status': new_status},
								success:function(resp)
								{
									if(resp == 'success'){
										$("#status_"+user_id).text(new_status);
										
										
										var params = "'"+user_id+"',"+"'"+new_status+"'";
										
										$("#block_"+user_id).html('');
										$("#block_"+user_id).html('<a onclick="block_user('+params+')" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="'+new_class+'"></i></a>');
										
										
										$("#suc_text").text('User is '+new_status+' Now');
										$("#SuccessModal").modal('show');
									}else{
										$("#err_text").text("Somthing went wrong!");
										$("#ErrorModal").modal('show');
									}
								}
							});
						}
						
						function delete_user(id){
							
							if(confirm("Are you sure to delete this user?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/users/delete_user",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#user_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
						
						</script>
						
						<!--<tr>
                          <td>Emma Redshaw</td>
                          <td>emma.red987@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="cancel-button" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>zoey.amanda009@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                             <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>james.noah@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                           <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                           <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                           <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>emma.red987@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>zoey.amanda009@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                           <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>james.noah@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                            <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>emma.red987@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                           <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>zoey.amanda009@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>james.noah@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>emma.red987@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                             <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>zoey.amanda009@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                            <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>james.noah@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                       <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>emma.red987@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                             <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>zoey.amanda009@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>james.noah@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                            <tr>
                          <td>Emma Redshaw</td>
                          <td>emma.red987@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                             <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>Zoey Amanda</td>
                          <td>zoey.amanda009@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Basic</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                           <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                          <tr>
                          <td>James Noah</td>
                          <td>james.noah@gmail.com</td>
                          <td>www.abc.com</td>
                          <td>Premium</td>
                          <td>Active</td>
                          <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>
                           <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="view-user-management.html"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" id="type-error" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>-->
        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       
      </div>
    </div>
  </div>
    </div>
  
  
<?php $this->load->view('admin/include/footer'); ?>

<script type="text/javascript">
        $(document).ready(function() {
			$('#tab_customers').DataTable( {
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					}
				]
				
				
				/* buttons: [
					'excelHtml5',
					'csvHtml5',
					'pdfHtml5'
				] */
			} );
		});
    </script>