<?php $this->load->view("admin/include/header"); ?>
  
  
 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Add Coupon</h3>
        </div>
        <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/coupon"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
        <div class="col-md-6"><select style="margin-bottom: 5px" class="form-control" name="coupontype" id="coupontype" onChange="changectype();"><option value="one">One Coupon</option><option value="bulkc">Bulk Coupons</option></select></div>
      </div>
      <div class="content-body">
        <!-- Form repeater section start -->
        <section>
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse singlecoupon show">
                 <h3 style="padding: 10px">Single Coupon</h3>
                  <div class="card-body">
                    <div class="">
                      <div >
                        <div>
                          <form method="post" action="<?php echo base_url(); ?>admin/coupon/do_add_coupon" class="">
                              
                                <?php if($this->session->flashdata('error')){ ?>
                                    <div class="alert alert-block  alert-danger">
                                        <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php } ?>
                              
                              
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupon Code</label>
                              <br>
                              <input type="text" name="coupon_code" class="form-control" required placeholder="Enter Code">
                            </div>
                              <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupon Discount Percentage</label>
                              <br>
                              <input type="number" min="1" max="100" name="coupon_discount_percentage" required class="form-control" placeholder="Enter Discount">
                            </div>
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Frequency</label>
                              <br>
                              
                                      <input type="number" name="frequency" min="1" class="form-control" required  placeholder="Coupon usage time"/>
                                    
                            </div>
                               <div class="form-group col-md-6">
                              <label for="bio" class="cursor-pointer">Expiry Date</label>
                              <br>
                                  <div class="form-group">
                                 
                                      <input type="date" name="expiry" class="form-control pickadate" required placeholder=" Pick-a-date"
                                      />
                                    
                                </div>

                            </div>
                            
                            <div class="form-group overflow-hidden">
                                <div class="col-12">
                                  <button type="submit" class="btn btn-primary">
                                    Add Coupon
                                  </button>
                                   
                                </div>
                          </div>
                            
                            
                          </form>
                          <hr>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                
                <div class="card-content bulkcoupon collapse hide">
                 <h3 style="padding: 10px">Bulk Coupons</h3>
                  <div class="card-body">
                    <div class="">
                      <div >
                        <div>
                          <form method="post" action="<?php echo base_url(); ?>admin/coupon/do_add_Bcoupon" class="">
                              
                                <?php if($this->session->flashdata('error')){ ?>
                                    <div class="alert alert-block  alert-danger">
                                        <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php } ?>
                              
                              
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupon Quantity</label>
                              <br>
                              <input type="text"  min="2" max="100"  name="coupon_quantity" class="form-control" required placeholder="How Many?">
                            </div>
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupon Code Prefix</label>
                              <br>
                              <input type="text" name="coupon_code" class="form-control" required placeholder="Enter Code">
                            </div>
                              <div class="form-group">
                              <label for="bio" class="cursor-pointer">Coupon Discount Percentage</label>
                              <br>
                              <input type="number" min="1" max="100" name="coupon_discount_percentage" required class="form-control" placeholder="Enter Discount">
                            </div>
                            <div class="form-group">
                              <label for="bio" class="cursor-pointer">Frequency</label>
                              <br>
                              
                                      <input type="number" name="frequency" min="1" class="form-control" required  placeholder="Coupon usage time"/>
                                    
                            </div>
                               <div class="form-group col-md-6">
                              <label for="bio" class="cursor-pointer">Expiry Date</label>
                              <br>
                                  <div class="form-group">
                                 
                                      <input type="date" name="expiry" class="form-control pickadate" required placeholder="Pick-a-date"
                                      />
                                    
                                </div>

                            </div>
                            
                            <div class="form-group overflow-hidden">
                                <div class="col-12">
                                  <button type="submit" class="btn btn-primary">
                                    Add Bulk Coupon
                                  </button>
                                   
                                </div>
                          </div>
                            
                            
                          </form>
                          <hr>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
     
      </div>
    </div>
  </div>
  <script>
function changectype()
	  {
		  var whattoshow=$("#coupontype").val();
		  if(whattoshow=="one")
			  {
				  $(".singlecoupon").removeClass("hide");
				  $(".singlecoupon").addClass("show");
				  $(".bulkcoupon").removeClass("show");
				  $(".bulkcoupon").addClass("hide");
			  }
		  else
			  {
				  $(".singlecoupon").removeClass("show");
				  $(".singlecoupon").addClass("hide");
				  $(".bulkcoupon").removeClass("hide");
				  $(".bulkcoupon").addClass("show");
			  }
	  }
</script>
  
  
<?php $this->load->view('admin/include/footer'); ?>