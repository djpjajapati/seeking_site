<?php $this->load->view("include/header.php"); ?>

<?php header('Access-Control-Allow-Origin: *'); ?>

<section class="package_sec1 package_sec1-new2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>You’re almost there...</h1>
                <p>Before completing your order please select the desired payment method.</p>
            </div>
        </div>
    </div>
</section>

<section class="payment_method">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pad_right9">
                <div class="payment_box-new payment_box-new1">
                    <h3>Your Selection:</h3>
                    <h4><span id="pakage_title"><?php echo $pakage->title; ?> </span>PACKAGE</h4>
                    <h5><span id="price_to_pay"><?php echo $pakage->price; ?>.00</span></h5>
                   
					<?php if($this->uri->segment(2) != 'proceed_to_renew'){ ?>
					
						<a href="<?php echo base_url(); ?>memorial/upgrade/<?php echo $memo->ID; ?>" class="red_btn selectn_btn">Change Selection</a>
                
					<?php } ?>
				</div>
            </div>
            <div class="col-md-8 pad_left9">
                <div class="payment_box-new">
                    <div class="payment-inner">
                      <h3>Your Payment Method:</h3>
                      <div class="row">
                          <div class="col-lg-2 col-md-4 col-6">
                            <div class="radio">
                                <input type="radio" id="inlineRadio1" value="mpesa" name="radioInline" checked="">
                                <label for="inlineRadio1"> Mpesa </label>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-6">
                              <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/mpesa.png">
                          </div>
                          <div class="col-lg-7 col-md-12 col-12">
                            <div class="form_country">
                             <input id="phone_number" type="number" class="form-control" disabled name="phone_number" value="1233456746" placeholder="Your Phone Number">
                             <span class="main_span"><img alt="img" src="<?php echo base_url(); ?>assets/img/Kenya-Emoji-Flag.png"><span class="inner_span">Kenya</span>  +254</span>
                            </div> 
                          </div>
                      </div>
                      <div class="space40"></div>
                      <div class="row">
                          <div class="col-md-6">
                              <p class="payment_text">Enter the Safaricom number you wish to make payment with. A request to authorize this transaction will be sent to your phone from Safaricom. Please complete it to complete payment.</p>
                          </div>
                          <div class="col-md-6" >
                              <ul class="list-inline">
                                  <li class="list-inline-item click_text">Click to send a verification code to your phone.</li>
                                  <li class="list-inline-item">
									<!--<a href="javascript:;" class="send_code"><button id="send_code" onclick="pay_with_mpesa()">Send Code </button></a>-->
									<input type="button" disabled class="send_code" style="line-height: 25px; cursor: pointer" id="send_code" onclick="pay_with_mpesa()" value="Send Code" />
								  </li>
                              </ul>
                          </div>
                      </div>
                    </div>
                    <hr>
                    
					<script>
					
					function pay_with_mpesa(){
						
						var phone_number = $("#phone_number").val();
						if(phone_number.length == 10){
							$("#phone_mpesa").val(phone_number);
							$("#myModal1").modal('show');
						}else{
							$("#err_text").text("Please enter a 10 digit phone number");
							$("#ErrorModal").modal('show');
						}
						
						
					}
					
					</script>
					
					<div class="payment-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-7 pad_right0">
                                <div class="radio radio_pad">
                                    <input type="radio" id="inlineRadio2" value="card" name="radioInline" checked="">
                                    <label for="inlineRadio2"> Credit Card / Debit Card </label>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-5 pad_left0">
                                <ul class="list-inline list_pay">
                                    <li class="list-inline-item"><a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/visa.png"></a></li>
                                    <li class="list-inline-item"><a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/mastercard.png"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="space30"></div>
                        <div class="row">
                            <div class="col-lg-7 col-md-6">
                                <div class="input_text">
                                    <input type="text" name="fname" class="form-control" value="<?php echo $user->fname; ?>" placeholder="First Name">
                                    <input type="text" name="lname" class="form-control" value="<?php echo $user->lname; ?>" placeholder="Last Name">
                                    <input type="text" name="email" class="form-control" value="<?php echo $user->email; ?>" placeholder="Email">
                                    
									
									<span id="coupon_error" style="color: red; display: none">Sorry! Coupon Code is not valid</span>
									<span id="coupon_success" style="color: green; display: none">Coupon Code is applied</span>
									</br>
									<span style="display: flex;">
										<input type="text" name="coupon" id="coupon" class="form-control" value="" placeholder="Coupon Code"> 
										<a onclick="apply_coupon()" style="margin-left: 10px;" href="javascript:;" class="send_code pull-right"> Apply Coupon</a>
                                   </span>
								   
								   
								   </br>
								   
								   <a href="javascript:;" onclick="open_modal('<?php echo $pakage->ID; ?>')" class="send_code">Confirm Pay</a>
																   
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <!--<div class="list-inline list_pay">
                                    <label for="sel1">Expiration</label>
                                    <select class="form-control select_class" id="sel1">
                                        <option>Month</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                    <select class="form-control" id="sel2">
                                        <option>Year</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>-->
                                <!--<a href="" class="send_code pull-right">Confirm Pay</a>-->
								
								
								
                            </div>
                        </div>

                    </div>
					
					
                </div>
            </div>
        </div>
    </div>
</section>








<!-- Modal -->
<div class="modal purchase_modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Payment Confirmation</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>You have selcted:</p>
        <h4 id="title"></h4>
        <h3><span style="font-size: 56px;" id="price"><?php echo $pakage->price; ?></span>.00</h3>
		
		<form id="ipay_form" method="post" action="https://payments.ipayafrica.com/v3/ke">
		
		<input type="hidden" name="live" value="0">
		<input type="hidden" name="oid" value="<?php echo time().rand(0,9997); ?>">
		<input type="hidden" name="inv" value="<?php echo time().rand(0,1234); ?>">
		<input type="hidden" name="ttl" id="price1" value="<?php echo $pakage->price; ?>">
		<input type="hidden" name="tel" value="256712375678">
		<!--<input type="hidden" name="eml" value="james.noah1172@gmail.com">-->
		<input type="hidden" name="eml" value="<?php echo $user->email; ?>">
		<input type="hidden" name="vid" value="gt14">
		<input type="hidden" name="curr" value="KES">
		<input type="hidden" name="p1" id="plan_id" value="1">
		<input type="hidden" name="p2" value="<?php echo $user->ID; ?>">
		<input type="hidden" name="p3" value="<?php echo $memo->ID; ?>">
		<input type="hidden" name="p4" value="<?php echo $user->lname; ?>">
		<input type="hidden" name="cbk" value="<?php echo base_url(); ?>memorial/upgrade_callback">
		<input type="hidden" name="cst" value="1">
		<input type="hidden" name="crl" value="0">
		<input name="hsh" type="hidden" id="hsh" value="">
		<br>
		
		<!--<button type="submit">&nbsp;Submit&nbsp;</button>-->
		<a href="javascript:;" data-dismiss="modal" class="cancel">cancel</a>
        <a href="javascript:;" onclick="generate_hash_upgrade()" class="proceed blue_bg">proceed</a>
		
		</form>
		
		
      </div>

      <!-- Modal footer -->
    </div>
  </div>
</div>
<!-- Modal -->



<!-- Modal -->
<div class="modal purchase_modal" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Mpesa Payment Confirmation</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>You have selcted:</p>
        <h4 id="title"></h4>
        <h3><span style="font-size: 56px;" id="price_mpesa1"><?php echo $pakage->price; ?></span>.00</h3>
		
		<!--<form id="ipay_form" method="post" action="https://payments.ipayafrica.com/v3/ke">-->
		
		<div class="alert alert-danger" id="err_message" style="display: none">
			<span class="resp_message"></span>
		</div>
		
		<div class="alert alert-success" id="suc_message" style="display: none">
			<span class="resp_message"></span>
		</div>
		
		<form id="ipay_form" method="post" action="<?php echo base_url(); ?>lnme/LipaNaMpesa">
		
		
		<input type="hidden" name="price_mpesa" id="price_mpesa" value="<?php echo $pakage->price; ?>">
		<input type="hidden" name="phone_mpesa" id="phone_mpesa" value="">
		
		<a href="javascript:;" data-dismiss="modal" class="cancel">cancel</a>
        <a id="pay_now_mpesa" href="javascript:;" onclick="pay_now_mpesa()" class="proceed blue_bg">proceed</a>
		
		</form>
		
      </div>

      <!-- Modal footer -->
    </div>
  </div>
</div>
<!-- Modal -->


<script>

	function pay_now_mpesa(){
		
		var pakage_title = $("#pakage_title").text();
		
		//alert(pakage_title); return; 
		
		var p1 = '<?php echo $pakage->ID; ?>';
		var p2 = '<?php echo $user->ID; ?>';
		var p3 = '<?php echo $memo->ID; ?>';
		var ttl = '<?php echo $pakage->price; ?>';
		var memo_id = '<?php echo $this->uri->segment(3); ?>'; 
		
		var amount = $("#price_mpesa").val();
		var phone_mpesa = $("#phone_mpesa").val();
		$.ajax({
				url: "<?php echo base_url(); ?>lnme/LipaNaMpesa_upgrade",
				type: "POST",
				data: {"memo_id": memo_id, "p1": p1, "p2": p2, "p3": p3, "ttl": ttl, "pakage_title": pakage_title, "amount":amount, "phone_mpesa":phone_mpesa},
				success: function (res) {
					var resp = JSON.parse(res);
					//alert(resp.errorCode);
					if(resp.errorCode){
					   
					   $(".resp_message").text(resp.errorMessage);
					   $("#suc_message").hide();
					   $("#err_message").show();
					  
					}else if(resp.ResponseCode == 0){
						$(".resp_message").text(resp.ResponseDescription+', Please check your phone to confirm payment');
						$("#err_message").hide();
						$("#suc_message").show();
						
						setInterval(function(){ check_for_callback(); }, 3000);
					}
					
					console.log(resp);	
					
					/* if($("#hsh").val() != ''){
						// reduce coupon if used
						var coupon_code = $("#coupon").val();
						if(coupon_code != ''){
							reduce_coupon(coupon_code);
						}
						// reduce coupon if used
						//$("#ipay_form").submit();
					} */
				}
			});
		
	}
	
	function check_for_callback(){
		
		alert("checking for callback");
		
		/* $.ajax({
				url: "<?php echo base_url(); ?>memorial/apply_coupon",
				type: "POST",
				data: {"coupon":coupon, "price":price},
				success: function (resp) {					
					console.log(resp);		
				}
		}); */
		
	}

	function apply_coupon(){
		
		var coupon = $("#coupon").val();
		var price = '<?php echo $pakage->price; ?>';
		
		if(coupon == ''){
			$("#coupon_error").text("Please enter coupon code");
			$("#coupon_error").show();
			return false;
		}
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/apply_coupon",
				type: "POST",
				data: {"coupon":coupon, "price":price},
				success: function (resp) {
					if(resp == 'invalid'){
						$("#coupon_error").text("Sorry! Coupon Code is not valid");
						$("#coupon_error").show();
					}else{
						$("#coupon_error").hide();
						$("#coupon_success").show();
						
						$("#price_to_pay").text(resp+'.00');
						$("#price").text(resp);
						$("#price1").val(resp);
						
						
						//for mpesa popup
						
						$("#price_mpesa1").text(resp);
						$("#price_mpesa").val(resp);
					}
					
					console.log(resp);	
					
				}
		});
		
	}

	function generate_hash_upgrade(){
		
		var live = $("input[name=live]").val();
		var oid = $("input[name=oid]").val();
		var inv = $("input[name=inv]").val();
		var ttl = $("input[name=ttl]").val();
		var tel = $("input[name=tel]").val();
		var eml = $("input[name=eml]").val();
		var vid = $("input[name=vid]").val();
		var curr = $("input[name=curr]").val();
		var p1 = $("input[name=p1]").val();
		var p2 = $("input[name=p2]").val();
		var p3 = $("input[name=p3]").val();
		var p4 = $("input[name=p4]").val();
		var cbk = $("input[name=cbk]").val();
		var cst = $("input[name=cst]").val();
		var crl = $("input[name=crl]").val();
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/generate_hash_upgrade",
				type: "POST",
				data: {"live":live, "oid":oid, "inv":inv, "ttl":ttl, "tel":tel, "eml":eml, "vid":vid, "curr":curr, "p1":p1, "p2":p2, "p3":p3, "p4":p4, "cbk":cbk, "cst":cst, "crl":crl},
				success: function (resp) {
					console.log(resp);	
					$("#hsh").val(resp);
					if($("#hsh").val() != ''){
						
						// reduce coupon if used
						var coupon_code = $("#coupon").val();
						if(coupon_code != ''){
							reduce_coupon(coupon_code);
						}
						// reduce coupon if used
						
						$("#ipay_form").submit();
					}
				}
			});
	}
	
	function reduce_coupon(coupon_code){
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/reduce_coupon",
				type: "POST",
				data: {"coupon_code":coupon_code},
				success: function (resp) {
					
				}
			});
		
	}
				
	function open_modal(id){
		
		//var title = '<?php echo $pakage->title; ?>'; //$("#title_"+id).text();
		//var price = '<?php echo $pakage->price; ?>'; //$("#price_"+id).text();
		$("#plan_id").val(id);
		
		//$("#title").text(title);
		//$("#price").text(price);
		//$("#price1").val(price);
		$("#myModal").modal('show');
		
	}
	
</script>






<?php $this->load->view("include/footer"); ?>


<script>

$('input[type=radio][name=radioInline]').change(function() {
    if (this.value == 'mpesa') {
		$("#send_code").prop('disabled', false);
        $("#phone_number").prop('disabled', false);
    }else {
        $("#phone_number").prop('disabled', true);
		$("#phone_number").val("");
		$("#send_code").prop('disabled', true);
    }
});

</script>
