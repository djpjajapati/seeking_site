<?php $this->load->view("admin/include/header"); ?>
  
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- eCommerce statistic -->
        <div class="row">
          <div class="col-xl-3 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-center">
                      <h3 class="info"><?php echo $total_memorials; ?></h3>
                      <h6>Memorials</h6>
                    </div>
                    <!--<div>
                      <i class="icon-basket-loaded info font-large-2 float-right"></i>
                    </div>-->
                  </div>
                  <!--<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 80%"
                    aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>-->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-center">
                      <h3 class="warning"><?php echo $total_members; ?></h3>
                      <h6>Members</h6>
                    </div>
                   <!-- <div>
                      <i class="icon-pie-chart warning font-large-2 float-right"></i>
                    </div>-->
                  </div>
                  <!--<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 65%"
                    aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>-->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-center">
                      <h3 class="success"><?php echo $total_login; ?></h3>
                      <h6>Logins</h6>
                    </div>
                   <!-- <div>
                      <i class="icon-user-follow success font-large-2 float-right"></i>
                    </div>-->
                  </div>
                <!--  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
                    aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>-->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-center">
                      <h3 class="danger"><?php echo $total_tributes; ?></h3>
                      <h6>Tributes</h6>
                    </div>
                    <!--<div>
                      <i class="icon-heart danger font-large-2 float-right"></i>
                    </div>-->
                  </div>
                <!--  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 85%"
                    aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>-->
                </div>
              </div>
            </div>
          </div>
        </div>
<div class="row match-height" >
          <div class="col-xl-7 col-12">
               <div class="card">
              <div class="card-header">
                <h4 class="card-title">Latest Memorials</h4>
                   <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a class="btn btn-sm btn-danger box-shadow-2 btn-min-width pull-right"
                      href="<?php echo base_url(); ?>admin/users/client_management">All Memorials</a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">
                <div id="new-orders" class="media-list position-relative">
                  <div class="table-responsive" >
                    <table id="new-orders-table" class="table table-hover table-xl mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">For</th>
                          <th class="border-top-0">Package</th>
                          <th class="border-top-0">Date</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					  <?php foreach($latest_memorials as $m){ ?>
					  
								<tr>
								  <td class="text-truncate"><?php echo $m->fname.' '.$m->lname; ?></td>
								  <td class="text-truncate p-1"><?php echo $m->pakage; ?></td>
								  <td class="text-truncate"><?php echo date('d-m-Y', strtotime($m->createdAt)); ?></td>
								</tr>
					  
					  <?php } ?>
                        <!--<tr>
                          <td class="text-truncate">John Doe</td>
                          <td class="text-truncate p-1">Basic</td>
                          <td class="text-truncate">20th-Jan-2019</td>
                        </tr>
                        <tr>
                          <td class="text-truncate">John Doe</td>
                          <td class="text-truncate p-1">Basic</td>
                          <td class="text-truncate">20th-Jan-2019</td>
                        </tr>
                        <tr>
                          <td class="text-truncate">John Doe</td>
                          <td class="text-truncate p-1">Basic</td>
                          <td class="text-truncate">20th-Jan-2019</td>
                        </tr>
                        <tr>
                          <td class="text-truncate">John Doe</td>
                          <td class="text-truncate p-1">Basic</td>
                          <td class="text-truncate">20th-Jan-2019</td>
                        </tr>
                        <tr>
                          <td class="text-truncate">John Doe</td>
                          <td class="text-truncate p-1">Basic</td>
                          <td class="text-truncate">20th-Jan-2019</td>
                        </tr>
                          <tr>
                          <td class="text-truncate" style="max-width:100px; ">JohnDoeJohDoeJohnDoeJohnDoeJohnDoeDoeJohnDoeJohnDoeJohnDoe</td>
                          <td class="text-truncate p-1">Basic</td>
                          <td class="text-truncate">20th-Jan-2019</td>
                        </tr>-->
                         
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
                    
            </div>
              
          </div>
          <div class="col-xl-5 col-lg-12">
            <div class="card">
             <div class="card-header">
                <h4 class="card-title">Recent Activities</h4>
                  <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a class="btn btn-sm btn-danger box-shadow-2 btn-min-width pull-right"
                      href="<?php echo base_url(); ?>admin/home/activities">All Activities</a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">
                <div id="new-orders" class="media-list position-relative">
                  <div class="table-responsive">
                    <table id="new-orders-table" class="table table-hover table-xl mb-0 ">
                      <thead>
                        <tr>
                          <th class="border-top-0">Detail</th>
                          <th class="border-top-0">Date</th>
                        </tr>
                      </thead>
                          <tbody>
						  
						  <?php foreach($activity_log as $act){ ?>
						  
								<tr>
                                  <td class="" style="max-width:243px; "><span><?php echo $act->description; ?></span></td>
                                  <td class="text-truncate"><?php echo date('d-m-Y', strtotime($act->createdAt)); ?></td>
                                </tr>
						  
						  
						  <?php } ?>
						  
                                <!--<tr>
                                  <td class="text-truncate" style="max-width:243px; ">Uploade a new video to the memorial for John Doe <br><span>> Sam Amira</span></td>
                                  <td class="text-truncate">20th-Jan-2019</td>
                                </tr>
                                <tr>
                                 <td class="text-truncate" style="max-width:243px; ">Uploade a new video to the memorial for John Doe <br><span>> Sam Amira</span></td>
                                  <td class="text-truncate">20th-Jan-2019</td>
                                </tr>
                                <tr>
                                 <td class="text-truncate" style="max-width:243px; ">Uploade a new video to the memorial for John Doe <br><span>> Sam Amira</span></td>
                                  <td class="text-truncate">20th-Jan-2019</td>
                                </tr>
                              <tr>
                              <td class="text-truncate" style="max-width:243px; ">Uploade a new video to the memorial for John Doe <br><span>> Sam Amira</span></td>
                                  <td class="text-truncate">20th-Jan-2019</td>
                                </tr>-->
                              
                               

                            </tbody>
                    </table>
                  </div>
                </div>
              </div>
                 
            </div>
             
          </div>
        </div>
       <div class="row match-height" >
           <div class="col-xl-4 col-12">
          <div id="recent-transactions" class="col-12" >
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Top 5 Memorials</h4>
              </div>
              <div class="card-content">
                <div class="table-responsive">
                  <table id="recent-orders" class="table table-hover table-xl mb-0">
                    <thead>
                      <tr>
                        <th class="border-top-0">For</th>
                        <th class="border-top-0">Views</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php foreach($top_memorials as $m){ ?>
					
					<tr>
                        <td class="text-truncate"><?php echo $m->fname.' '.$m->lname; ?></td>
                       <td>
                          <div class="progress progress-sm mt-1 mb-0" style="height: 0.9rem !important;width: 100%">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: <?php if($m->views > 100) echo '100'; else echo $m->views; ?>%;"
                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div><p><?php echo $m->views; ?></p>
                          </div>
                        </td>
                      </tr>
					
					<?php } ?>
				   
					  <!--
					  <tr>
                        <td class="text-truncate">John Doe Amin</td>
                       <td>
                          <div class="progress progress-sm mt-1 mb-0" style="height: 0.9rem !important;width: 100%">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 25%;"
                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div><p>12345</p>
                          </div>
                        </td>
                      </tr>
                    <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td>
                          <div class="progress progress-sm mt-1 mb-0 " style="height: 0.9rem !important;width: 100%">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 25%;"
                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div><p>12345</p>
                          </div>
                        </td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td>
                          <div class="progress progress-sm mt-1 mb-0 " style="height: 0.9rem !important;width: 100%">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 25%;"
                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div><p>12345</p>
                          </div>
                        </td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                       <td>
                          <div class="progress progress-sm mt-1 mb-0 " style="height: 0.9rem !important;width: 100%">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 25%;"
                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div><p>12345</p>
                          </div>
                        </td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td>
                          <div class="progress progress-sm mt-1 mb-0" style="height: 0.9rem !important;width: 100%">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 25%;"
                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div><p>12345</p>
                          </div>
                        </td>
                      </tr>-->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
           </div>
           <div class="col-xl-8 col-12">
          <div id="recent-transactions" class="col-12" >
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Recent Tributes</h4>
              </div>
              <div class="card-content">
                <div class="table-responsive">
                  <table id="recent-orders" class="table table-hover table-xl mb-0">
                    <thead>
                      <tr>
                        <th class="border-top-0">For</th>
                        <th class="border-top-0">Tributes</th>
                        <th class="border-top-0">By</th>
                        <th class="border-top-0">On</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php foreach($recent_tributes as $t){  ?>
						
						<tr>
							<td class="text-truncate"><?php echo $t->memo_name; ?></td>
							<td class="text-truncate"><?php echo ucfirst($t->action); ?></td>
							<td class="text-truncate"><?php echo $t->by; ?></td>
							<td class="text-truncate"><?php echo date('d-m-Y', strtotime($t->createdAt)); ?></td>
						  </tr>
						
					<?php } ?>
					
                      <!--<tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">Candle</td>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">20th-Jan-2019</td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">Candle</td>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">20th-Jan-2019</td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">Candle</td>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">20th-Jan-2019</td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">Candle</td>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">20th-Jan-2019</td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">Candle</td>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">20th-Jan-2019</td>
                      </tr>
                        <tr>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">Candle</td>
                        <td class="text-truncate">John Doe Amin</td>
                        <td class="text-truncate">20th-Jan-2019</td>
                      </tr>-->
                
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
           </div>
          </div>
      </div>
    </div>
  </div>
  
  
  
  
<?php $this->load->view('admin/include/footer'); ?>