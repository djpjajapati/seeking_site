<?php $this->load->view("admin/include/header"); ?>
  
	
	 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
                 <div class="row">
             <div class="col-md-6">
                <h2>Music Management</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/manage/add_music"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Add</button></a>
             </div>
          </div>
      <div class="content-body">
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Title</th>
                          <th>File</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>

						<?php foreach($music as $m){ ?>
								
								<tr id="m_<?php echo $m->ID; ?>">
								  <td><?php echo $m->title; ?></td>
									<td><?php echo $m->file; ?></td>
								  <td class="text-center">
									 <a onclick="delete_music('<?php echo $m->ID; ?>', '<?php echo $m->file; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
								</tr>
				

						 <?php } ?>

						<script>
						
							function delete_music(id, aud){
								
								if(confirm("Are you sure to delete this user?")){
									
									$.ajax({
										url:"<?php echo base_url(); ?>admin/manage/delete_music",
										type: "POST",
										data:{"id": id, "aud": aud},
										success:function(resp)
										{
											if(resp == 'success'){
												$("#m_"+id).remove();
											}else{
												$("#err_text").text("Somthing went wrong!");
												$("#ErrorModal").modal('show');
											}
										}
									});
									
								}
								
								
							}
						
						</script>


                        <!--<tr>
                          <td>abc</td>
                            <td>6 mins</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                        </tr>
                           <tr>
                          <td>abc</td>
                                <td>3 mins</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>4 mins</td>
                          <td class="text-center">
                          <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>5 mins</td>
                          <td class="text-center">
                            <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>2 mins</td>
                         <td class="text-center">
                            <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>3 mins</td>
                          <td class="text-center">
                              <a class="btn btn-danger btn-xs py-md-1 px-md-1"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>5 mins</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>6 mins</td>
                          <td class="text-center">
                            <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>1 mins</td>
                          <td class="text-center">
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                           <tr>
                          <td>abc</td>
                               <td>2 mins</td>
                         <td class="text-center">
                           <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>-->
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Zero configuration table -->
       
      </div>
    </div>
  </div>
					
<?php $this->load->view('admin/include/footer'); ?>