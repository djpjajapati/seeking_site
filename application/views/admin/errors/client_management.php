<?php $this->load->view("admin/include/header"); ?>
  
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
   

  
	 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
         <div class="row">
             <div class="col-md-6">
                <h2>Memorial Page Management</h2>
             </div>
          </div>
      <div class="content-body">

        <section id="horizontal">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header pb-md-0">
                      <div class="row">
                      <div class="col-md-6">
                          <!--<div class="col-md-3">
                          <select class="form-control" id="basicSelect">
                            <option>Select</option>
                            <option>Amsterdam</option>
                          </select>
                          </div>-->
                      </div>
                      <!--<div class="col-md-6 text-right">
                          <strong>Export As:</strong>
						<button type="button" onclick="javascript:demoFromHTML()" class="btn btn-dark square btn-min-width mr-1 mb-1">PDF</button>
                      </div>-->
                      </div>
                  </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <!--<table id="tab_customers" class="table display nowrap table-striped table-bordered scroll-horizontal">-->
                    <table id="tab_customers" class="table display nowrap table-striped responsive">
                      <thead>
                        <tr>
                          <th>Page URL</th>
                          <th>Created Date</th>
                          <th>Owner's Name</th>
                          <th>Owner's Email</th>
                          <th>Memorial Package</th>
						  <th>Package Expiry Date</th>
                          <th>Memorial Visibility</th>
                          <th>Page Managers</th>
                          <th>Tributes</th>
                          <th>Uploaded<br>Images</th>
                          <th>Uploaded<br>Videos</th>
                          <th>Uploaded<br>Music</th>
                          <!--<th>Featured Memorial</th>-->
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                       

						<?php foreach($users as $user){ ?>
						
							<tr id="user_<?php echo $user->ID; ?>">
							  <td><?php echo base_url().'In_Memory_Of/'.$user->page_url; ?></td>
							  <td><?php echo date('d-m-Y', strtotime($user->created_date)); ?></td>
							  
							  
							  <td><?php echo $user->fname.' '.$user->lname; ?></td>
							  <td><?php echo $user->email; ?></td>
							  <td>
								  <select onchange="update_pakage(this.value, '<?php echo $user->memo_id; ?>')" class="form-control form-control-sm">
										  <option value="BASIC" <?php if($user->pakage == 'BASIC') echo 'selected'; ?>>Basic</option>
										  <option value="PREMIUM" <?php if($user->pakage == 'PREMIUM') echo 'selected'; ?>>Premium</option>
								  </select></td>
								  
							  <td><?php echo date('d-m-Y', strtotime($user->created_date.' +1 years')); ?></td>  
							  <td><?php echo $user->visibility; ?></td>
							  <td><?php echo $user->page_managers; ?></td>
							  <td><?php echo $user->tributes_uploaded; ?></td>   
							  <td><?php echo $user->pictures_uploaded; ?></td>
							  <td><?php echo $user->videos_uploaded; ?></td>
							  <td><?php echo $user->music_uploaded; ?></td>
								   <!--<td>
								  <select onchange="update_featured(this.value, '<?php echo $user->memo_id; ?>')" class="form-control form-control-sm">
										  <option value="1" <?php if($user->featured == 1) echo 'selected'; ?> >Yes</option>
										  <option value="0" <?php if($user->featured == 0) echo 'selected'; ?> >No</option>
								  </select></td>-->
								<td>
								<a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View" href="<?php echo base_url(); ?>admin/users/view_client_management/<?php echo $user->memo_id; ?>"><i class="fas fa-eye text-white"></i></a>
								 <a onclick="delete_user('<?php echo $user->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" id="cancel-button" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a> 
								</td>
							</tr>
							
						
						
						<?php } ?>


						
						<script>
						
						function update_pakage(pakage, memo_id){
							
							$.ajax({
								url:"<?php echo base_url(); ?>admin/users/update_pakage",
								type: "POST",
								data:{"memo_id": memo_id, "pakage": pakage},
								success:function(resp)
								{
									if(resp == 'success'){
										$("#suc_text").text("Record Updated Successfully!");
										$("#SuccessModal").modal('show');
									}else{
										$("#err_text").text("Somthing went wrong!");
										$("#ErrorModal").modal('show');
									}
								}
							});
							
						}
						
						function update_featured(featured, memo_id){
							
							$.ajax({
								url:"<?php echo base_url(); ?>admin/users/update_featured",
								type: "POST",
								data:{"memo_id": memo_id, "featured": featured},
								success:function(resp)
								{
									if(resp == 'success'){
										$("#suc_text").text("Record Updated Successfully!");
										$("#SuccessModal").modal('show');
									}else{
										$("#err_text").text("Somthing went wrong!");
										$("#ErrorModal").modal('show');
									}
								}
							});
							
						}
						
						function delete_user(id){
							
							if(confirm("Are you sure to delete this user?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/users/delete_user",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#user_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
						
						</script>
						                          
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       
      </div>
    </div>
  </div>
    </div>
  
  
  
<?php $this->load->view('admin/include/footer'); ?>

<script type="text/javascript">
        $(document).ready(function() {
			$('#tab_customers').DataTable( {
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11 ]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 5, 6]//, 7, 8, 9, 10, 11 ]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11 ]
						}
					}
				]
				
				
				/* buttons: [
					'excelHtml5',
					'csvHtml5',
					'pdfHtml5'
				] */
			} );
		});
    </script>