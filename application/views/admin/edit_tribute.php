<?php $this->load->view("admin/include/header"); ?>
  
  
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
          <div class="row">
             <div class="col-md-6">
                <h2>Edit Tribute </h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/users/view_client_management/<?php echo $memo_id; ?>"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
          </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Dropzone section start -->
        <section id="dropzone-examples">
                      <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form method="post" action="<?php echo base_url(); ?>admin/users/update_tribute">
                      <div class="form-body">
                        
						<?php if($this->session->flashdata('error')){ ?>
							<div class="alert alert-block  alert-danger">
								<button data-dismiss="alert" class="close close-sm" style="font-size: medium;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
								<?php echo $this->session->flashdata('error'); ?>
							</div>
						<?php } ?>
						
						<?php if($this->session->flashdata('success')){ ?>
								<div class="alert alert-block  alert-success">
									<button data-dismiss="alert" class="close close-sm" style="font-size: medium;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
									<?php echo $this->session->flashdata('success'); ?>
								</div>
						<?php } ?>
						
						
                        <div class="row">
                            
							<div class="col-md-6">
								  <label>Action</label>
                                  <select name="action" class="form-control form-control-sm">
                                     										
										<option value="tribute" <?php if($tribute->action == 'tribute') echo 'selected'; ?>> Tribute </option>
										<option value="candle" <?php if($tribute->action == 'candle') echo 'selected'; ?>> Candle </option>
										<option value="note" <?php if($tribute->action == 'note') echo 'selected'; ?>> Note </option>
																			  
									
                                  </select>
                               <input type="hidden" name="memo_id" value="<?php echo $memo_id; ?>" />
                               <input type="hidden" name="tribute_id" value="<?php echo $tribute->ID; ?>" />
                             </div>
							 
							 <div class="col-md-6">
								  <label>Status</label>
                                  <select name="status" class="form-control form-control-sm">
                                     										
										<option value="Pending" <?php if($tribute->status == 'Pending') echo 'selected'; ?>> Pending </option>
										<option value="Approved" <?php if($tribute->status == 'Approved') echo 'selected'; ?>> Approved </option>
										
                                  </select>
                               
                             </div>
							                            
                            <div class="col-md-6">
								 <label>Comment</label>
								 <div class="form-group">
										<div class="input-group">
										  <textarea rows="5" name="comment" class="form-control" value="<?php echo $tribute->comment; ?>" > <?php echo $tribute->comment; ?> </textarea>
										</div>  
									</div>
							  </div>
                           
                           
                             <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i>Update
                        </button>
                            </div>
                        </div> 
                      </div>
                       
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
         
        </section>

      </div>
    </div>
  </div>
  
  
  
<?php $this->load->view('admin/include/footer'); ?>