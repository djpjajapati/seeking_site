<?php $this->load->view("admin/include/header"); ?>
  
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
  
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
          <div class="row">
             <div class="col-md-6">
                <h2>Coupon Management</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/coupon/add_coupon"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Add</button></a>
             </div>
          </div>
		  
		  
		 
		  
		  <h3>Expiring dates:</h3>
				<form method="post" action="<?php echo base_url(); ?>admin/coupon/search_coupons">

						<div class="row">
							<div class="col-md-4">
								<input type="date" class="form-control" placeholder="From" value="<?php echo $date1; ?>" name="date1" required />
							</div>
							
							</br>
							
							<div class="col-md-4">
								<input type="date" class="form-control" placeholder="To" value="<?php echo $date2; ?>" name="date2" required />
							</div>
							
							</br>
							
							<div class="col-md-3">
								<input type="submit" class="btn btn-success" value="Search" />
								<?php if(strlen($date1) > 1 && strlen($date2) > 1){ ?>
									<a href="<?php echo base_url(); ?>admin/coupon" class="btn btn-success"> Back </a>
								<?php } ?>
							</div>
						</div>				  
				</form>
	
		
		</br>
		  
		  <div class="content-body">
			<section id="configuration">
			  <div class="row">
				<div class="col-12">
				  <div class="card">
					<div class="card-content collapse show">
					  <div class="card-body card-dashboard">
						<!--<table class="table table-striped table-bordered zero-configuration">-->
						
						<table id="tab_customers" class="table display nowrap table-striped responsive">
						
						  <thead>
							<tr>
							  <th>Coupon Code</th>
								<th>Coupon Discount</th>
							  <th>Frequency</th>
								<th>Expiry Time</th>
								<th>Action</th>
							</tr>
						  </thead>
						  <tbody>
							  
							  <?php foreach($coupons as $c){ ?>
							  
									<tr id="c_<?php echo $c->ID; ?>">
									  <td><?php echo $c->coupon_code; ?></td>
										<td><?php echo $c->coupon_discount_percentage; ?>%</td>
										<td><?php echo $c->frequency; ?></td>
										<td><?php echo date('d-m-Y', strtotime($c->expiry)); ?> </td>
									  <td class="text-center">
										  <a href="<?php echo base_url(); ?>admin/coupon/edit_coupon/<?php echo $c->ID; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt text-white"></i></a>
										 <a onclick="delete_coupon('<?php echo $c->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
									  </td>
									</tr>
							  
							  <?php } ?>
							  
							  
							  
							  <script>
								  
								  function delete_coupon(id){
								
										if(confirm("Are you sure to delete this coupon?")){
											
											$.ajax({
												url:"<?php echo base_url(); ?>admin/coupon/delete_coupon",
												type: "POST",
												data:{"id": id},
												success:function(resp)
												{
													if(resp == 'success'){
														$("#c_"+id).remove();
													}else{
														$("#err_text").text("Somthing went wrong!");
														$("#ErrorModal").modal('show');
													}
												}
											});
											
										}
										
										
									}
								  
							  </script>
							  
						  </tbody>
						</table>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</section>
		  </div>
		  
		  
		  
		  
		  
		  <div class="row">
             <div class="col-md-6">
                <h2>Coupon Log</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 
             </div>
          </div>
		  <div class="content-body">
			<section id="configuration1">
			  <div class="row">
				<div class="col-12">
				  <div class="card">
					<div class="card-content collapse show">
					  <div class="card-body card-dashboard">
						<table id="tab_customers1" class="table display nowrap table-striped responsive">
						  <thead>
							<tr>
							  <th>Coupon Code</th>
								<th>Used By</th>
								<th>Pakage</th>
								<th>Memorial</th>
								<th>Created At</th>
							</tr>
						  </thead>
						  <tbody>
							  
							  <?php foreach($coupon_log as $c){ ?>
							  
									<tr id="log_<?php echo $c->ID; ?>">
									  <td><?php echo $c->coupon_code; ?></td>
										<td><?php echo $c->username; ?></td>
										<td><?php echo $c->pakage; ?></td>
										<td><?php echo $c->memorial; ?></td>
										<td><?php echo date('d-m-Y', strtotime($c->createdAt)); ?> </td>
									 
									</tr>
							  
							  <?php } ?>
					
						  </tbody>
						</table>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</section>
		  </div>
		  
		 
    </div>
 </div>
  
  
  
<?php $this->load->view('admin/include/footer'); ?>


<script type="text/javascript">
        $(document).ready(function() {
			
			$('#tab_customers').DataTable({
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3]
						}
					}
				]
			});
			
			$('#tab_customers1').DataTable({
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4]
						}
					}
				]
			});
			
		});
    </script>