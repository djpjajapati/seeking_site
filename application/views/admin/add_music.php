<?php $this->load->view("admin/include/header"); ?>
  
	
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Add Music</h3>
        </div>
          <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/manage/music"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
      </div>
      <div class="content-body">
        <section id="form-repeater">
              <div class="row">
            <div class="col-12">
              <div class="card" style="padding-bottom: 40px;">
                <div class="card-header">
                </div>
                
                      
                       <form method="post" action="<?php echo base_url(); ?>admin/manage/do_add_music" enctype="multipart/form-data" class="form">
                      
                                 <?php if($this->session->flashdata('error')){ ?>
                                    <div class="alert alert-block  alert-danger">
                                        <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php } ?>
                                
                                 <?php if($this->session->flashdata('success')){ ?>
                                    <div class="alert alert-block  alert-success">
                                        <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                <?php } ?>
                      
                      <div>
                        <div>
                       
                      
											
                              <div class="row col-md-12">
                                  
                                  <div class="col-md-6">
                                      <label for="bio" class="cursor-pointer">Title</label>
                                      <input type="text" name="title" class="form-control" id="text" required placeholder="Enter Title">
                                  </div>
                                   
                                         
                                      <div class="col-md-12" style="margin-top: 30px;">
                                          
                                        <label class="file center-block">
                                          <input type="file" name="audio" id="file" required>
                                          <span class="file-custom"></span>
                                        </label>
                                        
                                      </div>
                                      
                                     
                                      
                                      <div class="col-md-12" style="margin-top: 30px;">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary">
                                             Upload
                                          </button>
                                        </div>
                                      </div>
                                     
                                </div>
                          
                        </div>
                      </div>
                      
                      </form>
                      
              </div>
            </div>
          </div>
        </section>
        <!-- // Form control repeater section end -->
      </div>
    </div>
  </div>
					
<?php $this->load->view('admin/include/footer'); ?>