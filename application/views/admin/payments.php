<?php $this->load->view('admin/include/header'); ?>

		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

	<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">

          <h2>Payments</h2>
       
        
      <div class="content-body">

        <section id="horizontal">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table id="tab_customers" class="table display nowrap table-striped responsive">
                      <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subscribed Plan</th>
                            <th>Purchase Date</th>
                            <th>Expiry Date</th>
                            <th>Method</th>
                            <th>Discount</th>
                            <th>Amount Paid</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          
                          <?php foreach($memorials as $m){ ?>
                          
                          <tr>
                            <td><?php echo $m->fname.' '.$m->lname; ?></td>
                            <td><?php echo $m->remailadd; ?></td>
                            <td><?php echo $m->pakage; ?></td>
                            <td><?php echo date('Y-m-d', strtotime($m->createdAt)); ?></td>
                            <td><?php echo date('Y-m-d', strtotime($m->createdAt. " + 1 year")); ?></td>
                            <td>Ipay</td>
                            <td><?php echo $m->discount; ?></td>
                            <td><?php echo $m->price - $m->discount; echo ' '.$m->currency; ?></td>
                            <td><a target="_blank" href="<?php echo base_url(); ?>memorial/invoice/<?php echo $m->invoice_id; ?>"> View Invoice </a></td>
                        </tr>
                          
                          
                          <?php } ?>
                          
                        <!--<tr>
                            <td>Zoey</td>
                            <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                            <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                           <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                            <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                            <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                            <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                            <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                            <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                            <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                           <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                           <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                            <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                            <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                           <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                           <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Zoey</td>
                            <td>Zoey@gmail.com</td>
                            <td>Basic</td>
                            <td>20-June-2018</td>
                            <td>20-June-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>
                          <tr>
                            <td>Emma</td>
                            <td>Emma@gmail.com</td>
                            <td>Premium</td>
                            <td>10-Jan-2018</td>
                            <td>10-Jan-2020</td>
                            <td>MPessa</td>
                            <td>50%</td>
                            <td>5000 KSh</td>
                        </tr>
                          <tr>
                            <td>James</td>
                           <td>James@gmail.com</td>
                            <td>Basic</td>
                            <td>10-Feb-2019</td>
                            <td>10-Feb-2020</td>
                            <td>Ipay</td>
                            <td>10%</td>
                            <td>2000 KSh</td>
                        </tr>-->
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       
      </div>
    </div>
  </div>

<?php $this->load->view('admin/include/footer'); ?>

<script type="text/javascript">
        $(document).ready(function() {
			
			$('#tab_customers').DataTable({
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
						}
					}
				]
			});
			
			
		});
    </script>