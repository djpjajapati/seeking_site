<?php 
$this->load->view('admin/include/header.php');
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<?php 
$this->load->view('admin/include/top_menu.php');
?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php 
$this->load->view('admin/include/sidebar.php');
?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			View Users
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
					    <a href="<?=base_url()?>admin">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						Users
						
					</li>
					
					
				
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			
			<div class="row">
				<!--<div class="col-md-12">
						<<a href="<?php echo base_url(); ?>admin/users/add" class="btn green" style="margin-bottom: 10px; float:right">Add User</a>
				</div>-->
			</div>
			
			<div class="row">
				<div class="col-md-12">
                      <?php if($this->session->flashdata('success')){ ?>
                    <div class="alert alert-success alert-block fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                       
                        <p>
                            <?php echo $this->session->flashdata('success'); ?>
                        </p>
                    </div>
                    <?php } ?>
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View User List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
							
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>								
								 <th> SNo</th>
								 <th> Username</th>
								 <th> Email</th>
                                 <th> Created At</th>
            
                                 <th style="100px !important; padding-right:50px;"> Action</th>
							</tr>
							</thead>
							<tbody>
                            <?php $i=1; foreach($users as $row){ ?>
							<tr id="company_<?php echo $row['ID']; ?>" class="odd gradeX">
						      
								<td>
								
								<?php echo $i; ?>
								
								</td>
							  
								<td>
									 <?php echo $row['username']; ?>
								</td>
								
								<td>
									
								<?php echo $row['email']; ?> 
								</td>
								
								<td>
									
								<?php echo $row['createdAt']; ?> 
								</td>
															
                                <td class="center" style="width: 14%; padding:5px; vertical-align: middle; text-align: center;">
								  
                                  <!--<a href="<?php echo base_url();?>admin/users/edit/<?php echo $row['ID'] ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>-->
								  
                                  <a  onclick="del_company(<?php echo $row['ID']; ?>)" class="btn btn-primary btn-xs"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						     <?php $i++; } ?>
						
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		
		</div>
	</div>
	<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<?php 
$this->load->view('admin/include/footer.php');
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
  $('#sample_1').dataTable({
//"paging": true,
//"bSort": false
});
});


function del_company(id){
		
		if (confirm("Delete this record! Are you sure?") == false) {
                return;
		}
		
           $.post("<?php echo base_url(); ?>admin/users/delete_user/",{id:id},function(data)
           { 
				if(data == 1){
					$('#company_'+id).remove();
				}else{
					alert("something went wrong");
				}
           });
		
}

</script>
 
</body>
<!-- END BODY -->
</html>