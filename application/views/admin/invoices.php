<?php $this->load->view('admin/include/header'); ?>

		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

	<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">

          <h2>Invoices</h2>
       
        
      <div class="content-body">

        <section id="horizontal">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table id="tab_customers" class="table display nowrap table-striped responsive">
                      <thead>
                        <tr>
                            <th>Email</th>
                            <th>Plan</th>
                            <th>Date</th>
                            <th>Expiry</th>
                            <th>Amount Paid</th>
                            <th>Status</th>
                            <th>Action</th>
                            <th>Resend</th>
                        </tr>
                      </thead>
                      <tbody>
                          
                          <?php foreach($memorials as $m){ ?>
                          
                          <tr>
                            <td><?php echo $m->useremail; ?></td>
                            <td><?php echo $m->details; ?></td>
                            <td><?php echo date('j M Y', strtotime($m->wasgenerated)); ?></td>
                            <td><?php echo date('j M Y', strtotime($m->willexpireat)); ?></td>
                            <td><?php echo $m->totalprice; ?></td>
                            <td><?php echo $m->	status; ?></td>
                            
                            <td><a target="_blank" href="<?php echo base_url(); ?>memorial/invoice/<?php echo $m->id; ?>"> View Invoice </a></td>
                            <td><span id="inv<?php echo $m->id ?>" style="cursor: pointer" onClick="resendinvoice(<?php echo $m->id ?>)">Resend</span></td>
                        </tr>
                          
                          
                          <?php } ?>
                          
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       
      </div>
    </div>
  </div>

<?php $this->load->view('admin/include/footer'); ?>

<script type="text/javascript">
        $(document).ready(function() {
			
			$('#tab_customers').DataTable({
				dom: 'Bfrtip',
				buttons: [
					{
						extend: 'excelHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
						}
					},
					{
						extend: 'csvHtml5',
						exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
						}
					}
				]
			});
			
			
		});
	
	function resendinvoice(theid)
	{
		$( "#inv"+theid ).load( "<?php echo base_url().'memorial/resendinvoice/';?>"+theid, function() {
		  //alert( "Email sent successfully." );
		  $("#suc_text").text("Email sent successfully.");
		  $("#SuccessModal").modal("show");
		});
		
	}
    </script>