<?php $this->load->view("admin/include/header"); ?>
  
	<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
                 <div class="row">
             <div class="col-md-6">
                <h2>Testimonials Management</h2>
             </div>
          </div>
      <div class="content-body">
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Posted on</th>
                            <th>Rating</th>
                            <th>Description</th>
                            <th>Profanity Status</th>
                            <th>Flage</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          
                          <?php foreach($testimonials as $t){ ?>
                          
                          <tr id="t_<?php echo $t->id; ?>">
                          <td><?php echo $t->name; ?></td>
                            <td><?php echo date('d-m-y', $t->add_date); ?></td>
                            <td class="table-stars" style="text-align: center">
                                <ul class="list-inline">
                                    <?php for($i=0; $i < $t->rating; $i++){ ?>
                                        <li class="list-inline-item m-0"><a href=""><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                    <?php } ?>
                                </ul>
                            </td>
                            <td><?php echo $t->message; ?></td>
                            <td><?php if($t->profanity_status == 1) echo "Yes"; else echo "No"; ?></td>
							
                            <td><?php echo $t->flaggedcontent; ?></td>
                            
                         <td class="text-center">
                            <!--<a class="btn btn-primary btn-xs mb-md-1" data-toggle="tooltip" title="Block User"> <i class="fas fa-user-slash text-white"></i></a>-->
                            	<span id="block_<?php echo $t->user_id; ?>">
								  <a onclick="block_user('<?php echo $t->user_id; ?>', '<?php echo $t->user_status; ?>')" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="fas <?php if($t->user_status == 'Active') echo 'fa-user'; else echo 'fa-user-slash'; ?> text-white"></i></a>
								</span>
                             <a onclick="delete_testimonial('<?php echo $t->id; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1"  data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                          
                          <?php } ?>
                          
                          
                          	<script>
						
						function block_user(user_id, status){
							
							var new_status = 'Active';
							var new_class = 'fas fa-user text-white';
						
							if(status == 'Active'){
								new_status = 'Inactive';
								new_class = 'fas fa-user-slash text-white';
							}
							
							$.ajax({
								url:"<?php echo base_url(); ?>admin/users/update_user_status",
								type: "POST",
								data:{"user_id": user_id, 'new_status': new_status},
								success:function(resp)
								{
									if(resp == 'success'){
										//$("#status_"+user_id).text(new_status);
										
										
										var params = "'"+user_id+"',"+"'"+new_status+"'";
										
										$("#block_"+user_id).html('');
										$("#block_"+user_id).html('<a onclick="block_user('+params+')" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Block User"> <i class="'+new_class+'"></i></a>');
										
										
										$("#suc_text").text('User is '+new_status+' Now');
										$("#SuccessModal").modal('show');
									}else{
										$("#err_text").text("Somthing went wrong!");
										$("#ErrorModal").modal('show');
									}
								}
							});
						}
						
						function delete_testimonial(id){
								
								if(confirm("Are you sure to delete this testimonial?")){
									
									$.ajax({
										url:"<?php echo base_url(); ?>admin/manage/delete_testimonial",
										type: "POST",
										data:{"id": id},
										success:function(resp)
										{
											if(resp == 'success'){
												$("#t_"+id).remove();
											}else{
												$("#err_text").text("Somthing went wrong!");
												$("#ErrorModal").modal('show');
											}
										}
									});
									
								}
								
								
						}
                          
                        </script>
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

       
      </div>
    </div>
  </div>
  </div>
					
<?php $this->load->view('admin/include/footer'); ?>