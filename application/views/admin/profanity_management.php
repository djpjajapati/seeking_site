<?php $this->load->view("admin/include/header"); ?>
  
	 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
                 <div class="row">
             <div class="col-md-6">
                <h2>Profanity Management</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="javascript:;" onclick="add_word()"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Add</button></a>
             </div>
          </div>
      <div class="content-body">
          
          
          
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  
                  <?php if($this->session->flashdata('error')){ ?>
                        <div class="alert alert-block  alert-danger">
                            <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    <?php } ?>
                  
                  
                  <?php if($this->session->flashdata('success')){ ?>
                        <div class="alert alert-block  alert-success">
                            <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php } ?>
                  
                  
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Word</th>
                          <th>Frequency</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          
                          <?php foreach($profanity as $p){ ?>

                                <tr id="p_<?php echo $p->ID; ?>">
                                  <td><span id="w_<?php echo $p->ID; ?>"><?php echo $p->word; ?></span></td>
                                  <td><span id="freq_<?php echo $p->ID; ?>"><?php echo $p->frequency; ?></span></td>
                                  <td class="text-center">
                                      <a onclick="edit_word('<?php echo $p->ID; ?>')" class="btn btn-primary btn-xs" title="Edit"><i class="fas fa-pencil-alt text-white"></i></a>
                                         
                                     <a onclick="delete_word('<?php echo $p->ID; ?>')" class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                                  </td>
                                </tr>
                          
                          <?php } ?>
                          
                          
                          
                        <script>
                        
                            function add_word(){
                                
                                $("#inlineForm1").modal('show');
                                
                            }
                        
                            function edit_word(id){
                                
                                $("#p_id").val(id);
                                var word = $("#w_"+id).text();
                                $("#p_word").val(word);
                                $("#inlineForm").modal('show');
                                
                            }
                            
                            
						
							function delete_word(id){
								
								if(confirm("Are you sure to delete this user?")){
									
									$.ajax({
										url:"<?php echo base_url(); ?>admin/manage/delete_word",
										type: "POST",
										data:{"id": id},
										success:function(resp)
										{
											if(resp == 'success'){
												$("#p_"+id).remove();
											}else{
												$("#err_text").text("Somthing went wrong!");
												$("#ErrorModal").modal('show');
											}
										}
									});
									
								}
								
								
							}
							
							
							function update_word(){
							    
							    var id = $("#p_id").val();
							    var word = $("#p_word").val();
							    
							    $.ajax({
										url:"<?php echo base_url(); ?>admin/manage/update_profanity",
										type: "POST",
										data:{"id": id, "word": word},
										success:function(resp)
										{
										    $("#inlineForm").modal('hide');
											$("#w_"+id).text(resp);
										}
								});
							}
						
						</script>
                          
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        
        
        
        <!-- Modal -->
                         <div class="modal fade text-left" id="inlineForm1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
                          aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <label class="modal-title text-text-bold-600" id="myModalLabel33">Add Word</label>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form method="post" action="<?php echo base_url() ?>admin/manage/add_profanity">
                                  <div class="modal-body">
                                    <label>Word </label>
                                    <div class="form-group">
                                      <input name="word" required type="text" value="" class="form-control">

                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    
                                    <input type="submit" class="btn btn-outline-primary btn-lg" value="Save">
                                    
                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                                    value="close">
                                    
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
      
        </div>
        
        
        
        
        
        
        

        <!-- Modal -->
                         <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
                          aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit Word</label>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="#">
                                  <div class="modal-body">
                                    <label>Word </label>
                                    <div class="form-group">
                                      <input id="p_id" type="hidden" value="" class="form-control">
                                      <input id="p_word" type="text" value="" class="form-control">

                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    
                                    <input onclick="update_word()" type="button" class="btn btn-outline-primary btn-lg" value="Save">
                                    
                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                                    value="close">
                                    
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
      </div>
    </div>
  </div>
					
<?php $this->load->view('admin/include/footer'); ?>