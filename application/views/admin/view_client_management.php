<?php $this->load->view("admin/include/header"); ?>
  
   <style>
            .file-wrapper {
			  cursor: pointer;
			  display: inline-block;
			  overflow: hidden;
			  position: relative;
			}
			.file-wrapper input {
			  cursor: pointer;
			  font-size: 100px;
			  filter: alpha(opacity=1);
			  -moz-opacity: 0.01;
			  opacity: 0.01;
			  position: absolute;
			  right: 0;
			  top: 0;
			}
			.file-wrapper .button {
				  width: auto;
				height: 35px;
				line-height: 33px;
				border: #582236 solid 1px !important;
				color: #582236 !important;
				background: none !important;
				font-size: 14px !important;
				box-shadow: none;
				padding: 0 20px;
			}
			.mt-2{
				dislplay: none;
			}

        </style>
    
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
          <div class="row">
             <div class="col-md-6">
                <h2>View Memorial Page Management</h2>
             </div>
             <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url().'admin/users/client_management'; ?>"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
          </div>
        </div>
        </div>
      <div class="content-body">
        <!-- Basic tabs start -->
        <section id="basic-tabs-components">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 ">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-content">
				
                  <div class="card-body">
				  
				  <?php if($this->session->flashdata('success')){ ?>
						<div class="alert alert-success display-hide col-md-12">
							<button class="close" data-close="alert"></button>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
				<?php } ?>
				
				<?php if($this->session->flashdata('error')){ ?>
						<div class="alert alert-danger display-hide col-md-12">
							<button class="close" data-close="alert"></button>
							<?php echo $this->session->flashdata('error'); ?>
						</div>
				<?php } ?>
				  
                    <ul class="nav nav-tabs">
                      <li class="nav-item">
                        <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                        href="#tab1" aria-expanded="true">About</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link tab2_gallery" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2"
                        aria-expanded="false">Notices</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link tab3_gallery" id="base-tab3" data-toggle="tab" aria-controls="tab3" href="#tab3"
                        aria-expanded="false">Gallery</a>
                      </li>
                      <li class="nav-item">
                       <a class="nav-link tab4_gallery" id="base-tab4" data-toggle="tab" aria-controls="tab4" href="#tab4"
                        aria-expanded="false">Tributes</a>
                      </li>
					  <li class="nav-item">
                       <a class="nav-link tab5_gallery" id="base-tab5" data-toggle="tab" aria-controls="tab5" href="#tab5"
                        aria-expanded="false">Activities</a>
                      </li>
					  
					  
					  <li class="nav-item">
                       <a class="nav-link tab6_gallery" id="base-tab6" data-toggle="tab" aria-controls="tab6" href="#tab6"
                        aria-expanded="false">Send Notification</a>
                      </li>
                    </ul>
                    <div class="tab-content px-1 pt-1">
                      
					  
					  
					  <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                          
						  
						  <form method="post" action="<?php echo base_url(); ?>admin/users/update_memo" >
						  <div class="row">
                              <div class="col-md-6">
								  <label>First Name</label>
								 <div class="form-group">
										<div class="input-group">
											<input name="fname" type="text" value="<?php echo $memo->fname; ?>" class="form-control" placeholder="First Name">
											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>" />
										</div>  
									</div>
								</div>
								
								<div class="col-md-6">
								  <label>Last Name</label>
								 <div class="form-group">
										<div class="input-group">
											<input name="lname" type="text" class="form-control" value="<?php echo $memo->lname; ?>" placeholder="Last Name">
										</div>  
									</div>
								</div>
								
                           <div class="col-md-12">
                              <label>Quote</label>
                             <div class="form-group">
                                    <div class="input-group">

																		<?php 
																		
																				$about = 'Let the memory of '.$memo->fname.' '.$memo->lname.' be with us forever';
																				 if(strlen(trim($memo->about)) > 0) $about = $memo->about; 
																				 
																		?>

                                        <textarea type='text' name="about" value="<?php echo $about; ?>" rows="5" class="form-control"> <?php echo $about; ?>  </textarea>
                                    </div>  
                                </div>
                            </div>
                          <div class="col-md-12">
                              <label>Description</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <textarea type='text' name="description" value="<?php echo $memo->description; ?>" rows="5" class="form-control"><?php echo $memo->description; ?></textarea>
                                    </div>  
                                </div>
                            </div>
                              
							  
							  <!--<div class="col-md-6">
                                <div class="form-group" style="width: 300px">
                                    <label>Upload Display Image</label>
                                          <div class="col-sm-2 col-md-6 imgUp">
                                            <!--<div class="imagePreview" style="background-image: url(<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>)"></div>->
											
											<div id="uploaded_image" class="thumbnail">
												<img src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>" width="150" height="180" alt=""/> 
											  </div>
											
										<label style="width: 150px " class="btn btn-primary upload-btn">
                                            Upload<input id="cropped_image" name="cropped_image" type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                        </label>
                                          </div>
                                </div>
                            </div>-->
							
							<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>
								<link rel="stylesheet" href="<?php echo base_url(); ?>assets/crop/croppie.css" />
							
							<div style="text-align: center" class="">
									<div class="row">
									  <div class="col-md-12" >
											
											<div id="uploaded_image" class="thumbnail">
												<img src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>" width="150" height="180" alt=""/> 
											  </div>
											  
											  <!--<input id="cropped_image" name="cropped_image" type="text" value="nelson-mandela-blog1.png" />-->
											  <input id="cropped_image" name="cropped_image" type="hidden" value="<?php echo $memo->cropped_image; ?>" />
											  
												<span class="file-wrapper">
												  <input type="file" name="image" id="image" />
												  <span class="button red_btn">Choose a File</span>
												</span>
									  </div>
										
										
									  <div class="col-md-12" style="margin: 10px">
									  
										
										
									  </div>
									  
									  
									  
										
									</div>
									
								<input type="submit" class="btn btn-primary pull-right" value="Update" />
									
								</div>
							
							
							 </div>
							
							
							</form>
							
							
                         
                      </div>
					  
					  
					  
					  
					  
					  
                      <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                        
						<form method="post" action="<?php echo base_url(); ?>admin/users/update_notices" >
						
						<div class="row">
						
                           <div class="col-md-12">
                              <label>Notice Board For</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="notice_for" class="form-control" value="<?php if($notice->notice_for == ""){ echo $memo->fname.' '.$memo->lname; }else{ echo $notice->notice_for; } ?>" placeholder="Full Name">
                                    </div>  
                                </div>
                            </div>
							
							
                          
                           <div class="col-md-6">
                              <label>Date of Birth</label>
                             <div class="form-group">
                                    <div class="input-group">
                                      
                                      <input type='date' name="dob" class="form-control pickadate" value="<?php if($notice->dob == ""){ echo $memo->birthdate; }else{ echo $notice->dob; } ?>"
                                      />
                                    </div>  
                                </div>
                            </div>
                          <div class="col-md-6">
                              <label>Date of Death</label>
                             <div class="form-group">
                                     <div class="input-group">
                                      
                                      <input type='date' name="dod" class="form-control pickadate" value="<?php if($notice->dod == ""){ echo $memo->deathdate; }else{ echo $notice->dod; } ?>"
                                      />
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-6">
                              <label>Church/Mosque/Temple</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="house_of_worship" class="form-control" value="<?php echo $notice->house_of_worship; ?>" placeholder="Location,Town,Date,Time">
                                    </div>  
                                </div>
                            </div>
                          <div class="col-md-6">
                              <label>Country</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="country" class="form-control" value="<?php echo $notice->country; ?>" placeholder="Country,Public Cemetery,Date,Time">
                                    </div>  
                                </div>
                          </div>
                            
                          <div class="col-md-6">
                              <label>Town</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="town" class="form-control" value="<?php echo $notice->town; ?>" placeholder="Town/Center">
                                    </div>  
                                </div>
                            </div>
                            <div class="col-md-6">
                              <label>Cremation Details</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="cremation_detail" class="form-control" value="<?php echo $notice->cremation_detail; ?>" placeholder="Location,Crematorium,Time">
                                    </div>  
                                </div>
                            </div>
                          <div class="col-md-6">
                              <label>Funeral Committee Contact Person</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="contact_person" class="form-control" value="<?php echo $notice->contact_person; ?>" placeholder="1st Name,2nd Name,Phones">
                                    </div>  
                                </div>
                            </div>
                            <div class="col-md-6">
                              <label>Funeral Committee Meetings Venue</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="meeting_venue" class="form-control" value="<?php echo $notice->meeting_venue; ?>" placeholder="Town,Location,Building,Meeting Dates">
                                    </div>  
                                </div>
                            </div>
                          <div class="col-md-6">
                              <label>Burial/Funeral/Interment Location Google Map</label>
                             <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="google_map_loc" class="form-control" value="<?php echo $notice->google_map_loc; ?>" placeholder="Burial/Funeral/Interment Location Google Map">
                                    </div>  
                                </div>
                            </div>
							
							 <div class="col-md-6">
                              <label></label>
                             <div class="form-group">
                                    <div class="input-group">
										<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>" />
                                        <input type="submit" class="btn btn-primary" value="Update" />
                                    </div>  
                                </div>
                            </div>
							
							
							
                          </div>
						  
						
						  </form>
						  
						  
                      </div>
					  
					  
                      <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                             <div class="repeater-default collapse show">
                               <div class="row">
							   
							   <h4> Uplaod Picture</h4>
                            
							<?php foreach($pictures as $p){ ?>
							
								<!--<div class="col-md-3" id="p_<?php echo $p->ID; ?>">
									
										<button class="remove_btn" style="position: absolute;cursor: pointer;" onclick="delete_pic('<?php echo $p->ID; ?>', '<?php echo $p->pic; ?>')">X</button>
									
									<img class="img-fluid" alt="img" src="<?php echo base_url(); ?>uploads/<?php echo $p->pic; ?>">
								  </div>-->
							
								<div class="col-md-4" id="p_<?php echo $p->ID; ?>">
									<div class="form-group">
										<label>Upload Image</label>
											  <div class="col-sm-2 col-md-10 imgUp">
												<div class="imagePreview1"></div>
											
												  <i onclick="delete_pic('<?php echo $p->ID; ?>', '<?php echo $p->pic; ?>')" class="fa fa-times del"></i>
												  
												  <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>uploads/<?php echo $p->pic; ?>">
												  
											  </div>
									</div>
                                </div>
							
							<?php } ?>
							
							
							
							<form  class="col-md-12" style="margin: 30px" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/users/add_pic">
					
								  <div class="prehead_left">
									  <div class="row"> 
									  
											<?php if($this->session->flashdata('error_pic')){ ?>
													<div class="alert alert-danger display-hide">
														<button class="close" data-close="alert"></button>
														<?php echo $this->session->flashdata('error_pic'); ?>
													</div>
											<?php } ?>
																				  
											 <div class="col-md-12" style="display: inline-flex;">
															  
													<input type="file" id="upload_picture1" name="pic" required>
											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
																							
												  <div class="add-photo_new">
													<input type="submit" value="Upload" class="btn btn-primary" />
												  </div>
											 </div>
									 </div>
								</div>			
							 </form>
							
							
							
							<script>
							
							function delete_pic(id, pic){
								  
								  if(confirm("Are you sure?")){
									  $.ajax({
												url:"<?php echo base_url(); ?>admin/users/delete_pic",
												type: "POST",
												data:{"id": id, "pic": pic},
												success:function(resp)
												{
													if(resp == 'success'){
														$('#p_'+id).remove();
													}
													
												}
										}); 
								  }
							  }
							
							</script>
							
							
							
							
							
							<!--<div class="col-md-4">
                                <div class="form-group">
                                    <label>Upload Image</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                        
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>
                                   <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>-->
								   
								   
								   
								   
                                    <!--<div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i>Upload
                        </button>
                            </div>-->
                            </div>
                           <div class="row">
                            
							 <h4> Uplaod Video</h4>
							
							<?php foreach($videos as $v){ ?>
							
									<div class="col-md-4" id="v_<?php echo $v->ID; ?>">
										<div class="form-group">
											<label>Upload Videos</label>
											  <div class="col-sm-2 col-md-10 imgUp">
												<div class="imagePreview">
												
												<video width="100%" height="180px" controls>
												  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $v->video; ?>" type="video/mp4">
												  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $v->video; ?>" type="video/mp4">
												  Your browser does not support the video tag.
												</video>
																								
												</div>
												
												  <i onclick="delete_video('<?php echo $v->ID; ?>', '<?php echo $v->video; ?>')" class="fa fa-times del"></i>
											  </div>
										</div>
									</div>
									
							<?php } ?>
							
							
							<form class="col-md-12" style="margin: 30px" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/users/add_video">
					
								  <div class="prehead_left">
									  <div class="row"> 
									  
											<?php if($this->session->flashdata('error_video')){ ?>
													<div class="alert alert-danger display-hide">
														<button class="close" data-close="alert"></button>
														<?php echo $this->session->flashdata('error_video'); ?>
													</div>
											<?php } ?>
																				  
											 <div class="col-md-12" style="display: inline-flex;">
															  
													<input type="file" id="upload_video" name="video" required>
													<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
																							
												  <div class="add-photo_new">
													<input type="submit" value="Upload" class="btn btn-primary" />
												  </div>
											 </div>
									 </div>
								</div>			
							 </form>
							
							
							
							
						<script>
						  
						  function delete_video(id, vid){
							  
							  if(confirm("Are you sure?")){
								  $.ajax({
											url:"<?php echo base_url(); ?>admin/users/delete_vid",
											type: "POST",
											data:{"id": id, "vid": vid},
											success:function(resp)
											{
												if(resp == 'success'){
													$('#v_'+id).remove();
												}
												
											}
									}); 
							  }
						  }
						  
						  </script>	
							
							
							<!--<div class="col-md-4">
                                <div class="form-group">
                                    <label>Upload Videos</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                        
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>
                                   <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                          <div class="col-sm-2 col-md-10 imgUp">
                                            <div class="imagePreview"></div>
                                              <i class="fa fa-times del"></i>
                                          </div>
                                </div>
                                   </div>-->
								   
								   
                                    <!--<div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i>Upload
                        </button>
                            </div>-->
                            </div>
							
							
							<div class="row">
							  
							  <h4> Uplaod Music</h4>
							  							  
							  <div class="gal_box">
								<div class="row">
								  
								  <?php foreach($audios as $a){ ?>
										
										<div id="a_<?php echo $a->ID; ?>">
										
											 <div class="col-sm-2 col-md-10 imgUp">
													
														<audio id="player" controls="controls">
															<source id="mp3_src" src="<?php echo base_url(); ?>uploads/music/<?php echo $a->audio; ?>" type="audio/mp3"/>
														</audio>
													
														<i onclick="delete_audio('<?php echo $a->ID; ?>', '<?php echo $a->audio; ?>')" class="fa fa-times del"></i>
											</div>
										
										</div>
									
								  
								  <?php  } ?>
								  
								</div>
								
								
								
								<form  class="col-md-12" style="margin: 30px" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/users/add_music">
								 
									  <div class="row"> 
									  
											<?php if($this->session->flashdata('error_music')){ ?>
													<div class="alert alert-danger display-hide">
														<button class="close" data-close="alert"></button>
														<?php echo $this->session->flashdata('error_music'); ?>
													</div>
											<?php } ?>
																				  
											 <div class="col-md-12" style="display: inline-flex;">
															  
												  <input type="file" id="upload_music" name="audio" required>
												  <input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
										
												  <div class="add-photo_new">
													<input type="submit" value="Upload" class="btn btn-primary" />
												  </div>
											 </div>
									 </div>
									
							 </form>
								
								
								
								
								
							  </div>
							  							  
                          </div>
                               
                      <script>
						
						function delete_audio(id, aud){
							  
							  if(confirm("Are you sure?")){
								  $.ajax({
											url:"<?php echo base_url(); ?>admin/users/delete_audio",
											type: "POST",
											data:{"id": id, "aud": aud},
											success:function(resp)
											{
												if(resp == 'success'){
													$('#a_'+id).remove();
												}
												
											}
									}); 
							  }
						}
							
						</script>					  

                      <!--<div class="form-group overflow-hidden">
                        <div class="col-12">
                          <button  class="btn btn-primary">
                            <i class="ft-plus"></i> Add
                          </button>
                        </div>
                      </div>-->
					  
					  

                    </div>
                      </div>
                        <div class="tab-pane" id="tab4" aria-labelledby="base-tab4">

                            <table class="table table-striped table-bordered zero-configuration" d="configuration">
                      <thead>
                        <tr>
                          <th>Name</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Flag</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					  <?php foreach($tributes as $t){ ?>
						<tr id="tribute_<?php echo $t->ID; ?>">
                          <td><?php echo $t->by; ?></td>
                            <td><?php echo ucfirst($t->action); ?></td>
                            <td><?php echo $t->comment; ?></td>
                            <td><?php echo $t->flaggedcontent; ?></td>
                           <td class="text-center">
                           <a class="btn btn-primary btn-xs mb-md-1" href="<?php echo base_url(); ?>admin/users/edit_tribute/<?php echo $memo->ID.'/'.$t->ID; ?>" data-toggle="tooltip" title="Edit"> <i class="fas fa-pencil-alt text-white"></i></a>
                           <a class="btn btn-danger btn-xs mb-md-1" onclick="delete_tribute('<?php echo $t->ID; ?>');"  data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
						
					  <?php } ?>
					  
					  
					  <script>
					  
						function delete_tribute(id){
							  
							  if(confirm("Are you sure?")){
								  $.ajax({
											url:"<?php echo base_url(); ?>admin/users/delete_tribute",
											type: "POST",
											data:{"id": id},
											success:function(resp)
											{
												if(resp == 'success'){
													$('#tribute_'+id).remove();
												}
												
											}
									}); 
							  }
						}
							
					
					  </script>
					  
                      </tbody>
                    </table>
         
                        
                        </div>
                        <!--<div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i>Update
                        </button>
                            </div>-->
							
							
							
				<div class="tab-pane" id="tab5" aria-labelledby="base-tab5">

                    <table class="table table-striped table-bordered zero-configuration" id="configuration1">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Description</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					  <?php $sno=1; foreach($activities as $act){ ?>
						<tr id="act_<?php echo $act->ID; ?>">
                          <td><?php echo $sno; ?></td>
                            <td><?php echo $act->description; ?></td>
                            <td><?php echo date('d-m-Y', strtotime($act->createdAt)); ?></td>
                           <td class="text-center">
							<a class="btn btn-danger btn-xs mb-md-1" onclick="delete_activity('<?php echo $act->ID; ?>');"  data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
						
					  <?php $sno++; } ?>
					  
					  
					  <script>
					  
						 function delete_activity(id){
							
							if(confirm("Are you sure to delete this user?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/users/delete_activity",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#act_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
							
					
					  </script>
					  
                      </tbody>
                    </table>
         
                        
                   </div>
				   
				   
				   
				   
				   
				   <div class="tab-pane" id="tab6" aria-labelledby="base-tab6">

                    <table class="" style="width: 100%" id="configuration123">
                      <thead>
                        <tr>
                          <th>Send Notification</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					  
						<tr>
                          <td>
						  
						  <textarea id="memorial_message" name="memorial_message" value="" rows="5" class="form-control basic" placeholder="Enter text here..."></textarea>
						  
						  </td>
                        </tr>
						
						<tr>
							<td>
								<input style="float: right;" onclick="send_message()" type="button" class="btn btn-primary pull-right" value="Send" />
							</td>
						</tr>
						
					 
					  <script>
					  
					  function send_message(){
						  
						  var msg = $("#memorial_message").val();
						  var memo_id = '<?php echo $memo->ID; ?>';
						
						  if(msg.length == 0){
							  $("#err_text").text("Please enter your message!");
							  $("#ErrorModal").modal('show');
							  return false;
						  }
						  
						 
						  $.ajax({
							url:"<?php echo base_url(); ?>admin/users/send_message",
							type: "POST",
							data:{"memo_id": memo_id, "msg": msg},
							success:function(resp)
							{
								if(resp == 'success'){
									$("#suc_text").text("Message sent successfully!");
									$("#SuccessModal").modal('show');
								}else{
									$("#err_text").text("Somthing went wrong!");
									$("#ErrorModal").modal('show');
								}
							}
						});
						  
					  }
					  
						 function delete_activity(id){
							
							if(confirm("Are you sure to delete this user?")){
								
								$.ajax({
									url:"<?php echo base_url(); ?>admin/users/delete_activity",
									type: "POST",
									data:{"id": id},
									success:function(resp)
									{
										if(resp == 'success'){
											$("#act_"+id).remove();
										}else{
											$("#err_text").text("Somthing went wrong!");
											$("#ErrorModal").modal('show');
										}
									}
								});
								
							}
							
							
						}
							
					
					  </script>
					  
                      </tbody>
                    </table>
         
                        
                   </div>
							
							
							
							
                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
  
  <!--<div class="modal fade text-left show" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" style="display: block; padding-right: 17px;">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit Word</label>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		  </button>
		</div>
		<form action="#">
		  <div class="modal-body">
			<label>Word </label>
			<div class="form-group">
			  <input type="text" value="word" class="form-control">
			</div>
		  </div>
		  <div class="modal-footer">
			<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
			<input type="submit" class="btn btn-outline-primary btn-lg" value="Save">
		  </div>
		</form>
	  </div>
	</div>
  </div>-->
 



<!--  Crop Modal Start Here   -->
<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 text-center">
						  <div id="image_demo" style="width:100%; margin-top:30px"></div>
					</div>
					<div class="col-md-4" style="padding-top:30px;">
						<br />
						<br />
						<br/>
						  
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="red_btn crop_image">Upload</button>
				<!--<button id="uploadimageModal_btn" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
			</div>
		</div>
	</div>
</div>
<!--  Crop Modal End Here   -->
								
 
  
<?php //$this->load->view('admin/include/footer'); ?>

<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 All rights reserved. </span>
    </p>
  </footer>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/extensions/dropzone.min.js" type="text/javascript"></script>
  <script src="app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/extensions/dropzone.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
  type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
  type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/codemirror/lib/codemirror.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
         <script>
      $('.basic').summernote({
		popover: { },
		toolbar: [
 			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		  ]
		
      });
    </script>






 

    <script>


$(".imgAdd").click(function(){
  $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
});
$(document).on("click", "i.del" , function() {
	//$(this).parent().remove();
});
$(function() {
    $(document).on("change",".uploadFile", function()
    {
    		var uploadFile = $(this);
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
            }
        }
      
    });
});





function getUrlParameter(sParam) {

	var sPageURL = decodeURIComponent(window.location.search.substring(1)),

		sURLVariables = sPageURL.split('&'),

		sParameterName,

		i;



	for (i = 0; i < sURLVariables.length; i++) {

		sParameterName = sURLVariables[i].split('=');



		if (sParameterName[0] === sParam) {

			return sParameterName[1] === undefined ? true : sParameterName[1];

		}

	}

};


$(function(){

		var tab_index = getUrlParameter('tab');
		if(tab_index == 'gallery'){
			
			$(".tab3_gallery").click();

		}else if(tab_index == 'notices'){

			$(".tab2_gallery").click();

		}else{
			
		}	

	});


</script>




<script>
 
		$(document).ready(function(){

			$image_crop = $('#image_demo').croppie({
			enableExif: true,
			viewport: {
			  width:280,
			  height:280,
			  type:'square' //circle
			},
			boundary:{
			  width:300,
			  height:300
			}
		  });

		  $('#image').on('change', function(){
			var reader = new FileReader();
			reader.onload = function (event) {
			  $image_crop.croppie('bind', {
				url: event.target.result
			  }).then(function(){
				console.log('jQuery bind complete');
			  });
			}
			reader.readAsDataURL(this.files[0]);
			$('#uploadimageModal').modal('show');
		  });

		  $('.crop_image').click(function(event){
			$image_crop.croppie('result', {
			  type: 'canvas',
			  size: 'viewport'
			}).then(function(response){
			  $.ajax({
				url:"<?php echo base_url(); ?>admin/users/upload_crop_image",
				type: "POST",
				data:{"image": response},
				success:function(img)
				{
				  $('#uploadimageModal').modal('hide');
				  //$('#uploaded_image').html(data.text);
				  $('#uploaded_image').html('<img src="<?php echo base_url(); ?>uploads/'+img+'" width="130" height="130" class="thumbnail" />');
				  $('#cropped_image').val(img);
				}
			  });
			})
		  });

		});  
		</script>


								
								
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/croppie/croppie.js"></script>
<!--<script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>-->







<!---- Success Modal -->
  <div class="modal fade" id="SuccessModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Success!</h4>
        </div>
        <div class="modal-body">
		  <p style="color: green" id="suc_text"> Record updated Successfully! </p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Success Modal -->
 
 <!---- Error Modal -->
  <div class="modal fade" id="ErrorModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Error!</h4>
        </div>
        <div class="modal-body">
		  <p style="color: red" id="err_text"> Something went wrong! </p>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Error Modal -->





</body>
</html>