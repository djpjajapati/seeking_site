<?php $this->load->view("admin/include/header"); ?>
  
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
                 <div class="row">
					 <div class="col-md-6">
						<h2>Content Management</h2>
					 </div>
					 <div class="col-md-6 mb-md-3">
						 <a href="<?php echo base_url(); ?>admin/manage/add_static_page"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Add</button></a>
					 </div>
				 </div>
      
	  <div class="row" >
		<?php if($this->session->flashdata('error')){ ?>
			<div class="col-md-12" >
				<div class="alert alert-block  alert-danger">
					<button data-dismiss="alert" class="close close-sm" style="font-size: medium;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			</div>
		<?php } ?>
		
		<?php if($this->session->flashdata('success')){ ?>
				<div class="col-md-12" >
					<div class="alert alert-block  alert-success">
						<button data-dismiss="alert" class="close close-sm" style="font-size: medium;" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
						<?php echo $this->session->flashdata('success'); ?>
					</div>
				</div>
		<?php } ?>
		
		</div>
	  
	  
	  <div class="content-body">
	  
        <section id="configuration">
		
		
		
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Page Title</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					 <?php foreach($pages as $page){ ?>
					 
							<tr>
							  <td><?php echo $page->title; ?></td>
							   <td class="text-center">
								  <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>admin/manage/edit_static_page/<?php echo $page->ID; ?>"><i class="fas fa-pencil-alt text-white"></i></a>
									  <a class="btn btn-primary btn-xs"  data-toggle="tooltip" title="View" href="<?php echo base_url(); ?>admin/manage/view_static_page/<?php echo $page->ID; ?>"><i class="fas fa-eye text-white"></i></a>
								 <!--<a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete" onclick="del_page('<?php echo $page->ID; ?>')"><i class="far fa-trash-alt text-white"></i></a>-->
							  </td>
							</tr>
					 
					 
					 <?php } ?>
					  	

					<script>
					
					function del_page(){
						//alert("del page goes here");
					}
					
					</script>
						
                        <!--<tr>
                          <td>About Us</td>
                          <td class="text-center">
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>admin/manage/add_static_page"><i class="fas fa-pencil-alt text-white"></i></a>
                                  <a class="btn btn-primary btn-xs"  data-toggle="tooltip" title="View"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                           <tr>
                          <td>Privacy &amp; Policy</td>
                           <td class="text-center">
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>admin/manage/add_static_page"><i class="fas fa-pencil-alt text-white"></i></a>
                                  <a class="btn btn-primary btn-xs"  data-toggle="tooltip" title="View"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>-->
                        
						
						<!--<tr>
                          <td>FAQ</td>
                           <td class="text-center">
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>admin/manage/add_static_page"><i class="fas fa-pencil-alt text-white"></i></a>
                                  <a class="btn btn-primary btn-xs"  data-toggle="tooltip" title="View"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>Terms &amp; Conditions</td>
                          <td class="text-center">
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>admin/manage/add_static_page"><i class="fas fa-pencil-alt text-white"></i></a>
                                  <a class="btn btn-primary btn-xs"  data-toggle="tooltip" title="View"><i class="fas fa-eye text-white"></i></a>
                             <a class="btn btn-danger btn-xs py-md-1 px-md-1" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt text-white"></i></a>
                          </td>
                        </tr>-->
						
						
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

       
      </div>
    </div>
  </div>
    </div>
  
  
  
  
<?php $this->load->view('admin/include/footer'); ?>
