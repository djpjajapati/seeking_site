<?php $this->load->view('admin/include/header'); ?>


  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Add Featured Memorial</h3>
          <div class="row breadcrumbs-top d-inline-block">
          </div>
        </div>
      <!--  <div class="content-header-right col-md-6 col-12">
          <div class="dropdown float-md-right">
            <button class="btn btn-danger dropdown-toggle round btn-glow px-2" id="dropdownBreadcrumbButton"
            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
            <div class="dropdown-menu" aria-labelledby="dropdownBreadcrumbButton"><a class="dropdown-item" href="#"><i class="la la-calendar-check-o"></i> Calender</a>
              <a class="dropdown-item" href="#"><i class="la la-cart-plus"></i> Cart</a>
              <a class="dropdown-item" href="#"><i class="la la-life-ring"></i> Support</a>
              <div class="dropdown-divider"></div><a class="dropdown-item" href="#"><i class="la la-cog"></i> Settings</a>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body">
        <!-- Dropzone section start -->
        <section id="dropzone-examples">
          
		  <!--<div class="row">
            <div class="col-12">
              <div class="alert alert-warning" role="alert">
                <strong>Warning!</strong> Don’t forget to specify an url option if you’re
                not using a form element, since Dropzone doesn’t know where to post
                to without an action attribute.
              </div>
            </div>
          </div>-->
                      
					  
		 <form id="add_featured_memo_form" method="post" action="<?php echo base_url(); ?>admin/featured/do_add_featured" enctype="multipart/form-data">


			<?php if($this->session->flashdata('error')){ ?>
			 <div class="alert alert-block  alert-danger">
				  <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button">
					  <i class="fa fa-times"></i>
				  </button>
				   <?php echo $this->session->flashdata('error'); ?>
			  </div>
			 
		   <?php } ?>
		   
		   <?php if($this->session->flashdata('success')){ ?>
			 <div class="alert alert-block  alert-success">
				  <button data-dismiss="alert" class="close close-sm" style="font-size: 15px;" type="button">
					  <i class="fa fa-times"></i>
				  </button>
				   <?php echo $this->session->flashdata('success'); ?>
			  </div>
			 
		   <?php } ?>
		 
		    <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                      <div class="form-body">
                        
                        <div class="row">
                          <div class="col-md-6">
                              <label>First Name</label>
                            <div class="form-group half-group">
                              <input type="text" class="form-control" name="fname" placeholder="First Name" required>
                            </div>
                            </div>
                              <div class="col-md-6">
                            <label>Last Name</label>
                            <div class="form-group">
                              <input type="text" class="form-control" name="lname" placeholder="Last Name" required>
                            </div>
                              </div>
                          <div class="col-md-6">
                              <label>Date of Birth</label>
                             <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text">
                                          <span class="la la-calendar-o"></span>
                                        </span>
                                      </div>
                                      <input type="date" name="dob" id="dob" class="form-control" max="<?php echo date('Y-m-d'); ?>" placeholder="Pick-a-date" required />
                                    
									</div>  
                                </div>
								
								<p id="date_error" style="color:red"></p>
								
                            </div>
                            <div class="col-md-6">
                            <label>Date of Death</label>
                             <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text">
                                          <span class="la la-calendar-o"></span>
                                        </span>
                                      </div>
                                      <input type="date" name="dod" id="dod" onblur="check_date(this.value)" max="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder=" Pick-a-date" required />
                                    </div>  
                                </div>
                          </div>
                            <div class="col-md-12">
                                <label>Place of Death</label>
                            <div class="form-group">
                              <input type="text" class="form-control" name="placeofdeath" placeholder="Place of Death" required>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--<div class="form-actions">
                        <div class="text-left">
                          <button type="submit" class="btn btn-primary">Submit <i class="ft-thumbs-up position-right"></i></button>
                          <button type="reset" class="btn btn-warning">Reset <i class="ft-refresh-cw position-right"></i></button>
                        </div>
                      </div>-->
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
		  
		  <script>
		  
		  function check_date(dod){
			  var dob = $("#dob").val();
			 if(dod < dob){
				 $("#date_error").text("Date of Death must be greater than Date of Birth");
			 }else{
				 $("#date_error").text("");
			 }
		  }
		  
		  </script>
		  
		  
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" >Upload Display Image</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    
					
					
					
								<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>
								<link rel="stylesheet" href="<?php echo base_url(); ?>assets/crop/croppie.css" />
							
								<div style="text-align: center" class="">
										<div class="row">
										  <div class="col-md-12" >
												
												<div id="uploaded_image" class="thumbnail">
													<img src="<?php echo base_url(); ?>assests/img/default.png" width="150" height="180" alt=""/> 
												  </div>
												  
												  <!--<input id="cropped_image" name="cropped_image" type="text" value="nelson-mandela-blog1.png" />-->
												  <input id="cropped_image" name="cropped_image" type="hidden" value="" />
												  
													<span class="file-wrapper">
													  <input type="file" name="image" id="image" required />
													  <!--<span class="button red_btn">Choose a File</span>-->
													</span>
										  </div>
											
										  <div class="col-md-12" style="margin: 10px">
										  </div>
										</div>
									<!--<input type="submit" class="btn btn-primary pull-right" value="Update" />-->	
								</div>
			
                   
                  </div>
                </div>
              </div>
            </div>
            </div>
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Uplaod Images</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <p class="card-text">Select multiple files.
                      </p>
					  
					  <input type="file" name="userfile[]" multiple  />
					  
                    <!--<form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                      <div class="dz-message">Drop Files Here To Upload</div>
                    </form>-->
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" >Upload Music</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                   
                      <div class="form-group col-12 mb-2">
                       
                          <div>
                            <div class="row mb-1">
                              <div class="col-9 col-xl-10">
                                <label class="file center-block">
                                  <input type="file" name="audio" required>
                                  <span class="file-custom"></span>
                                </label>
                              </div>
                              <!--<div class="col-2 col-xl-1">
                                <button type="button"  class="btn btn-icon btn-danger mr-1"><i class="ft-x"></i></button>
                              </div>-->
                            </div>
                          </div>
                     
                        <!--<button type="button" class="btn btn-primary">
                          <i class="ft-plus"></i> Add file
                        </button>-->
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
            </div>
          
		  <script src="<?php echo base_url(); ?>/assets/ckeditor/ckeditor.js"></script>
		  
		  <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Add Quote </h4>
                  </div>
                  <div class="card-content collapse show">
                   <textarea id="editor" name="about" class="form-control" rows="2" placeholder="Add Quote" ></textarea>
                  </div>
                </div>
          </div>
        </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Bio</h4>
                  </div>
                  <div class="card-content collapse show">
                   <textarea id="editor1" name="description" class="form-control" rows="5" placeholder="Add Bio"></textarea>
                  </div>
                </div>
          </div>
        </div>
		
		
			<script>
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
				CKEDITOR.replace( 'editor' );
				CKEDITOR.replace( 'editor1' );
			</script>
		
		
          <div class="text-left">
               <button type="submit" class="btn btn-primary">Add</button>
          </div>
		  
		  </form>
		  
        </section>

      </div>
    </div>
  </div>
  

<!--  Crop Modal Start Here   -->
<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 text-center">
						  <div id="image_demo" style="width:100%; margin-top:30px"></div>
					</div>
					<div class="col-md-4" style="padding-top:30px;">
						<br />
						<br />
						<br/>
						  
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="red_btn crop_image">Upload</button>
				<!--<button id="uploadimageModal_btn" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
			</div>
		</div>
	</div>
</div>
<!--  Crop Modal End Here   -->
  
  

 <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 All rights reserved. </span>
    </p>
  </footer>

<script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/extensions/dropzone.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/extensions/dropzone.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
  type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
  type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/codemirror/lib/codemirror.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
         <script>
      $('.basic').summernote({
		popover: { },
		toolbar: [
 			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		  ]
		
      });
    </script>
	
	
	
	<script>
 
		$(document).ready(function(){

			$image_crop = $('#image_demo').croppie({
			enableExif: true,
			viewport: {
			  width:478,
			  height:588,
			  type:'square' //circle
			},
			boundary:{
			  width:400,
			  height:588
			}
		  });
		  
		  $('#image').on('change', function(){
			var reader = new FileReader();
			reader.onload = function (event) {
			  $image_crop.croppie('bind', {
				url: event.target.result
			  }).then(function(){
				console.log('jQuery bind complete');
			  });
			}
			reader.readAsDataURL(this.files[0]);
			$('#uploadimageModal').modal('show');
		  });

		  
		  $('.crop_image').click(function(event){
			$image_crop.croppie('result', {
			  type: 'canvas',
			  size: 'viewport'
			}).then(function(response){
			  $.ajax({
				url:"<?php echo base_url(); ?>admin/users/upload_crop_image",
				type: "POST",
				data:{"image": response},
				success:function(img)
				{
				  $('#uploadimageModal').modal('hide');
				  //$('#uploaded_image').html(data.text);
				  $('#uploaded_image').html('<img src="<?php echo base_url(); ?>uploads/'+img+'" width="130" height="130" class="thumbnail" />');
				  $('#cropped_image').val(img);
				}
			  });
			})
		  });

		});  
		</script>


								
<!-- For using quick crop refer to https://codepen.io/blackjacques/pen/bqgNoa -->								
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>
	
