<?php $this->load->view('admin/include/header'); ?>

<div class="app-content content">
    <div class="content-wrapper">
       <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Edit Pages</h3>
        </div>
          <div class="col-md-6 mb-md-3">
                 <a href="<?php echo base_url(); ?>admin/manage/content_management"><button class="btn btn-danger  round btn-glow px-2 float-right" type="button"  aria-haspopup="true" aria-expanded="false">Back</button></a>
             </div>
      </div>
      <div class="content-body">
        <!-- Dropzone section start -->
        <section id="dropzone-examples">
          <!--<div class="row">
            <div class="col-12">
              <div class="alert alert-warning" role="alert">
                <strong>Warning!</strong> Don’t forget to specify an url option if you’re
                not using a form element, since Dropzone doesn’t know where to post
                to without an action attribute.
              </div>
            </div>
          </div>-->
       <form method="post" action="<?php echo base_url(); ?>admin/manage/update_content">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Edit Title</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    
                      <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                               
                            <div class="form-group">
                              <input type="text" name="title" class="form-control" value="<?php echo $page->title; ?>" placeholder="Title Here">
							  <input type="hidden" name="id" value="<?php echo $page->ID; ?>" />
                            </div>
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
      
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Content</h4>
                  </div>
                  <div class="card-content collapse show">
                   <textarea name="basic" value="" rows="5" class="form-control basic" placeholder="Description"><?php echo $page->description; ?></textarea>
                  </div>
                </div>
          </div>
         </div>

          <div class="text-left">
                          <button type="submit" class="btn btn-primary">Update</button>
          </div>
		  
		  </form>
		  
        </section>
        <!-- // Dropzone section end -->
      </div>
    </div>
  </div>
  
  <?php //$this->load->view('admin/include/footer'); ?>
  
  <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 All rights reserved. </span>
    </p>
  </footer>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/extensions/dropzone.min.js" type="text/javascript"></script>
  <script src="app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/extensions/dropzone.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
  type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
  type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/codemirror/lib/codemirror.js"
  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
         <script>
      $('.basic').summernote({
		popover: { },
		toolbar: [
 			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		  ]
		
      });
    </script>

</body>
</html>