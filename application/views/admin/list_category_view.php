<?php 
$this->load->view('admin/include/header.php');
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<?php 
$this->load->view('admin/include/top_menu.php');
?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php 
$this->load->view('admin/include/sidebar.php');
?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			View Categories
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
					    <a href="<?=base_url()?>admin/">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					
					
					
					<li>
						Categories
						
					</li>
					
					
				
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			
			<div class="row">
				<div class="col-md-12">
						<a href="<?php echo base_url(); ?>admin/category/add" class="btn green" style="margin-bottom: 10px; float:right">Add Category</a>
				</div>
			</div>
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                      <?php if($this->session->flashdata('success')){ ?>
                    <div class="alert alert-success alert-block fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                       
                        <p>
                            <?php echo $this->session->flashdata('success'); ?>
                        </p>
                    </div>
                    <?php } ?>
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View Categories List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
							
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>								
								 <th> SNo</th>
								 <th> Title</th>
								 <th> Image </th>
								 <th> Created At</th>
								 
                                 <th style="100px !important; padding:5px;"> Action</th>
							</tr>
							</thead>
							<tbody>
                            <?php $i=1; foreach($categories as $row){ ?>
							<tr id="company_<?php echo $row['ID']; ?>" class="odd gradeX">
						      
								<td>
								
								<?php echo $i; ?>
								
								</td>
							  
								<td>
									 <?php echo $row['Name']; ?>
								</td>

                                <td>
									<?php $src = base_url()."assets/logo.png"; if(strlen($row['Image']) > 2) $src = base_url()."uploads/categories/".$row['Image']; ?>
                                    <img src="<?php echo $src; ?>" style="width:160px; height: 110px">
								</td>
								
								<td>
									
								<?php echo $row['createdAt']; ?> 
								</td>
								
							
                                <td class="center" style="width: 14%; padding:5px; vertical-align: middle; text-align: center;">
								
								  <a href="<?php echo base_url();?>admin/category/edit/<?php echo $row['ID'] ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
								  
                                  <a  onclick="del_company(<?php echo $row['ID']; ?>)" class="btn btn-primary btn-xs"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						     <?php $i++; } ?>
						
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		
		</div>
	</div>
	<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<?php 
$this->load->view('admin/include/footer.php');
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
  $('#sample_1').dataTable({
//"scrollY": "200px",
//"scrollCollapse": true,
"paging": true,
"bSort": false
/* aoColumnDefs: [
  {
     bSortable: false,
	 ordering: false
	 
   //  aTargets: [ -1,-2, -3, -4 ]
  }
] */
});
});


function del_company(id){
		
		if (confirm("Delete this record! Are you sure?") == false) {
                return;
		}
		
           $.post("<?php echo base_url(); ?>admin/category/delete_category/",{id:id},function(data)
           { 
				if(data == 'success'){
					$('#company_'+id).remove();
				}else{
					alert(data);
				}
           });
		
}

</script>
 
</body>
<!-- END BODY -->
</html>