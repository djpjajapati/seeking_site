<?php 

$theurl="https	://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>



<section class="footer_bg">
	<div class="container">
		<div class="footer_class">
			<div class="row">
				<div class="col-md-2  pad_right0"> <a href="<?php echo base_url(); ?>home"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/foot_logo.png"></a> </div>
				<div class="col-md-7 ">
					<ul class="list-inline foot_link ">
						<li class="list-inline-item "><a href="<?php echo base_url(); ?>home">Home</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>about">Safiri Salama</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>memorial/create_memorial">Create Memorial</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>featured">Featured Memorial</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>memorial/pay_now">Plans & Features</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>contact">Contact</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 pad_left0">
					<div class="prehead_left">
						<ul class="list-inline pull-right">
						<li class="list-inline-item">
						<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=
							<?php echo $theurl ?>">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
					</li>
				
					
					<!--<li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
							<li class="list-inline-item">
		<a target="_blank" 
		href="https://twitter.com/share?text=
		<?php echo $meta_description ?>&url=<?php echo $theurl ?>&hashtags=memories,remembering&via=safirisalama">
									
								<i class="fa fa-twitter" aria-hidden="true"></i>	
								</a>
							</li>
					
					
							<li style="display: none" class="list-inline-item">
								<a target="_blank" href="https://api.whatsapp.com/send?phone=+254722311588&text=
									<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
									<i class="fa fa-whatsapp" aria-hidden="true"></i>
								</a>
																					
							</li>
																			
							<li class="list-inline-item">
							
								<a href="mailto:?subject=I wanted you to see this site&amp;body= <?php echo $meta_description ?>. Here is the link to visit:  <?php echo $theurl ?>"
								title="Share by Email">
								<i class="fa fa-envelope" aria-hidden="true"></i>
								</a>
							
							</li>
							<li class="list-inline-item"><a href="">Share this page</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="list-inline copyright_link">
					<li class="list-inline-item"><a href="javascript:void(0);">Copyright@SafiriSalama LLC. All rights Reserved</a>
					</li>
					<li class="list-inline-item"><a href="<?php echo base_url(); ?>home/terms_conditions">Terms of Use</a>
					</li>
					<li class="list-inline-item"><a href="<?php echo base_url(); ?>home/privacy_policy">Privacy Policy</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
</footer> 