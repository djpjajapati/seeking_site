<div class="modal fade" id="SuccessModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 style="line-height: 1; color: black" class="modal-title">Success!</h4> </div>
			<div class="modal-body">
				<p style="color: green" id="suc_text"> Record updated Successfully! </p>
			</div>
			<div style="padding: 15px; text-align: right"> <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button> </div>
		</div>
	</div>
</div>
<div class="modal fade" id="ErrorModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 style="line-height: 1; color: black" class="modal-title">Error!</h4> </div>
			<div class="modal-body">
				<p style="color: red" id="err_text"> Something went wrong! </p>
			</div>
			<div style="padding: 15px; text-align: right"> <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button> </div>
		</div>
	</div>
</div>
<section class="footer_bg">
	<div class="container">
		<div class="footer_class">
			<div class="row">
				<div class="col-md-2  pad_right0"> <a href="<?php echo base_url(); ?>home"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img / foot_logo . png "></a> </div>
				<div class="col-md-7 ">
					<ul class="list-inlinefoot_link ">
						<li class="list-inline-item "><a href="<?php echo base_url(); ?>home">Home</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>about">Safiri Salama</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>memorial/create_memorial">Create Memorial</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>">Featured Memorial</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>memorial/pay_now">Plans & Features</a>
						</li>
						<li class="list-inline-item"><a href="<?php echo base_url(); ?>contact">Contact</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 pad_left0">
					<div class="prehead_left">
						<ul class="list-inline pull-right">
							<li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							</li>
							<li class="list-inline-item"><a target="_blank" href="https://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							</li>
							<li class="list-inline-item"><a href="">Share this page</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="list-inline copyright_link">
					<li class="list-inline-item"><a href="javascript:void(0);">Copyright@SafiriSalama LLC. All rights Reserved</a>
					</li>
					<li class="list-inline-item"><a href="">Terms of Use</a>
					</li>
					<li class="list-inline-item"><a href="">Privacy Policy</a>
					</li>
					<li class="list-inline-item"><a href="">Sitemap</a>
					</li>
					<li class="list-inline-item"><a href="<?php echo base_url(); ?>contact">Contact Us</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer> 
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>   
<script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>

<script type="text/javascript ">
	$.fn.jQuerySimpleCounter = function ( options ) {
		var settings = $.extend( {
			start: 0,
			end: 100,
			easing: 'swing',
			duration: 400,
			complete: ''
		}, options );
		var thisElement = $( this );
		$( {
			count: settings.start
		} ).animate( {
			count: settings.end
		}, {
			duration: settings.duration,
			easing: settings.easing,
			step: function () {
				var mathCount = Math.ceil( this.count );
				thisElement.text( mathCount );
			},
			complete: settings.complete
		} );
	};
	$( '#number1' ).jQuerySimpleCounter( {
		end: 17,
		duration: 3000
	} );
	$( '#number2' ).jQuerySimpleCounter( {
		end: 1267,
		duration: 3000
	} );
	$( '#number3' ).jQuerySimpleCounter( {
		end: 278,
		duration: 2000
	} );
	$( '#number4' ).jQuerySimpleCounter( {
		end: 350,
		duration: 2500
	} ); /* AUTHOR LINK */
	$( '.about-me-img' ).hover( function () {
		$( '.authorWindowWrapper' ).stop().fadeIn( 'fast' ).find( 'p' ).addClass( 'trans' );
	}, function () {
		$( '.authorWindowWrapper' ).stop().fadeOut( 'fast' ).find( 'p' ).removeClass( 'trans' );
	} );
</script>
<script type="text/javascript">
	$( '.owl-carousel' ).owlCarousel( {
		loop: true,
		margin: 35,
		nav: true,
		items: 3,
		smartSpeed: 750,
		navText: [ '<img src="<?php echo base_url(); ?>assets/img/left.png">', '<img src="<?php echo base_url(); ?>assets/img/right.png ">' ],
		responsive: {
			0: {
				items: 1
			},
			480: {
				items: 1
			},
			767: {
				items: 2
			},
			1200: {
				items: 3
			}
		}
	} );
</script> 
< /body></html >