<?php 

	$sess_data=$this->session->userdata('user');
	$login_id = 0;
	$login_email="";
	$fname="";
	$lname="";

	if($this->session->userdata('user')){

		$login_id = $sess_data->ID;	 
		$fname = $sess_data->fname;	 
		$lname = $sess_data->lname;	 
		$login_email = $sess_data->email;
	}
	$theurl="https	://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	
?>

<!doctype html>
<html class="no-js" lang="en">
<head><meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" href="<?php echo base_url(); ?>assets/img/icon.png" type="image/x-icon">
<!-- title -->
<title>
	<?php echo $title; ?>
</title>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78245835-12"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-78245835-12');
</script>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">

 <!--<meta property="og:image:width" content="200px">
  <meta property="og:image:height" content="200px">-->
 


<?php if($this->uri->segment(1) == 'featured'){ ?>

<!--<meta property="og:image" content="<?php echo 'https://safirisalama.com/uploads/1561972911.png'; ?>">
<meta property="og:image:width" content="200px">
  <meta property="og:image:height" content="200px">
<meta property="og:type" content="article">-->


<?php } ?>


<link rel="alternate" type="application/rss+xml" title="Safiri Salama Feed" href="https://safirisalama.com/feed" />

<link rel="stylesheet" type="text/css" href="
	<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="
		<?php echo base_url(); ?>assets/css/style.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
			<link rel="stylesheet" href="
				<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
				<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
					<link rel="stylesheet" href="
						<?php echo base_url(); ?>assets/css/prettyPhoto.css">
						<!-- css for slider -->
						<!--[if IE 9]>
						<link rel="stylesheet" type="text/css" href="css/ie.css" />
						<![endif]-->
						<!--[if IE]>
						<script src="js/html5shiv.min.js"></script>
						<![endif]-->
						
						
						
						
						
						<!--<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5ce3bb2d7ff0c00012df0d80&product='inline-share-buttons' async='async'></script>-->
						
						
						
						
					</head>
					<body>
						<section class="prehead d-none d-md-block">
							<div class="container">
								<div class="row">
									<div class="col-md-4">
										<div class="prehead_left">
											<ul class="list-inline">
												<li class="list-inline-item">
													<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=
														<?php echo $theurl ?>">
														<i class="fa fa-facebook" aria-hidden="true"></i>
													</a>
												</li>
											
												
												<!--<li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
												<li class="list-inline-item">
<a target="_blank" 
href="https://twitter.com/share?text=
<?php echo $meta_description ?>&url=<?php echo $theurl ?>&hashtags=memories,remembering&via=safirisalama">
														
													<i class="fa fa-twitter" aria-hidden="true"></i>	
													</a>
												</li>
												
												
												<li style="display: none" class="list-inline-item">
													<a target="_blank" href="https://api.whatsapp.com/send?phone=+254722311588&text=
														<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
														<i class="fa fa-whatsapp" aria-hidden="true"></i>
													</a>
																										
												</li>
																								
												<li class="list-inline-item">
												
													<a href="mailto:?subject=I wanted you to see this site&amp;body= <?php echo $meta_description ?>. Here is the link to visit:  <?php echo $theurl ?>"
													   title="Share by Email">
													   <i class="fa fa-envelope" aria-hidden="true"></i>
													</a>
												
												</li>
												
												
												<li class="list-inline-item">
													<a href="#" onclick="copyinglink()">Share this page</a>
												</li>
											</ul>
											<p>
												<a href="tel:(+254) 748 80 80 80">
													<i class="fa fa-phone" aria-hidden="true"></i>CALL US  (+254) 748 80 80 80
												</a>
											</p>
										</div>
									</div>
									<div class="col-md-4">
										<a href="<?php echo base_url(); ?>home">
											<img class="img-fluid" alt="img" src="
												<?php echo base_url(); ?>assets/img/logo.png">
											</a>
										</div>
										<div class="col-md-4">
											<div class="prehead_right pull-right d-flex align-items-center justify-content-center" style="height: 100%;">
												<ul class="list-inline pull-right">
													
													<?php if($this->session->userdata('user')){ ?>
													<li class="list-inline-item">
														<a href="
															<?php echo base_url(); ?>memorial/mymemorials">My Memorials
														</a>
													</li>
													<li class="list-inline-item">
														<a href="<?php echo base_url(); ?>profile">
															<?php echo ucfirst($fname); ?>
														</a>
													</li>
													<li class="list-inline-item">
														<a href="
															<?php echo base_url(); ?>logout">Logout
														</a>
													</li>
													<?php }else{ ?>
													<li class="list-inline-item">
														<a href="
															<?php echo base_url(); ?>login">SIGN IN
														</a>
													</li>
													<!--<li class="list-inline-item"><a href="
													<?php echo base_url(); ?>login/register">SIGN UP</a></li>-->
													<?php } ?>
													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="prehead d-block d-md-none">
								<div class="container">
									<div class="row">
										<div class="col-12">
											<a href="<?php echo base_url(); ?>home">
												<img class="img-fluid" alt="img" src="
													<?php echo base_url(); ?>assets/img/logo.png">
												</a>
											</div>
										</div>
										<div class="row">
											<div class="col-6">
												<div class="prehead_left">
													
													
													<ul class="list-inline">
														<li class="list-inline-item">
															<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=
																<?php echo $theurl ?>">
																<i class="fa fa-facebook" aria-hidden="true"></i>
															</a>
														</li>
														<!--<li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
														<li class="list-inline-item">
															<a target="_blank" href="https://twitter.com/share?url=
																<?php echo $theurl ?>" >
																<i class="fa fa-twitter" aria-hidden="true"></i>
															</a>
														</li>
														
														
														<li style="display: none" class="list-inline-item">
															<a target="_blank" href="https://api.whatsapp.com/send?phone=+254722311588&text=
																<?php echo $theurl ?>" >
																<i class="fa fa-whatsapp" aria-hidden="true"></i>
															</a>
																												
														</li>
																										
														<li class="list-inline-item">
														
															<a href="mailto:?subject=I wanted you to see this site&amp;body=<?php echo $theurl; ?>"
															   title="Share by Email">
															   <i class="fa fa-envelope" aria-hidden="true"></i>
															</a>
														
														</li>
														
														
														<li class="list-inline-item">
															<a href="">Share this page</a>
														</li>
													</ul>
													
													
													<p>
														<a href=""> 
															<i class="fa fa-phone" aria-hidden="true"></i>CALL US  (+254) 748 80 80 80
														</a>
													</p>
												</div>
											</div>
											<div class="col-6">
												<div class="prehead_right pull-right">
													<ul class="list-inline pull-right">
														
														<?php if($this->session->userdata('user')){ ?>
														<li class="list-inline-item">
															<a href="
																<?php echo base_url(); ?>memorial/mymemorials">My Memorials
															</a>
														</li>
														<li class="list-inline-item">
															<a href="<?php echo base_url(); ?>profile">
																<?php echo ucfirst($fname); ?>
															</a>
														</li>
														<li class="list-inline-item">
															<a href="
																<?php echo base_url(); ?>logout">Logout
															</a>
														</li>
														<?php }else{ ?>
														<li class="list-inline-item">
															<a href="
																<?php echo base_url(); ?>login">SIGN IN
															</a>
														</li>
														<!--<li class="list-inline-item"><a href="
														<?php echo base_url(); ?>login/register">SIGN UP</a></li>-->
														<?php } ?>
														
														
													</ul>
												</div>
											</div>
										</div>
									</div>
								</section>
								<nav class="navbar navbar-expand-md navbar-light">
									<div class="container">
										<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
											<span>
												<i class="fa fa-bars" aria-hidden="true"></i>
											</span>
										</button>
										<div class="collapse navbar-collapse" id="navbarSupportedContent">
											<ul class="navbar-nav full-width">
												<li class="nav-item 
													<?php if($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') echo 'active'; ?>">
													<a class="nav-link" href="
														<?php echo base_url(); ?>home">Home
													</a>
												</li>
												<li class="nav-item 
													<?php if($this->uri->segment(1) == 'about' || ( $this->uri->segment(1) == 'contact' && $this->uri->segment(2) != '') ) echo 'active'; ?>">
													<a class="nav-link" href="<?php echo base_url(); ?>about">Safiri Salama</a>
													<ul class="sub-menu">
														<li class="nav-item">
															<a href="
																<?php echo base_url(); ?>about" class="nav-link">About
															</a>
														</li>
														<li class="nav-item">
															<a href="
																<?php echo base_url(); ?>contact/Testimonials" class="nav-link">Testimonials
															</a>
														</li>
													</ul>
												</li>
												<li class="nav-item <?php if($this->uri->segment(1) == 'memorial' && $this->uri->segment(2) != 'pay_now') echo 'active'; ?>">
													<a class="nav-link" href="<?php echo base_url(); ?>memorial"> Memorials </a>
													<ul class="sub-menu">
														<li class="nav-item">
															<a href="
																<?php echo base_url(); ?>memorial/create_memorial" class="nav-link">Create Memorial
															</a>
														</li>
														<li class="nav-item">
															<a href="
																<?php echo base_url(); ?>memorial" class="nav-link">View Memorials
															</a>
														</li>
													</ul>
												</li>
												<li class="nav-item <?php if($this->uri->segment(1) == 'featured') echo 'active'; ?>">
													<a class="nav-link" href="<?php echo base_url(); ?>featured/view_featured_memorial">Featured Memorial</a>
													<ul class="sub-menu">
														<li class="nav-item"><a href="<?php echo base_url(); ?>featured/view_featured_memorial" class="nav-link">View Featured Memorial</a></li>
														<li class="nav-item"><a href="<?php echo base_url(); ?>featured" class="nav-link">View All Featured Memorials</a></li>
													</ul>
												</li>
												
												<!--<li class="nav-item"> 
													<?php if($this->uri->segment(1) == 'featured') echo 'active'; ?>">
													<a class="nav-link" href="<?php echo base_url(); ?>featured">Featured Memorial</a>
												</li> -->
												
												<li class="nav-item">
													<a class="nav-link 
														<?php if($this->uri->segment(1) == 'memorial' && $this->uri->segment(2) == 'pay_now') echo 'active'; ?>" href="
														<?php echo base_url(); ?>memorial/pay_now">Packages
													</a>
												</li>
												<li class="nav-item 
													<?php if($this->uri->segment(1) == 'contact' && $this->uri->segment(2) == '') echo 'active'; ?>">
													<a class="nav-link" href="
														<?php echo base_url(); ?>contact">Contact Us
													</a>
												</li>
												
												
												<?php if($this->uri->segment(1) == 'memorial' && $this->uri->segment(2) == 'tribute'){ ?>
												
												<li>
												
													<div style="display: none">
														<audio id="player1" class="playback" controls="controls" autoplay>
															<source id="mp3_src1" src="<?php echo base_url(); ?>uploads/music/<?php echo $memo->memorial_music; ?>" type="audio/mp3"/>
														</audio>
													</div>
													
													<span id="stop_music"> 
														<a style="cursor: pointer; color: white;" onclick="toggle_music('stop')"><i style="margin: 12px;    font-size: 25px;width: 30px;" class="fa fa-volume-up" aria-hidden="true"></i> </a> 
													</span>
													<span id="play_music" style="display: none"> 
														<a style="cursor: pointer; color: white;" onclick="toggle_music('play')"><i style="margin: 12px;     font-size: 25px;width: 30px;" class="fa fa-volume-off" aria-hidden="true"></i> </a> 
													</span>
												
												<li>
												
												<?php } ?>
												
												
											</ul>
										</div>
									</div>
								</nav>
<script>
function copyinglink() {
  /* Get the text field */
  var copyText = "https://safirisalama.com/";

  /* Select the text field */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
}
	</script>
								