<!---- Success Modal -->  
<div class="modal fade" id="SuccessModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-header">
            <h4 style="line-height: 1; color: black" class="modal-title">Success!</h4>
         </div>
         <div class="modal-body">
            <p style="color: green" id="suc_text"> Record updated Successfully! </p>
         </div>
         <div style="padding: 15px; text-align: right">            
         <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>		</div>
      </div>
   </div>
</div>
<!-- End Success Modal -->  <!---- Error Modal -->  
<div class="modal fade" id="ErrorModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-header">
            <h4 style="line-height: 1; color: black" class="modal-title">Error!</h4>
         </div>
         <div class="modal-body">
            <p style="color: red; font-size: 15px;" id="err_text"> Something went wrong! </p> 
         </div>
         <div style="padding: 15px; text-align: right">            
         <button type="button" class="btn btn-primary btn-small-edit" data-dismiss="modal">Close</button>		
         </div>
      </div>
   </div>
</div>
<!-- End Error Modal -->

<?php 
include_once('footer_menu.php'); 
?>

<!-- javascript libraries --><script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>   
<script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>   
<script type="text/javascript">       
 $.fn.jQuerySimpleCounter = function( options ) {        
    var settings = $.extend({            
        start:  0,            
        end:    100,            
        easing: 'swing',            
        duration: 400,            
        complete: ''        
    }, options );        
    
    var thisElement = $(this);        
    $({count: settings.start}).animate({count: settings.end}, {            
        duration: settings.duration,            
        easing: settings.easing,            
        step: function() {                
            var mathCount = Math.ceil(this.count);                
            thisElement.text(mathCount);            
        },            
        complete: settings.complete        
        });    
        };$('#number1').jQuerySimpleCounter({end: 17,duration: 3000});
        $('#number2').jQuerySimpleCounter({end: 1267,duration: 3000});
        $('#number3').jQuerySimpleCounter({end: 278,duration: 2000});
        $('#number4').jQuerySimpleCounter({end: 350,duration: 2500});    /* AUTHOR LINK */     
        $('.about-me-img').hover(function(){            
            $('.authorWindowWrapper').stop().fadeIn('fast').find('p').addClass('trans');        
            }, function(){            
                $('.authorWindowWrapper').stop().fadeOut('fast').find('p').removeClass('trans');        
        });
                
        </script>
        
        <script type="text/javascript">    
        $('.owl-carousel').owlCarousel({        
            loop:true,        
            margin:35,        
            nav:true,        
            items: 3,        
            smartSpeed: 750,        
            navText: [
                '<img src="<?php echo base_url(); ?>assets/img/left.png">', 
                '<img src="<?php echo base_url(); ?>assets/img/right.png">'],       
                 responsive : {            0 : {                
                     items: 1            
                     },           
                      480 : {               
                           items: 1            
                           },            
                           767 : {                
                               items: 2            
                               },            
                               1200 : {                
                                   items: 3            
                                   }       
                }    
            });
            </script>
 </body>
</html>