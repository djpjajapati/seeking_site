<?php $this->load->view("include/header.php"); ?>

   <section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="log_box">
                    <h5 class="text-center">Profile</h5>
                    <div class="superadminform">
                        <form class="space40 comment-form" name="login_form" id="login_form" method="post" action="<?php echo base_url(); ?>profile/update" enctype="multipart/form-data">
						
							<?php if($this->session->flashdata('error')){ ?>
								<div class="alert alert-block  alert-danger">
									<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
									<?php echo $this->session->flashdata('error'); ?>
								</div>
							<?php } ?>
							
							<?php if($this->session->flashdata('success')){ ?>
								<div class="alert alert-block  alert-success">
									<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
									<?php echo $this->session->flashdata('success'); ?>
								</div>
							<?php } ?>
						
						<?php if($profile_percentage < 100){ ?>
								<div class="alert alert-block  alert-danger">
									<strong></strong>
									Your Profile is <?php echo $profile_percentage; ?>%, Please complete your profile
								</div>
						<?php } ?>
						
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-half">
                                <input type="text" class="form-control" id="fname" placeholder="First Name" value="<?php echo $user->fname; ?>" name="fname">
								<input type="hidden" name="id" value="<?php echo $user->ID; ?>" />
							</div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group form-group-half">
                                <input type="text" class="form-control" id="lastname" placeholder="Last Name" value="<?php echo $user->lname; ?>" name="lname">
                            </div>
                            </div>
                            </div>
                            <div class="form-group form-group-half">
                                <input type="email" class="form-control" disabled id="email" placeholder="Email" value="<?php echo $user->email; ?>" name="email">
                            </div>
                            <div class="form-group form-group-half">
                                <input type="text" class="form-control" id="username" placeholder="Username" value="<?php echo $user->username; ?>" name="username">
                            </div>
							
							
							
							<div class="form-group form-group-half">
                                <input type="text" class="form-control" id="phone" placeholder="Phone" value="<?php if($user->phone != 0) echo $user->phone; ?>" name="phone">
                            </div>
							
							<div class="form-group form-group-half">
								<select class="form-control" style="height: 100%;" name="gender" id="gender">
									<option value="">Gender</option>
									<option value="Male" <?php if($user->gender == 'Male') echo 'selected'; ?>>Male</option>
									<option value="Female" <?php if($user->gender == 'Female') echo 'selected'; ?>>Female</option>
								</select>
                            </div>
							
							
							
							<div class="form-group form-group-half">
                                <input type="date" class="form-control" id="birthday" placeholder="Date of Birth" value="<?php echo $user->birthday; ?>" name="birthday">
                            </div>
							
							<div class="form-group form-group-half">
                                <input type="text" class="form-control" id="town_you_live_in" placeholder="Town You Live In" value="<?php echo $user->town_you_live_in; ?>" name="town_you_live_in">
                            </div>
							
							<div class="form-group form-group-half">
                                <select onchange="get_kenya(this.value)" class="form-control" style="height: 100%;" name="country_you_live_in" id="country_you_live_in">
									<option value="">Country you live in</option>
									<?php foreach($countries as $con){ ?>
									
										<option value="<?php echo $con->id; ?>" <?php if($user->country_you_live_in == $con->id) echo 'selected'; ?>><?php echo $con->name; ?></option>									
									<?php } ?>
								</select>
							
							</div>
							
							<div id="county_you_live" style="<?php if($user->country_you_live_in != 113) echo 'display: none';  ?>" class="form-group form-group-half">
                                <input type="text" class="form-control" id="county" placeholder="County You Live In" value="<?php echo $user->county; ?>" name="county">
                            </div>
							
							
							
							
							
                            <div class="row">
								<p style="margin-left: 15px;">Leave below fields empty if you don't want to change password </p>
								<div class="col-md-6">
									<div class="form-group form-group-half">
										<input type="password" class="form-control" id="password" placeholder="Password" name="password" value="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group form-group-half">
										<input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" value="">
									</div>
                                </div>
                            </div>
                            <div style="text-align: center" class="nospanstuff">

                                        <div class="row">

										  <div class="col-md-5">
												
												<div class="thumbnail">
													
													<?php $src = base_url()."assets/img/default.png"; if(strlen($user->pic) > 0) { $src = base_url()."uploads/".$user->pic; } ?>
													
											  	<img src="<?php echo $src; ?>" width="130" height="130" id="userImage" alt=""> 
											  
											  </div>
											
										  </div>
											
											<div class="col-md-7 text-left">
								
												
												<div class="add-photo_new m-0 mt-md-5">
												<a href="" class="red_btn btn-choose mt-3" style="color: #fff !important">Choose File</a>
												<input type="file" name="UserPhoto" id="UserPhoto">
											  </div>
												
												<!--<a href="" class="red_btn tribute_box-btn m-0 mt-2"><i class="fa fa-upload"></i> Upload</a>-->
											</div>
                                            

                                        </div>


                                    </div>
                           <div class="row">
                               <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-4">
                            <input type="hidden" name="action" value="ajax_logimein">
                            <input type="submit" class="logimein log_btn" id="loginButton" name="subbtnlast" value="Update">
                            <p class="errormsg"></p>
                               </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view("include/footer.php"); ?>

<script>

function forceNumeric(){
    var $input = $(this);
    $input.val($input.val().replace(/[^\d]+/g,''));
}
$('body').on('propertychange input', 'input[name="phone"]', forceNumeric);

function get_kenya(country){
	
	if(country == 113){
		$("#county_you_live").show();
	}else{
		$("#county_you_live").hide();
	}
	
}

function readURL(input) {
            
		if (input.files && input.files[0]) {
				
				var reader = new FileReader();
									
				reader.onload = function (e) {
					$('#userImage').attr('src', e.target.result);
				}
									
				reader.readAsDataURL(input.files[0]);
		}
}
                                        
$("#UserPhoto").change(function(){
		readURL(this);
});

</script>