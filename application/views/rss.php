<?php echo '<?xml version="1.0" encoding="utf-8"?>' . "\n"; 
    
	foreach ($latest_memorials as $loop) {
?>    
       
<item>
    <title>
        <a href="<?php echo site_url('memorial/tribute/'.$loop->webaddress); ?>"> 
		<?php
		 echo xml_convert($loop->fname.' '.$loop->lname);
		?>
		</a>
	</title>
          
<description>
    <?php
	 echo xml_convert($loop->about);
	?>
</description>      
 
<created>

<?php
	 echo xml_convert($loop->created);
?>

</created>
 
</item>        
     
<?php
	}
?>
