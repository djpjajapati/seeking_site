<?php $this->load->view('include/header'); ?>    

<style>
    .galleryimg
    {
    -webkit-border-radius: 12px;
-moz-border-radius: 12px;
border-radius: 8px;
    }
</style>
<style>
/* width */
::-webkit-scrollbar {
  width: 7px;
	border-radius: 10px;
	
}
::-webkit-scrollbar:hover {
  width: 7px;
	border-radius: 10px;
	
}
/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
</style>
<section class="about_nelson">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-5">
			<div class="position-relative">
            		<img width="100%" src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>" class="img-fluid mainimg wp-post-image" alt="<?php echo $memo->fname.' '.$memo->lname; ?>" srcset="">              
				</div>
						<ul class="list-inline d-flex mandela_three space15">
							<div class="imagegallery">  
							  <?php foreach($pictures as $p){ ?>
							  

									   <a href="<?php echo base_url(); ?>uploads/<?php echo $p->image; ?>">
											<img  class="galleryimg galleryimg-mb" style="width: 157px; object-fit: cover; object-position: 0 0; height: 100px" src="<?php echo base_url(); ?>uploads/<?php echo $p->image; ?>" alt="">
										</a>
	

							  <?php } ?>
							</div>
							  
                        </ul>
        </div>
        <div class="col-12 col-md-7">
         <div class="featured_box">
          <div class="scrollcontent"  style="overflow-y:auto;height: 301px;">
            <h2 class="nameHead" style="line-height: 1.2;"><?php echo $memo->fname.' '.$memo->lname; ?></h2>
                        <h4 class="yeartag"><?php echo date('Y', strtotime($memo->dob)).'-'.date('Y', strtotime($memo->dod)); ?></h4>
            <div class="descrip mb-0"><p></p><p><?php echo $memo->description; ?></p>
			<p></p>
                      </div>
         
                                         
        </div>
			</div>
			 <div class="infoBox">
            <div class="d-flex mb-1">
              <label class="lblone">Date of Birth:</label>
              <p class="lbltwo"><?php echo date('d/m/Y', strtotime($memo->dob)); ?></p>
            </div>
            <div class="d-flex mb-1">
              <label class="lblone">Date of Death:</label>
              <p class="lbltwo"><?php echo date('d/m/Y', strtotime($memo->dod)); ?></p>
            </div>
            <div class="d-flex">
              <label class="lblone">Place of Death:</label>
              <p class="lbltwo"><?php echo $memo->placeofdeath; ?></p>
            </div>
				 <!--<a href="#" class="edit_class"><i class="fa fa-pencil"></i> Edit</a>-->
          </div>
          <div class="featured_box">
              <div class="scrollcontent" style="overflow-y:auto;height: 77px;">
            <div class="quoteTag mb-0 ">
			
				<?php $string = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $memo->about);  // remove first and last paragraph <p> and </p> ?>
			
				<p><img src="<?php echo base_url(); ?>assets/img/quote1.png" width="24" height="24" class="d-inline-block mr-2" alt="..."> <?php echo trim($string) ?> <img src="<?php echo base_url(); ?>assets/img/quote2.png" width="24" height="24" class="d-inline-block" alt="..."></p>
</div>
</div>
          </div>
		  
		  
		   <ul class="list-inline tribute_list tl">
					<?php if($this->session->userdata('user')){ ?>
						<li class="list-inline-item" style="width: 31%"><a onclick="leave_tribute('ttab2')" href="javascript:;" class="red_btn lea_tri">Lay a Flower</a></li>
						<li class="list-inline-item" style="width: 31%"><a onclick="leave_tribute('ttab1')" href="javascript:;" class="red_btn">Light a Candle</a></li>
						<li class="list-inline-item" style="width: 31%"><a onclick="leave_tribute('ttab3')" href="javascript:;" class="red_btn">Leave a Note</a></li>
					<?php }else{ ?>
						<li class="list-inline-item" style="width: 31%"><a href="<?php echo base_url(); ?>login" class="red_btn lea_tri">Lay a Flower</a></li>
						<li class="list-inline-item" style="width: 31%"><a href="<?php echo base_url(); ?>login" class="red_btn">Light a Candle</a></li>
						<li class="list-inline-item" style="width: 31%"><a href="<?php echo base_url(); ?>login" class="red_btn">Leave a Note</a></li>
						
					<?php } ?>
          </ul>
           
              <!--<a href="javascript:;" class="btn btntribute">Leave a Tribute</a>
			  <a href="javascript:;" class=" btn btntribute ">Light a Candle</a>    -->
      </div>
      </div>
      <div class="space40 d-xl-none"></div>
      <div class="row space40">
        <div class="col-md-8">
			 
		
		<div>
                    
					 
                    <div class="leave_tribute" style="margin-bottom: 5px">
                        <span class="tri_span"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/pencil.png"><span id="tribute_title">Leave a Tribute</span></span>
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" id="ttab1" onclick="update_tribute(this.id)" href="#tab1"><img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle.png">
                            <img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="ttab2" onclick="update_tribute(this.id)" href="#tab2"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower1.png">
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="ttab3" onclick="update_tribute(this.id)" href="#tab3"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather.png">
                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather1.png">
                            </a>
                          </li>
                        </ul>
                        <div class="tab-content">
							<input type="hidden" id="leave_tab" />
                            <div id="tab1" class="tab-pane active"><br>
                              <textarea class="form-control" rows="8" id="comment1" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab2" class="tab-pane fade"><br>
                              <textarea class="form-control" rows="8" id="comment2" placeholder="Add your tribute text here"></textarea>
                            </div>
                            <div id="tab3" class="tab-pane fade"><br>
                              <textarea class="form-control" rows="8" id="comment3" placeholder="Add your tribute text here"></textarea>
                            </div>
                        </div>
                        <ul class="list-inline leave_ul">
                          <li class="list-inline-item">by <?php echo $this->session->userdata('user')->fname.' '.$this->session->userdata('user')->lname; ?></li>
                          <li class="list-inline-item pull-right">
                            <a href="#!" onclick="add_tribute()" class="red_btn">Leave a Tribute</a>
                          </li>
                        </ul>
                    </div>
				
					
					<div class="main_tri" id="tribute_div">
                      
					  <?php foreach($tributes as $t){ ?>
					  
					  <?php

					  $t_img = 'feather1.png';
					  if($t->action == "candle"){
						  $t_img = 'christmas-candle.png';
					  }elseif($t->action == 'tribute'){
						  $t_img = 'flower1.png';
					  }

					  ?>
					  
					  
						  <div class="featured_box" id="tribute<?php echo $t->ID ?>">
							  <div class="media">
							  
							  <img class="flower-mb" src="<?php echo base_url(); ?>assets/img/<?php echo $t_img; ?>" alt="mage">
							  
								<div class="media-body commnt-mb">
								  <h5><?php echo $t->by; ?> <i><?php echo $t->d_action; ?></i>
								   
								  </h5>
								  <i onClick="markinappropriate(<?php echo $t->ID ?>)" style="cursor: pointer" title="Flag as inappropriate" class="pull-right fa fa-flag" aria-hidden="true"></i>
								  <p class="comment-mb"><?php echo $t->comment; ?></p>								  
								  </br>
								  <h6><?php echo date('l', strtotime($t->createdAt)).', '.date("F", strtotime($t->createdAt)).' '.date("d", strtotime($t->createdAt)); ?> <?php echo date("Y",strtotime($t->createdAt)); ?> <?php echo date("h:i:a",strtotime($t->createdAt)); ?> </h6>
									<h6>
									  <?php 
										if($this->session->userdata('user')->ID==$t->createdby) 
										{
										?>
										<a href="javascript:;" onClick="openeditmodal(<?php echo $t->ID ?>)">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="javascript:;" onClick="deletemytribute(<?php echo $t->ID ?>,<?php echo $t->createdby ?> )">Delete</a>
										<?php }?>
									</h6>
								  <?php //if($t->action == "candle"){ ?>
									<!--<img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">-->
								  <?php //} ?>
								</div>
							  </div>
						  </div>
						  
					  <?php } ?>
					  
                    </div>
					
				
					
                  </div>
				  
				  <script>
				  
				  function leave_tribute(tab){
			
					  if(tab == 'ttab1'){
						  $("#leave_tab").val('candle');
						  $("#tribute_title").text("Light a Candle");
					  }else if(tab == 'ttab2'){
						  $("#leave_tab").val('tribute');
						  $("#tribute_title").text("Lay a Flower");
					  }else{
						  $("#leave_tab").val('note');
						  $("#tribute_title").text("Leave a Note");
					  }
					  
					  $(".leave_tribute").show();
					  $('#'+tab).click();					  
				  }
				  
				  function update_tribute(tab){
					  if(tab == 'ttab1'){
						  $("#leave_tab").val('candle');
						  $("#tribute_title").text("Light a Candle");
					  }else if(tab == 'ttab2'){
						  $("#leave_tab").val('tribute');
						  $("#tribute_title").text("Lay a Flower");
					  }else{
						  $("#leave_tab").val('note');
						  $("#tribute_title").text("Leave a Note");
					  }
				  }
				  
				  function add_tribute(){
					  
					  var memo_id = '<?php echo $memo->ID; ?>';
					  var memo_created_id = '<?php echo $memo->createdby; ?>';
					  var login_user = '<?php if($this->session->userdata('user')) echo $this->session->userdata('user')->ID;  ?>';
					  var action = $("#leave_tab").val();
					  var comment = '';
					  if(action == 'candle'){
						  comment = $("#comment1").val();
					  }else if(action == 'tribute'){
						  comment = $("#comment2").val();
					  }else if(action == 'note'){
						  comment = $("#comment3").val();
					  }else{
						  
					  }
					  if(comment=="" || comment.length < 20)
						  {
							  alert("Atleast 20 characters required.");
							  return;
						  }
					  $.ajax({
						url:"<?php echo base_url(); ?>featured/add_tribute",
						type: "POST",
						data:{"memo_id": memo_id, "action": action, "comment": comment, "memo_created_id": memo_created_id},
						success:function(resp)
						{
							if(resp == 'block'){
								$("#err_text").text("You are block from this memorial!");
								$("#ErrorModal").modal("show");
								return false;
							}else{
								$(".leave_tribute").hide();
								$("#tribute_div").html("");
								$("#tribute_div").html(resp);
							}
							
							/* if(login_user != memo_created_id){
								$("#suc_text").text("Tribute will be posted after the approval from page owner!");
								$("#SuccessModal").modal("show");
							} */
							
							
						}
					}); 
					  
				  }
				  
				  
				  </script>

      </div>
    
        <div class="col-lg-4 col-md-5">
         <div class="counte_Class">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pageviews">

                        <div class="card-header-page">
                         	<ul>
								<li><span><?php echo $memo->views; ?></span> <br />Page View</li>
								<li><span><?php echo count($tributes); ?></span> <br />Tributes Posted</li>
								<li><span><?php echo $flowers_laid; ?></span><br />Flowers Laid</li>
							</ul>
                        </div>
                    </div>
                        </div>
                    </div>
        </div>
         
        <p class="space30"></p>
        <div class="player info">

                 <h1>Now Playing</h1>
                 <h4><?php echo str_replace(".mp3", "", $memo->audio); ?></h4>
          <div class="button-items music-mb" style="margin: 0 39px;">
                    <!--<audio id="music" preload="auto" loop="false">
						  <source src="<?php echo base_url(); ?>uploads/music/<?php echo $memo->audio; ?>" type="audio/mp3">
                    </audio>-->
					
					<audio id="music" controls loop autoplay>
						<source id="mp3_src" src="<?php echo base_url(); ?>uploads/music/<?php echo $memo->audio; ?>" type="audio/mp3"/>
					</audio>
                    
                 </div>
			<br />
        </div>
			<p class="space30"></p>
			<a href="<?php echo base_url(); ?>featured" class="red_btn btn-block" style="max-width: 100%;">View All Featured Memorial</a>

        </div>
      </div>
    </div>
  </section>

  
  
  
  
   <div class="modal fade" id="edittribute" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="line-height: 1; color: black" class="modal-title">Edit Content</h4>
        </div>
        <div class="modal-body">
		<Label>Please enter text</label>
		  <textarea class="form-control" id="editingtribute"></textarea>
		</div>
        
		<div style="padding: 15px; text-align: right">
            <button type="button" class="btn btn-primary btn-small-edit" onclick="updatethetribute()" style="background: #582236; border-color: #582236;">Update</button>
            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>
		</div>
      </div>
      
    </div>
  </div>
 <!-- End Success Modal -->
  
  

<?php $this->load->view('include/footer'); ?>    


<script>

$(document).ready(function(){
  
	var memo_id = '<?php echo $memo->ID; ?>';
	var views = '<?php echo $memo->views + 1; ?>';
	
	$.ajax({
		url:"<?php echo base_url(); ?>login/update_featured_views",
		type: "POST",
		data:{"memo_id": memo_id, "views": views},
		success:function(resp)
		{
			///
		}
	});  
});

</script>

<script>
function markinappropriate(theid)
	{
		  $.ajax({
			url:"<?php echo base_url(); ?>featured/markinappropriate",
			type: "POST",
			data:{"id": theid},
			success:function(resp)
			{
				if(resp == 'Error'){
					alert("You have already submitted.")
				}
				else
					{
					alert(resp)
					}

			}
			}); 
	}
	
	function deletemytribute(theid,adderid)
	{
		if (confirm('Are you sure you want to delete?')) {
		  $.ajax({
			url:"<?php echo base_url(); ?>featured/deletemytribute",
			type: "POST",
			data:{"id": theid ,"adderid": adderid},
			success:function(resp)
			{
				$( "#tribute"+theid ).remove();
			}
			}); 
		}
	}
	
		function openeditmodal(theid){	
			$("#edittribute").modal("show");
			var hery=$( "#tribute"+theid ).find( "p" ).text();
			$("#editingtribute").text(hery);
			globalID=theid;
		}
	
		function updatethetribute()
			{
				var newmessage= $("#editingtribute").val();
			  $.ajax({
				url:"<?php echo base_url(); ?>featured/updatemytribute",
				type: "POST",
				data:{"id": globalID , "thetribute": newmessage},
				success:function(resp)
				{
					$( "#tribute"+globalID ).find( "p" ).text(resp);
					$("#edittribute").modal("hide");
				}
				}); 
			}
	//$('#mp3_src1').attr('src', '<?php echo base_url(); ?>uploads/music/<?php echo $memo->memorial_music; ?>');

</script>

<script>

var x = document.getElementById("music");
	x.play();
	
//window.onload = function() {
  //var context = new AudioContext();
//}
</script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/simpleLightbox-master/dist/simpleLightbox.min.css">
        <script src="<?php echo base_url(); ?>assets/simpleLightbox-master/dist/simpleLightbox.min.js"></script>

<script>
$('.imagegallery a').simpleLightbox();

</script>

</head>
