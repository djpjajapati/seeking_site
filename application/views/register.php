<?php $this->load->view("include/header.php"); ?>

	<section class="package_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="log_box">
                    <h5>Create your account.</h5>
                    <div class="superadminform">
                        <form action="<?php echo base_url(); ?>login/add_access" class="space40 comment-form" name="login_form" id="login_form" method="post">
							 
							 <?php if($this->session->flashdata('success')){ ?>
							  <div class="alert alert-block  alert-success">
								  <button data-dismiss="alert" class="close close-sm" type="button">
									  <i class="fa fa-times"></i>
								  </button>
								  <strong></strong> <?php echo $this->session->flashdata('success'); ?>
							  </div>

						  <?php } ?>

						  <?php if($this->session->flashdata('error')){ ?>
							  <div class="alert alert-danger">
								  <button class="close" data-close="alert"></button>
								  <?php echo $this->session->flashdata('error'); ?>
							  </div>
						  <?php } ?>
							 
							 <div class="form-group">
                                <input type="text" class="form-control" id="fname" placeholder="First Name" value="" name="fname" required>
                            </div>
							<div class="form-group">
                                <input type="text" class="form-control" id="lname" placeholder="Last Name" value="" name="lname" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" placeholder="Email" value="" name="email" required>
                            </div>
                            <div class="form-group">
								(6 characters minimum)
                                <input type="password" class="form-control" minlength="" pattern=".{6,}" id="password" placeholder="Password" name="password" value="" required>
                            </div>
							
							<div class="form-group">
                                <input type="password" class="form-control" pattern=".{6,}" id="cpassword" placeholder="Confirm Password" name="cpassword" value="" required>
                            </div>
                            
                            <input type="hidden" name="action" value="ajax_logimein">
                            <input type="submit" class="logimein log_btn" id="" name="subbtnlast" value="Register">
                            <p class="errormsg"></p>
                        </form>
                        <!-- <a href="" class="log_btn">LOG IN</a>-->
						
							<!--<p class="social_text">- or log in with social media -</p>

                                <div class="apsl-login-networks theme-4 clearfix text-center">
									<div class="social-networks">
										<a href="#" title="Login with facebook">
											<div class="apsl-icon-block icon-facebook">
												<i class="fa fa-facebook"></i>
												<span class="apsl-long-login-text">Login with facebook</span>
											</div>
										</a>
										<a href="#" title="Login with twitter">
											<div class="apsl-icon-block icon-twitter">
												<i class="fa fa-twitter"></i>
												<span class="apsl-long-login-text">Login with twitter</span>
											</div>
										</a>
										<a href="#" title="Login with google">
										<div class="apsl-icon-block icon-google">
											<i class="fa fa-google"></i>
											<span class="apsl-long-login-text">Login with google</span>
										</div>
									 </a>
								</div>
							</div>-->
						
                        <p class="mt-2"><a href="<?php echo base_url(); ?>login" class="log_links">Already have account please click for login.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="footer_bg">
  <div class="container">
    <div class="footer_class">
      <div class="row">
        <div class="col-md-2  pad_right0">
            <a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/foot_logo.png"></a>
        </div>
        <div class="col-md-7">
            <ul class="list-inline foot_link">
                <li class="list-inline-item"><a href="index.html">Home</a></li>
                <li class="list-inline-item"><a href="about.html">Safiri Salama</a></li>
                <li class="list-inline-item"><a href="create-memorial.html">Create Memorial</a></li>
                <li class="list-inline-item"><a href="featured-memorial.html">Featured Memorial</a></li>
                <li class="list-inline-item"><a href="pricing.html">Plans &amp; Features</a></li>
                <li class="list-inline-item"><a href="contact.html">Contact</a></li>
            </ul>
        </div>
        <div class="col-md-3 pad_left0">
            <div class="prehead_left">
                <ul class="list-inline pull-right">
                    <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li class="list-inline-item"><a href="">Share this page</a></li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- javascript libraries -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
        $(function () {
          $("#datepicker").datepicker({ 
                autoclose: true, 
                todayHighlight: true
          }).datepicker('update', new Date());
        });
        $(function () {
          $("#datepicker1").datepicker({ 
                autoclose: true, 
                todayHighlight: true
          }).datepicker('update', new Date());
        });
    </script>



</body>
</html>