<?php $this->load->view('include/header'); ?>    
<div id="demo" class="carousel slide" data-ride="carousel">
   <!-- Indicators -->  <!-- <ul class="carousel-indicators">    <li data-target="#demo" data-slide-to="0" class="active"></li>    <li data-target="#demo" data-slide-to="1"></li>    <li data-target="#demo" data-slide-to="2"></li>  </ul> -->  <!-- The slideshow -->  
   <div class="carousel-inner">
      <div class="carousel-item active">
         <img src="<?php echo base_url(); ?>assets/img/Banner1.jpg" alt="img" class="car_img">      
         <div class="header_content header_bg">
            <div class="container">
               <div class="left_car">
                  <div class="left_head">
                     <h1>Memories</h1>
                     <span>Designed to Last</span>                
                     <p>Bring all the warm memories of your loved one<br /> together in a Memorial Website</p>
                     <a href="<?php echo base_url(); ?>memorial/create_memorial" class="white_btn">Create a memorial</a>            
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="carousel-item">
         <img src="<?php echo base_url(); ?>assets/img/Banner2.jpg" alt="img" class="car_img">      
         <div class="header_content header_bg">
            <div class="container">
               <div class="left_car">
                  <div class="left_head">
                     <h1>Every Life is a Story</h1>
                     <p>“I don't want to forget our memories...<br> I want to remember them without pain"</p>
                     <a href="<?php echo base_url(); ?>memorial/create_memorial" class="white_btn">Create a memorial</a>            
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="carousel-item">
         <img src="<?php echo base_url(); ?>assets/img/Banner3.jpg" alt="img" class="car_img">      
         <div class="header_content header_bg">
            <div class="container">
               <div class="left_car">
                  <div class="left_head">
                     <span>When  Someone you love<br /> becomes a Memory…</span>                
                     <p>…the Memory becomes a treasure.</p>
                     <a href="<?php echo base_url(); ?>memorial/create_memorial" class="white_btn">Create a memorial</a>            
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Left and right controls -->  <a class="carousel-control-prev" href="#demo" data-slide="prev">    <span class="carousel-control-prev-icon"></span>  </a>  <a class="carousel-control-next" href="#demo" data-slide="next">    <span class="carousel-control-next-icon"></span>  </a> 
</div>
<section class="memorials">
   <div class="container">
      <div class="search_box search_box_main">
         <div class="row">
            <div class="col-md-12">
               <form method="post" action="<?php echo base_url(); ?>memorial/gallery" >
                  <div class="row ">
                     <div class="col-md-5">
                        <h3>Search for a memorial:</h3>
                        <div class="row pad_right5">
                           <div class="col-md-8 pad6">                                      
								<input name="keywords" type="text" class="form-control" placeholder="Names">                                  
							</div>
                           <div class="col-md-4 pad6">                                      
						   <button class="btn_start" style="cursor: pointer" type="submit">SEARCH</button>                                  
						   </div>
                        </div>
                     </div>
                     <div class="col-md-7">
                        <h3>I want to create a memorial for ...</h3>
                        <div class="row pad_left5">
							<div class="col-md-4 pad6">                                      
								<input id="fname" type="text" class="form-control" placeholder="First Name">                                  
							</div>
							
                            <div class="col-md-4 pad6">                                      
								<input type="text" id="lname" class="form-control" placeholder="Last Name">                                  
							</div>
							
                            <div class="col-md-4 pad6">                                     
								<button class="btn_start" style="cursor: pointer" onclick="search_memo()" type="button">GET STARTED</button>  
						    </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <script>				
		  function search_memo(){						
				var fname = $("#fname").val();			
				var lname = $("#lname").val();			
				window.location.href = '<? echo base_url(); ?>memorial/create_memorial?fname='+fname+'&lname='+lname;
			}				
		</script>		        
      <div class="row">
         <div class="col-md-12">
            <h2 class="h2_class pull-left">Recent Memorials </h2>
            <a href="<?php echo base_url(); ?>memorial" class="red_btn pull-right">View All Memorials</a>				
            <div class="clearfix"></div>
            <div class="memo_class">
               <div class="owl-carousel owl-theme">
                  <?php foreach($memorials as $memo){ ?>					  
                  <a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memo->webaddress; ?>">
                     <div class="item">
                        <div class="memo_block">
                           <img class="img-fluid" alt="<?php echo $memo->fname.' '.$memo->lname; ?>" src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>">								
                           <p><?php echo $memo->fname.' '.$memo->lname; ?></p>
                           <span><?php echo date('Y', strtotime($memo->birthdate)); ?>-<?php echo date('Y', strtotime($memo->deathdate)); ?></span>							
                        </div>
                     </div>
                  </a>
                  <?php } ?>					  					 <!-- <div class="item">                        <div class="memo_block">                            <img class="img-fluid" alt="img" src="img/img1.png">                            <p>Mr. Brat K</p>                            <span>1986-2000</span>                        </div>                      </div>                      <div class="item">                        <div class="memo_block">                                <img class="img-fluid" alt="img" src="img/img2.png">                                <p>Mrs. Siana</p>                                <span>1986-2000</span>                        </div>                      </div>                      <div class="item">                        <div class="memo_block">                            <img class="img-fluid" alt="img" src="img/img1.png">                            <p>Mr. Brat K</p>                            <span>1986-2000</span>                        </div>                      </div>                      <div class="item">                        <div class="memo_block">                                <img class="img-fluid" alt="img" src="img/img2.png">                                <p>Mrs. Siana</p>                                <span>1986-2000</span>                        </div>                      </div>                      <div class="item">                        <div class="memo_block">                            <img class="img-fluid" alt="img" src="img/img1.png">                            <p>Mr. Brat K</p>                            <span>1986-2000</span>                        </div>                      </div>                      <div class="item">                        <div class="memo_block">                                <img class="img-fluid" alt="img" src="img/img2.png">                                <p>Mrs. Siana</p>                                <span>1986-2000</span>                        </div>                      </div>  -->					                    
               </div>
            </div>
            <div class="counte_Class">
               <div class="row">
                  <div class="col-md-12">
                     <div id="projectFacts" class="sectionClass">
                        <div class="fullWidth eight columns">
                           <div class="projectFactsWrap ">
                              <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_visitor; ?>" style="visibility: visible;">
                                 <p id="number32" class="number"><?php echo $total_visitor; ?></p>
                                 <p>Our Visitors</p>
                              </div>
                              <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_memorial; ?>" style="visibility: visible;">
                                 <p id="number32" class="number"><?php echo $total_memorial; ?></p>
                                 <p>Memorials Created</p>
                              </div>
                              <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_tributes; ?>" style="visibility: visible;">
                                 <p id="number33" class="number"><?php echo $total_tributes; ?></p>
                                 <p>Tributes Written</p>
                              </div>
                              <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $total_picture_uploaded; ?>" style="visibility: visible;">
                                 <p id="number34" class="number"><?php echo $total_picture_uploaded; ?></p>
                                 <p>Pictures Uploaded</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>



<?php $this->load->view("include/footer_menu.php"); ?>




<!-- javascript libraries --><script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>  <script type="text/javascript">        
$.fn.jQuerySimpleCounter = function( options ) {        
var settings = $.extend({            
start:  0,            
end:    100,            
easing: 'swing',            
duration: 400,            
complete: ''        
}, options );        

var thisElement = $(this);        
	$({count: settings.start}).animate({count: settings.end}, {            
	duration: settings.duration,            
	easing: settings.easing,            
	step: function() {               
			var mathCount = Math.ceil(this.count);                
			thisElement.text(mathCount);            
			},            
			complete: settings.complete        
	});    
};

$('#number1').jQuerySimpleCounter({end: 17,duration: 3000});
$('#number2').jQuerySimpleCounter({end: 1267,duration: 3000});
$('#number3').jQuerySimpleCounter({end: 278,duration: 2000});
$('#number4').jQuerySimpleCounter({end: 350,duration: 2500});    /* AUTHOR LINK */     
$('.about-me-img').hover(function(){            
$('.authorWindowWrapper').stop().fadeIn('fast').find('p').addClass('trans');        
}, function(){            
	$('.authorWindowWrapper').stop().fadeOut('fast').find('p').removeClass('trans');        
});</script>

<script type="text/javascript">    
	$('.owl-carousel').owlCarousel({        
		autoplay:true,        
		loop:true,        
		margin:30,        
		nav:true,        
		items: 5,        
		smartSpeed: 750,        
		navText: ['<img src="<?php echo base_url(); ?>assets/img/left.png">', '<img src="<?php echo base_url(); ?>assets/img/right.png">'],        
		responsive : {            
			0 : { items: 2},  480 : { items: 2}, 767 : {  items: 3 }, 1200 : { items: 5} }  });
			
</script>
</body>
</html>