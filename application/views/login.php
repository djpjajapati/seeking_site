<?php $this->load->view("include/header.php"); ?>
    <section class="package_sec1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="log_box">
                        <h5>Please login to your account.</h5>
                        <div class="superadminform">
                            <form action="<?php echo base_url(); ?>login/login_access" class="space40 comment-form" name="login_form" id="login_form" method="post">
                                <?php if($this->session->flashdata('error')){ ?>
                                    <div class="alert alert-block  alert-danger">
                                        <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                    <?php } ?>
                                        <?php if($this->session->flashdata('success')){ ?>
                                            <div class="alert alert-block  alert-success">
                                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button> <strong></strong>
                                                <?php echo $this->session->flashdata('success'); ?>
                                            </div>
                                            <?php } ?>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="" required> </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="" required> </div>
                                                <div class="form-check">
                                                    <label>
                                                        <!--<input type="checkbox" name="check" value="1"> <span class="label-text">Remember Me</span>-->
                                                    </label>
                                                </div>
                                                <input type="hidden" name="action" value="ajax_logimein">
                                                <input type="submit" class="logimein log_btn" id="loginButton" name="subbtnlast" value="LOG IN">
                                                <p class="errormsg"></p>
                            </form>
                            <!-- <a href="" class="log_btn">LOG IN</a>-->
                            <p class="social_text">- or log in with social media -</p>
                            <div class="apsl-login-networks theme-4 clearfix text-center">
                                <div class="social-networks">
                                    <a href="<?php echo $fb_url; ?>" title="Login with facebook">
                                        <div class="apsl-icon-block icon-facebook"> <i class="fa fa-facebook"></i> <span class="apsl-long-login-text">Login with facebook</span> </div>
                                    </a>
                                    <a href="<?php echo base_url()."login/login_twitter "; ?>" title="Login with twitter">
                                        <div class="apsl-icon-block icon-twitter"> <i class="fa fa-twitter"></i> <span class="apsl-long-login-text">Login with twitter</span> </div>
                                    </a>
                                    <a href="<?php echo $gmail_url; ?>" title="Login with google">
                                        <div class="apsl-icon-block icon-google"> <i class="fa fa-google"></i> <span class="apsl-long-login-text">Login with google</span> </div>
                                    </a>
                                </div>
                            </div>
                            <p class="mt-2"><a href="<?php echo base_url(); ?>login/forgot" class="log_links">Forgot your password?</a></p>
                            <!--<p class="mt-2"><a href="<?php echo base_url(); ?>login/register" class="log_links">Don’t have an account? Register as a new user</a></p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer_bg">
        <div class="container">
            <div class="footer_class">
                <div class="row">
                    <div class="col-md-2  pad_right0"> <a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/foot_logo.png"></a> </div>
                    <div class="col-md-7">
                        <ul class="list-inline foot_link">
							  <li class="list-inline-item"><a href="<?php echo base_url(); ?>home">Home</a></li>
							  <li class="list-inline-item"><a href="<?php echo base_url(); ?>about">Safiri Salama</a></li>
							  <li class="list-inline-item"><a href="<?php echo base_url(); ?>memorial/create_memorial">Create Memorial</a></li>
							  <li class="list-inline-item"><a href="<?php echo base_url(); ?>">Featured Memorial</a></li>
							  <li class="list-inline-item"><a href="<?php echo base_url(); ?>memorial/pay_now">Plans & Features</a></li>
							  <li class="list-inline-item"><a href="<?php echo base_url(); ?>contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 pad_left0">
                        <div class="prehead_left">
                            <ul class="list-inline pull-right">
                                <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								 <!--<li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->                    
								 <li class="list-inline-item"><a target="_blank" href="https://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" ><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								 
								 <li class="list-inline-item">
									<a target="_blank" href="https://api.whatsapp.com/send?phone=91XXXXXXXXXX&text=
										<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
										<i class="fa fa-whatsapp" aria-hidden="true"></i>
									</a>
																						
								</li>
								 
								<li class="list-inline-item">
															
									<a href="mailto:?subject=I wanted you to see this site&amp;body=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"
									   title="Share by Email">
									   <i class="fa fa-envelope" aria-hidden="true"></i>
									</a>
								
								</li>
                                <li class="list-inline-item"><a href="">Share this page</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- javascript libraries -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(function() {
            $("#datepicker").datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker('update', new Date());
        });
        $(function() {
            $("#datepicker1").datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker('update', new Date());
        });
    </script>
    </body>

    </html>