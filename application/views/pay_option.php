<?php $this->load->view("include/header.php"); ?>

<?php header('Access-Control-Allow-Origin: *'); ?>

<style>
    .pulse {
  animation: pulse 1s 1s forwards;
}

.jump {
  animation: jump 1s 2s forwards cubic-bezier(.84,-0.54,.31,1.19);
  animation-iteration-count: 20;
}

.waggle {
  animation: waggle 1s 3s forwards ease-out;
  animation-delay: 3s;
  animation-iteration-count: infinite;
}

.sheen {
  position: relative;
  overflow: hidden;
}

.sheen:after {
  animation: sheen 1s 4s forwards;
  content: '';
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: linear-gradient(to bottom, transparent, rgba(255,255,255,0.5) 50%, transparent);
  transform: rotateZ(60deg) translate(-1em, 4em);
}

.spin {
  animation: spin 1.5s 5s forwards cubic-bezier(.56,-0.35,.22,1.5);
}

.fade {
  animation: fade 1s 6.5s forwards;
}

@keyframes pulse {
  0% {
    background-color: orange;
  }
  25% {
    background-color: red;
  }
  50 {
    background-color: orange;
  }
  75% {
    background-color: red;
  }
}

@keyframes jump {
  0% {
    transform: none;
  }
  50% {
    transform: translateY(-2em);
  }
}

@keyframes waggle {
  0% {
    transform: rotateZ(0) scale(1.1);
  }
  20% {
    transform: rotateZ(0) scale(1.1);
  }
  40% {
    transform: rotateZ(0) scale(1.1);
  }
  60% {
    transform: rotateZ(0) scale(1);
  }
  80% {
    transform: rotateZ(0) scale(1.1);
  }
  100% {
    transform: rotateZ(0) scale(1.1);
  }
}

@keyframes sheen {
  100% {
    transform: rotateZ(60deg) translate(0, -4em);
  }
}

@keyframes spin {
  50% {
    transform: rotateZ(-20deg);
    animation-timing-function: ease;
  }
  100% {
    transform: rotateZ(360deg);
  }
}

@keyframes fade {
  25% {
    opacity: 0.25;
  }
  50% {
    opacity: 1;
  }
  75% {
    opacity: 0.25;
  }
  100% {
    opacity: 1;
  }
}

.start-now, .start-now:after {
  animation-delay: 3;
}


.allfreecoupo {
line-height: 0px;
}


.allfreecoupon:active {
}

.allfreecoupon:focus {
}


    
</style>
<section class="package_sec1 package_sec1-new2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>You’re almost there...</h1>
                <p>Before completing your order please select the desired payment method.</p>
            </div>
        </div>
    </div>
</section>


<section class="payment_method">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pad_right9">
                <div class="payment_box-new payment_box-new1">
                    <h3>Your Selection:</h3>
                    <h4><span id="pakage_title"><?php echo $pakage->title; ?> </span>PACKAGE</h4>
                    <h5><span id="price_to_pay"><?php echo $pakage->price; ?>.00</span></h5>
                    <a href="<?php echo base_url(); ?>memorial/pay_now" class="red_btn selectn_btn">Change Selection</a>
                </div>
            </div>
            <div class="col-md-8 pad_left9">
                <div class="payment_box-new">
                    <div class="payment-inner">
                      <h3>Your Payment Method:</h3>
                      <div class="row">
                          <div class="col-lg-2 col-md-4 col-6">
                            <div class="radio">
                                <input type="radio" id="inlineRadio1" value="mpesa" name="radioInline" checked>
                                <label for="inlineRadio1"> Mpesa </label>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-6">
                              <img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/mpesa.png">
                          </div>
                          <div class="col-lg-7 col-md-12 col-12">
                            <div class="form_country">
							<?php 
							
							$phone_no = $_SESSION['add_memo']['rphone'];
							
							$phone_no = str_replace('(', '', $phone_no);
							$phone_no = str_replace(')', '', $phone_no);
							$phone_no = str_replace('-', '', $phone_no);
							
							?>
                             <input id="phone_number" onKeyPress="kenyanumber();" type="text" class="form-control" name="phone_number" value="<?php echo $phone_no; ?>" placeholder="Your Phone Number">
                             <span class="main_span"><img alt="img" src="<?php echo base_url(); ?>assets/img/Kenya-Emoji-Flag.png"><span class="inner_span">Kenya</span>  +254</span>
                            </div> 
                          </div>
                      </div>
                      <div class="space40"></div>
                      <div class="row">
                          <div class="col-md-6">
                              <p class="payment_text">Enter the Safaricom number you wish to make payment with. A request to authorize this transaction will be sent to your phone from Safaricom. Please complete it to complete payment.</p>
                          </div>
                          <div class="col-md-6" >
                              <ul class="list-inline">
                                  <li class="list-inline-item click_text">Click to send a verification code to your phone.</li>
                                  <li class="list-inline-item">
									<!--<a href="javascript:;" class="send_code"><button id="send_code" onclick="pay_with_mpesa()">Send Code </button></a>-->
									<input type="button" class="send_code" style="line-height: 25px; cursor: pointer" id="send_code" onclick="pay_with_mpesa()" value="Send Code" />
								  </li>
                              </ul>
                          </div>
                      </div>
                    </div>
                    <hr>
                    
					<script>
					
					function pay_with_mpesa(){
						
						var phone_number = $("#phone_number").val();
						
						phone_number = phone_number.replace(/^0+/, '');
						
						$("#phone_number").val(phone_number);
						
						if(phone_number.length == 9){
							$("#phone_mpesa").val(phone_number);
							$("#myModal1").modal('show');
						}else{
							$("#err_text").text("Please enter a 9 digit phone number");
							$("#ErrorModal").modal('show');
							
							
							
						}
						
						
					}
					function kenyanumber() {
						var thenumber = $("#phone_number").val();
							var changednum=thenumber.replace(/^0+/, '');
							if (changednum.length >9) {
								changednum = changednum.substring(0, 9);
							}
						$("#phone_number").val(changednum);
						}
						var thenumber = $("#phone_number").val();
							var changednum=thenumber.replace(/^0+/, '');
							if (changednum.length >9) {
								changednum = changednum.substring(0, 9);
							}
						$("#phone_number").val(changednum);
					</script>
					
					<div class="payment-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-7 pad_right0">
                                <div class="radio radio_pad">
                                    <input type="radio" id="inlineRadio2" value="card" name="radioInline">
                                    <label for="inlineRadio2"> Credit Card / Debit Card </label>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-5 pad_left0">
                                <ul class="list-inline list_pay">
                                    <li class="list-inline-item"><a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/visa.png"></a></li>
                                    <li class="list-inline-item"><a href=""><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/mastercard.png"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="space30"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input_text">
                                   <div class="col-md-6 float-left">
                                    <input type="text" name="fname" class="form-control" value="<?php echo $user->fname; ?>" placeholder="First Name">
									</div>
                                   <div class="col-md-6 float-left">
                                    <input type="text" name="lname" class="form-control" value="<?php echo $user->lname; ?>" placeholder="Last Name">
 									</div>
                                    <div class="col-md-6 float-left">
                                  <input type="text" name="email" class="form-control" value="<?php echo $user->email; ?>" placeholder="Email">
                                    
 									</div>
                                    <div class="col-md-6 float-left">
								   								   
								   <a href="javascript:;" onclick="open_modal('<?php echo $pakage->ID; ?>')" class="send_code">Confirm Pay</a>
 									</div>
																   
                                </div>
                            </div>
                            <div class="col-md-12">
                                	<hr>
									<div class="col-md-12">
                                    <label for="inlineRadio2"> Safiri Salama Coupon </label><br>
									<span id="coupon_error" style="color: red; display: none">Sorry! Coupon Code is not valid</span>
									<span id="coupon_success" style="color: green; display: none">Congratulations!!! Coupon Code is applied, Please select payment method to proceed with new price.</span>
									</div>
									<div class="col-md-10  float-left">
										<input type="text" style="font-size: 11px;" name="coupon" id="coupon" class="form-control" value="" placeholder="Coupon Code"> 
									</div>
									<div class="col-md-2  float-right">
										<a onclick="apply_coupon()" style="margin-left: 10px;" href="javascript:;" class="send_code pull-right applybtn"> Apply Coupon</a>
										<button style="display:none" class="waggle send_code allfreecoupon start-now" onclick="open_modal('<?php echo $pakage->ID; ?>')">Proceed</button>
									</div>
 		                            </div>
                            <br>
                          <div class="col-md-12" style="padding: 10px 45px">
                              <p class="payment_text">If you have a Gift Card and/or Coupon code, please enter that separately above.<br>
							 Only one coupon and one gift card allowed per order.</p>                              
                          </div>
                        </div>

                    </div>
					
					
                </div>
            </div>
        </div>
    </div>
</section>








<!-- Modal -->
<div class="modal purchase_modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Payment Confirmation</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>Your payable amount is:</p>
        <h4 id="title"></h4>
        <h3><span style="font-size: 56px;" id="price"><?php echo $pakage->price; ?></span>.00</h3>
		
		<form id="ipay_form" method="post" action="https://payments.ipayafrica.com/v3/ke">
		
		<input type="hidden" name="live" value="1">
		<input type="hidden" name="oid" value="<?php echo time().rand(0,9997); ?>">
		<input type="hidden" name="inv" value="<?php echo time().rand(0,1234); ?>">
		<input type="hidden" name="ttl" id="price1" value="<?php echo $pakage->price; ?>">
		<input type="hidden" name="tel" value="<?php echo $user->phone; ?>">
		<!--<input type="hidden" name="eml" value="james.noah1172@gmail.com">-->
		<input type="hidden" name="eml" value="<?php echo $user->email; ?>">
		<input type="hidden" name="vid" value="gt14">
		<input type="hidden" name="curr" value="KES">
		<input type="hidden" name="p1" id="plan_id" value="1">
		<input type="hidden" name="p2" value="<?php echo $user->ID; ?>">
		<input type="hidden" name="p3" value="<?php echo $user->fname; ?>">
		<input type="hidden" name="p4" value="<?php echo $user->lname; ?>">
		<input type="hidden" name="cbk" value="<?php echo base_url(); ?>memorial/payment_callback">
		<input type="hidden" name="cst" value="1">
		<input type="hidden" name="crl" value="0">
		
		<input type="hidden" name="mpesa" value="0">
        <input type="hidden" name="airtel" value="0">
        <input type="hidden" name="equity" value="0">
        <input type="hidden" name="elipa" value="0">
		
		<input name="hsh" type="hidden" id="hsh" value="">
		<br>
		
		<!--<button type="submit">&nbsp;Submit&nbsp;</button>-->
		<a href="javascript:;" data-dismiss="modal" class="cancel">cancel</a>
        <a href="javascript:;" onclick="generate_hash()" class="proceed blue_bg">proceed</a>
		
		</form>
		
		
      </div>

      <!-- Modal footer -->
    </div>
  </div>
</div>
<!-- Modal -->



<!-- Modal -->
<div class="modal purchase_modal" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Mpesa Payment Confirmation</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>Your payable amount is:</p>
        <h4 id="title"></h4>
        <h3><span style="font-size: 56px;" id="price_mpesa1"><?php echo $pakage->price; ?></span>.00</h3>
		
		<!--<form id="ipay_form" method="post" action="https://payments.ipayafrica.com/v3/ke">-->
		
		<div class="alert alert-danger" id="err_message" style="display: none">
			<span class="resp_message"></span>
		</div>
		
		<div class="alert alert-success" id="suc_message" style="display: none">
			<span class="resp_message"></span>
		</div>
		
		<form id="ipay_form11" method="post" action="<?php echo base_url(); ?>lnme/LipaNaMpesa">
		
		
		<input type="hidden" name="amount" id="price_mpesa" value="<?php echo $pakage->price; ?>">
		<input type="hidden" name="phone_mpesa" id="phone_mpesa" value="<?php echo $_SESSION['add_memo']['rphone']; ?>">
		
		<input type="hidden" name="pakage_title" value="<?php echo $pakage->title; ?>">
		<input type="hidden" name="p1" value="<?php echo $pakage->ID; ?>">
		<input type="hidden" name="p2" value="<?php echo $user->ID; ?>">
		<input type="hidden" name="p3" value="<?php echo $memo->ID; ?>">
		<input type="hidden" name="p4" value="<?php echo $user->lname; ?>">
		
		<input type="hidden" name="ttl" id="price2" value="<?php echo $pakage->price; ?>">
		
		<a href="javascript:;" data-dismiss="modal" class="cancel">cancel</a>
        <a id="pay_now_mpesa" href="javascript:;" onclick="pay_now_mpesa()" class="proceed blue_bg">proceed</a>
		
		</form>
		
      </div>

      <!-- Modal footer -->
    </div>
  </div>
</div>
<!-- Modal -->


<script>
var intervalholder=0;
	function pay_now_mpesa(){
		if(intervalholder!=0)
			{
		clearInterval(intervalholder);
			}
		//$("#ipay_form11").submit();
		//return;
		var pakage_title = $("#pakage_title").val();
		var p1 = '<?php echo $pakage->ID; ?>';
		var p2 = '<?php echo $user->ID; ?>';
		var p3 = '<?php echo $memo->ID; ?>';
		var ttl = '<?php echo $pakage->price; ?>';
		var ttl = $("#price2").val();

		
		var amount = $("#price_mpesa").val();
		var phone_mpesa = $("#phone_mpesa").val();
		$.ajax({
				url: "<?php echo base_url(); ?>lnme/LipaNaMpesa",
				type: "POST",
				data: {"p1": p1, "p2": p2, "p3": p3, "ttl": ttl, "pakage_title": pakage_title, "amount":amount, "phone_mpesa":phone_mpesa},
				success: function (res) {
					var resp = JSON.parse(res);
					//alert(resp.errorCode);
					if(resp.errorCode){
					   
					   $(".resp_message").text(resp.errorMessage);
					   $("#suc_message").hide();
					   $("#err_message").show();
					  
					}else if(resp.ResponseCode == 0){
						$.post( "<?php echo base_url(); ?>lnme/registerreqindb", { requestcode: resp.CheckoutRequestID } );
						$(".resp_message").text(resp.ResponseDescription+', Please check your phone to confirm payment');
						$("#err_message").hide();
						$("#suc_message").show();
						
						intervalholder= setInterval(function(){ check_for_callback(); }, 10000);
						
					}
/* 					else{  // for simulation
						window.location.href = "<?php echo base_url(); ?>memorial/payment_success?redirect=mymemorials";
					} */
					
					console.log(resp);	
					
					/* if($("#hsh").val() != ''){
						// reduce coupon if used
						var coupon_code = $("#coupon").val();
						if(coupon_code != ''){
							reduce_coupon(coupon_code);
						}
						// reduce coupon if used
						//$("#ipay_form").submit();
					} */
				}
			});
		
	}
	var checkcallbackcounter=0;
	function check_for_callback(){
		
		//alert("checking for callback");
		if(checkcallbackcounter>30)
			{
				clearInterval(intervalholder);
				$("#myModal1").modal('hide');
				alert("Payment unsuccessful. No response. ");
			}
		$.ajax({
				url: "<?php echo base_url(); ?>lnme/check_mpesa_response",
				type: "POST",
				data: {},
				success: function (resp) {					
					//console.log(resp);
					if(resp){
						if(resp == 'success'){
							$.post( "<?php echo base_url(); ?>lnme/processafterMpesa", function( data ) {
								if(data==1)
									{
								window.location.href = "<?php echo base_url(); ?>memorial/payment_success?redirect=mymemorials";	
									}
							});

						}
						else{
							$("#err_text").text(resp);
							$("#ErrorModal").modal('show');
							clearInterval(intervalholder);
							$("#myModal1").modal('hide');
							/*window.setTimeout(function() {
								window.location.href = "<?php echo base_url(); ?>memorial/create_memorial";
							}, 3000);*/
						}
					}
					
				}
		});
		
	}

	function apply_coupon(){
		
		var coupon = $("#coupon").val();
		var price = '<?php echo $pakage->price; ?>';
		
		if(coupon == ''){
			$("#coupon_error").text("Please enter coupon code");
			$("#coupon_error").show();
			return false;
		}
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/apply_coupon",
				type: "POST",
				data: {"coupon":coupon, "price":price},
				success: function (resp) {
					if(resp == 'invalid'){
						$("#coupon_error").text("Sorry! Coupon Code is not valid");
						$("#coupon_error").show();
					}else{
						$("#coupon_error").hide();
						$("#coupon_success").show();
						
						$("#price_to_pay").text(resp+'.00');
						//added by charles start
						if(resp==0)
						{
						   	$(".applybtn").hide();
						   	$(".allfreecoupon").show();
					        $("#coupon_success").html("Congratulations!!! You have got 100% discount. Please click the button on the right to continue.");
						}
						//added by charles end
						$("#price").text(resp);
						$("#price1").val(resp);
						$("#price2").val(resp);
						
						
						//for mpesa popup
						$("#price_mpesa1").text(resp);
						$("#price_mpesa").val(resp);
						$("#price2").val(resp);
					}
					
					console.log(resp);	
					
				}
		});
		
	}

	function generate_hash(){
		
		var live = $("input[name=live]").val();
		var oid = $("input[name=oid]").val();
		var inv = $("input[name=inv]").val();
		var ttl = $("input[name=ttl]").val();
		var tel = $("input[name=tel]").val();
		var eml = $("input[name=eml]").val();
		var vid = $("input[name=vid]").val();
		var curr = $("input[name=curr]").val();
		var p1 = $("input[name=p1]").val();
		var p2 = $("input[name=p2]").val();
		var p3 = $("input[name=p3]").val();
		var p4 = $("input[name=p4]").val();
		var cbk = $("input[name=cbk]").val();
		var cst = $("input[name=cst]").val();
		var crl = $("input[name=crl]").val();
		
		// params added on 14 june david
		/*var mpesa = $("input[name=mpesa]").val();
		var airtel = $("input[name=airtel]").val();
		var equity = $("input[name=equity]").val();
		var elipa = $("input[name=elipa]").val();
		"mpesa": mpesa, "airtel":airtel, "equity":equity, "elipa": elipa*/
		// code added on 14 june david
		
		var coupon_code = $("#coupon").val();
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/generate_hash",
				type: "POST",
				data: {"live":live, "oid":oid, "inv":inv, "ttl":ttl, "tel":tel, "eml":eml, "vid":vid, "curr":curr, "p1":p1, "p2":p2, "p3":p3, "p4":p4, "cbk":cbk, "cst":cst, "crl":crl, "coupon_code": coupon_code},
				success: function (resp) {
					
					console.log(resp);	
					if(resp == 'skip_payment' || resp == '1skip_payment'){  
						window.location.href = "<?php echo base_url(); ?>memorial/payment_success?redirect=mymemorials";
						//window.location.href = "<?php echo base_url(); ?>memorial/mymemorials";
						return;
					}else{
						
						$("#hsh").val(resp);
						if($("#hsh").val() != ''){
							
							// reduce coupon if used
							var coupon_code = $("#coupon").val();
							if(coupon_code != ''){
								reduce_coupon(coupon_code);
							}
							// reduce coupon if used
							
							$("#ipay_form").submit();
						}
					}
				}
			});
	}
	
	function reduce_coupon(coupon_code){
		
		$.ajax({
				url: "<?php echo base_url(); ?>memorial/reduce_coupon",
				type: "POST",
				data: {"coupon_code":coupon_code},
				success: function (resp) {
					
				}
			});
		
	}
				
	function open_modal(id){
		
		//var title = '<?php echo $pakage->title; ?>'; //$("#title_"+id).text();
		//var price = '<?php echo $pakage->price; ?>'; //$("#price_"+id).text();
		$("#plan_id").val(id);
		
		//$("#title").text(title);
		//$("#price").text(price);
		//$("#price1").val(price);
		$("#myModal").modal('show');
		
	}
	
</script>






<?php $this->load->view("include/footer"); ?>


<script>

//$("#send_code").prop('disabled', false);
//$("#phone_number").prop('disabled', false);

$('input[type=radio][name=radioInline]').change(function() {
    if (this.value == 'mpesa') {
		$("#send_code").prop('disabled', false);
        $("#phone_number").prop('disabled', false);
    }else {
        $("#phone_number").prop('disabled', true);
		$("#send_code").prop('disabled', true);
    }
});

  var element = $(".allfreecoupon");
  setTimeout(function() {
    element.removeClass(".waggle");
    element.removeClass("start-now");
    element.addClass('.waggle');
    element.addClass('start-now');
  }, 5);


</script>
