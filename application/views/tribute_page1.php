<?php $this->load->view("include/header.php"); 




$device = "desktop";
$useragent = $_SERVER['HTTP_USER_AGENT'];	
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
{ 
	 $device = 'mobile';
}





?>



<section class="tribute_head" style="background: url('<?php echo base_url(); ?>assets/img/<?php echo $memo->background; ?>') no-repeat"> 

    <div class="container">

        <div class="row">

            <div class="col-md-4 col-4">

				<div class="preview_src">

					<img id="main_memorial_image" class="img-fluid w-100 img10" alt="<?php echo $memo->fname.' '.$memo->lname; ?>" src="<?php echo base_url().'uploads/'.$memo->cropped_image; ?>">

				</div>

				

				<?php if($is_page_admin){ ?>

					<!--<span class="edit_class" style="position: absolute;" onclick="edit_pic()"><i class="fa fa-pencil" aria-hidden="true"></i><?php if($device == 'desktop'){ echo 'Edit'; } ?></span>-->

					<span class="file-wrapper" style="margin: 20px">
						<input class="edit_class" type="file" name="image" id="image_main">
						<!--<span class="button red_btn">Choose a File</span>-->
					</span>

				<?php } ?>

				

            </div>

            <div class="col-md-8 col-8">

                <p>In loving memory of...</p>

                <h1>	<span id="memo_title"><?php echo $memo->fname.' '.$memo->lname; ?> </span> 

						<?php if($is_page_admin){ ?>

							<span class="edit_class" style="position: inherit;" onclick="edit_title('<?php echo $memo->fname; ?>', '<?php echo $memo->lname; ?>')"><i class="fa fa-pencil" aria-hidden="true"></i><?php if($device == 'desktop'){ echo 'Edit'; } ?></span>

						<?php } ?>

				</h1>

				

                <ul class="nav nav-tabs d-none d-md-flex">

                  <li class="nav-item">

                    <a id="about_tab" class="about_tab nav-link active" data-toggle="tab" href="#home">ABOUT</a>

                  </li>

                  <li class="nav-item">

                    <a id="notice_tab" class="notice_tab nav-link" data-toggle="tab" href="#menu1">NOTICES</a>

                  </li>

                  <li class="nav-item">

                    <a id="gallary_tab" class="gallary_tab nav-link" data-toggle="tab" href="#menu2">GALLERY</a>

                  </li>

                  <li class="nav-item">

                    <a id="tribute_tab" class="tribute_tab nav-link" data-toggle="tab" href="#menu3">TRIBUTES</a>

                  </li>

                </ul>

            </div>

        </div>

    </div>

</section>



<section class="tribute_main">

    <div class="tribute_head d-flex d-md-none">

        <ul class="nav nav-tabs ">

                  <li class="nav-item">

                    <a id="about_tab" class="about_tab nav-link active" data-toggle="tab" href="#home">ABOUT</a>

                  </li>

                  <li class="nav-item">

                    <a id="notice_tab" class="notice_tab nav-link" data-toggle="tab" href="#menu1">NOTICES</a>

                  </li>

                  <li class="nav-item">

                    <a id="gallary_tab" class="gallary_tab nav-link" data-toggle="tab" href="#menu2">GALLERY</a>

                  </li>

                  <li class="nav-item">

                    <a id="tribute_tab" class="tribute_tab nav-link" data-toggle="tab" href="#menu3">TRIBUTES </a>

                  </li>

                </ul>

        

    </div>

    <div class="container">

        <div class="row">

		
<style>

	.lds-ripple {

	  display: inline-block;

	  position: relative;

	  width: 64px;

	  height: 64px;

	}

	.lds-ripple div {

	  position: absolute;

	  border: 4px solid #582236;

	  opacity: 1;

	  border-radius: 50%;

	  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;

	}

	.lds-ripple div:nth-child(2) {

	  animation-delay: -0.5s;

	}

	@keyframes lds-ripple {

	  0% {

		top: 28px;

		left: 28px;

		width: 0;

		height: 0;

		opacity: 1;

	  }

	  100% {

		top: -1px;

		left: -1px;

		width: 58px;

		height: 58px;

		opacity: 0;

	  }

	}

</style>
		

<?php if($device == 'desktop'){ ?>
     



            <div class="col-md-4 col-12">

                <div class="space70"></div>
                <div class="space30 d-none d-md-none"></div>
				<?php if($is_page_admin){ ?>
				
				<div class="tribute_box-left tribute_box-new">
                    <div class="prehead_left">
                        <a href="" class="">Invite Family and Friends to this memorial:</a>
                        <a href="javascript:;" onclick="invite_modal()" class="red_btn tribute_box-btn"><span>+</span>Invite Now</a>
                    </div>
                </div>
				
				<?php } ?>

				

                <div class="tribute_box-left">
                    <div class="prehead_left">
                        <a href="">Share with a friend</a>
                        <ul class="list-inline">
                            <li class="list-inline-item">														
								<a target="_blank" href="https://www.facebook.com/share.php?title=<?php echo $memo->fname.' '.$memo->lname; ?>&u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>

								<script>

									function fbs_click() {
										u=location.href;t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');

										return false;
									}

									</script>
							</li>                           

                            <li class="list-inline-item">
								<a target="_blank" href="https://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >								
									<i class="fa fa-twitter" aria-hidden="true"></i>							
								</a>																					
							</li>							

							<li style="display: none" class="list-inline-item">								
								<a target="_blank" href="https://api.whatsapp.com/send?phone=+254722311588&text=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
									<i class="fa fa-whatsapp" aria-hidden="true"></i>								
								</a>																												
							</li>																										

							<li class="list-inline-item">															
								<a href="mailto:?subject=I wanted you to see this memorial for <?php echo $memo->fname.' '.$memo->lname; ?>&amp;body=<?php echo $title.' '.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" title="Share by Email">
									<i class="fa fa-envelope" aria-hidden="true"></i>								
								</a>														
							</li>
                        </ul>
                    </div>
                </div>

				

                <div class="tribute_box-left tribute_box-new1">
                    <div class="prehead_left">
                        <a href="javascript:;" class="tribute_box-text">Sign up for Notifications</a>
                        <a href="javascript:;" onclick="open_sign_up_model()" class="red_btn tribute_box-btn tribute_box-btn1">Sign Up</a>
                        <div class="clearfix"></div>
                    </div>
                </div>

								

                <div id="accordion">

				<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>

				<link rel="stylesheet" href="<?php echo base_url(); ?>assets/crop/croppie.css" />

					<?php if($is_page_admin){ ?>

					<form id="add_pic_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_pic" >

                    <div class="tribute_box-left tribute_box-new">
                      <div class="prehead_left">
                          <a href="" class="">Add Picture</a>
                          <div >
						  <?php if(count($pictures) > 0){ ?>
							  <a href="<?php echo base_url(); ?>uploads/<?php echo $pictures[0]->pic; ?>" data-rel="prettyPhoto[25727]" class="prettyPhoto kids_picture">
								<img id="first_pic" class="img-fluid" src="<?php echo base_url(); ?>uploads/<?php echo $pictures[0]->pic; ?>" />
							  </a>
						  <?php } ?>

						  </div>

						 <div class="row" style="margin-top: 20px"> 
						 <div class="col-md-12">
								<?php if($this->session->flashdata('error')){ ?>
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button>
											<?php echo $this->session->flashdata('error'); ?>
										</div>

								<?php } ?>
						 </div>
							  <div class="col-md-6">
								<!--<input type="file" id="upload_picture" name="pic" required>-->
								<input type="file" name="image" id="image" required>
								<input id="cropped_image" name="cropped_image" type="hidden" value="" />
								<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
								<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">
							  </div>
							  <div class="col-md-6">
								  <div class="add-photo_new">
									<a href="javascript:;" onclick="add_pic()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>
								  </div>
								   <div id="loader_main" style="display: none;" class="lds-ripple"><div></div><div></div></div>
							  </div>
						  </div>
                      </div>
                    </div>
				</form>

				<?php } ?>

				<?php if($is_page_admin){ ?>
				
                    <div class="card card1">
                        <div class="card-header">
                              <a class="collapsed card-link card-link1" data-toggle="collapse" href="#collapseThree1">
                                <i class="red_clr"><?php echo count($notification); ?> New </i>  Important Notifications<span><i class="fa fa-exclamation" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseThree1" class="collapse" data-parent="#accordion">
                            <div class="card-body">
								<ul class="list-unstyled list_page-activity">
								<?php foreach($notification as $noti){  ?>
									<li><p style="color: white"><?php echo $noti->comment; ?></p></span></li>
								 <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

				<?php } ?>

                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFour">
                                Page Activity<span><i class="fa fa-users" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseFour" class="collapse show" data-parent="#accordion">
                            <div class="card-body">
                                <ul class="list-unstyled list_page-activity">
                                  <?php foreach($tributes_activity as $t){ ?>
									<li><?php echo $t->by.' '.$t->d_action; ?><span><?php echo $t->timeago; ?></span></li>								  
								  <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFive">
                                Page Views   <span><?php echo $memo->views; ?></span>
                              </a>
                        </div>
                    </div>
					<div class="card">
                        <div class="card-header">
                              <a style="text-align: center" class="collapsed card-link activity" onclick="report('<?php echo $memo->ID; ?>')" data-toggle="collapse" href="javascript:;">
                                REPORT MEMORIAL  <span></span>
                              </a>
                        </div>
                    </div>
                </div>
            </div>

				
<?php } ?>

					

			

			

			

			

			

			

			

			

			

			

			

			

            <div class="col-md-8 col-12 d-md-block d-block">

               

                <div class="tab-content">

                    <div class="tab-pane active" id="home">

                      <div class="tribute_box">

                        <h3>

							<img src="<?php echo base_url(); ?>assets/img/quote1.png" class="d-inline-block" alt=""/> 

							<span class="memo_about"><?php if(strlen(trim($memo->about)) > 0) echo $memo->about; else echo 'Let the memory of '.$memo->fname.' '.$memo->lname.' be with us forever'; ?></span> 

							<img src="<?php echo base_url(); ?>assets/img/quote2.png" class="d-inline-block" alt=""/>

							</h3>

							

							<?php if($is_page_admin){ ?>

							

									<span class="edit_class" onclick="edit_memo('about');"><i class="fa fa-pencil" aria-hidden="true"></i><?php if($device == 'desktop'){ echo 'Add Quote'; } ?></span>

							

							<?php } ?>

							

                        

                      </div>

                      <div class="tribute_box">

                        <ul class="tri_list_box">

												

						<span class="memo_dates">

						

						<?php 	$d1 = new DateTime($memo->birthdate);

								$d2 = new DateTime($memo->deathdate);

								$diff = $d2->diff($d1); 

						?>

						

                          <li><?php echo $diff->y; ?> Years Old </li>

                          <li>Born on <?php echo date('l', strtotime($memo->birthdate)).', '.date("F", strtotime($memo->birthdate)).' '.date("d", strtotime($memo->birthdate)); ?> <?php echo date("Y",strtotime($memo->birthdate)); ?> in <?php echo $memo->countryorigin1; ?></li>

                          <li>Passed a way on <?php echo date('l', strtotime($memo->deathdate)).', '.date("F", strtotime($memo->deathdate)).' '.date("d", strtotime($memo->deathdate)); ?> <?php echo date("Y",strtotime($memo->deathdate)); ?> in <?php echo $memo->placeofdeath; ?>, <?php echo $memo->countryorigin1; ?></li>

                        

						</span>

						

						</ul>

						

						<?php if($is_page_admin){ ?>

						

							<span class="edit_class" onclick="edit_memo('dates')"><i class="fa fa-pencil" aria-hidden="true"></i><?php if($device == 'desktop'){ echo 'Edit'; } ?></span>

							

						<?php } ?>

                      </div>

                      <div class="tribute_box">

                            <span class="memo_desc">
								<?php echo $memo->description; ?>
							</span>

							

							<?php if($is_page_admin){ ?>

							

								<span class="edit_class" onclick="edit_memo('desc');"><i class="fa fa-pencil" aria-hidden="true"></i><?php if($device == 'desktop'){ echo 'Add Description'; } ?></span>

								

							<?php } ?>

                      </div>

                      <!--<a href="#" class="add_story">Leave a Tribute</a>-->

                      <?php if($is_page_admin){ ?>

					  <a href="javascript:;" onclick="go_to_tribute_tab()" class="add_story">Leave a Tribute</a>

					  <?php } ?>

                    </div>

					

					

					

					<script>

					

					function go_to_tribute_tab(){

						

						$("#tribute_tab").click();

						

					}

					

					function edit_memo(action){

						

						 $("#action").val(action);

						

						 $("#about_sec,#desc_sec,#dates_sec").hide(); 

						 

						 if(action == 'dates'){

							 $('#dates_sec').show();

						 }else if(action == 'desc'){

							 $('#desc_sec').show();

						 }else{

							 $('#about_sec').show();

						 }

						 

						 var memo_id = <?php echo $memo->ID; ?>;

						 var about = $(".memo_about").text();

						 //var description = $(".memo_desc").text();
						 var description = $(".memo_desc").html();

						 $("#edit_about").val(about);

						 CKEDITOR.instances['edit_desc'].setData(description);
						 //$("#edit_desc").val(description);

						 $("#MemoEditModal").modal("show"); 

												

					}

					

					</script>

					

					

					

					<!---- Modal -->

					  <div class="modal fade" id="MemoEditModal" role="dialog">

						<div class="modal-dialog">

						

						  <!-- Modal content-->

						  <div class="modal-content">

							<div class="modal-header">

							  <h4 style="line-height: 1; color: black" class="modal-title">Edit Memorial</h4>

							</div>

							<div class="modal-body">

							

							<div class="alert alert-success" id="edit_memo_message" style="display: none">

							  <strong>Success!</strong> Record Updated successfully.

							</div>

							

							<input type="hidden" id="action" type="text" />

							

							<div id="about_sec">

								<label> About </label>

								<textarea id="edit_about" class="form-control" name="about"></textarea>

								<p id="edit_about_error" style="display: none; color: red;">This field is required</p>

							</div>

							<script src="<?php echo base_url(); ?>/assets/ckeditor/ckeditor.js"></script>

							<div id="desc_sec">							

								<label> Edit Personal Information </label>

								<textarea id="edit_desc" class="form-control" name="description"></textarea>

								<p id="edit_desc_error" style="display: none; color: red;">This field is required</p>

							</div>

							<script>
								// Replace the <textarea id="editor1"> with a CKEditor
								// instance, using default configuration.
								CKEDITOR.replace( 'edit_desc' );
							</script>




							<div id="dates_sec">							

								<!--<label> Dates Section </label>-->

								

								<div class="col-md-12">

									<div class="row">

										<div class="col-md-7 pad_right0">

											<label> Date of Birth </label>

											

										</div>

										<div class="col-md-5">

											<input class="form-control date_form" id="birthdate" name="birthdate"  max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d", strtotime($memo->birthdate)); ?>" type="date"  />

										

										</div>

									</div>

								</div>

								

								<br>

								

								<div class="col-md-12">

									<div class="row">

										<div class="col-md-7 pad_right0">

											<label> Date of Death </label>

										</div>

										<div class="col-md-5">

											<input class="form-control date_form" id="deathdate" name="deathdate" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d", strtotime($memo->deathdate)); ?>" type="date" />

											

										</div>

									</div>

								</div>

								

								</br>

								

								<div class="col-md-12">

									<div class="row">

										<div class="col-md-7 pad_right0">

											<label> Place of Death </label>

										</div>

										<div class="col-md-5">

											<input class="form-control date_form" id="placeofdeath" name="placeofdeath" value="<?php echo $memo->placeofdeath; ?>" type="text" />

											

										</div>

									</div>

								</div>

								

								</br>

								

								<p id="edit_dates_error" style="display: none; color: red; text-align: center">Date of Death must be greater that Date of birth</p>

								

								

							</div>

	

							<div></div>

							

							

							</div>

							

							<div style="padding: 15px; text-align: right">

								<button type="button" class="btn btn-primary btn-small-edit" onclick="update_memo()" style="background: #582236; border-color: #582236;">Update</button>

								<button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

							</div>

						  </div>

						  

						</div>

					  </div>

					 <!-- End Success Modal -->

					 

					 

					 <script>

					 

					 function update_memo(){

						 

						 var memo_id = <?php echo $memo->ID; ?>;

						 var about = $("#edit_about").val();

						 //var desc = $("#edit_desc").val();
						 var desc = CKEDITOR.instances['edit_desc'].getData();
						
						 var action = $("#action").val();

						 

						 var birthdate = $("#birthdate").val();

						 var deathdate = $("#deathdate").val();

						 var placeofdeath = $("#placeofdeath").val();

						 

						 if(action == 'dates'){

							

							birthdatecheck = new Date(birthdate);

							dathdatecheck = new Date(deathdate);

							

							if (birthdatecheck > dathdatecheck) {

								$("#edit_dates_error").show();

								return false;

							}else{

								$("#edit_dates_error").hide();

							}

							 

						 }

						 

						 

						 $.ajax({

								url:"<?php echo base_url(); ?>memorial/update_memo",

								type: "POST",

								data:{"memo_id": memo_id, "about": about,"desc": desc, "action": action, "birthdate": birthdate, "deathdate": deathdate, "placeofdeath": placeofdeath},

								success:function(resp)

								{

									if(action == 'about'){

										$(".memo_about").text(resp);

									}else if(action == 'desc'){

										$(".memo_desc").html("");
										$(".memo_desc").html(resp);

									}if(action == 'dates'){

										$(".memo_dates").html("");

										$(".memo_dates").html(resp);

									}

									

									$("#MemoEditModal").modal("hide");

									

								}

							});

					 }

					 

					 </script>

					

					

					

					

					

                  <div class="tab-pane fade tab_notice" id="menu1">

                    <div class="notice-form">

                      <div class="form-group row m-0 mb-2">

						  <div class="col-md-6">

							<label for="text1">Notice Board For</label>

						  </div>

						  <div class="col-md-6">

                        	<input type="text" value="<?php if($notice->notice_for == ""){ echo $memo->fname.' '.$memo->lname; }else{ echo $notice->notice_for; } ?>" class="form-control notice_frm" id="notice_for" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="First Name, Last Name">

						  </div>

                      </div>

                      <div class="form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text2">Date of Birth</label>

						  </div>

						  <div class="col-md-6">

                        	<input type="text" value="<?php if($notice->dob == ""){ echo $memo->birthdate; }else{ echo $notice->dob; } ?>" class="form-control notice_frm" id="dob" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Date, Month, Year, Place">

						  </div>

                      </div>

                      <div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text3">Date of Death</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php if($notice->dod == ""){ echo $memo->deathdate; }else{ echo $notice->dod; } ?>"class="form-control notice_frm" id="dod" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Date, Month, Year, Place">

						  </div>

                      </div>



					  <?php if($is_page_admin || !empty($notice->house_of_worship)){ ?>

                      <div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text4">Church/Mosque/Temple</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php echo $notice->house_of_worship; ?>" class="form-control notice_frm" id="house_of_worship" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Location, Town, Date, Time">

						  </div>

                      </div>

					  <?php } ?>




					  <?php if($is_page_admin || !empty($notice->country)){ ?>

                      <div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text5">Country</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php echo $notice->country; ?>" class="form-control notice_frm" id="country" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Enter your country">

						  </div>

                      </div>
					
					  <?php } ?>
					

					  <?php if($is_page_admin || !empty($notice->town)){ ?>

						<div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text5">Town</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php echo $notice->town; ?>" class="form-control notice_frm" id="town" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Town/Centre">

						  </div>

                      </div>


					  <?php } ?>



					  <?php if($is_page_admin || !empty($notice->cremation_detail)){ ?>

                      <div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text6">Cremation Details</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php echo $notice->cremation_detail; ?>" class="form-control notice_frm" id="cremation_detail" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Location, Crematorium, Time">

						  </div>

                      </div>


					  <?php } ?>




					  <?php if($is_page_admin || !empty($notice->contact_person)){ ?>

                      <div class="form-group form-group row m-0 mb-2">

                        <div class="col-md-6">

						  <label for="text7">Funeral Committee Contact Person</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php echo $notice->contact_person; ?>" class="form-control notice_frm" id="contact_person" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="1st Name, 2nd Name, Phones">

						  </div>

                      </div>

					  <?php } ?>



					  <?php if($is_page_admin || !empty($notice->meeting_venue)){ ?>

                      <div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <label for="text8">Funeral Committee Meetings Venue</label>

						  </div>

						  <div class="col-md-6">

                        <input type="text" value="<?php echo $notice->meeting_venue; ?>" class="form-control notice_frm" id="meeting_venue" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Town , Location, Buidling, Meeting Dates">

						  </div>

                      </div>

                      
					  <?php } ?>
					  

					  <style type="text/css">

							#map {

								width: 100%;

								height: 400px;

							}

							.mapControls {

								margin-top: 10px;

								border: 1px solid transparent;

								border-radius: 2px 0 0 2px;

								box-sizing: border-box;

								-moz-box-sizing: border-box;

								height: 32px;

								outline: none;

								box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);

							}

							#google_map_loc {

								background-color: #fff;

								font-family: Roboto;

								font-size: 15px;

								font-weight: 300;

								margin-left: 12px;

								padding: 0 11px 0 13px;

								text-overflow: ellipsis;

								width: 50%;

							}

							#google_map_loc:focus {

								border-color: #4d90fe;

							}

						</style>

						

					  

					  

					  

					  

					  <div class="form-group form-group row m-0 mb-2">

						  <div class="col-md-6">

                        <!--<label for="text9">Burial/Funeral/Interment Location Google Map</label>-->

							  </div>

						  <div class="col-md-6">

					<?php if($is_page_admin || !empty($notice->google_map_loc)){ ?>

                        <input type="text" value="<?php echo $notice->google_map_loc; ?>" class="form-control" id="google_map_loc" <?php if(!$is_page_admin){ echo 'disabled'; } ?> placeholder="Burial/Funeral/Interment Location Google Map">

					<?php } ?>
						  </div>

                      </div>

					  <?php //if($is_page_admin || !empty($notice->google_map_loc)){ ?> 

					  <div id="map" class="map_class">

                      <?php //} ?>  

                      </div>

					  

					  <script>

						function initMap() {

							

							var map = new google.maps.Map(document.getElementById('map'), {

							  center: {lat: 22.3038945, lng: 70.80215989999999},

							  zoom: 17

							});

							

							

							var lat1, lng1, marker;

							var user1Location = '<?php echo $notice->google_map_loc; ?>';

							

							if(user1Location.length > 1){

								

								var geocoder = new google.maps.Geocoder();

								//convert location into longitude and latitude

								geocoder.geocode({

									address: user1Location

								}, function(locResult) {

									//console.log(locResult);

									lat1 = parseFloat(locResult[0].geometry.location.lat());

									lng1 = parseFloat(locResult[0].geometry.location.lng());

									

									//document.getElementById('lat').value = lat1;

									//document.getElementById('lng').value = lng1;

									

									map = new google.maps.Map(document.getElementById('map'), {

									  //center: {lat: parseInt(lat1), lng: parseInt(lng1)},

									  center: {lat: lat1, lng: lng1},

									  zoom: 17

									});

									

									

									marker = new google.maps.Marker({

										position: {lat: lat1, lng: lng1},

										//position: {lat: 40.7091164, lng: -74.01465150000001},

										map: map,

										title: '<?php echo $notice->google_map_loc; ?>'

									  });

									

									

									//$("#testDiv").html("latitude:" + lat1 + "<p>longitude:" + lng1 + "</p>");

								});

								

							}

							

													

							

							/* var lat1 = document.getElementById('lat').value;

							var lng1 = document.getElementById('lng').value;

							

							alert(lat1);

							alert(lng1); */

							

							//infowindow.close();

							

							//marker.setVisible(true);

							

							/* var map = new google.maps.Map(document.getElementById('map'), {

							  center: {lat: 22.3038945, lng: 70.80215989999999},

							  zoom: 13

							}); */

							

							

							var input = document.getElementById('google_map_loc');

							map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

						   

							var autocomplete = new google.maps.places.Autocomplete(input);

							autocomplete.bindTo('bounds', map);

						  

							var infowindow = new google.maps.InfoWindow();

							var marker = new google.maps.Marker({

								map: map,

								anchorPoint: new google.maps.Point(0, -29)

							});

						  

							autocomplete.addListener('place_changed', function() {

								

								//alert("calling save_notes");

								//save_notices();

								

								infowindow.close();

								marker.setVisible(false);

								var place = autocomplete.getPlace();

								

								/* If the place has a geometry, then present it on a map. */

								if (place.geometry.viewport) {

									map.fitBounds(place.geometry.viewport);

								} else {

									map.setCenter(place.geometry.location);

									map.setZoom(17);

								}

								marker.setIcon(({

									url: place.icon,

									size: new google.maps.Size(71, 71),

									origin: new google.maps.Point(0, 0),

									anchor: new google.maps.Point(17, 34),

									scaledSize: new google.maps.Size(35, 35)

								}));

								marker.setPosition(place.geometry.location);

								marker.setVisible(true);

							  

								var address = '';

								if (place.address_components) {

									address = [

									  (place.address_components[0] && place.address_components[0].short_name || ''),

									  (place.address_components[1] && place.address_components[1].short_name || ''),

									  (place.address_components[2] && place.address_components[2].short_name || '')

									].join(' ');

								}

							  

								infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);

								infowindow.open(map, marker);

								

								/* Location details */

								document.getElementById('google_map_loc').value = place.formatted_address;

								document.getElementById('lat-span').innerHTML = place.geometry.location.lat();

								document.getElementById('lon-span').innerHTML = place.geometry.location.lng();

								

							});

						}

						</script>

						<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWpO4jMKxJzeU0hOBmPgGwbQ4jRtxTEjA&libraries=places&callback=initMap" async defer></script>-->

						<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&libraries=places&callback=initMap"></script>

											  

						

						<div id="notices_alert" style="display: none" class="row alert alert-success">

							Notices updated successfully

						</div>

						

                     

                      <ul class="list-inline notice_list">

                        <li class="list-inline-item">Last updated:  <?php echo $notice->timeago; ?></li>

                        <li class="list-inline-item">Posted by:  <?php echo $notice->updated_by; ?></li>

						<li class="list-inline-item">Posted on: <?php echo date('Y-m-d', strtotime($notice->createdAt)); ?> </li>

						<?php if($is_page_admin){  ?>

							<li class="list-inline-item">
								<a href="javascript:;" onclick="save_notices()" class="edit_class" style="position: unset"> <b> Save Notices </b> </a>
							</li>

						<?php } ?>

                      </ul>

                    </div>

					

                    <!--  -->

                  </div>

                  <div class="tab-pane gal_img fade" id="menu2">

                    

                    <div class="gal_sub">

                      <ul class="nav nav-tabs">

                        <li class="nav-item">

                          <a class="p_gallery nav-link active" data-toggle="tab" href="#photoTab">Photos</a>

                        </li>

                        <li class="nav-item">

                          <a class="v_gallery nav-link" data-toggle="tab" href="#videoTab">VIDEOS</a>

                        </li>

                        <li class="nav-item">

                          <a class="a_gallery nav-link" data-toggle="tab" href="#audioTab">AUDIO</a>

                        </li>

                      </ul>

                      <!-- Tab panes -->

                      <div class="tab-content">

                        <div class="tab-pane active" id="photoTab">

                          <div class="gal_box">

                            <div class="row row-mb">

								

							

							<?php foreach($pictures as $p){ 

							

								if($is_page_admin || $p->status=='Approved'){

							?>

							

								<div class="col-md-3 imagegallery" id="p_<?php echo $p->ID; ?>">

									<?php if($is_page_admin){ ?>

										<a class="remove_btn btn btn-danger btn-sm" style="position: absolute;cursor: pointer;" onclick="delete_pic('<?php echo $p->ID; ?>', '<?php echo $p->pic; ?>')">

											<i class="fa fa-trash"></i>

										</a>

										

										

										<?php if($p->status=='Pending'){ ?>

											<!--<a class="remove_btn btn btn-success btn-sm" style="position: static;cursor: pointer; z-index: 100;" onclick="approve_pic(this, '<?php echo $p->ID; ?>')">

												<i class="fa fa-check"></i>

											</a>-->

										

										<?php } ?>

										

									<?php } ?>

									<a href="<?php echo base_url(); ?>uploads/<?php echo $p->pic; ?>" data-rel="prettyPhoto[25727]" class="prettyPhoto kids_picture">

									<img class="img-fluid" alt="<?php echo $memo->fname.' '.$memo->lname; ?>" src="<?php echo base_url(); ?>uploads/<?php echo $p->pic; ?>">

									</a>

								  </div>

							

								<?php } } ?>

							

							<?php if($photo_limit && $is_page_super_admin){ ?>

									<div class="col-md-12 container alert alert-danger">

										Photo limit exceeded, Please upgrade your pakage

									</div>

							<?php } ?>

							

							

                            </div>

                          </div>

						  

						  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->

						  

						  <script>

						  

						  

						  function delete_pic(id, pic){

							  

							  if(confirm("Are you sure?")){

								  $.ajax({

											url:"<?php echo base_url(); ?>memorial/delete_pic",

											type: "POST",

											data:{"id": id, "pic": pic},

											success:function(resp)

											{

												if(resp == 'success'){

													$('#p_'+id).remove();

												}

												

											}

									}); 

							  }

						  }

						  

						  function approve_pic(l, id){

							  

							  $.ajax({

										url:"<?php echo base_url(); ?>memorial/approve_pic",

										type: "POST",

										data:{"id": id},

										success:function(resp)

										{

											if(resp == 'success'){

												l.remove();

											}else{

												$("#err_text").text("somthing went wrong");

												$("#ErrorModal").modal("show");

											}

											

										}

								}); 

								

						  }

						  

						  </script>

						  

						  

						  <form id="add_pic_form1" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_pic_gallary">

					

							  <div class="prehead_left">

								 

								  <div class="row"> 

								  

									 <?php //if($is_page_admin){ ?>

									 <?php if($this->session->userdata('user')){ ?>

									 
										 

										 <?php if($this->session->flashdata('error_pic')){ ?>

													<div class="alert alert-danger display-hide">

														<button class="close" data-close="alert"></button>

														<?php echo $this->session->flashdata('error_pic'); ?> 

													</div>

											<?php } ?>

										 

										 <?php if($this->session->flashdata('suc_pic')){ ?>

													<div class="alert alert-danger display-hide">

														<button class="close" data-close="alert"></button>

														<?php echo $this->session->flashdata('suc_pic'); ?> 

													</div>

											<?php } ?>

										 
											

										  <div class="col-md-10">

										  

											<input type="file" id="upload_picture1" name="pic" required>

											

											<input id="cropped_image1" name="cropped_image" type="hidden" value="" />

											

											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">

											<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">

										  

										  </div>

											  

										  <div class="col-md-2">

											  <div class="add-photo_new">

												<a href="javascript:;" onclick="add_pic1()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>

												

											  </div>

											  

											  <div id="loader" style="display: none;" class="lds-ripple"><div></div><div></div></div>

											  

		<style>

			

			.lds-ripple {

			  display: inline-block;

			  position: relative;

			  width: 64px;

			  height: 64px;

			}

			.lds-ripple div {

			  position: absolute;

			  border: 4px solid #582236;

			  opacity: 1;

			  border-radius: 50%;

			  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;

			}

			.lds-ripple div:nth-child(2) {

			  animation-delay: -0.5s;

			}

			@keyframes lds-ripple {

			  0% {

				top: 28px;

				left: 28px;

				width: 0;

				height: 0;

				opacity: 1;

			  }

			  100% {

				top: -1px;

				left: -1px;

				width: 58px;

				height: 58px;

				opacity: 0;

			  }

			} 

		

		</style>

				  

									</div>

										  

								  <?php } else { ?>

									<div style="    padding: 10px 50px;">

									  You need to login to upload media. <a href="<?php echo base_url(); ?>login">Login Now</a>

									</div>

								  <?php } ?>

										  

								 </div>

							</div>

					

							<script>

							var globalID=0;

							function add_pic1(){

								var pic = $("#upload_picture1").val();

								if(pic == ''){

									$("#err_text").text("Please select image to upload");

									$("#ErrorModal").modal("show");

								}else{

									

									var memo_id = '<?php echo $memo->ID; ?>';

									var name_limit = 'photo_limit';

									$.ajax({

										url:"<?php echo base_url(); ?>memorial/is_pakage_expired",

										type: "POST",

										data:{"memo_id": memo_id, "name_limit": name_limit},

										success:function(resp)

										{

											if(resp == 'yes'){

												$("#err_text").text("Limit Exceeded"); 

												$("#ErrorModal").modal("show"); 

												return false;

											}else{

												$("#loader").show();

												$("#add_pic_form1").submit();

											}

										}

									}); 

									

								}

								

								

							}

							

							</script>

							

						</form>

						  

						  

						  

                        </div>

                        <div class="tab-pane fade" id="videoTab">

                            <div class="gal_box">

                            <div class="row">

							

							<?php foreach($videos as $v){ 

								if($is_page_admin || $v->status=='Approved'){

							?>

							

								<div class="col-md-3 video-mb" id="v_<?php echo $v->ID; ?>">

									

									<?php if($is_page_admin){ ?>

										<a class="remove_btn btn btn-danger btn-sm" style="position: absolute;cursor: pointer; z-index: 100;" onclick="delete_video('<?php echo $v->ID; ?>', '<?php echo $v->video; ?>')">

											<i class="fa fa-trash"></i>

											</a>

											

											<?php if($v->status=='Pending'){ ?>

											

												<a class="remove_btn btn btn-success btn-sm" style="position: static;cursor: pointer; z-index: 100;" onclick="approve_video(this, '<?php echo $v->ID; ?>')">

												<i class="fa fa-check"></i>

												</a>

												

											<?php } ?>

											

											

									<?php } ?>

									<video width="180" height="157" controls>

									  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $v->video; ?>" type="video/mp4">

									  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $v->video; ?>" type="video/mp4">

									  Your browser does not support the video tag.

									</video>

									

								  </div>

							

								<?php } } ?>

								

								 <?php if($video_limit && $is_page_super_admin){ ?>

									<div class="col-md-12 container alert alert-danger">

										Video limit exceeded, Please upgrade your pakage

									</div>

								<?php } ?>

								

                            </div>

                          </div>

						  

						  

						 

						  

						  

						  <script>

						  

						  function delete_video(id, vid){

							  

							  if(confirm("Are you sure?")){

								  $.ajax({

											url:"<?php echo base_url(); ?>memorial/delete_vid",

											type: "POST",

											data:{"id": id, "vid": vid},

											success:function(resp)

											{

												if(resp == 'success'){

													$('#v_'+id).remove();

												}

												

											}

									}); 

							  }

						  }

						  

						  function approve_video(l, id){

							  

							  $.ajax({

										url:"<?php echo base_url(); ?>memorial/approve_video",

										type: "POST",

										data:{"id": id},

										success:function(resp)

										{

											if(resp == 'success'){

												l.remove();

											}else{

												$("#err_text").text("somthing went wrong");

												$("#ErrorModal").modal("show");

											}

											

										}

								}); 

								

						  }

						  

						  </script>

						  

						  

						  

						  <form id="add_video_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_video">

					

							  <div class="prehead_left">

								 

								 <?php //if($is_page_admin){ ?>

								 <?php if($this->session->userdata('user')){ ?>

								 

								  <div class="row"> 



										 

										 

											<?php if($this->session->flashdata('error_video')){ ?>

													<div class="alert alert-danger display-hide">

														<button class="close" data-close="alert"></button>

														<?php echo $this->session->flashdata('error_video'); ?>

													</div>

											<?php } ?>

											

											<?php if($this->session->flashdata('succ_video')){ ?>

													<div class="alert alert-success display-hide">

														<button class="close" data-close="alert"></button>

														<?php echo $this->session->flashdata('succ_video'); ?>

													</div>

											<?php } ?>

										 

											

										  <div class="col-md-10">

																			  

											<input type="file" id="upload_video" name="video" required>

											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">

											<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">

										  </div>

											  

										  <div class="col-md-2">

											  <div class="add-photo_new">

												<a href="javascript:;" onclick="add_video()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>

												

											  </div>

											  

											  <div id="loader_video" style="display: none;" class="lds-ripple"><div></div><div></div></div>

											  

										  </div>

								 </div>

								 

								 <?php } else { ?>

									<div style="    padding: 10px 50px;">

									  You need to login to upload media. <a href="<?php echo base_url(); ?>login">Login Now</a>

									</div>

								  <?php } ?>

								 

							</div>

					

							<script>

							

							function add_video(){

								var pic = $("#upload_video").val();

								if(pic == ''){

									$("#err_text").text("Please select video to upload");

									$("#ErrorModal").modal("show");

								}else{

									

									var memo_id = '<?php echo $memo->ID; ?>';

									var name_limit = 'video_limit';

									

									$.ajax({

										url:"<?php echo base_url(); ?>memorial/is_pakage_expired",

										type: "POST",

										data:{"memo_id": memo_id, "name_limit": name_limit},

										success:function(resp)

										{

											if(resp == 'yes'){

												$("#err_text").text("Limit Exceeded"); 

												$("#ErrorModal").modal("show"); 

												return false;

											}else{

												$("#loader_video").show();

												$("#add_video_form").submit();

											}

										}

									}); 

									

									

								}

								

								

							}

							

							</script>

							

						</form>

						  

						  

						  

                        </div>

                        <div class="tab-pane fade" id="audioTab">

                          <div class="gal_box">

                            <div class="row">

                              

							  <?php $a_counter=0; foreach($audios as $a){ 

							  

							  if($is_page_admin || $a->status=='Approved'){

							  

							  ?>

									

									<div class="col-md-6" style="position: relative;" id="a_<?php echo $a->ID; ?>">

									

									<audio id="player" class="playback" controls >

										<source id="mp3_src" src="<?php echo base_url(); ?>uploads/music/<?php echo $a->audio; ?>" type="audio/mp3"/>

									</audio>

									<?php if($is_page_admin){ ?>

											<a class="btn btn-danger btn-sm" style="cursor: pointer; float: right; margin-top:13px;" onclick="delete_audio('<?php echo $a->ID; ?>', '<?php echo $a->audio; ?>')">

												<i class="fa fa-trash"></i>

											</a>

											

											

											<?php if($a->status=='Pending'){ ?>

											

												<a class="remove_btn btn btn-success btn-sm" style="position: static;cursor: pointer; z-index: 100;" onclick="approve_audio(this, '<?php echo $a->ID; ?>')">

												<i class="fa fa-check"></i>

												</a>

												

											<?php } ?>

											

									<?php } ?>

									</div>

							  

							  <?php $a_counter++; } } ?>

							  

							  

							  <?php if($audio_limit && $is_page_super_admin){ ?>

									<div class="col-md-12 container alert alert-danger">

										Audio limit exceeded, Please upgrade your pakage

									</div>

								<?php } ?>

							  

							  

							  

                            </div>

                          </div>

						  

						  <script>

						  

						  function delete_audio(id, aud){

							  

							  if(confirm("Are you sure?")){

								  $.ajax({

											url:"<?php echo base_url(); ?>memorial/delete_audio",

											type: "POST",

											data:{"id": id, "aud": aud},

											success:function(resp)

											{

												if(resp == 'success'){

													$('#a_'+id).remove();

												}

												

											}

									}); 

							  }

						  }

						  

						  function approve_audio(l, id){

							  

							  $.ajax({

										url:"<?php echo base_url(); ?>memorial/approve_audio",

										type: "POST",

										data:{"id": id},

										success:function(resp)

										{

											if(resp == 'success'){

												l.remove();

											}else{

												$("#err_text").text("somthing went wrong");

												$("#ErrorModal").modal("show");

											}

											

										}

								}); 

								

						  }

						  

						  

						  </script>

						  

						  <form id="add_music_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_music">

					

							  <div class="prehead_left">

								 

								  <div class="row"> 

									<?php //if($is_page_admin){ ?>

									<?php if($this->session->userdata('user')){ ?>

										

										 

											<?php if($this->session->flashdata('error_music')){ ?>

													<div class="alert alert-danger display-hide">

														<button class="close" data-close="alert"></button>

														<?php echo $this->session->flashdata('error_music'); ?>

													</div>

											<?php } ?>

											

											<?php if($this->session->flashdata('succ_music')){ ?>

													<div class="alert alert-success display-hide">

														<button class="close" data-close="alert"></button>

														<?php echo $this->session->flashdata('succ_music'); ?>

													</div>

											<?php } ?>

										 

									

											

										  <div class="col-md-10">

										  									  

											<input type="file" id="upload_music" name="audio" required>

											<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">

											<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">

										  </div>

											  

										  <div class="col-md-2">

											  <div class="add-photo_new">

												<a href="javascript:;" onclick="add_music()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>

												<div id="loader-audio" style="display: none;" class="lds-ripple"><div></div><div></div></div>

											  </div>

										  </div>

										 

									<?php }else{ ?>

									<div style="padding: 10px 50px;">

									  You need to login to upload media. <a href="<?php echo base_url(); ?>login">Login Now</a>

									</div>

								  <?php } ?>

										  

								 </div>

							</div>

					

							<script>

							

							function add_music(){

								var pic = $("#upload_music").val();

								if(pic == ''){

									$("#err_text").text("Please select music to upload");

									$("#ErrorModal").modal("show");

								}else{

									

									var memo_id = '<?php echo $memo->ID; ?>';

									var name_limit = 'audio_limit';

									$.ajax({

										url:"<?php echo base_url(); ?>memorial/is_pakage_expired",

										type: "POST",

										data:{"memo_id": memo_id, "name_limit": name_limit},

										success:function(resp)

										{

											if(resp == 'yes'){

												$("#err_text").text("Limit Exceeded"); 

												$("#ErrorModal").modal("show"); 

												return false;

											}else{

												$('#loader-audio').show();

												$("#add_music_form").submit();

											}

										}

									}); 

				

								}

								

								

							}

							

							

							

							//$("video").click(function() {

								

								//var x = document.getElementById("player");

								//x.pause();

								/* $('audio').each(function(){

									this.pause(); // Stop playing

									this.currentTime = 0; // Reset time

								}); */ 

								

							//});

							

							

							

							

							

							

							</script>

							

						</form>

						  

						  

						  

						  

						  

						  

                        </div>

                      </div>

                      <div class="space20"></div>

                      <div class="row">

                        <div class="col-md-4"></div>

                        

                        <div class="col-md-8">

                          <div class="">

                            <!--<span class="red_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Photos</span>

                            <input id="selectfile" type="file" name="">-->

							

							

												

							

							

							

                          </div>

                        </div>

                      </div> 

                    </div>

                  </div>

                  

				  

				  <div class="tab-pane fade" id="menu3">

                    

					

					

					

					

					  <ul class="list-inline tribute_list">

							<?php if($this->session->userdata('user')){ ?>

								<li class="list-inline-item"><a onclick="leave_tribute('ttab2')" href="javascript:;" class="red_btn lea_tri">Lay a flower</a></li>

								<li class="list-inline-item"><a onclick="leave_tribute('ttab1')" href="javascript:;" class="red_btn">Light a Candle</a></li>

								<li class="list-inline-item"><a onclick="leave_tribute('ttab3')" href="javascript:;" class="red_btn">Leave a Note</a></li>

						    <?php }else{ ?>

							

								<li class="list-inline-item"><a href="<?php echo base_url(); ?>login" class="red_btn lea_tri">Lay a flower</a></li>

								<li class="list-inline-item"><a href="<?php echo base_url(); ?>login" class="red_btn">Light a Candle</a></li>

								<li class="list-inline-item"><a href="<?php echo base_url(); ?>login" class="red_btn">Leave a Note</a></li>

								

							<?php } ?>

                      </ul>

                      <h4 class="waitingalert"></h4>

                    <div class="leave_tribute">

                        <span class="tri_span"><img class="img-fluid" alt="img" src="<?php echo base_url(); ?>assets/img/pencil.png"><span id="tribute_title">Leave a Tribute</span></span>

                        <ul class="nav nav-tabs" role="tablist">

                          <li class="nav-item">

                            <a class="nav-link active" data-toggle="tab" id="ttab1" onclick="update_tribute(this.id)" href="#tab1"><img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle.png">

                            <img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/christmas-candle1.png">

                            </a>

                          </li>

                          <li class="nav-item">

                            <a class="nav-link" data-toggle="tab" id="ttab2" onclick="update_tribute(this.id)" href="#tab2"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower.png">

                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/flower1.png">

                            </a>

                          </li>

                          <li class="nav-item">

                            <a class="nav-link" data-toggle="tab" id="ttab3" onclick="update_tribute(this.id)" href="#tab3"><img class="img-fluid default_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather.png">

                            <img class="img-fluid active_img" alt="img" src="<?php echo base_url(); ?>assets/img/feather1.png">

                            </a>

                          </li>

                        </ul>

                        <div class="tab-content">

							<input type="hidden" id="leave_tab" />

                            <div id="tab1" class="tab-pane active"><br>

                              <textarea class="form-control" rows="8" id="comment1" placeholder="Add your tribute text here"></textarea>

                            </div>

                            <div id="tab2" class="tab-pane fade"><br>

                              <textarea class="form-control" rows="8" id="comment2" placeholder="Add your tribute text here"></textarea>

                            </div>

                            <div id="tab3" class="tab-pane fade"><br>

                              <textarea class="form-control" rows="8" id="comment3" placeholder="Add your tribute text here"></textarea>

                            </div>

                        </div>

                        <ul class="list-inline leave_ul">

                          <li class="list-inline-item">by <?php echo $this->session->userdata('user')->fname.' '.$this->session->userdata('user')->lname; ?></li>

                          <li class="list-inline-item pull-right">

                            <a href="javscript:;" onclick="add_tribute()" class="red_btn">Leave a Tribute</a>

                          </li>

                        </ul>

                    </div>

					

					</br>

					

					<div class="main_tri" id="tribute_div">

                      

					  <?php foreach($tributes as $t){ ?>

					  

					  <?php



					  $t_img = 'feather1.png';

					  if($t->action == "candle"){

						  $t_img = 'christmas-candle.png';

					  }elseif($t->action == 'tribute'){

						  $t_img = 'flower1.png';

					  }



					  ?>

					  

					  

						  <div class="featured_box" id="tribute<?php echo $t->ID ?>">

							  <div class="media">

							  

							  <img class="" src="<?php echo base_url(); ?>assets/img/<?php echo $t_img; ?>" alt="mage">

							  

								<div class="media-body">

								  <h5><?php echo $t->by; ?> <i><?php echo $t->d_action; ?></i>

								   

								  </h5>

								  <i onClick="markinappropriate(<?php echo $t->ID ?>)" style="cursor: pointer" title="Flag as inappropriate" class="pull-right fa fa-flag" aria-hidden="true"></i>

								  <p><?php echo $t->comment; ?></p>								  

								  </br>

								  <h6><?php echo date('l', strtotime($t->createdAt)).', '.date("F", strtotime($t->createdAt)).' '.date("d", strtotime($t->createdAt)); ?> <?php echo date("Y",strtotime($t->createdAt)); ?> <?php echo date("h:i:a",strtotime($t->createdAt)); ?> </h6>

									<h6>

									  <?php 

										if($this->session->userdata('user')->ID==$t->createdby) 

										{

										?>

										<a href="javascript:;" onClick="openeditmodal(<?php echo $t->ID ?>)">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

										<a href="javascript:;" onClick="deletemytribute(<?php echo $t->ID ?>,<?php echo $t->createdby ?> )">Delete</a>

										<?php }?>

									</h6>

								  <?php //if($t->action == "candle"){ ?>

									<!--<img class="img-fluid img13" alt="img" src="<?php echo base_url(); ?>assets/img/img13.png">-->

								  <?php //} ?>

								</div>

							  </div>

						  </div>

						  

					  <?php } ?>

					  

                    </div>

					

				

					

                  </div>

				  

				  <script>

				  

				  function leave_tribute(tab){

			

					  if(tab == 'ttab1'){

						  $("#leave_tab").val('candle');

						  $("#tribute_title").text("Light a Candle");

						  

					  }else if(tab == 'ttab2'){

						  $("#leave_tab").val('tribute');

						  $("#tribute_title").text("Lay a Flower");

					  }else{

						  $("#leave_tab").val('note');

						  $("#tribute_title").text("Leave a Note");

					  }

					  

					  $(".leave_tribute").show();

					  $('#'+tab).click();					  

				  }

				  

				  function update_tribute(tab){

					  if(tab == 'ttab1'){

						  $("#leave_tab").val('candle');

						  $("#tribute_title").text("Light a Candle");

					  }else if(tab == 'ttab2'){

						  $("#leave_tab").val('tribute');

						  $("#tribute_title").text("Lay a Flower");

					  }else{

						  $("#leave_tab").val('note');

						  $("#tribute_title").text("Leave a Note");

					  }

				  }

				  

				  function add_tribute(){

					  var memo_id = '<?php echo $memo->ID; ?>';

					  var memo_created_id = '<?php echo $memo->createdby; ?>';

					  var login_user = '<?php if($this->session->userdata('user')) echo $this->session->userdata('user')->ID;  ?>';

					  var action = $("#leave_tab").val();

					  var comment = '';

					  if(action == 'candle'){

						  comment = $("#comment1").val();

					  }else if(action == 'tribute'){

						  comment = $("#comment2").val();

					  }else if(action == 'note'){

						  comment = $("#comment3").val();

					  }else{

						  

					  }

					  if(comment=="" || comment.length < 20)
					  {
						  alert("Atleast 20 characters required.");
						  return;
					  }
					  
					  
					  $(".waitingalert").html("Your tribute is being posted.");

						$(".leave_tribute").hide();
					  

					  $.ajax({

						url:"<?php echo base_url(); ?>memorial/add_tribute",

						type: "POST",

						data:{"memo_id": memo_id, "action": action, "comment": comment, "memo_created_id": memo_created_id},

						success:function(resp)

						{

							  $(".waitingalert").html(" ")

							if(resp == 'block'){

								$("#err_text").text("You are block from this memorial!");

								$("#ErrorModal").modal("show");

								return false;

							}else{

								$(".leave_tribute").hide();

								$("#tribute_div").html("");

								$("#tribute_div").html(resp);

							}

							

							/* if(login_user != memo_created_id){

								$("#suc_text").text("Tribute will be posted after the approval from page owner!");

								$("#SuccessModal").modal("show");

							} */

							

							

						}

					}); 

					  

				  }

				  

				  

				  </script>

				  

				  

                </div>

                

            </div>

        </div>


<?php if($device == 'mobile'){ ?>
     
            <div class="col-md-4 col-12">

                <div class="space70"></div>
                <div class="space30 d-none d-md-none"></div>
				<?php if($is_page_admin){ ?>
				
				<div class="tribute_box-left tribute_box-new">
                    <div class="prehead_left">
                        <a href="" class="">Invite Family and Friends to this memorial: DESKTOP</a>
                        <a href="javascript:;" onclick="invite_modal()" class="red_btn tribute_box-btn"><span>+</span>Invite Now</a>
                    </div>
                </div>
				
				<?php } ?>

				

                <div class="tribute_box-left">
                    <div class="prehead_left">
                        <a href="">Share with a friend</a>
                        <ul class="list-inline">
                            <li class="list-inline-item">														
								<a target="_blank" href="https://www.facebook.com/share.php?title=<?php echo $memo->fname.' '.$memo->lname; ?>&u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>

								<script>

									function fbs_click() {
										u=location.href;t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');

										return false;
									}

									</script>
							</li>                           

                            <li class="list-inline-item">
								<a target="_blank" href="https://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >								
									<i class="fa fa-twitter" aria-hidden="true"></i>							
								</a>																					
							</li>							

							<li style="display: none" class="list-inline-item">								
								<a target="_blank" href="https://api.whatsapp.com/send?phone=+254722311588&text=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
									<i class="fa fa-whatsapp" aria-hidden="true"></i>								
								</a>																												
							</li>																										

							<li class="list-inline-item">															
								<a href="mailto:?subject=I wanted you to see this memorial for <?php echo $memo->fname.' '.$memo->lname; ?>&amp;body=<?php echo $title.' '.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" title="Share by Email">
									<i class="fa fa-envelope" aria-hidden="true"></i>								
								</a>														
							</li>
                        </ul>
                    </div>
                </div>

				

                <div class="tribute_box-left tribute_box-new1">
                    <div class="prehead_left">
                        <a href="javascript:;" class="tribute_box-text">Sign up for Notifications</a>
                        <a href="javascript:;" onclick="open_sign_up_model()" class="red_btn tribute_box-btn tribute_box-btn1">Sign Up</a>
                        <div class="clearfix"></div>
                    </div>
                </div>

								

                <div id="accordion">

				<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>

				<link rel="stylesheet" href="<?php echo base_url(); ?>assets/crop/croppie.css" />

					<?php if($is_page_admin){ ?>

					<form id="add_pic_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/add_pic" >

                    <div class="tribute_box-left tribute_box-new">
                      <div class="prehead_left">
                          <a href="" class="">Add Picture</a>
                          <div >
						  <?php if(count($pictures) > 0){ ?>
							  <a href="<?php echo base_url(); ?>uploads/<?php echo $pictures[0]->pic; ?>" data-rel="prettyPhoto[25727]" class="prettyPhoto kids_picture">
								<img id="first_pic" class="img-fluid" src="<?php echo base_url(); ?>uploads/<?php echo $pictures[0]->pic; ?>" />
							  </a>
						  <?php } ?>

						  </div>

						 <div class="row" style="margin-top: 20px"> 
						 <div class="col-md-12">
								<?php if($this->session->flashdata('error')){ ?>
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button>
											<?php echo $this->session->flashdata('error'); ?>
										</div>

								<?php } ?>
						 </div>
							  <div class="col-md-6">
								<!--<input type="file" id="upload_picture" name="pic" required>-->
								<input type="file" name="image" id="image" required>
								<input id="cropped_image" name="cropped_image" type="hidden" value="" />
								<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">
								<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">
							  </div>
							  <div class="col-md-6">
								  <div class="add-photo_new">
									<a href="javascript:;" onclick="add_pic()" class="red_btn tribute_box-btn tribute_box-btn1"><span>+</span>Add </a>
								  </div>
								   <div id="loader_main" style="display: none;" class="lds-ripple"><div></div><div></div></div>
							  </div>
						  </div>
                      </div>
                    </div>
				</form>

				<?php } ?>

				<?php if($is_page_admin){ ?>
				
                    <div class="card card1">
                        <div class="card-header">
                              <a class="collapsed card-link card-link1" data-toggle="collapse" href="#collapseThree1">
                                <i class="red_clr"><?php echo count($notification); ?> New </i>  Important Notifications<span><i class="fa fa-exclamation" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseThree1" class="collapse" data-parent="#accordion">
                            <div class="card-body">
								<ul class="list-unstyled list_page-activity">
								<?php foreach($notification as $noti){  ?>
									<li><p style="color: white"><?php echo $noti->comment; ?></p></span></li>
								 <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

				<?php } ?>

                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFour">
                                Page Activity<span><i class="fa fa-users" aria-hidden="true"></i></span>
                              </a>
                        </div>
                        <div id="collapseFour" class="collapse show" data-parent="#accordion">
                            <div class="card-body">
                                <ul class="list-unstyled list_page-activity">
                                  <?php foreach($tributes_activity as $t){ ?>
									<li><?php echo $t->by.' '.$t->d_action; ?><span><?php echo $t->timeago; ?></span></li>								  
								  <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                              <a class="collapsed card-link activity" data-toggle="collapse" href="#collapseFive">
                                Page Views   <span><?php echo $memo->views; ?></span>
                              </a>
                        </div>
                    </div>
					<div class="card">
                        <div class="card-header">
                              <a style="text-align: center" class="collapsed card-link activity" onclick="report('<?php echo $memo->ID; ?>')" data-toggle="collapse" href="javascript:;">
                                REPORT MEMORIAL  <span></span>
                              </a>
                        </div>
                    </div>
                </div>
            </div>

				
<?php } ?>

<!--  Crop Modal Start Here   -->

				<div id="uploadimageModal" class="modal" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<div class="modal-body">
								<div class="row">
									<div class="col-md-12 text-center">
										  <div id="image_demo" style="width:100%; height:auto"></div>
											<p class="col-md-12" style="line-height: normal; color: #582236;">Click on the image and drag around to adjust it.</p>
									</div>


								</div>

							</div>

							<div class="modal-footer">

								<button type="button" class="red_btn crop_image">Upload</button>
								<!--<button id="uploadimageModal_btn" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

							</div>
						</div>
					</div>
				</div>

				<!--  Crop Modal End Here   -->




				<!--  Crop Main Modal Start Here   -->

				<div id="uploadimageModalMain" class="modal" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<div class="modal-body">
								<div class="row">
									<div class="col-md-12 text-center">
										  <div id="image_demo1" style="width:100%; margin-top:30px"></div>
											<p class="col-md-12" style="line-height: normal; color: #582236;">Click on the image and drag around to adjust it.</p>
									</div>

									<div class="col-md-4" style="padding-top:30px;">

										<br />
										<br />
										<br/>																			  
									</div>

								</div>

							</div>

							<div class="modal-footer">

								<button type="button" class="red_btn crop_image_main">Upload</button>
								<!--<button id="uploadimageModal_btn" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

							</div>
						</div>
					</div>
				</div>

				<!--  Crop Modal End Here   -->



			<script>

			
			
			function add_pic(){

						var pic = $("#image").val();

						if(pic == ''){

							$("#err_text").text("Please select image to upload");

							$("#ErrorModal").modal("show");

						}else{

							

							var memo_id = '<?php echo $memo->ID; ?>';

							var name_limit = 'photo_limit';

							$.ajax({

								url:"<?php echo base_url(); ?>memorial/is_pakage_expired",

								type: "POST",

								data:{"memo_id": memo_id, "name_limit": name_limit},

								success:function(resp)

								{

									if(resp == 'yes'){

										$("#err_text").text("Limit Exceeded"); 

										$("#ErrorModal").modal("show"); 

										

									}else{

										$('#loader_main').show();

										$("#add_pic_form").submit();

									}

								}

							}); 

							

							

						}

						

						

					}
			

			function report(memo_id){

				

				$("#report_memo_message").hide();

				$("#report_memo_err_message").hide();

				$("#report_desc").val("");

				

				

				$("#MemoReportModal").modal("show");

				

			}

			

			</script>

			

			

			

			

			

			<!---- Modal -->

					  <div class="modal fade" id="MemoReportModal" role="dialog">

						<div class="modal-dialog">

						

						  <!-- Modal content-->

						  <div class="modal-content">

							<div class="modal-header">

							  <h4 style="line-height: 1; color: black" class="modal-title">Report Memorial</h4>

							</div>

							<div class="modal-body">

							

							<div class="alert alert-success" id="report_memo_message" style="display: none">

							  <strong>Success!</strong> Your message has been sent for review.

							</div>

							

							<div class="alert alert-danger" id="report_memo_err_message" style="display: none">

							  <strong>Error!</strong> Already Submitted .

							</div>

							

							<div>							

								<label> Message </label>

								<textarea id="report_desc" rows="4" class="form-control" name="description" placeholder="Enter text here..."></textarea>

								<p id="report_desc_error" style="display: none; color: red;">This field is required</p>

							</div>

						

							</div>

							

							<div style="padding: 15px; text-align: right">

								<button type="button" class="btn btn-primary btn-small-edit" onclick="report_memo()" style="background: #582236; border-color: #582236;">Report</button>

								<button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

							</div>

						  </div>

						  

						</div>

					  </div>

					 <!-- End Success Modal -->

					 

					 

					 <script>

					 

					 function report_memo(){

						 

						 var memo_id = <?php echo $memo->ID; ?>;

						 var report_desc = $("#report_desc").val();

						

						 if(report_desc == ''){

							$("#report_desc_error").show();

							return false;

						 }else{

							 $("#report_desc_error").hide();

						 }

						 

						 

						 $.ajax({

								url:"<?php echo base_url(); ?>memorial/report_memo",

								type: "POST",

								data:{"memo_id": memo_id, "report_desc": report_desc},

								success:function(resp)

								{	

									if(resp == 'Error'){

										$("#report_memo_err_message").show();

										$("#report_memo_message").hide();

									}else{

										$("#report_memo_message").show();

										$("#report_memo_err_message").hide();

									}

									

								}

							});

					 }

					 

					 </script>











    </div>

</section>



<script>



function edit_title(fname, lname){

	

	var memo_id = '<?php echo $memo->ID; ?>';

	$.ajax({

		url:"<?php echo base_url(); ?>memorial/get_memo_title",

		type: "POST",

		data:{"memo_id": memo_id},

		success:function(resp)

		{

			resp = JSON.parse(resp);

			

			$("#edit_fname").val(resp.fname);

			$("#edit_lname").val(resp.lname);

			$("#MemoEditTitleModal").modal('show');

			

		}

	});

}



function edit_pic(){

	

	var memo_id = '<?php echo $memo->ID; ?>';

	$("#MemoEditPicModal").modal('show');

}



</script>





<!---- Modal -->

<div class="modal fade" id="MemoEditPicModal" role="dialog">

<div class="modal-dialog">



  <!-- Modal content-->

  <div class="modal-content">

	<div class="modal-header">

	  <h4 style="line-height: 1; color: black" class="modal-title">Edit Memorial Picture</h4>

	</div>

	<div class="modal-body">



		<form id="update_pic_form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>memorial/update_memo_pic" >

					

                    <div style="margin-bottom: 25px;">

                      <div class="prehead_left">

                         

						 <div class="row" style="margin-top: 20px"> 

						

							  <div class="col-md-6">

								

								<input type="file" name="image" id="image1" required>

								

								<input id="cropped_image2" name="cropped_image" type="hidden" value="" />

								

								<input type="hidden" name="memo_id" value="<?php echo $memo->ID; ?>">

								<input type="hidden" name="webaddress" value="<?php echo $memo->webaddress; ?>">

							  </div>

							  

							  <div class="col-md-4">

								  <div class="add-photo_new">

								  

									<a href="javascript:;" onclick="update_pic()" class="red_btn tribute_box-btn tribute_box-btn1">Update </a>

									  

									

								  </div>

								  

							  </div>

							  

							  

							   <div class="col-md-2">

									<button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

							  </div>

							  

						  </div>

						  

                      </div>

                    </div>

					

					<script>

					

					function update_pic(){

						var pic = $("#cropped_image2").val();

						if(pic == ''){

							$("#err_text").text("Please select image to upload");

							$("#ErrorModal").modal("show");

						}else{

							

							var memo_id = '<?php echo $memo->ID; ?>';

							//$('#loader_main').show();

							$("#update_pic_form").submit();

													

						}

						

						

					}

					

					</script>

					

                    

					

				</form>





			</div>

		</div>

	</div>

</div>





<!---- Modal -->

<div class="modal fade" id="MemoEditTitleModal" role="dialog">

<div class="modal-dialog">



  <!-- Modal content-->

  <div class="modal-content">

	<div class="modal-header">

	  <h4 style="line-height: 1; color: black" class="modal-title">Edit Memorial</h4>

	</div>

	<div class="modal-body">

		

	<div>							

		

		

		<p id="edit_memo_error" class="pad_right0" style="display: none; color: red; margin-left: 12px; margin-bottom: 12px;">Please enter required field</p>

			

		<div class="col-md-12">

			<div class="row">

				<div class="col-md-4 pad_right0">

					<label> First Name </label>

					

				</div>

				<div class="col-md-8">

					<input class="form-control" id="edit_fname" name="edit_fname" value="" type="text"  />

				

				</div>

			</div>

		</div>

		

		<br>

		

		<div class="col-md-12">

			<div class="row">

				<div class="col-md-4 pad_right0">

					<label> Last Name </label>

				</div>

				<div class="col-md-8">

					<input class="form-control" id="edit_lname" name="edit_lname" value="" type="text" />

					

				</div>

			</div>

		</div>

		

		</br>

		

	</div>



	<div></div>

	

	

	</div>

	

	<div style="padding: 15px; text-align: right">

		<button type="button" class="btn btn-primary btn-small-edit" onclick="update_memo_title()" style="background: #582236; border-color: #582236;">Update</button>

		<button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

	</div>

  </div>

  

</div>

</div>

<!-- End Success Modal -->

					 

					 

					 <script>

					 

					 function update_memo_title(){

						 

						 var memo_id = '<?php echo $memo->ID; ?>';

						 var fname = $("#edit_fname").val();

						 var lname = $("#edit_lname").val();

						

						 if(fname == '' || lname == ''){ 

							$("#edit_memo_error").show();

							return false;							 

						 }else{

							 $("#edit_memo_error").hide();

						 }

						 

						 

						 $.ajax({

								url:"<?php echo base_url(); ?>memorial/update_memo_title",

								type: "POST",

								data:{"memo_id": memo_id, "fname": fname,"lname": lname},

								success:function(resp)

								{

									$("#memo_title").text(resp);

									$("#MemoEditTitleModal").modal("hide");

									

								}

							});

					 }

					 

					 </script>




<!---- Modal -->

  <div class="modal fade" id="SignupModal" role="dialog">

    <div class="modal-dialog">

    

      <!-- Modal content-->

      <div class="modal-content">

        <div class="modal-header">

          <h4 style="line-height: 1; color: black" class="modal-title">Signup For Notification</h4>

        </div>

        <div class="modal-body">

		

		<div class="alert alert-success" id="notification_email_msg1" style="display: none">

		  <strong></strong> <span id="noti_msg"> You have successfully signup for notification. </span>

		</div>

		

		<Label> Email Address</label>

		  <input type="text" id="notification_email" value="<?php if($this->session->userdata('user')) echo $this->session->userdata('user')->email;  ?>" class="form-control" />

		  <p id="email_error1" style="display: none; color: red;">This field is required</p>

		</div>

        

		<div style="padding: 15px; text-align: right">

            <button type="button" class="btn btn-primary btn-small-edit" onclick="sign_up_notification()" style="background: #582236; border-color: #582236;">Signup</button>

            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

		</div>

      </div>

      

    </div>

  </div>

 <!-- End Success Modal -->

 

   <div class="modal fade" id="edittribute" role="dialog">

    <div class="modal-dialog">

    

      <!-- Modal content-->

      <div class="modal-content">

        <div class="modal-header">

          <h4 style="line-height: 1; color: black" class="modal-title">Edit Content</h4>

        </div>

        <div class="modal-body">

		<Label>Please enter text</label>

		  <textarea class="form-control" id="editingtribute"></textarea>

		</div>

        

		<div style="padding: 15px; text-align: right">

            <button type="button" class="btn btn-primary btn-small-edit" onclick="updatethetribute()" style="background: #582236; border-color: #582236;">Update</button>

            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

		</div>

      </div>

      

    </div>

  </div>

 <!-- End Success Modal -->



 

 <script>

				

		function open_sign_up_model(){	

				$("#SignupModal").modal("show");

		}

		

		function sign_up_notification(){

			 

			 var memo_id = '<?php echo $memo->ID; ?>';

			 var email = $("#notification_email").val();

			 

			 if(email == ""){

				 $("#email_error1").show();

			 }else{

				 $("#email_error1").hide();

			 }

			 

			 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

			 if(!regex.test(email)) {

				$("#email_error1").text("Invalid email address!");

				$("#email_error1").show();

				return false;

			 }else{

				 $("#email_error1").hide();

			 }

			 

			 

			 $.ajax({

				url:"<?php echo base_url(); ?>login/sign_up_notification",

				type: "POST",

				data:{"memo_id": memo_id, "email": email},

				success:function(resp)

				{

					

					$("#noti_msg").text(resp);

					$("#notification_email_msg1").show();

					

				}

			}); 

			 

		 }

				

</script>

 



<!---- Modal -->

  <div class="modal fade" id="InviteModal" role="dialog">

    <div class="modal-dialog">

    

      <!-- Modal content-->

      <div class="modal-content">

        <div class="modal-header">

          <h4 style="line-height: 1; color: black" class="modal-title">Family OR Friend Email</h4>

        </div>

        <div class="modal-body">

		

		<div class="alert alert-success" id="sent_email_msg" style="display: none">

		  <strong>Success!</strong> Invitation Email sent successfully.

		</div>

		

		<div class="alert alert-danger" id="sent_email_err_msg" style="display: none">

		  <strong></strong> Invitation Already Sent.

		</div>

		

		<Label> Email Address</label>

		  <input type="text" id="invited_email" class="form-control" />

		  <p id="email_error" style="display: none; color: red;">This field is required</p>

		</div>

        

		<div style="padding: 15px; text-align: right">

            <button type="button" class="btn btn-primary btn-small-edit" onclick="send_invitation()" style="background: #582236; border-color: #582236;">Invite</button>

            <button type="button" class="btn btn-gray btn-small-edit" data-dismiss="modal">Close</button>

		</div>

      </div>

      

    </div>

  </div>

 <!-- End Success Modal -->

 

 

  <script>

 

 function invite_modal(){

	 

		var memo_id = '<?php echo $memo->ID; ?>';

		var name_limit = 'page_managers';

		

		$.ajax({

			url:"<?php echo base_url(); ?>memorial/is_pakage_expired",

			type: "POST",

			data:{"memo_id": memo_id, "name_limit": name_limit},

			success:function(resp)

			{

				if(resp == 'yes'){

					$("#err_text").text("Limit Exceeded"); 

					$("#ErrorModal").modal("show"); 

					

				}else{

					$("#invited_email").val("");

					$("#sent_email_msg").hide();

					$("#sent_email_err_msg").hide();

					$("#InviteModal").modal("show");

				}

			}

		}); 

 }

 

 function send_invitation(){

	 

	 var memo_id = '<?php echo $memo->ID; ?>';

	 var createdBy = '<?php echo $memo->createdby; ?>';

	 var webaddress = '<?php echo $memo->webaddress; ?>';

	 var memory_of = '<?php echo strtoupper($memo->fname).' '.strtoupper($memo->lname); ?>';  

	 var email = $("#invited_email").val();

	 

	 if(email == ""){

		 $("#email_error").show();

	 }else{

		 $("#email_error").hide();

	 }

	 

	 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	 if(!regex.test(email)) {

		$("#email_error").text("Invalid email address!");

		$("#email_error").show();

		return false;

	 }else{

		 $("#email_error").hide();

	 }

	 

	 

	 $.ajax({

		url:"<?php echo base_url(); ?>login/send_invitation",

		type: "POST",

		data:{"memo_id": memo_id, "email": email, "webaddress": webaddress, "memory_of": memory_of, "createdBy": createdBy},

		success:function(resp)

		{

			if(resp == 'success'){

				$("#sent_email_msg").show();

				$("#sent_email_err_msg").hide();

			}else{

				$("#sent_email_err_msg").show();

				$("#sent_email_msg").hide();

			}

		}

	}); 

	 

 }

 

 

 </script>

 

 



<?php $this->load->view("include/footer"); ?>





<script>





/* $("#google_map_loc").autocomplete('Home/GetCompanyNames', {

//minChars: 1,

//width: 402,

//matchContains: "word",

autoFill: true,

select: function (event, ui) {

    var label = ui.item.label;

    var value = ui.item.value;

   //store in session

   alert(value);

  //document.valueSelectedForAutocomplete = value 

}

}); */



				

$(".notice_frm").focusout(function(){

		//save_notices();

	

});





function save_notices(){

	//alert("saving");

		var notice_for = $("#notice_for").val();

		var dob = $("#dob").val();

		var dod = $("#dod").val();

		var house_of_worship = $("#house_of_worship").val();

		var country = $("#country").val();

		var town = $("#town").val();

		var cremation_detail = $("#cremation_detail").val();

		var contact_person = $("#contact_person").val();

		var meeting_venue = $("#meeting_venue").val();

		var google_map_loc = $("#google_map_loc").val();

		//alert(google_map_loc);

		var memo_id = <?php echo $memo->ID; ?>;

		

		$.ajax({

			url:"<?php echo base_url(); ?>memorial/update_notices",

			type: "POST",

			data:{"memo_id": memo_id, "notice_for": notice_for, "dob": dob, "dod": dod, "house_of_worship": house_of_worship, 

			"country": country, "town": town, "cremation_detail": cremation_detail, "contact_person": contact_person,

			"meeting_venue": meeting_venue, "google_map_loc": google_map_loc},

			success:function(resp)

			{

				$("#notices_alert").fadeIn();
				setTimeout(function(){ 
					$("#notices_alert").fadeOut();
				 }, 2000);
				
			}

		});

	

}





$(function(){

    $("audio").on("play", function() {

        $("audio").not(this).each(function(index, audio) {

            audio.pause();

        });

		

		$("video").each(function(index, video) {

            video.pause();

        });

		

    });

	

	

	$("video").on("play", function() {

        $("video").not(this).each(function(index, video) {

            video.pause();

        });

		

		$("audio").each(function(index, audio) {

            audio.pause();

        });

		

    });

	

});				


document.getElementById("player1").addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);


function toggle_music(act){

	var x = document.getElementById("player1"); 

	

	if(act == 'stop'){

		

		//stop music

		//$("audio").each(function(index, audio) {

            x.pause();

        //});

		

		$("#stop_music").hide();

		$("#play_music").show();

		

	}else{

		

		// play music

		x.play();

		$("#play_music").hide();

		$("#stop_music").show();

		

	}

}





$(document).ready(function(){

  

	var memo_id = '<?php echo $memo->ID; ?>';

	var views = '<?php echo $memo->views + 1; ?>';

	

	$.ajax({

		url:"<?php echo base_url(); ?>login/update_views",

		type: "POST",

		data:{"memo_id": memo_id, "views": views},

		success:function(resp)

		{

			///

		}

	});

  

});





function getUrlParameter(sParam) {



	var sPageURL = decodeURIComponent(window.location.search.substring(1)),



		sURLVariables = sPageURL.split('&'),



		sParameterName,



		i;







	for (i = 0; i < sURLVariables.length; i++) {



		sParameterName = sURLVariables[i].split('=');







		if (sParameterName[0] === sParam) {



			return sParameterName[1] === undefined ? true : sParameterName[1];



		}



	}



};







$(function(){



		var tab_index = getUrlParameter('tab');





		if(tab_index == 'a_gallery'){



			$(".gallary_tab").click();

			$(".a_gallery").click();



		}else if(tab_index == 'v_gallery'){



			$(".gallary_tab").click();

			$(".v_gallery").click();



		}else if(tab_index == 'p_gallery'){



			$(".gallary_tab").click();

			$(".p_gallery").click();



		}else{



			//$(".gallary_tab").click();

			//$(".p_gallery").click();



		}	



	});





</script>



<script>  

								$(document).ready(function(){



									$image_crop = $('#image_demo').croppie({

									enableExif: true,

									viewport: {

									  width:280,

									  height:280,

									  type:'square' //circle

									},

									boundary:{

									  width:300,

									  height:300

									}

								  });

								  $image_crop1 = $('#image_demo1').croppie({

									enableExif: true,

									viewport: {

									width:280,

									height:280,

									type:'square' //circle

									},

									boundary:{

									width:300,

									height:300

									}

									});

								  

								 

								  $('#image').on('change', function(){

		

									// check width and height of image

									  var _URL = window.URL || window.webkitURL;

									  var file, img;

									  if (file = this.files[0]) {

										img = new Image();

										img.onload = function () {

											

											if(this.width < 120 || this.height < 120){

												//alert(this.width);

												$("#err_text").text("Image is too small, Please try a larger image. Min dimension: 120px");

												$("#ErrorModal").modal('show');

												

											}else if(this.width > 24000 || this.height > 24000){

												

												$("#err_text").text("Image is too large, Please try a smaller image. Max dimension: 24000px");

												$("#ErrorModal").modal('show');

												

											}else{

													var reader = new FileReader();

													reader.onload = function (event) {

													  $image_crop.croppie('bind', {

														url: event.target.result

													  }).then(function(){

														console.log('jQuery bind complete');

													  });

													}

												

													reader.readAsDataURL(file);

													$('#uploadimageModal').modal('show');

											}

											

										};

										

										img.src = _URL.createObjectURL(file);

									}

									

								  });

								  

								  $('#image1').on('change', function(){

		

									// check width and height of image

									  var _URL = window.URL || window.webkitURL;

									  var file, img;

									  if (file = this.files[0]) {

										img = new Image();

										img.onload = function () {

											

											if(this.width < 120 || this.height < 120){

												//alert(this.width);

												$("#err_text").text("Image is too small, Please try a larger image. Min dimension: 120px");

												$("#ErrorModal").modal('show');

												

											}else if(this.width > 24000 || this.height > 24000){

												

												$("#err_text").text("Image is too large, Please try a smaller image. Max dimension: 24000px");

												$("#ErrorModal").modal('show');

												

											}else{

													var reader = new FileReader();

													reader.onload = function (event) {

													  $image_crop.croppie('bind', {

														url: event.target.result

													  }).then(function(){

														console.log('jQuery bind complete');

													  });

													}

												

													reader.readAsDataURL(file);

													$('#uploadimageModal').modal('show');

											}

											

										};

										

										img.src = _URL.createObjectURL(file);

									}

									

								  });

								  

								  $('#upload_picture1').on('change', function(){

									  

									  

									  // check width and height of image

									  var _URL = window.URL || window.webkitURL;

									  var file, img;

									  if (file = this.files[0]) {

										img = new Image();

										img.onload = function () {

											

											if(this.width < 120 || this.height < 120){

												//alert(this.width);

												$("#err_text").text("Image is too small, Please try a larger image. Min dimension: 120px");

												$("#ErrorModal").modal('show');

												

											}else if(this.width > 24000 || this.height > 24000){

												

												$("#err_text").text("Image is too large, Please try a smaller image. Max dimension: 25000px");

												$("#ErrorModal").modal('show');

												

											}else{

													var reader = new FileReader();

													reader.onload = function (event) {

													  $image_crop.croppie('bind', {

														url: event.target.result

													  }).then(function(){

														console.log('jQuery bind complete');

													  });

													}

												

													reader.readAsDataURL(file);

													$('#uploadimageModal').modal('show');

											}

											

										};

										

										img.src = _URL.createObjectURL(file);

									}

								  });

								  

								  $('#image_main').on('change', function(){

		

										// check width and height of image

										var _URL = window.URL || window.webkitURL;

										var file, img;

										if (file = this.files[0]) {

											img = new Image();

											img.onload = function () {

												

												if(this.width < 120 || this.height < 120){

													//alert(this.width);

													$("#err_text").text("Image is too small, Please try a larger image. Min dimension: 120px");

													$("#ErrorModal").modal('show');

													

												}else if(this.width > 24000 || this.height > 24000){

													

													$("#err_text").text("Image is too large, Please try a smaller image. Max dimension: 24000px");

													$("#ErrorModal").modal('show');

													

												}else{

														var reader = new FileReader();

														reader.onload = function (event) {

														$image_crop1.croppie('bind', {

															url: event.target.result

														}).then(function(){

															console.log('jQuery bind complete');

														});

														}

													

														reader.readAsDataURL(file);

														$('#uploadimageModalMain').modal('show');

												}

												

											};

											

											img.src = _URL.createObjectURL(file);

										}



								  });



								  $('.crop_image').click(function(event){

									$image_crop.croppie('result', {

									  type: 'canvas',

									  size: 'viewport'

									}).then(function(response){

									  $.ajax({

										url:"<?php echo base_url(); ?>memorial/upload_crop_image",

										type: "POST",

										data:{"image": response},

										success:function(img)

										{

										  $('#uploadimageModal').modal('hide');

										  //$('#uploaded_image').html(data.text);

										  $('#uploaded_image').html('<img src="<?php echo base_url(); ?>uploads/'+img+'" width="130" height="130" class="thumbnail" />');

										  $('#cropped_image').val(img);

										  $('#cropped_image1').val(img);

										  $('#cropped_image2').val(img);

										  

										  $('#first_pic').attr("src","<?php echo base_url(); ?>uploads/"+img);

										}

									  });

									})

								  });



								  $('.crop_image_main').click(function(event){

										$image_crop1.croppie('result', {

										type: 'canvas',

										size: 'viewport'

										}).then(function(response){

										$.ajax({

											url:"<?php echo base_url(); ?>memorial/upload_crop_image",

											type: "POST",

											data:{"image": response},

											success:function(img)

											{

											$('#uploadimageModalMain').modal('hide');

											var memo_id = '<?php echo $memo->ID; ?>';
											var webaddress = '<?php echo $memo->webaddress; ?>';
											var cropped_image = img;

											$.ajax({

												url:"<?php echo base_url(); ?>memorial/update_memo_pic_ajax",

												type: "POST",

												data:{"memo_id": memo_id, "webaddress": webaddress, "cropped_image": cropped_image},

												success:function(resp)

												{
													if(resp == 'success'){
														//location.reload();
														$('#main_memorial_image').attr("src","<?php echo base_url(); ?>uploads/"+img);
													}
												}

											});

											}

										});

										})

										});



								});  

								</script>



<script>

function markinappropriate(theid)

	{

		  var memo_id = '<?php echo $memo->ID; ?>';

		  

		  $.ajax({

			url:"<?php echo base_url(); ?>memorial/markinappropriate",

			type: "POST",

			data:{"id": theid, "memo_id": memo_id},

			success:function(resp)

			{

				if(resp == 'Error'){

					alert("You have already submitted.")

				}

				else{

					alert(resp)

				}



			}

			}); 

	}

	

	function deletemytribute(theid,adderid)

	{

		if (confirm('Are you sure you want to delete?')) {

		  $.ajax({

			url:"<?php echo base_url(); ?>memorial/deletemytribute",

			type: "POST",

			data:{"id": theid ,"adderid": adderid},

			success:function(resp)

			{

				$( "#tribute"+theid ).remove();

			}

			}); 

		}

	}

	

		function openeditmodal(theid){	

			$("#edittribute").modal("show");

			var hery=$( "#tribute"+theid ).find( "p" ).text();

			$("#editingtribute").text(hery);

			globalID=theid;

		}

	

		function updatethetribute()

			{

				var newmessage= $("#editingtribute").val();

			  $.ajax({

				url:"<?php echo base_url(); ?>memorial/updatemytribute",

				type: "POST",

				data:{"id": globalID , "thetribute": newmessage},

				success:function(resp)

				{

					$( "#tribute"+globalID ).find( "p" ).text(resp);

					$("#edittribute").modal("hide");

				}

				}); 

			}

	$('#mp3_src1').attr('src', '<?php echo base_url(); ?>uploads/music/<?php echo $memo->memorial_music; ?>');



</script>

								

							

<!--<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="<?php echo base_url(); ?>assets/crop/croppie.js"></script>

<!--<script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>-->



    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/simpleLightbox-master/dist/simpleLightbox.min.css">

        <script src="<?php echo base_url(); ?>assets/simpleLightbox-master/dist/simpleLightbox.min.js"></script>



<script>

$('.imagegallery a').simpleLightbox();

	//if you want to open the video in light box. sometimes you need to <a class="lightBoxVideoLink" href="https://www.youtube.com/embed/qtQgbdmIO30?autoplay=true">show a video</a> in light-box (Video can be placed somewhere in gallery as well).

//$('.lightBoxVideoLink').simpleLightbox();

</script>





<script>



// A $( document ).ready() block.

$( document ).ready(function() {

    console.log( "ready!" );

});



</script>





