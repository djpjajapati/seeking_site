<?php $this->load->view("include/header.php"); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.checked {
  color: orange !important;
}
</style>
<section class="testi_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>This is why </p>
                <h1>We Do what We Do.</h1>
            </div>
        </div>
    </div>
</section>
<section class="testi_main">
    <div class="container">
        <div class="row">
			<div class="col-md-12">
			<h2>Testimonials</h2>
			<br />
			</div>
            <div class="col-md-12">
            	 <?php
				 if(!empty($results))
				 {
					 foreach($results as $val)
					 {
				 	 ?>
                         <div class="testi_box">
                            <div class="row">
                                <div class="col-lg-2 border_left col-md-3">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><span class="fa fa-star <?php if($val->rating>=1){ echo 'checked';}?>"></span></li>
                                        <li class="list-inline-item"><span class="fa fa-star <?php if($val->rating>=2){ echo 'checked';}?>"></span></li>
                                        <li class="list-inline-item"><span class="fa fa-star <?php if($val->rating>=3){ echo 'checked';}?>"></span></li>
                                        <li class="list-inline-item"><span class="fa fa-star <?php if($val->rating>=4){ echo 'checked';}?>"></span></li>
                                        <li class="list-inline-item"><span class="fa fa-star <?php if($val->rating==5){ echo 'checked';}?>"></span></li>
                                    </ul>
                                    <span><?php echo timespan($val->add_date, time(),1); ?> ago</span>
                                </div>
                                <div class="col-lg-10 col-md-9">
                                    <p><?php echo $val->message;?></p>
                                    <ul class="list-inline right_testi">
                                        <li class="list-inline-item"><?php echo $val->name;?></li>
                                        <li class="list-inline-item pull-right">
											<i onClick="markinappropriate(<?php echo $val->id; ?>)" style="cursor: pointer" title="Flag as inappropriate" class="pull-right fa fa-flag" aria-hidden="true"></i>
										</li>
                                        <!--<li class="list-inline-item pull-right"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</li>-->																				
										<!--<li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
										
										
										<!--<li class="list-inline-item pull-right">								
											<a target="_blank" href="https://api.whatsapp.com/send?phone=91XXXXXXXXXX&text=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
												<i class="fa fa-whatsapp" aria-hidden="true"></i>								
											</a>																												
										</li>-->																										
										
										<li class="list-inline-item pull-right">															
											<a href="mailto:?subject=Tribute&amp;body=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" title="Share by Email">
												<i class="fa fa-envelope" aria-hidden="true"></i>								
											</a>														
										</li>
										
										<li class="list-inline-item pull-right">
											<a target="_blank" href="https://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" >
												<i class="fa fa-twitter" aria-hidden="true"></i>
											</a>
										</li>
										
										<li class="list-inline-item pull-right">
											<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>
										
										
                                    </ul>
                                </div>
                            </div>
                        </div>
                 	 <?php
					 }
				 }
				 else
				 {
				 ?>
                 	<div class="testi_box">
                        <div class="row">
                        	<div class="col-md-12 text-danger">No Record Found!</div>
                        </div>
                    </div>
                 <?php	 
				 }
				 ?>
             </div>
        </div>
    </div>
</section>

<script>

function markinappropriate(theid){
		  
		  $.ajax({
			url:"<?php echo base_url(); ?>contact/markinappropriate",
			type: "POST",
			data:{"id": theid},
			success:function(resp)
			{
				if(resp == 'Error'){
					alert("You have already submitted.")
				}
				else{
					alert(resp)
				}
			}
		}); 
}

</script>


<script src='https://www.google.com/recaptcha/api.js'></script> 
<?php $this->load->view("include/footer"); ?>

