<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library('image_lib');
			
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			$this->email = ""; 
			if(isset($sess_data->username))
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				$this->email = $sess_data->email; 
			}      
	}

	public function index()
	{		
		//redirect('login'); exit;
		$this->data['title'] = "Online Memorials Share Your Memories Safiri Salama | SafiriSalama.com";
		$this->data['meta_description'] = "Memories Designed to Last. Bring all the warm memories of your loved one together in a Memorial Website";
		$this->data['meta_keywords'] = "Online Memorials Share Your Memories Safiri Salama | SafiriSalama.com";
		$this->data['memorials'] = $this->db->query("select * from memorials where status='Active' and visibility = 'public' order by ID desc limit 10")->result();
		$this->data['total_memorial'] = $this->db->query("select count(*) as cnt from memorials")->row()->cnt;
		$this->data['total_visitor'] = $this->db->query("SELECT sum(views) as cnt FROM memorials")->row()->cnt;
		$this->data['total_tributes'] = $this->db->query("select count(*) as cnt from memo_tribute")->row()->cnt + $this->db->query("select count(*) as cnt from featured_memo_tribute")->row()->cnt;
		$this->data['picture_uploaded'] = $this->db->query("select count(*) as cnt from memo_pic")->row()->cnt;
		$this->data['total_picture_uploaded'] = $this->data['picture_uploaded'] + $this->data['total_memorial'];
        //echo $this->data['total_picture_uploaded']; exit;
		$this->load->view("index", $this->data);
        
	}
	
	public function terms_conditions()
	{		
		$this->data['title'] = "Terms and Conditions Safiri Salama";
		$this->data['meta_description'] = "Terms and Conditions Safiri Salama";
		$this->data['meta_keywords'] = "Terms and Conditions Safiri Salama";
		$this->data['page'] = $this->db->query("select * from content_management where ID = 1")->row();
        $this->load->view("terms_conditions", $this->data);
        
	}
	
	public function privacy_policy()
	{		
		$this->data['title'] = "Privacy Policy Safiri Salama";
		$this->data['meta_description'] = "Privacy Policy Safiri Salama";
		$this->data['meta_keywords'] = "Privacy Policy Safiri Salama";
		$this->data['page'] = $this->db->query("select * from content_management where ID = 2")->row();
        $this->load->view("privacy_policy", $this->data);
        
	}
	
		
}
