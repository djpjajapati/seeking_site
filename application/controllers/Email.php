<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Email extends CI_Controller {
	function __construct()
	{
		parent::__construct();
 	}
	public function AnniversaryEmail()
	{
		$this->load->library("Smtp_email");	  
		$this->load->library("Emailtemp");
 		$sender_email = "no-reply@safirisalama.com";
		$sender_name = "SafiriSalama Memorials";
		$receiver_email = "nickkurat017@gmail.com";
		$message_subject = "Anniversary Email";
		$message_body = $this->emailtemp->anniversary_email_template();
 		echo $this->smtp_email->send($sender_email, $sender_name, $receiver_email, $message_subject, $message_body);
   	}
	
	public function PostSummaryEmail()
	{		 
		$this->load->library("Smtp_email");	 
		$this->load->library("Emailtemp");	
  		$sender_email = "smtptesting@goalwebs.com";
		$sender_name = "SafiriSalama Memorials";
		$receiver_email = "charli007111@gmail.com";
		$message_subject = "post summary email";
		$message_body = $this->emailtemp->post_summary_email_template();
		
		$this->smtp_email->send($sender_email, $sender_name, $receiver_email, $message_subject, $message_body);
 	}
	
	public function SuggestionEmail()
	{		 
		$this->load->library("Smtp_email");	 
		$this->load->library("Emailtemp");
 		$sender_email = "smtptesting@goalwebs.com";
		$sender_name = "SafiriSalama Memorials";
		$receiver_email = "james.noah1172@gmail.com";
		$message_subject = "Suggestion Email";
		$message_body = $this->emailtemp->suggestion_email_template();
		
		$this->smtp_email->send($sender_email, $sender_name, $receiver_email, $message_subject, $message_body);
 	}
}
