<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct(){
			parent::__construct();
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			$this->login_email = ""; 
			if($this->session->userdata('user'))
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				$this->login_email = $sess_data->email; 
			}      
	}

	public function index()
	{		
		$this->data['title'] = "Profile";
		$this->data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row();
		$this->data['countries'] = $this->db->query("select * from countries")->result();
		$this->data['profile_percentage'] = $this->profile_percentage();
		$this->load->view("profile", $this->data); 
        
	}
	
	public function profile_percentage(){
		
		$user = $this->db->query("select * from users where ID = ".$this->ID)->row();
		$percentage = 30;
		
		if(!empty($user->username)){
			$percentage = $percentage + 10;
		}
		if(!empty($user->pic)){
			$percentage = $percentage + 10;
		}
		if(!empty($user->phone)){
			$percentage = $percentage + 10;
		}
		if(!empty($user->gender)){
			$percentage = $percentage + 10;
		}
		if($user->birthday != '0000-00-00'){
			$percentage = $percentage + 10;
		}
		if(!empty($user->town_you_live_in)){
			$percentage = $percentage + 10;
		}
		if(!empty($user->country_you_live_in)){
			$percentage = $percentage + 10;
		}
		
		return $percentage;
	}
	
	public function update(){
		
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		//$this->form_validation->set_rules('username', 'username', 'required');
		
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('profile');
			}
		  else
			{	
				 $id = $this->input->post('id');
			     $data['fname'] = $this->input->post('fname');
			     $data['lname'] = $this->input->post('lname');
				 $data['username'] = $this->input->post('username');
				 
				 $data['phone'] = $this->input->post('phone');
				 $data['gender'] = $this->input->post('gender');
				 $data['birthday'] = $this->input->post('birthday');
				 $data['town_you_live_in'] = $this->input->post('town_you_live_in');
				 $data['country_you_live_in'] = $this->input->post('country_you_live_in');
				 $data['county'] = $this->input->post('county');
				 
				 $password = $this->input->post('password');
				 $cpassword = $this->input->post('cpassword');
				 
				 if(strlen($password) > 0){
					 
					 if(strlen($password) < 5){
						 $this->session->set_flashdata('error', "Password must be atleast 6 characters");    
						 redirect('profile');
					 }elseif($password != $cpassword){
						 $this->session->set_flashdata('error', "Password does not match");    
						 redirect('profile');
					 }else{
						 $data['password'] = md5($this->input->post('password'));
					 }
				 }
				 
               /////////////////////
				
				
                $config =  array(
                  'upload_path'     => './uploads/',
                  'allowed_types'   => "gif|jpg|png|jpeg",
                  'max_size'        => "0",
                  'max_height'      => "1768",
                  'max_width'       => "2048"  
                );
					
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload('UserPhoto'))
				{
					$filed = $this->upload->data();
					$data['pic'] = $filed["file_name"];	
				}
				else
				{
					
				}
				
				///////////////////////////			 
          
				$this->db->where("ID", $id)->update("users", $data);
         	    $this->session->set_flashdata('success', "Profile has been updated successfully");    
				redirect('profile');
				 
			}
		
	}
			
}
