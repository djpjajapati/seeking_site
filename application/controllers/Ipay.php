<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ipay extends CI_Controller {

	function __construct(){
			parent::__construct();	
			$this->load->library('smtp_email');
			
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			$this->email = ""; 
			if(isset($sess_data->ID))
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				$this->email = $sess_data->email; 
			}  
	}

	public function index()
	{
		   $data['pakages'] = $this->db->query("select * from payment_plan")->result();
		   $data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row(); 
		   $this->load->view('pay_option', $data);
	}
	
	public function callback(){
		
		$resp = $this->paytabs->verify_payment($_POST['payment_reference']);
		
		if($resp->response_code == '100'){
			$data['result'] = $resp->result;
			$data['response_code'] = $resp->response_code;
			$data['pt_invoice_id'] = $resp->pt_invoice_id;
			$data['amount'] = $resp->amount;
			$data['currency'] = $resp->currency;
			$data['reference_no'] = $resp->reference_no;
			$data['transaction_id'] = $resp->transaction_id;
			$this->db->insert("paytabs_response", $data);
			
			echo "RESPONSE:  <pre>"; print_r($data);
			echo "Request data:  <pre>"; print_r($_SESSION['paytabs_request_data']);
			
		}else{
			echo "Request Fail: ";
			echo $resp->result;
		}
	}
	
}
