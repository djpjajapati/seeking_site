<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Notifications extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library("Smtp_email");	 
		$this->load->library("Emailtemp");
 	}
	public function index()
	{
		redirect('cron/notifications/cron_invoice');
   	}
	
	public function cron_invoice() 
	{
		
		$from=$this->config->config['admin_email'];
		if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
		
		// send email for pending memorials
		$onedaysago = date('Y-m-d', strtotime("-1 day"));
		$threedaysago = date('Y-m-d', strtotime("-3 day"));
		$sevendaysago = date('Y-m-d', strtotime("-7 day"));
		
		$pending_memorials = $this->db->query("select * from pending_memorials where date(createdAt) = '$onedaysago' or date(createdAt) = '$threedaysago' or date(createdAt) = '$sevendaysago'")->result();
		
		foreach($pending_memorials as $memo){
			
			$usr = $this->db->query("select fname, lname, email from users where ID = ".$memo->createdby)->row();
			
			$to=$usr->email;
			$to_name = $usr->fname.' '.$usr->lname;
			$memorial_name = $memo->fname.' '.$memo->lname;
			$vlink = base_url().'memorial/create_memorial';
			$content = 'You have recently tried to create memorial for '.$memorial_name.' To continue with your memorial follow the <a target="_blank" href="'.$vlink.'"><b>link</b></a> ';
			
			//$message_body=$row->details."Auto Invoice resend triggered. Will be activated after confirmation of invoice email content.";
			$from_name="Safiri Salama - Invoice";
			$subject="Memorial Reminder";
			$message_body = $this->emailtemp->memorial_reminder_template($content, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			
		}		
		// End email for pending memorials
				
		$onedaybefore = date('Y-m-d', strtotime("+1 day"));
		$oneweekbefore = date('Y-m-d', strtotime("+7 day"));
		$onemonthbefore = date('Y-m-d', strtotime("+30 day"));
				
		// if aniversaries
		$aniversaries_memo = $this->db->query("select * from memorials where deathdate = '$onedaybefore'")->result();
		foreach($aniversaries_memo as $memo)
		{
			$birthdate = new DateTime($memo->birthdate);
			$to = new DateTime('today');
			$ann_year = $birthdate->diff($to)->y;
			$ann_year = $this->ordinal($ann_year);
			
			$usr = $this->db->query("select fname, lname, email from users where ID = ".$memo->createdby)->row();
			$to=$usr->email; //'davidmark0772@gmail.com';
			$to_name = $usr->fname.' '.$usr->lname;
			$memorial_name = $memo->fname.' '.$memo->lname;
			$vlink = base_url().'memorial/tribute/'.$memo->webaddress;
						
			$content = '<p style="color: #582236" class="descr">Tomorrow will be the '.$ann_year.' anniversary of '.$memorial_name.'. You can remember '.$memorial_name.' by <b>leaving a new tribute, adding a new photo, or sharing a recently-remmebered story</b> in his memory.</p>';
			$from_name="Safiri Salama - Invoice";
			$subject="Anniversary Reminder";
			$message_body = $this->emailtemp->anniversary_email_template($content, $to_name, $vlink);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		}
		// end
		
		
		
		
		// If expiring in one month
		$foundrenewableonemonth = $this->db->query("select * from allinvoices where recursiveat = '$onemonthbefore'")->result();
		foreach($foundrenewableonemonth as $row)
		{
			$usr = $this->db->query("select fname, lname from users where ID = ".$row->userid)->row();
			$memo = $this->db->query("select fname, lname from memorials where ID = ".$row->memorialid)->row();
			
			$to=$row->useremail;
			$to_name = $usr->fname.' '.$usr->lname;
			$memorial_name = $memo->fname.' '.$memo->lname;
			$vlink = base_url().'memorial/manage_memorial/'.$row->memorialid;
			$invoice_link = base_url().'memorial/invoice/'.$row->id.'?pay='.$row->memorialid;
			$content = 'Your subscription for memorial '.$memorial_name.' is expiring within one month, Click <a target="_blank" href="'.$vlink.'"><b>Here</b></a> to renew your subscription pakage, Click <a target="_blank" href="'.$invoice_link.'"><b>Invoice</b></a> to view your invoice';
			
			//$message_body=$row->details."Auto Invoice resend triggered. Will be activated after confirmation of invoice email content.";
			$from_name="Safiri Salama - Invoice";
			$subject="Auto invoice triggered";
			$message_body = $this->emailtemp->payment_renewal_template($content, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		}
		// end
		
		
		// If expiring in one week
		$foundrenewableoneweek = $this->db->query("select * from allinvoices where recursiveat = '$oneweekbefore'")->result();
		foreach($foundrenewableoneweek as $row)
		{
			$usr = $this->db->query("select fname, lname from users where ID = ".$row->userid)->row();
			$memo = $this->db->query("select fname, lname from memorials where ID = ".$row->memorialid)->row();
			
			$to=$row->useremail;
			$to_name = $usr->fname.' '.$usr->lname;
			$memorial_name = $memo->fname.' '.$memo->lname;
			$vlink = base_url().'memorial/manage_memorial/'.$row->memorialid;
			$invoice_link = base_url().'memorial/invoice/'.$row->id.'?pay='.$row->memorialid;
			$content = 'Your subscription for memorial '.$memorial_name.' is expiring within one week, Click <a target="_blank" href="'.$vlink.'"><b>Here</b></a> to renew your subscription pakage, Click <a target="_blank" href="'.$invoice_link.'"><b>Invoice</b></a> to view your invoice';
			
			//$message_body=$row->details."Auto Invoice resend triggered. Will be activated after confirmation of invoice email content.";
			$from_name="Safiri Salama - Invoice";
			$subject="Auto invoice triggered";
			$message_body = $this->emailtemp->payment_renewal_template($content, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		}
		// end
		
		
		// If expiring today
		$whatisdatetoday=strtotime(date('Y-m-d'));
		$foundrenewable = $this->db->query("select * from allinvoices where recursiveunix = ".$whatisdatetoday)->result();
		foreach($foundrenewable as $row)
		{
			
			$usr = $this->db->query("select fname, lname from users where ID = ".$row->userid)->row();
			$memo = $this->db->query("select fname, lname from memorials where ID = ".$row->memorialid)->row();
			
			$to=$row->useremail;
			$to_name = $usr->fname.' '.$usr->lname;
			$memorial_name = $memo->fname.' '.$memo->lname;
			$vlink = base_url().'memorial/manage_memorial/'.$row->memorialid;
			$invoice_link = base_url().'memorial/invoice/'.$row->id.'?pay='.$row->memorialid;
			$content = 'Your subscription for memorial '.$memorial_name.' is expiring today, Click <a target="_blank" href="'.$vlink.'"><b>Here</b></a> to renew your subscription pakage, Click <a target="_blank" href="'.$invoice_link.'"><b>Invoice</b></a> to view your invoice';
			
			//$message_body=$row->details."Auto Invoice resend triggered. Will be activated after confirmation of invoice email content.";
			$from_name="Safiri Salama - Invoice";
			$subject="Auto invoice triggered";
			
			
			$message_body = $this->emailtemp->payment_renewal_template($content, $to_name);
			
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			
			// will be updated from site
			//$invoiceparams=array("userid"=>$row->userid,"useremail"=>$row->useremail, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s',strtotime($row->willexpireat)), "recursiveat"=>date('Y-m-d', strtotime($row->willexpireat.'+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime($row->willexpireat.'+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$row->details,"subtotal"=>$row->nextprice,"discount"=>0,"totalprice"=>$row->nextprice,"nextprice"=>$row->nextprice,"status"=>"payable", "memorialid"=>$row->memorialid);
			//$this->db->where('id', $row->id)->update("allinvoices", $invoiceparams);  // update that invoice
			//$newinvoiceid=$this->createaninvoice($invoiceparams);
		}	
	}
	
	public function ordinal($number) {
		
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if((($number % 100) >= 11) && (($number%100) <= 13))
			return $number. 'th';
		else
			return $number. $ends[$number % 10];
	}
}
