<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {

	function __construct(){
			parent::__construct();		
			$this->load->library('session'); 
			$this->load->helper('xml'); 
			
			
			// initialize
			//$this->db_settings = $a_db;
			
			// RSS channel properties
			$a_channel = array(
			  "title" => "pontikis.net",
			  "link" => "http://www.pontikis.net",
			  "description" => "Tech blog & web labs",
			  "language" => "en",  // optional
			  "image_title" => "pontikis.net", // optional
			  "image_link" => "http://www.pontikis.net", // optional
			  "image_url" => "http://www.pontikis.net/feed/rss.png" // optional
			);
			
			
			$this->xmlns = ($xmlns ? ' ' . $xmlns : '');
			$this->channel_properties = $a_channel;
			$this->site_url = 'https://safirisalama.com';
			$this->site_name = 'www.safarisalama.com';
			$this->full_feed = 'https://safirisalama.com/feed';
			
	}

	public function index()
	{
		
			$data['encoding'] = 'utf-8';    
			$data['feed_name'] = 'www.safarisalama.com';        
			$data['feed_url'] = 'https://safirisalama.com/feed';        
			$data['page_description'] = 'Welcome to www.safarisalama.com feed url page';       
			$data['page_language'] = 'en-ca';        
			$data['creator_email'] = 'safarisalama@gmail.com';        
			
			$latest_memorials = $this->db->query("select * from memorials order by ID desc limit 10")->result();
			
			foreach($latest_memorials as $memo){
				
				$day = date('d', strtotime($memo->createdAt));
				$memo->created = $this->ordinal($day).' '.date('M', strtotime($memo->createdAt)).' '.date('Y', strtotime($memo->createdAt));
				
			}
			
			//echo "<pre>";
			//print_r($latest_memorials);
			//exit;
			
			$data['latest_memorials'] = $latest_memorials;
			
			header("Content-Type: application/rss+xml");      
			$this->load->view('rss', $data);
	}
	
	public function create_feed() {
 
			$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
		 
			$xml .= '<rss version="2.0"' . $this->xmlns . '>' . "\n";
		 
			// channel required properties
			$xml .= '<channel>' . "\n";
			$xml .= '<title>' . $this->channel_properties["title"] . '</title>' . "\n";
			$xml .= '<link>' . $this->channel_properties["link"] . '</link>' . "\n";
			$xml .= '<description>' . $this->channel_properties["description"] . '</description>' . "\n";
		 
			// channel optional properties
			if(array_key_exists("language", $this->channel_properties)) {
			  $xml .= '<language>' . $this->channel_properties["language"] . '</language>' . "\n";
			}
			if(array_key_exists("image_title", $this->channel_properties)) {
			  $xml .= '<image>' . "\n";
			  $xml .= '<title>' . $this->channel_properties["image_title"] . '</title>' . "\n";
			  $xml .= '<link>' . $this->channel_properties["image_link"] . '</link>' . "\n";
			  $xml .= '<url>' . $this->channel_properties["image_url"] . '</url>' . "\n";
			  $xml .= '</image>' . "\n";
			}
		 
			// get RSS channel items
			$now =  date("YmdHis"); // get current time  // configure appropriately to your environment
			//$rss_items = $this->get_feed_items($now);
			$rss_items = $this->db->query("select * from memorials order by ID desc limit 10")->result();
		 
			foreach($rss_items as $rss_item) {
			  $xml .= '<item>' . "\n";
			  $xml .= '<title>' . $rss_item->fname.' '.$rss_item->lname . '</title>' . "\n";
			  $xml .= '<link>' . $rss_item->webaddress . '</link>' . "\n";
			  $xml .= '<description>' . $rss_item->about . '</description>' . "\n";
			  $xml .= '<pubDate></pubDate>' . "\n";
			  $xml .= '<category></category>' . "\n";
			  $xml .= '<source></source>' . "\n";
		 
			  if($this->full_feed) {
				$xml .= '<content:encoded>' . $rss_item->description . '</content:encoded>' . "\n";
			  }
		 
			  $xml .= '</item>' . "\n";
			}
		 
			$xml .= '</channel>';
		 
			$xml .= '</rss>';
		 
			echo $xml;
  }
  
  public function test(){
	  
	  $xml = new SimpleXMLElement('<xml/>');

	  $latest_memorials = $this->db->query("select * from memorials order by ID desc limit 10")->result();
	
	  foreach($latest_memorials as $memo){
		  
		  $title='<a href="'.base_url().'memorial/tribute/'.$memo->webaddress.'">'.$memo->fname.' '.$memo->lname.'</a>';
		  
		  $day = date('d', strtotime($memo->createdAt));
		  $memo->created = $this->ordinal($day).' '.date('M', strtotime($memo->createdAt)).' '.date('Y', strtotime($memo->createdAt));
		  
		  
		  $track = $xml->addChild('track');
		  $track->addChild('title', $title);
		  $track->addChild('desc', $memo->about);
		  $track->addChild('created', $memo->created);
		  
		  
	  }
		
	  Header('Content-type: text/xml');
	  print($xml->asXML());
	  
  }
	
  public function ordinal($number) {
		
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if((($number % 100) >= 11) && (($number%100) <= 13))
			return $number. 'th';
		else
			return $number. $ends[$number % 10];
  }
	
}
