<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class In_Memory_Of extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library('image_lib');
			
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			$this->email = ""; 
			if(isset($sess_data->username))
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				$this->email = $sess_data->email; 
			}      
	}

	public function index($webaddress){
		
		
		$memo = $this->db->query("select * from memorials where webaddress = '$webaddress'")->row();
		$memo->background = $this->db->query("select background from memorial_templates where ID = ".$memo->memorial_template)->row()->background;
		$this->data['memo'] = $memo;
		
		$this->data['title'] = $memo->fname.' '.$memo->lname;
		
		$this->data['pictures'] = $this->db->query("select * from memo_pic where memo_id = '$memo->ID' order by ID desc")->result();
		$this->data['videos'] = $this->db->query("select * from memo_video where memo_id = '$memo->ID' order by ID desc")->result();
		$this->data['audios'] = $this->db->query("select * from memo_audio where memo_id = '$memo->ID' order by ID desc")->result();
		
		$notice = $this->db->query("select * from memo_notices where memo_id = '$memo->ID'")->row();
		if($notice){
			$res1 = $this->db->query("select fname, lname from users where ID = ".$notice->updatedBy)->row();
			$notice->updated_by = $res1->fname.' '.$res1->lname;
			$notice->timeago=$this->get_timeago(strtotime($notice->updatedAt));
		}
		
		$this->data['notice'] = $notice;
		$tributes = $this->db->query("select * from memo_tribute where memo_id = '$memo->ID' and status = 'Approved'")->result();
		foreach($tributes as $t){
			$res = $this->db->query("select ID, fname, lname from users where ID = ".$t->createdby)->row();
			$t->by = $res->fname.' '.$res->lname;
			
			if($t->action == 'candle'){
				$t->d_action = 'light a Candle';
			}elseif($t->action == 'tribute'){
				$t->d_action = 'left a Tribute';
			}else{
				$t->d_action = 'left a Note';
			}
		}
		
		$this->data['is_page_admin'] = false; //$this->is_page_admin($memo->ID, $this->ID);
		
		//echo $this->data['is_page_admin']; exit;
		
		$tributes_activity = $this->db->query("select * from memo_tribute where memo_id = '$memo->ID' order by ID desc limit 5")->result();
		foreach($tributes_activity as $t){
			$res = $this->db->query("select ID, fname, lname from users where ID = ".$t->createdby)->row();
			$t->by = $res->fname.' '.$res->lname;
			$t->timeago=$this->get_timeago(strtotime($t->createdAt));
			if($t->action == 'candle'){
				$t->d_action = 'light a Candle';
			}elseif($t->action == 'tribute'){
				$t->d_action = 'left a Tribute';
			}else{
				$t->d_action = 'left a Note';
			}
		}
		
		$this->data['tributes'] = $tributes;
		$this->data['tributes_activity'] = $tributes_activity;
		
		$this->load->view("tribute_page", $this->data);
		
	}
	function get_timeago($ptime){
		
		$estimate_time = time() - $ptime;
		if( $estimate_time < 1 ){
			return 'less than 1 second ago';
		}
		$condition = array( 
					12 * 30 * 24 * 60 * 60  =>  'year',
					30 * 24 * 60 * 60       =>  'month',
					24 * 60 * 60            =>  'day',
					60 * 60                 =>  'hour',
					60                      =>  'minute',
					1                       =>  'second'
		);

		foreach( $condition as $secs => $str )
		{
			$d = $estimate_time / $secs;
			if( $d >= 1 ){
				$r = round( $d );
				return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
			}
		}
	}
	
	/* public function is_page_admin($memo_id, $user_id){
			
		$cnt = $this->db->query("select count(*) as cnt from memorials where ID='$memo_id' and createdby = '$user_id'")->row()->cnt;
		if($cnt > 0){
			return true;
			exit;
		}else{
			$cnt = $this->db->query("select count(*) as cnt from memorial_admin where memo_id = '$memo_id' and user_id = '$user_id'")->row()->cnt;
			if($cnt > 0){
				return true;
				exit;
			}else{
				return false;
				exit;
			}
		}		
	} */
	
	public function is_page_super_admin($memo_id, $user_id){
			
		$cnt = $this->db->query("select count(*) as cnt from memorials where ID='$memo_id' and createdby = '$user_id'")->row()->cnt;
		if($cnt > 0){
			return true;
			exit;
		}else{
			return false;
			exit;
		}
	}
		
}
