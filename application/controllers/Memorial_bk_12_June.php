<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Memorial extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library("Smtp_email");	 
			$this->load->library("Emailtemp");
 					
			$this->load->library('session'); 
			$this->load->helper('form'); 
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			//$this->email = ""; 
			//if(isset($sess_data->ID))
			if($this->session->userdata('user')) 
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				//$this->email = $sess_data->email; 
			} 

			$memorials = $this->db->query("select * from memorials")->result();
			
			foreach($memorials as $m){
				if($m->package_end_date < date('Y-m-d')){
					//echo $m->ID.'<br>';
					$this->db->where("ID", $m->ID)->update("memorials", array("status"=>"Inactive"));
				}
			}

	}

	public function index()
	{	
	
		$this->data['title'] = "Memorials | Safiri Salama";
		$this->data['meta_description'] = "Memorial description | Safiri Salama";
		$this->data['meta_keywords'] = "Memo keywords";
		$this->data['last_name'] = "";
		$this->data['memorials'] = $this->db->query("select * from memorials where status='Active' and visibility = 'public' order by ID desc limit 12")->result();
        $this->data['total'] = $this->db->query("select count(*) as cnt from memorials where status='Active' and visibility = 'public'")->row()->cnt;
		//echo "<pre>"; print_r($this->data['memorials']); exit;
		$this->load->view("memorial", $this->data);
        
	}
	
	public function search_memorials(){
		
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$yborn = $this->input->post('yborn');
		$ydeath = $this->input->post('ydeath');
		$gender = $this->input->post('gender');
		
		$condition='';
		
		if(!empty($fname)){
			$condition = " AND fname like '%$fname%' ";
		}
		
		if(!empty($lname)){
			$condition = " AND lname like '%$lname%' ";
		}
		
		
		if(!empty($yborn)){
			$condition.=" AND YEAR(birthdate) = ".$yborn;
		}
		
		if(!empty($ydeath)){
			$condition.=" AND YEAR(deathdate) = ".$ydeath;
		}
		
		if(!empty($gender)){
			$condition.=" AND gender = '$gender'";
		}
		
		//$memorials = $this->db->query("select * from memorials where status='Active' and visibility = 'public' ".$condition." order by ID desc limit ".$limit)->result();
        $memorials = $this->db->query("select * from memorials where status='Active' and visibility = 'public' ".$condition." order by ID desc")->result();
		//print_r($this->db->last_query());  exit;
		
		if(count($memorials) > 0){ foreach($memorials as $memorial){ 
					$full_name = $memorial->fname.' '.$memorial->lname;
		?>
				
			<div class="col-lg-3 col-md-4 mt-3">
				<div class="mem_box" style="cursor: pointer;" onClick="view_detail('<?php echo $memorial->webaddress; ?>')">
					
					<div class="media" height="88%">
						
					<img width="150" height="150" src="<?php echo base_url(); ?>uploads/<?php echo $memorial->cropped_image; ?>" class="mr-3 wp-post-image" alt="">								
					<div class="media-body">
					   <h4><a class="elipses" href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>"><?php if(strlen($full_name) > 11) echo substr($full_name, 0, 11); else echo $full_name; ?></a></h4>
						<p><?php echo date('Y', strtotime($memorial->birthdate)); ?>-<?php echo date('Y', strtotime($memorial->deathdate)); ?></p>
					  </div>
						
					</div>
					
					 <a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>" class="memorial_link">View Memorial</a>
				</div>
			</div>
				
		<?php } }else{ ?>
		
						<h3 style="margin-left: 50px;">No Records Found</h3>
		
		<?php } ?>
			
			<div class="clearfix"></div>   
			
		<?php 
		
	}
	
	public function invoice($theid){
		
		$invoicedata=$this->db->query("select * from allinvoices where id = ".$theid)->row();
		//echo "<pre>"; print_r($invoicedata); exit;
		if($invoicedata){
			//$memo_id=$invoicedata->userid;
			$memo_id=$invoicedata->memorialid;
			
			
			/* $cnt = $this->db->query("select count(*) as cnt from allinvoices where memorialid = '$memo_id'")->row()->cnt;
			if($cnt == 1){
				$action = 'Create';
			}else{
				
			} */

			$this->data['invoicedata'] =$invoicedata;
			$this->data['title'] = "Invoice";
			$memo = $this->db->query("select * from memorials where ID = '$memo_id'")->row();
			$memo->pakage_price = $this->db->query("select price from payment_plan where title = '$memo->pakage'")->row()->price;
			//echo "<pre>"; print_r($memo); exit;
			
			$this->data['memo'] = $memo;
			$this->data['user'] = $this->db->query("select * from users where ID = '$memo->createdby'")->row();
			$this->data['payment'] = $this->db->query("select * from ipay_response where memo_id = '$memo->ID' order by ID desc")->row();
						
			$this->load->view('invoice_template', $this->data); 
		}else{
			// same link from admin 
			redirect($_SERVER['HTTP_REFERER']);
			redirect('admin/manage/payments');
		}
		
		
	}
	
	public function search()
	{		
		$this->data['title'] = "Memorials | Safiri Salama"; 
		$this->data['description'] = "Memorials | Safiri Salama"; 
		$last_name = $this->input->post('last_name');
		$last_name = str_replace("'", "", $last_name);
		$this->data['last_name'] = $last_name;
		$this->data['memorials'] = $this->db->query("select * from memorials where status='Active' and visibility = 'public' and (lname like '%$last_name%' or fname like '%$last_name%') order by ID desc limit 12")->result();
        $this->data['total'] = $this->db->query("select count(*) as cnt from memorials where status='Active' and visibility = 'public' and (lname like '%$last_name%' or fname like '%$last_name%') ")->row()->cnt;
		$this->load->view("memorial", $this->data);
        
	}
	
	public function mymemorials(){
		//echo "my memorial goes here"; exit; 
		$this->data['title'] = "My Memorials | Safiri Salama";
		$this->data['description'] = "My Memorials | Safiri Salama";
		$my_memorials = $this->db->query("select * from memorials where createdby = '$this->ID' order by ID desc")->result();
		
		$id="";
		if(count($my_memorials) > 0){
			$ids = ' and memorial_admin.memo_id NOT IN(';
			foreach($my_memorials as $m){
				$ids.=$m->ID.',';
			}
			
			$ids = rtrim($ids,',');
			$ids.=') ';
		}
		
		
		
		$this->data['my_memorials1'] = $this->db->query("select * from memorial_admin join memorials where memorial_admin.user_id = '$this->ID' and memorial_admin.memo_id = memorials.ID ".$ids." order by memorials.ID desc")->result();
		//echo $this->db->last_query();
		//echo "<pre>"; print_r($this->data['my_memorials1']); exit;
		
		$this->data['my_memorials'] = $my_memorials;
		$this->data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row();
		//echo "<pre>"; print_r($this->data['user']); 
		//exit;
		$this->load->view("manage_multipal_memorial", $this->data);
		
	}
	
	public function get_unique_address(){
		
		//$fname = ucfirst($this->input->post('fname'));
		$fname = strtolower($this->input->post('fname'));
		$lname = strtolower($this->input->post('lname'));
		$mname = strtolower($this->input->post('mname'));
		
		$webaddress = $fname.'-'.$lname;
		if($mname != ''){
			$webaddress = $fname.'-'.$mname.'-'.$lname;
		}
		
		$cnt = $this->db->query("select count(*) as cnt from memorials where webaddress = '$webaddress'")->row()->cnt;
		if($cnt > 0){
			$webaddress = $webaddress.rand(10,1000); 
		}
		
		$webaddress = str_replace(' ','',$webaddress);
		echo trim($webaddress);
	}
	
	public function block_user(){
		
		$tribute_id = $this->input->post('tribute_id');
		$memo_id = $this->input->post('memo_id');
		$user_id = $this->input->post('user_id');
		
		$this->db->where("memo_id", $memo_id);
		$this->db->where("user_id", $user_id);
		$this->db->delete('memorial_admin');
		
		$this->db->where("ID", $tribute_id)->delete('memo_tribute');
		
		$this->db->insert("block_users", array("memo_id"=>$memo_id, "user_id"=>$user_id, "block_by"=>$this->ID));
		
		echo "success";
		
	}
	
	public function update_status($id, $status){
		
		$this->db->where("ID", $id)->update("memorials", array("status"=>$status)); //query("update update_status")
		redirect('memorial/mymemorials');
		
	}
	public function markinappropriate(){
		
		$memo_id = $this->input->post('memo_id');
		$memo = $this->db->query("select * from memorials where ID = '$memo_id'")->row();
		
		$ip = $this->input->ip_address();
		$id = $this->input->post('id');
		if($id!=$this->session->userdata('inappID'))
		{
			$this->db->where('ID', $id);
			$this->db->set('flaggedcontent', 'flaggedcontent+1', FALSE);
			$this->db->update('memo_tribute');
			echo "Thank you. Your request has been submitted.";
			$this->session->set_userdata('inappIP', $ip);
			$this->session->set_userdata('inappID', $id);
			
			
			// send email to page owner				
			$from=$this->config->config['admin_email'];
			if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
			$from_name="SafiriSalama Memorials";
			$subject="Inapropriate Tribute";
			$vlink = base_url().'memorial/tribute/'.$memo->webaddress;
			
			// send to page admin
			$page_admin = $this->db->query("select fname, lname, email from users where ID = ".$memo->createdby)->row();
			
			$to=$page_admin->email;				
			$to_name=$page_admin->fname.' '.$page_admin->lname;				
			
			$content='<p> A tribute has been marked as inapropriate against the memorial "'.$memo->fname.' '.$memo->lname.'" click <a href="'.$vlink.'">link</a> to view detail.</br>';
			$title = 'Inapropriate Tribute';
			
			$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
			//echo $message_body; exit;
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end
			
		}
		else
		{
			echo "Error";
			
		}
		
	}
	public function updatemytribute()
	{
		$id = $this->input->post('id');
		$newtribute = $this->input->post('thetribute');
		$newtribute=$this->profanitytest($newtribute,"altered","tributes",0,$this->ID);
		$this->db->where("ID", $id)->update("memo_tribute", array("comment"=>$newtribute)); 
		echo $newtribute;

	}
	public function deletemytribute()
	{
		$id = $this->input->post('id');
		$adderid = $this->input->post('adderid');
		$this->db->where("ID", $id)->delete('memo_tribute');
	}
	public function tribute($webaddress){
		
		
		$memo = $this->db->query("select * from memorials where webaddress = '$webaddress'")->row();
		
		if($memo->status == 'Inactive'){
			redirect('memorial');
		}
		
		$memo->background = $this->db->query("select background from memorial_templates where ID = ".$memo->memorial_template)->row()->background;
		
		$this->data['photo_limit'] = $this->is_expired($memo->ID, 'photo_limit');
		$this->data['video_limit'] = $this->is_expired($memo->ID, 'video_limit');
		$this->data['audio_limit'] = $this->is_expired($memo->ID, 'audio_limit');
		
		$this->data['memo'] = $memo;
		
		$this->data['title'] = 'In the Memory of '.$memo->fname.' '.$memo->lname.'. Share memories of '.$memo->fname.' '.$memo->lname.' on Safiri Salama | Safirisalama.com';
		$this->data['meta_description'] = $memo->about;
		$this->data['meta_keywords'] = $memo->webaddress;
		
		$this->data['pictures'] = $this->db->query("select * from memo_pic where memo_id = '$memo->ID' order by ID desc")->result();
		$this->data['videos'] = $this->db->query("select * from memo_video where memo_id = '$memo->ID' order by ID desc")->result();
		$this->data['audios'] = $this->db->query("select * from memo_audio where memo_id = '$memo->ID' order by ID desc")->result();
		
		$notice = $this->db->query("select * from memo_notices where memo_id = '$memo->ID'")->row();
		if($notice){
			$res1 = $this->db->query("select fname, lname from users where ID = ".$notice->updatedBy)->row();
			$notice->updated_by = $res1->fname.' '.$res1->lname;
			$notice->timeago=$this->get_timeago(strtotime($notice->updatedAt));
		}
		
		$this->data['notice'] = $notice;
		$tributes = $this->db->query("select * from memo_tribute where memo_id = '$memo->ID' and status = 'Approved' order by ID desc")->result();
		foreach($tributes as $t){
			$res = $this->db->query("select ID, fname, lname from users where ID = ".$t->createdby)->row();
			$t->by = $res->fname.' '.$res->lname;
			
			if($t->action == 'candle'){
				$t->d_action = 'lit a candle';
			}elseif($t->action == 'tribute'){
				$t->d_action = 'laid a flower';
			}else{
				$t->d_action = 'left a note';
			}
		}
		
		$this->data['is_page_admin'] = $this->is_page_admin($memo->ID, $this->ID);
		$this->data['is_page_super_admin'] = $this->is_page_super_admin($memo->ID, $this->ID);
		
		if($this->is_page_admin($memo->ID, $this->ID)){
			//echo $memo->ID
			$this->db->where("memo_id", $memo->ID)->update("memo_tribute", array("notification_status"=>1)); 
			//exit;
		}
		
		
		$this->data['notification'] = $this->db->query("select * from memo_tribute where memo_id = '$memo->ID' and profanity_status = 1 and notification_status=0 order by ID desc limit 5")->result();
		
		//echo $this->data['is_page_admin']; exit;
		
		$tributes_activity = $this->db->query("select * from memo_tribute where memo_id = '$memo->ID' order by ID desc limit 5")->result();
		foreach($tributes_activity as $t){
			$res = $this->db->query("select ID, fname, lname from users where ID = ".$t->createdby)->row();
			$t->by = $res->fname.' '.$res->lname;
			$t->timeago=$this->get_timeago(strtotime($t->createdAt));
			if($t->action == 'candle'){
				$t->d_action = 'lit a candle';
			}elseif($t->action == 'tribute'){
				$t->d_action = 'laid a flower';
			}else{
				$t->d_action = 'left a note';
			}
		}
		
		$this->data['tributes'] = $tributes;
		$this->data['tributes_activity'] = $tributes_activity;
		
		$this->load->view("tribute_page", $this->data);
		
	}
	
	public function report_memo(){
		
		//$this->session->unset_userdata('inappID1');
		//$this->session->unset_userdata('inappIP1');
		
		$id = $this->input->post('memo_id');
		$report_desc = $this->input->post('report_desc'); 
		
		
		$ip = $this->input->ip_address();
		
		if($id!=$this->session->userdata('inappID1'))
		{
			$data['message'] = $report_desc;
			$data['memo_id'] = $id;
			$data['user_id'] = $this->ID;
			$data['createdAt'] = date('Y-m-d H:i:s');
			
			$this->db->insert("report_memorials", $data);
			
			echo "Thank you. Your request has been submitted.";
			$this->session->set_userdata('inappIP1', $ip);
			$this->session->set_userdata('inappID1', $id);
		}
		else
		{
			echo "Error";
			
		}
		
		
	}
	
	public function is_expired($memo_id, $name_limit){
		
		$pakage = $this->db->query("select pakage from memorials where ID = ".$memo_id)->row()->pakage;
		$limit = $this->db->query("select ".$name_limit." as lmt from payment_plan where title = 'BASIC'")->row()->lmt;
		if($pakage != 'BASIC'){
			$limit = $this->db->query("select ".$name_limit." as lmt from payment_plan where title = 'PREMIUM'")->row()->lmt;
		}
		
		$added_limit = 0;
		
		if($name_limit == 'photo_limit'){
			$added_limit = $this->db->query("select count(*) as cnt from memo_pic where memo_id = ".$memo_id)->row()->cnt;
		}elseif($name_limit == 'video_limit'){
			$added_limit = $this->db->query("select count(*) as cnt from memo_video where memo_id = ".$memo_id)->row()->cnt;
		}elseif($name_limit == 'audio_limit'){
			$added_limit = $this->db->query("select count(*) as cnt from memo_audio where memo_id = ".$memo_id)->row()->cnt;
		}elseif($name_limit == 'page_managers'){
			$added_limit = $this->db->query("select count(*) as cnt from memorial_admin where memo_id = ".$memo_id)->row()->cnt;
		}
		
		if($added_limit >= $limit){
			return true;
			exit;
		}else{
			return false;
			exit;
		}
	}
	
	public function update_notices(){
		
		$memo_id = $this->input->post('memo_id');
		
		$data['notice_for'] = $this->input->post('notice_for');
		$data['dob'] = $this->input->post('dob');
		$data['dod'] = $this->input->post('dod');
		$data['house_of_worship'] = $this->input->post('house_of_worship');
		$data['country'] = $this->input->post('country');
		$data['town'] = $this->input->post('town');
		$data['cremation_detail'] = $this->input->post('cremation_detail');
		$data['contact_person'] = $this->input->post('contact_person');
		$data['meeting_venue'] = $this->input->post('meeting_venue');
		$data['google_map_loc'] = $this->input->post('google_map_loc');
		$data['updatedAt'] = date('Y-m-d H:i:s'); 
		$data['updatedBy'] = $this->ID; 
		
		$cnt = $this->db->query("select count(*) as cnt from memo_notices where memo_id = ".$memo_id)->row()->cnt;
		if($cnt > 0){
			$this->db->where("memo_id", $memo_id)->update("memo_notices", $data);			
		}else{
			$data['memo_id'] = $memo_id;
			$data['createdBy'] = $this->ID;
			$this->db->insert("memo_notices", $data);
		}
		
		echo '1';
	}
	
	public function update_memo(){
		
			$action = $this->input->post('action');
			$memo_id = $this->input->post('memo_id');
			$about = $this->input->post('about');
			$actual_about = $this->input->post('about');
			$desc = $this->input->post('desc');
			$actual_description = $this->input->post('desc');
			
			$birthdate = $this->input->post('birthdate');
			$deathdate = $this->input->post('deathdate');
			$placeofdeath = $this->input->post('placeofdeath');
			$desc=$this->profanitytest($desc,"altered","description",$memo_id,$this->ID);
			$about=$this->profanitytest($about,"altered","about",$memo_id,$this->ID);
			if($action == 'about'){
				$this->db->where("ID", $memo_id)->update("memorials", array("about"=>$about, "actual_about"=>$actual_about));
				echo $about;; exit;
			}elseif($action == 'desc'){
				$this->db->where("ID", $memo_id)->update("memorials", array("description"=>$desc, "actual_description"=>$actual_description));
				echo $desc; exit;
			}elseif($action == 'dates'){
				$this->db->where("ID", $memo_id)->update("memorials", array("birthdate"=>$birthdate, "deathdate"=>$deathdate, "placeofdeath"=>$placeofdeath));
				$memo = $this->db->query("select * from memorials where ID = '$memo_id'")->row();
				
				$d1 = new DateTime($memo->birthdate);
				$d2 = new DateTime($memo->deathdate);
				$diff = $d2->diff($d1); 
				
				?>		
				  <li><?php echo $diff->y; ?> Years Old </li>
				  <li>Born on <?php echo date('l', strtotime($memo->birthdate)).', '.date("F", strtotime($memo->birthdate)).' '.date("d", strtotime($memo->birthdate)); ?> <?php echo date("Y",strtotime($memo->birthdate)); ?> in <?php echo $memo->countryorigin1; ?></li>
				  <li>Passed a way on <?php echo date('l', strtotime($memo->deathdate)).', '.date("F", strtotime($memo->deathdate)).' '.date("d", strtotime($memo->deathdate)); ?> <?php echo date("Y",strtotime($memo->deathdate)); ?> in <?php echo $memo->placeofdeath; ?>, <?php echo $memo->countryorigin1; ?></li>
                        
				<?php 
				
			}else{
				
			}
	}
	
	public function get_memo_title(){
		
		$memo_id = $this->input->post('memo_id');
		$memo = $this->db->query("select fname, lname from memorials where ID = '$memo_id'")->row();
		echo json_encode($memo);
	}
	
	public function update_memo_title(){ 
		
			$memo_id = $this->input->post('memo_id');
			$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			
			
			$this->db->where("ID", $memo_id)->update("memorials", array("fname"=>$fname, "lname"=>$lname));
			$memo = $this->db->query("select * from memorials where ID = '$memo_id'")->row();
			if($memo){
				echo $memo->fname.' '.$memo->lname;
			}
	}
	
	public function update_memo1(){
		
			$memo_id = $this->input->post('memo_id');
			$memorial_music = $this->input->post('memorial_music');
			$memorial_template = $this->input->post('memorial_template');
			$visibility = $this->input->post('visibility');
			$this->db->where("ID", $memo_id)->update("memorials", array("memorial_music"=>$memorial_music, "memorial_template"=>$memorial_template, "visibility"=>$visibility));
			echo 'success';
	}
	
	public function update_memo_pic(){ 
		
		$memo_id = $this->input->post('memo_id');
		$webaddress = $this->input->post('webaddress');
		$cropped_image = $this->input->post('cropped_image');
							
		/* $config =  array(
            'upload_path'     => './uploads/',
            'allowed_types'   => "gif|jpg|png",
            'max_size'        => "*"

              'allowed_types' => 'avi|mp4|flw|mov',
            'max_height'      => "1768",
            'max_width'       => "2048"  
        ); */
		
		//$this->db->insert("memo_pic", $data);
		
		$this->db->where("ID", $memo_id)->update("memorials", array("cropped_image"=>$cropped_image));
		
		// update activity_log
		$usr = $this->db->query("select fname, lname from users where ID = ".$this->ID)->row();
		$memo = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
		$description = $usr->fname.' '.$usr->lname." updated main picture to the memorial for ".$memo->fname.' '.$memo->lname.'.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log
		
		redirect("memorial/tribute/".$webaddress);
		
	}
	
	
	public function add_pic(){
		
		$memo_id = $this->input->post('memo_id');
		$webaddress = $this->input->post('webaddress');
		$data['createdby'] = $this->ID;
		$data['memo_id'] = $memo_id;
		$data['pic'] = $this->input->post('cropped_image');
		$data['createdAt'] = date("Y-m-d H:i:s");
					
		/* $config =  array(
            'upload_path'     => './uploads/',
            'allowed_types'   => "gif|jpg|png",
            'max_size'        => "*"

              'allowed_types' => 'avi|mp4|flw|mov',
            'max_height'      => "1768",
            'max_width'       => "2048"  
        ); */
		
		$this->db->insert("memo_pic", $data);
		
		// update activity_log
		$usr = $this->db->query("select fname, lname from users where ID = ".$this->ID)->row();
		$memo = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
		$description = $usr->fname.' '.$usr->lname." upload a new picture to the memorial for ".$memo->fname.' '.$memo->lname.'.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log
		
		redirect("memorial/tribute/".$webaddress);
		
	}
		
	public function add_pic_gallary(){
		
		$webaddress = $this->input->post('webaddress');
		$memo_id = $this->input->post('memo_id');
		
		$data['createdby'] = $this->ID;
		$data['memo_id'] = $memo_id; 
		$data['pic'] = $this->input->post('cropped_image');
		$data['createdAt'] = date("Y-m-d H:i:s");
		
		/* if(!$this->is_page_admin($memo_id, $this->ID)){
			$data['status'] = 'Pending';
			$this->session->set_flashdata('suc_pic', "Picture will be displayed after admin approval");
		} */
		
		$this->db->insert("memo_pic", $data);
		
		// update activity_log
		$usr = $this->db->query("select fname, lname from users where ID = ".$this->ID)->row();
		$memo = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
		$description = $usr->fname.' '.$usr->lname." upload a new picture to the memorial for ".$memo->fname.' '.$memo->lname.'.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log
		
		redirect("memorial/tribute/".$webaddress."?tab=p_gallery");
		
	}
	
	
	
	public function add_video(){
		
		$webaddress = $this->input->post('webaddress');
		$data['createdby'] = $this->ID;
		$memo_id = $this->input->post('memo_id');
		$data['memo_id'] = $memo_id;
		$data['createdAt'] = date("Y-m-d H:i:s");
		
		if(!$this->is_page_admin($memo_id, $this->ID)){
			$data['status'] = 'Pending';
		}
		
		$config =  array(
            'upload_path'     => './uploads/videos/',
            'allowed_types'   => "avi|mp4|flw|mov",
            'max_size'        => "*"

            /*  'allowed_types' => 'avi|mp4|flw|mov',
            'max_height'      => "1768",
            'max_width'       => "2048"  */
        );

        $this->load->library('upload', $config);

        if($this->upload->do_upload("video"))
        {
            $filed = $this->upload->data();
            $data['video'] = $filed["file_name"];
			
			
			/* if(!$this->is_page_admin($memo_id, $this->ID)){
				$data['status'] = 'Pending';
				$this->db->insert("memo_video", $data);
				$this->session->set_flashdata('succ_video', "Video will be displayed after admin approval");
			}else{
				$this->db->insert("memo_video", $data);
			} */
			
			$this->db->insert("memo_video", $data);
			
			// update activity_log
			$usr = $this->db->query("select fname, lname from users where ID = ".$this->ID)->row();
			$memo = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
			$description = $usr->fname.' '.$usr->lname." upload a new video to the memorial for ".$memo->fname.' '.$memo->lname.'.';
			$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
			// end update activity_log
			
        }
        else
        {
            //if (!empty($_FILES['userfile']['name'])) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error_video', $error['error']);
            //}
        }
		
		redirect("memorial/tribute/".$webaddress."?tab=v_gallery");
		
	}
	
	public function approve_video(){
		
		$this->db->where("ID", $this->input->post('id'))->update("memo_video", array("status"=>"Approved"));
		echo "success";
		
	}
	
	public function approve_pic(){
		
		$this->db->where("ID", $this->input->post('id'))->update("memo_pic", array("status"=>"Approved"));
		echo "success";
		
	}
	
	public function approve_audio(){
		
		$this->db->where("ID", $this->input->post('id'))->update("memo_audio", array("status"=>"Approved"));
		echo "success";
		
	}
	
	public function add_music(){
		
		$webaddress = $this->input->post('webaddress');
		$data['createdby'] = $this->ID;
		$memo_id = $this->input->post('memo_id');
		$data['memo_id'] = $memo_id;
		$data['createdAt'] = date("Y-m-d H:i:s");
				
		$config =  array(
            'upload_path'     => './uploads/music/',
            'allowed_types'   => "mp3",
            'max_size'        => "*"

            /*  'allowed_types' => 'avi|mp4|flw|mov',
            'max_height'      => "1768",
            'max_width'       => "2048"  */
        );

        $this->load->library('upload', $config);

        if($this->upload->do_upload("audio"))
        {
            $filed = $this->upload->data();
            $data['audio'] = $filed["file_name"];
			
			/* if(!$this->is_page_admin($memo_id, $this->ID)){
				$data['status'] = 'Pending';
				$this->db->insert("memo_audio", $data);
				$this->session->set_flashdata('succ_music', "File will be displayed after admin approval");
			}else{
				$this->db->insert("memo_audio", $data);
			} */
			
			$this->db->insert("memo_audio", $data);
			
			// update activity_log
			$usr = $this->db->query("select fname, lname from users where ID = ".$this->ID)->row();
			$memo = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
			$description = $usr->fname.' '.$usr->lname." upload a new audio to the memorial for ".$memo->fname.' '.$memo->lname.'.';
			$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
			// end update activity_log
			
        }
        else
        {
            //if (!empty($_FILES['userfile']['name'])) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error_music', $error['error']);
            //}
        }
		
		redirect("memorial/tribute/".$webaddress."?tab=a_gallery");
		
	}
	
	public function delete_pic(){
		
		$id = $this->input->post('id');
		$pic = $this->input->post('pic');
		
		$this->db->where('ID', $id)->delete('memo_pic');
		$path = FCPATH.'uploads/'.$pic;
		unlink($path);
		echo 'success';
		
	}
	
	public function delete_vid(){
		
		$id = $this->input->post('id');
		$vid = $this->input->post('vid');
		
		$this->db->where('ID', $id)->delete('memo_video');
		$path = FCPATH.'uploads/videos/'.$vid;
		unlink($path);
		echo 'success';
		
	}
	
	public function delete_audio(){
		
		$id = $this->input->post('id');
		$aud = $this->input->post('aud');
		
		$this->db->where('ID', $id)->delete('memo_audio');
		$path = FCPATH.'uploads/music/'.$aud;
		unlink($path);
		echo 'success';
		
	}
	
	public function test_email(){
		//$vlink = base_url().'memorial/tribute/';
		//$this->general_model->send_email('smtptesting@goalwebs.com', 'davidmark0772@gmail.com', "notifii", "New tribute has been added, visit your memorial to check. ".$vlink, "gffgf");
	}
	
	public function add_tribute(){		
		$memo_id = $this->input->post('memo_id');
		$memo = $this->db->query("select * from memorials where ID = ".$memo_id)->row();
		
		$memo_created_id = $this->input->post('memo_created_id');
		$data['memo_id'] = $memo_id;
		$data['action'] = $this->input->post('action');
		$comment = $this->input->post('comment');
		
		
		$data['actual_text'] = $comment;
		//echo profanity_search($comment); exit;
		$comment=$this->profanitytest($comment,"altered","tributes",$memo_id,$memo->createdby);
		$data['comment'] = $comment;
		$data['createdby'] = $this->ID;
		$data['createdAt'] = date("Y-m-d H:i:s");
		
		//if($memo_created_id == $this->ID){
			$data['status'] = 'Approved';
		//} 
		
		
		$block_cnt = $this->db->query("select count(*) as cnt from block_users where memo_id = '$memo_id' and user_id = '$this->ID'")->row()->cnt;  //("block_users", array("memo_id"=>$memo_id, "user_id"=>$user_id, "block_by"=>$this->ID));
		
		if($block_cnt > 0){
			echo "block";
			exit;
		}else{
			
			$this->db->insert("memo_tribute", $data);
			
			// update activity_log
			$usr = $this->db->query("select fname, lname from users where ID = ".$this->ID)->row();
			$description = $usr->fname.' '.$usr->lname." added a tribute to the memorial for ".$memo->fname.' '.$memo->lname.'.';
			$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
			// end update activity_log
			
		
			$tributes = $this->db->query("select * from memo_tribute where memo_id = '$memo_id' and status = 'Approved' order by ID desc")->result();
			foreach($tributes as $t){
				$res = $this->db->query("select ID, fname, lname from users where ID = ".$t->createdby)->row();
				$t->by = $res->fname.' '.$res->lname;
				
				if($t->action == 'candle'){
					$t->d_action = 'lit a candle';
					$t->img = 'christmas-candle.png';
				}elseif($t->action == 'tribute'){
					$t->d_action = 'laid a flower';
					$t->img = 'flower1.png';
				}else{
					$t->d_action = 'left a note';
					$t->img = 'feather1.png';
				}
			}
			
			
			// send email to page owner, page managers and people who signup for notification for this memorial
						
			$from=$this->config->config['admin_email'];
			if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
			$from_name="SafiriSalama Memorials";
			$subject="Tribute Email";
			$vlink = base_url().'memorial/tribute/'.$memo->webaddress;
			
				// send to page admin
				$page_admin = $this->db->query("select fname, lname, email from users where ID = ".$memo_created_id)->row();
				
				$to=$page_admin->email;				
				$to_name=$page_admin->fname.' '.$page_admin->lname;				
				
				$content='<p> A new tribute has been added to the memorial "'.$memo->fname.' '.$memo->lname.'" click <a href="'.$vlink.'">link</a> to view detail.</br><p>"'.$comment.'"</p>';
				$title = 'Notification Email Admin';
				
				$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
				//echo $message_body; exit;
				$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
				//end		

				// send to page managers
				$page_managers = $this->db->query("select memorial_admin.user_id, users.* from memorial_admin left join users on memorial_admin.user_id = users.ID where memorial_admin.memo_id = ".$memo_id)->result();
				foreach($page_managers as $manager){
					
					$to_name=$manager->fname.' '.$manager->lname;
					$to=$manager->email;				
					$content='<p> A new tribute has been added to the memorial "'.$memo->fname.' '.$memo->lname.'" click <a href="'.$vlink.'">link</a> to view detail.</br><p>"'.$comment.'"</p>';
					$title='Notification Email Manager';
					
					$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
					$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);					
				}
				//end 
				
				// send to subscribers
				$notification_emails = $this->db->query("select * from notification_emails where memo_id = ".$memo_id)->result();
				foreach($notification_emails as $noti){
					$to_name= explode('@', $noti->email)[0]; //$noti->email;
					$to=$noti->email;				
					$content='<p> A new tribute has been added to the memorial "'.$memo->fname.' '.$memo->lname.'" click <a href="'.$vlink.'">link</a> to view detail.</br><p>"'.$comment.'"</p>';
					$title ='Notification Email Subscriber';
					
					$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
					$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);	
				} 
				//end 
			
			
			// End send email
			
			foreach($tributes as $t){ ?>
						  
			  <div class="featured_box" id="tribute<?php echo $t->ID ?>">
				  <div class="media">
				  
				  <img class="" src="<?php echo base_url(); ?>assets/img/<?php echo $t->img; ?>" alt="mage">
				  
					<div class="media-body">
					  <h5><?php echo $t->by; ?> <i><?php echo $t->d_action; ?></i></h5>
					   <i onClick="markinappropriate(<?php echo $t->ID ?>)" style="cursor: pointer" title="Flag as inappropriate" class="pull-right fa fa-flag" aria-hidden="true"></i>
					  <p><?php echo $t->comment; ?></p>								  
					  </br>
					  <h6><?php echo date('l', strtotime($t->createdAt)).', '.date("F", strtotime($t->createdAt)).' '.date("d", strtotime($t->createdAt)); ?> <?php echo date("Y",strtotime($t->createdAt)); ?> <?php echo date("h:i:a",strtotime($t->createdAt)); ?> </h6>
								<h6>
									  <?php 
										if($this->session->userdata('user')->ID==$t->createdby) 
										{
										?>
										<a href="javascript:;" onClick="openeditmodal(<?php echo $t->ID ?>)">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="javascript:;" onClick="deletemytribute(<?php echo $t->ID ?>,<?php echo $t->createdby ?> )">Delete</a>
										<?php }?>
									</h6>
					</div>
				  </div>
			  </div>
			  
		  <?php }
			
		}
		
	}
		
	public function is_page_admin($memo_id, $user_id){
			
		$cnt = $this->db->query("select count(*) as cnt from memorials where ID='$memo_id' and createdby = '$user_id'")->row()->cnt;
		if($cnt > 0){
			return true;
			exit;
		}else{
			$cnt = $this->db->query("select count(*) as cnt from memorial_admin where memo_id = '$memo_id' and user_id = '$user_id'")->row()->cnt;
			if($cnt > 0){
				return true;
				exit;
			}else{
				return false;
				exit;
			}
		}		
	}
	
	public function is_page_super_admin($memo_id, $user_id){
			
		$cnt = $this->db->query("select count(*) as cnt from memorials where ID='$memo_id' and createdby = '$user_id'")->row()->cnt;
		if($cnt > 0){
			return true;
			exit;
		}else{
			return false;
			exit;
		}
	}
	
	
	public function manage_memorial($id){
		
		if(!$this->is_page_admin($id, $this->ID)){
			redirect('memorial');
			exit;
		}
		
		$this->data['title'] = "Manage Memorial | Safiri Salama"; 
		$this->data['description'] = "Manage Memorial | Safiri Salama"; 
		$memo = $this->db->query("select * from memorials where ID= ".$id)->row();		
		$t = $this->db->query("select class, background from memorial_templates where ID = ".$memo->memorial_template)->row();
		$memo->background = $t->background;
		$memo->tclass = $t->class;
		$memo->plan_id = $this->db->query("select ID from payment_plan where title = '$memo->pakage'")->row()->ID;
		
		//$this->data['audios'] = $this->db->query("select * from memo_audio where memo_id = '$memo->ID' order by ID desc")->result();
		
		$page_admin = $this->db->query("select * from memorial_admin where memo_id = '$memo->ID'")->result();
		foreach($page_admin as $a){
			$res = $this->db->query("select * from users where ID = ".$a->user_id)->row();
			$a->name = $res->fname.' '.$res->lname;
		}
		
		
		$pending_tributes = $this->db->query("select * from memo_tribute where memo_id = '$memo->ID' and status = 'Approved' ORDER BY id DESC")->result();
		foreach($pending_tributes as $t){
			$res = $this->db->query("select ID, fname, lname from users where ID = ".$t->createdby)->row();
			$t->by = $res->fname.' '.$res->lname;
			
			if($t->action == 'candle'){
				$t->d_action = 'lit a candle';
			}elseif($t->action == 'tribute'){
				$t->d_action = 'laid a flower';
			}else{
				$t->d_action = 'left a note';
			}
		}
		
		
		$this->data['page_admin'] = $page_admin;
		$this->data['is_page_super_admin'] = $this->is_page_super_admin($id, $this->ID);
		$this->data['is_page_admin'] = $this->is_page_admin($id, $this->ID);
		$this->data['memo'] = $memo;
		$this->data['pending_tributes'] = $pending_tributes;
		
		$this->data['audios'] = $this->db->query("select * from memorial_music order by ID desc")->result();
		$this->data['memorial_templates'] = $this->db->query("select * from memorial_templates order by ID desc")->result();
		
        $this->load->view("manage_memorial", $this->data);
		
	}
	
	public function delete_memorial(){
		
		$memo_id = $this->input->post('memo_id');
		
		
		$this->db->where("memo_id", $memo_id)->delete("memo_notices");
		$this->db->where("memo_id", $memo_id)->delete("memo_tribute");
		$this->db->where("memo_id", $memo_id)->delete("memo_pic"); 
		$this->db->where("memo_id", $memo_id)->delete("memo_audio");
		$this->db->where("memo_id", $memo_id)->delete("memo_video");
		$this->db->where("memo_id", $memo_id)->delete("memorial_admin");
		
		$this->db->where("ID", $memo_id)->delete("memorials");
		echo "success";
		
	}
	
	public function approve_tribute(){
		$this->db->where("ID", $this->input->post('id'));
		$this->db->update("memo_tribute", array("status"=>'Approved'));
		echo "success";
	}
	
	public function delete_tribute(){
		$this->db->where("ID", $this->input->post('id'));
		$this->db->delete("memo_tribute");
		echo "success";
	}
		
	public function remove_admin(){
		
		$user_id = $this->input->post('user_id');
		$memo_id = $this->input->post('memo_id');
		
		if($this->is_page_super_admin($memo_id, $this->ID)){
			
			$this->db->where("memo_id", $memo_id);
			$this->db->where("user_id", $user_id);
			$this->db->delete('memorial_admin');
			echo 'success';
			
		}else{
			echo "You are not allowed to remove";
			exit;
		}
		
	}
	
	public function del_memo(){
		
		//$this->db->where("ID", $this->input->post('memo_id'));
		//$this->db->update("memorials", array("visibility"=>private));
		//echo "success";
		
	}
	
	public function sort_by(){
		
		$limit = $this->input->post('limit');
		$keywords = $this->input->post('keywords');
		$sortby = $this->input->post('sortby');
		$order = $this->input->post('order');
		
		$condition='';
		
		if($sortby == 'dod'){
			$condition.=' ORDER BY deathdate '.$order;
		}elseif($sortby == 'gender'){
			$condition.=' ORDER BY gender '.$order;
		}elseif($sortby == 'fname'){
			$condition.=' ORDER BY fname '.$order;
		}elseif($sortby == 'lname'){
			$condition.=' ORDER BY lname '.$order;
		}else{
			$condition.=' ORDER BY ID desc';
		}
				
		$memorials = $this->db->query("SELECT * FROM memorials where status='Active' and visibility = 'public' and (lname like '%$keywords%' or fname like '%$keywords%') ".$condition." limit ".$limit)->result();
		//print_r($this->db->last_query());  exit;
		foreach($memorials as $memorial){ 
		
			$full_name = $memorial->fname.' '.$memorial->lname;
		
		?>
		
				<div class="col-lg-3 col-md-4 mt-3">
					<div class="mem_box" style="cursor: pointer;"  onclick="view_detail('<?php echo $memorial->webaddress; ?>')">
						<div class="media">
						<img width="150" height="150" src="<?php echo base_url(); ?>uploads/<?php echo $memorial->cropped_image; ?>" class="mr-3 wp-post-image" alt="">								
						<div class="media-body">
						   <h4><a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>"><?php if(strlen($full_name) > 11) echo substr($full_name, 0, 11); else echo $full_name; ?></a></h4>
							<p><?php echo date('Y', strtotime($memorial->birthdate)); ?>-<?php echo date('Y', strtotime($memorial->deathdate)); ?></p>
						  </div>

						</div>

						 <a href="<?php echo base_url(); ?>memorial/tribute/<?php echo $memorial->webaddress; ?>" class="memorial_link">View Memorial</a>
					</div>
				</div>
		
		<?php } ?>
			
			<div class="clearfix"></div> <?php 
		
	}
	
	public function gallery()
	{		
		$this->data['title'] = "Search Results | Safiri Salama"; 
		$keywords = $this->input->post('keywords');
		
		
		$where = "";
		if(is_numeric($keywords) && $keywords > 0){
			$where.=' AND ID = '.$keywords;
		}elseif($keywords == 'All'){
			$where = "";
		}else{
			$keys = explode(" ",$keywords);
			foreach($keys as $key){
				$where.= " AND (lname like '%$key%' or fname like '%$key%' or mname like '%$key%' or nname like '%$key%' or countryorigin1 like '%$key%' or citytown like '%$key%' or placeofdeath like '%$key%' or birthdate like '%$key%' or deathdate like '%$key%' or gender like '%$key%' or webaddress like '%$key%' or relationname like '%$key%' or rfname like '%$key%' or rsurname like '%$key%'  or rphone like '%$key%' or countryorigin2 like '%$key%' or raddress like '%$key%' or remailadd like '%$key%' or rpnm like '%$key%' or current_town like '%$key%' or visibility like '%$key%' or pakage like '%$key%' or status like '%$key%' or about like '%$key%' or description like '%$key%' or featured like '%$key%')";
				//echo $key;
			}
			//$where = " AND (lname like '%$keywords%' or fname like '%$keywords%')";
		}
		
		//echo $where; exit;
		
		$this->data['memorials_list'] = $this->db->query("select * from memorials where status='Active' and visibility = 'public' order by ID desc")->result();
		$memorials = $this->db->query("select * from memorials where status='Active' and visibility = 'public' ".$where." order by ID desc limit 4")->result();
        foreach($memorials as $memo){
			$res = $this->db->query("select fname, lname from users where ID = ".$memo->createdby)->row();
			$memo->creator = $res->fname.' '.$res->lname;
		}
		$this->data['memorials'] = $memorials;
		$this->load->view("memorial_gallery", $this->data);
        
	}
	
	public function search_gallery(){
		
		$limit = $this->input->post('limit');
		
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$yborn = $this->input->post('yborn');
		$ydeath = $this->input->post('ydeath');
		$gender = $this->input->post('gender');
		
		$condition='';
		
		if(!empty($fname)){
			$condition = " AND fname like '%$fname%' ";
		}
		
		if(!empty($lname)){
			$condition = " AND lname like '%$lname%' ";
		}
		
		
		if(!empty($yborn)){
			$condition.=" AND YEAR(birthdate) = ".$yborn;
		}
		
		if(!empty($ydeath)){
			$condition.=" AND YEAR(deathdate) = ".$ydeath;
		}
		
		if(!empty($gender)){
			$condition.=" AND gender = '$gender'";
		}
		
		$memorials = $this->db->query("select * from memorials where status='Active' and visibility = 'public' ".$condition." order by ID desc limit ".$limit)->result();
        
		//print_r($this->db->last_query());  exit;
		
		foreach($memorials as $memo){
			$res = $this->db->query("select fname, lname from users where ID = ".$memo->createdby)->row();
			$memo->creator = $res->fname.' '.$res.lname;
		}
		
		
		?>

                <h3 class="results_class">Your search has returned  <span id="search_result_cnt"> <?php echo count($memorials); ?> </span>  results</h3>
                <div class="search_block-new">
                    
					<?php foreach($memorials as $memo){ ?>
					
						<div class="search_box-new">
							<div class="row">
								<div class="col-md-2 col-3">
									<img class="img-fluid w-100" alt="img" src="<?php echo base_url(); ?>uploads/<?php echo $memo->cropped_image; ?>">
								</div>
								<div class="col-md-10 col-9 pad_mob_left0">
									<h4><?php echo $memo->fname.' '.$memo->lname; ?> (<?php echo date('Y', strtotime($memo->birthdate)).'-'.date('Y', strtotime($memo->deathdate)); ?>)<span class="pull-right d-none d-md-block">Memorial created by <?php echo $memo->creator; ?> on <?php echo date('m-d-Y', strtotime($memo->createdAt)); ?></span></h4>
									<p><a target="_blank" href="<?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?>"><?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?></a></p>
									<span class="pull-right d-block d-md-none">Memorial created by <?php echo $memo->creator; ?> on <?php echo date('m-d-Y', strtotime($memo->createdAt)); ?></span>
									<div class="d-none d-md-block">
										<p class="new_text-search "><?php echo substr($memo->about, 0, 180); if(strlen($memo->about) > 180) echo '...'; ?></p>
										<a href="<?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?>" class="red_btn memorial_btn">VIEW MEMORIAL</a>
									</div>
								</div>
								<div class="col-12 d-block d-md-none">
									<p class="new_text-search"><?php echo substr($memo->about, 0, 180); if(strlen($memo->about) > 180) echo '...'; ?></p>
									<a href="<?php echo base_url().'memorial/tribute/'.$memo->webaddress; ?>" class="red_btn memorial_btn">VIEW MEMORIAL</a>
								</div>
							</div>
						</div>
						
					<?php } ?>
					
					
                </div>
                <a href="javascript:;" onclick="search_gallery('<?php echo count($memorials); ?>')" class="setting_btn setting_btn1">Show More Memorials</a>
				
				<input id="current_limit" type="hidden" value="<?php if(count($memorials) < 4 ) echo '4'; else echo count($memorials); ?>" />
				
		<?php 
		
	}
	
	public function check_address(){
		
		$webaddress = $this->input->post('webaddress');
		$cnt = $this->db->query("select count(*) as cnt from memorials where webaddress = '$webaddress'")->row()->cnt;
		echo $cnt;
		exit;
	}
	
	public function create_memorial()
	{	
		unset($_SESSION['add_memo']);
		//redirect('login'); exit;
		$this->data['title'] = "Create Memorial | Safiri Salama";
		$this->data['memorial_templates'] = $this->db->query("select * from memorial_templates order by ID desc")->result();
		$this->data['memorial_music'] = $this->db->query("select * from memorial_music order by ID desc")->result();
		$this->data['countries'] = $this->db->query("select * from countries")->result();
		$this->data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row();
		
		//if(isset($this->input->post('fname'))){
			//$this->data['fname'] = $this->input->post('fname');
			//$this->data['lname'] = $this->input->post('lname');
		//}
		
		$this->data['pending_memo'] = $this->db->query("select * from pending_memorials where createdby > 0 and createdby = ".$this->ID)->row();
		
		//echo $this->ID; exit;
		
        $this->load->view("create_memorial", $this->data); 
		
	}
	
	public function save_memo_info_if_left_in_the_way(){
		
		 $webaddress = $this->input->post('webaddress');
		 $webaddress = str_replace(' ', '_', $webaddress);
		 $createdby = $this->input->post('createdby');
		 
		 $memo['memorial_template'] = $this->input->post('memorial_template');
		 $memo['memorial_music'] = $this->input->post('memorial_music');
		 $memo['fname'] = $this->input->post('fname');
		 $memo['lname'] = $this->input->post('lname');
		 $memo['mname'] = $this->input->post('mname');
		 $memo['nname'] = $this->input->post('nname');
		 $memo['countryorigin1'] = $this->input->post('countryorigin1');
		 $memo['citytown'] = $this->input->post('citytown');
		 $memo['cropped_image'] = $this->input->post('cropped_image');
		 $memo['placeofdeath'] = $this->input->post('placeofdeath');
		 $memo['birthdate'] = date('Y-m-d', strtotime($this->input->post('birthdate')));
		 $memo['deathdate'] = date('Y-m-d', strtotime($this->input->post('deathdate')));
		 $memo['gender'] = $this->input->post('gender'); 
		 $memo['webaddress'] = $webaddress;
		 
		 $memo['relationname'] = $this->input->post('relationname');
		 $memo['rfname'] = $this->input->post('rfname');
		 $memo['rsurname'] = $this->input->post('rsurname');
		 $memo['rphone'] = $this->input->post('rphone');
		 $memo['countryorigin2'] = $this->input->post('countryorigin2');
		 $memo['raddress'] = $this->input->post('raddress');
		 $memo['remailadd'] = $this->input->post('remailadd');
		 $memo['current_town'] = $this->input->post('rpnm');  
		 $memo['createdby'] = $this->input->post('createdby');
		 
		 $memo['createdAt'] = date('Y-m-d H:i:s'); 
		 
		 $res = $this->db->query("select * from pending_memorials where createdby = ".$createdby)->result();
		 if(count($res) > 0){
			 $this->db->where("createdby", $createdby)->update("pending_memorials", $memo);
			 echo "record updated for pending memo";
		 }else{
			 $this->db->insert("pending_memorials", $memo);
			 echo "recored inserted for pending memo";
		 }
		
	}
	
	public function get_template(){
		
		$id = $this->input->post('template_id');
		$template = $this->db->query("select * from memorial_templates where ID = ".$id)->row();
		//echo $template->class;
		echo json_encode($template); 
		
	}
	
	public function add_memo(){
		
		$this->form_validation->set_rules('memorial_template', 'Template', 'required');
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('citytown', 'City or Town', 'required');
		$this->form_validation->set_rules('placeofdeath', 'Place Of Death', 'required');
		$this->form_validation->set_rules('remailadd', 'Email', 'required');
		$this->form_validation->set_rules('createdby', 'Login ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('memorial/create_memorial');
			}
		  else
			{
				 $webaddress = $this->input->post('webaddress');
				 $webaddress = str_replace(' ', '_', $webaddress);
				 
				 $add_memo['memorial_template'] = $this->input->post('memorial_template');
				 $add_memo['memorial_music'] = $this->input->post('memorial_music');
				 $add_memo['fname'] = $this->input->post('fname');
				 $add_memo['lname'] = $this->input->post('lname');
				 $add_memo['mname'] = $this->input->post('mname');
				 $add_memo['nname'] = $this->input->post('nname');
				 $add_memo['countryorigin1'] = $this->input->post('countryorigin1');
				 $add_memo['citytown'] = $this->input->post('citytown');
				 $add_memo['cropped_image'] = $this->input->post('cropped_image');
				 $add_memo['placeofdeath'] = $this->input->post('placeofdeath');
				 $add_memo['birthdate'] = date('Y-m-d', strtotime($this->input->post('birthdate')));
				 $add_memo['deathdate'] = date('Y-m-d', strtotime($this->input->post('deathdate')));
				 $add_memo['gender'] = $this->input->post('gender'); 
				 $add_memo['webaddress'] = $webaddress;
				 
				 $add_memo['relationname'] = $this->input->post('relationname');
				 $add_memo['rfname'] = $this->input->post('rfname');
				 $add_memo['rsurname'] = $this->input->post('rsurname');
				 $add_memo['rphone'] = $this->input->post('rphone');
				 $add_memo['countryorigin2'] = $this->input->post('countryorigin2');
				 $add_memo['raddress'] = $this->input->post('raddress');
				 $add_memo['remailadd'] = $this->input->post('remailadd');
				 //$add_memo['rpnm'] = $this->input->post('rpnm');   // r alertnative phone number
				 $add_memo['current_town'] = $this->input->post('rpnm');   // r alertnative phone number changed to current_town
				 
				 
				 $add_memo['createdby'] = $this->input->post('createdby');  
				 $add_memo['visibility'] = $this->input->post('visibility'); 

				 $add_memo['package_start_date'] = date('Y-m-d');
				 $add_memo['package_end_date'] = date('Y-m-d', strtotime('+1 year'));
				 
				 $add_memo['createdAt'] = date('Y-m-d H:i:s');
	         
			 
				$_SESSION['add_memo'] = $add_memo; 
				
				redirect('memorial/pay_now');
				//$this->load->view('pricing', array("pakages"=>$pakages));
			 
         	     //$this->db->insert('memorials', $add_memo);
                 //$id=$this->db->insert_id();
                   
				  // send email and notification after memorial created //
					/* $from=$this->config->config['admin_email'];
					$from_name='Admin';
					$name=$this->input->post('fname')." ".$this->input->post('lname'); 
					$to=$this->input->post('remailadd');
					$subject='Account Creation';
					
					$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
					$content='Your account has been created successfully, Please click the <a href="'.$vlink.'">link</a> to verify your email.</br>';
					$email_data['title']='<span>Verification<span> Email';
					$email_data['content']= $content;                 
					$message=$this->load->view('emails/general',$email_data,true); 
					$this->smtp_email->send($from,$from_name,$to,$subject,$message); */	

					//redirect('memorial');
			}
		
	} 
	
	public function pay_now(){	
		
		//print_r($_SESSION['add_memo']); exit;
		
		
		$data['title'] = "Packages | Safiri Salama";
		$data['meta_description'] = "List of pakages | Safiri Salama";
		$data['meta_keywords'] = "List of pakages | Safiri Salama";
		
		$data['is_memo'] = 0;
		if(isset($_SESSION['add_memo'])) {
			// session isn't started
			$data['is_memo'] = 1; 
			$data['add_memo'] = $_SESSION['add_memo'];		
		}
		
		//print_r($_SESSION['add_memo']); exit;
		$data['pakages'] = $this->db->query("select * from payment_plan")->result();
		$data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row(); 
		
		//print_r($_SESSION['add_memo']); exit;
		$this->load->view('pricing', $data);
		
	}
	
	public function upgrade($id){ 	
		
		$data['pakages'] = $this->db->query("select * from payment_plan where title != 'BASIC'")->result();
		$data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row(); 
		$data['memo_id'] = $id;
		$this->load->view('pricing_upgrade', $data); 
		
	}
	
	public function proceed_to_payment($p_id){
		
		//print_r($_SESSION['add_memo']); exit;	
		$data['is_memo'] = 0;
		if(isset($_SESSION['add_memo'])) {
			// session isn't started
			$data['is_memo'] = 1; 
			$data['add_memo'] = $_SESSION['add_memo'];		
		}
		
		$pakage = $this->db->query("select * from payment_plan where ID = ".$p_id)->row();
		if($pakage){
			$data['pakage'] = $pakage;
			$data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row(); 
			$this->load->view('pay_option', $data);
		}else{
			redirect('memorial/pay_now');
		}
		
		
	}
	
	public function proceed_to_upgrade($memo_id, $p_id){
		
		/* $data['is_memo'] = 0;
		if(isset($_SESSION['add_memo'])) {
			// session isn't started
			$data['is_memo'] = 1; 
			$data['add_memo'] = $_SESSION['add_memo'];		
		} */
		
		$memo = $this->db->query("select * from memorials where ID = ".$memo_id)->row();
		$pakage = $this->db->query("select * from payment_plan where ID = ".$p_id)->row();
		
		
		if($pakage && $memo){
			//echo "dddd"; exit;
			$data['memo'] = $memo;
			$data['pakage'] = $pakage;
			$data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row(); 
			$this->load->view('pay_option_upgrade', $data);
		}else{
			redirect('memorial/manage_memorial/'.$memo_id);
		}
		
		
	}
	
	public function proceed_to_renew($memo_id, $p_id){
		
		$memo = $this->db->query("select * from memorials where ID = ".$memo_id)->row();
		$pakage = $this->db->query("select * from payment_plan where ID = ".$p_id)->row();
		
		
		if($pakage && $memo){
			//echo "dddd"; exit;
			$data['action'] = 'renew';
			$data['memo'] = $memo;
			$data['pakage'] = $pakage;
			$data['user'] = $this->db->query("select * from users where ID = ".$this->ID)->row(); 
			$this->load->view('pay_option_upgrade', $data);
		}else{
			redirect('memorial/manage_memorial/'.$memo_id);
		}
		
		
	}
	
	public function generate_hash(){
		
		$datastring =  $_POST['live'].$_POST['oid'].$_POST['inv'].$_POST['ttl'].$_POST['tel'].$_POST['eml'].$_POST['vid'].$_POST['curr'].$_POST['p1'].$_POST['p2'].$_POST['p3'].$_POST['p4'].$_POST['cbk'].$_POST['cst'].$_POST['crl'];
		$hashkey ="5d14Gtplreih6eiuf";//use "demo" for testing where vid also is set to "demo"
		/********************************************************************************************************
		* Generating the HashString sample
		*/
		
		$pakage = $this->db->query("select title from payment_plan where ID = ".$_POST['p1'])->row()->title;
		$_SESSION['add_memo']['pakage'] = $pakage;
		$_SESSION['add_memo']['amount_paid'] = $_POST['ttl'];
		
		$generated_hash = hash_hmac('sha1',$datastring , $hashkey);
		
		if($_POST['ttl'] <= 0){
			// put coupon in session
			$coupon_code = $this->input->post('coupon_code');
			$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
			if($res){
				$new_freq = $res->frequency - 1;
				if($new_freq >= 0){ 
					//$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					$_SESSION['coupon']['coupon_code'] = $coupon_code; 
				}
			}
			//end session
			$this->skip_payment_process();
			exit;
		}else{
			echo $generated_hash;
		}		
	}
	
	public function generate_hash_upgrade(){  
		
		unset($_SESSION['update_memo']);
		
		$datastring =  $_POST['live'].$_POST['oid'].$_POST['inv'].$_POST['ttl'].$_POST['tel'].$_POST['eml'].$_POST['vid'].$_POST['curr'].$_POST['p1'].$_POST['p2'].$_POST['p3'].$_POST['p4'].$_POST['cbk'].$_POST['cst'].$_POST['crl'];
		$hashkey ="5d14Gtplreih6eiuf";//use "demo" for testing where vid also is set to "demo"
		/********************************************************************************************************
		* Generating the HashString sample
		*/
		
		$pakage = $this->db->query("select title from payment_plan where ID = ".$_POST['p1'])->row()->title;
				
		$update_memo['plan_id'] = $_POST['p1'];
		$update_memo['user_id'] = $_POST['p2'];
		$update_memo['memo_id'] = $_POST['p3'];
		$update_memo['price'] = $_POST['ttl'];
		$update_memo['pakage'] = $pakage;
		$update_memo['status'] = "Active";
		$update_memo['createdAt'] = date('Y-m-d H:i:s');
		$update_memo['package_start_date'] = date('Y-m-d');
		$update_memo['package_end_date'] = date('Y-m-d', strtotime('+1 year'));
				
		$_SESSION['update_memo'] = $update_memo; 
		
		$generated_hash = hash_hmac('sha1',$datastring , $hashkey);
				
		if($_POST['ttl'] <= 0){
			// put coupon in session
			$coupon_code = $this->input->post('coupon_code');
			$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
			if($res){
				$new_freq = $res->frequency - 1;
				if($new_freq >= 0){ 
					//$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					$_SESSION['coupon']['coupon_code'] = $coupon_code; 
				}
			}
			//end session
			$this->skip_payment_process_upgrade();
			exit;
		}else{
			echo $generated_hash;
		}
	}
	
	public function generate_hash1(){
		
		$datastring =  $_POST['live'].$_POST['oid'].$_POST['inv'].$_POST['amount'].$_POST['tel'].$_POST['eml'].$_POST['vid'].$_POST['curr'].$_POST['p1'].$_POST['p2'].$_POST['p3'].$_POST['p4'].$_POST['cst'].$_POST['cbk']; //,$_POST['crl'];
		$hashkey ="5d14Gtplreih6eiuf";//use "demo" for testing where vid also is set to "demo"
		/********************************************************************************************************
		* Generating the HashString sample
		*/
		
		//$pakage = $this->db->query("select title from payment_plan where ID = ".$_POST['p1'])->row()->title;
		//$_SESSION['add_memo']['pakage'] = $pakage;
		
		$generated_hash = hash_hmac('sha256',$datastring , $hashkey); 
		echo $generated_hash; 
		
	}
	
	public function reduce_coupon(){
		
		$coupon_code = $this->input->post('coupon_code');
		$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
		if($res){
			$new_freq = $res->frequency - 1;
			if($new_freq >= 0){ 
				//$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
				$_SESSION['coupon']['coupon_code'] = $coupon_code; 
			}
		}
		
	}
	
	public function payment_success(){
		$this->load->view('payment_success');
	}
	
	public function skip_payment_process(){
		
		$pakage = $_SESSION['add_memo']['pakage'];
		$price = $_SESSION['add_memo']['amount_paid'];
		$webaddress = $_SESSION['add_memo']['webaddress'];
		$remailadd = $_SESSION['add_memo']['remailadd'];
		$memorial_name = $_SESSION['add_memo']['fname'].' '.$_SESSION['add_memo']['lname'];
		
		$pakage_detail = $this->db->query("select price from payment_plan where title = '$pakage'")->row();
		$pakage_price=$pakage_detail->price;
		$username = $_SESSION['add_memo']['rfname'].' '.$_SESSION['add_memo']['rsurname'];
	
		//insert query 			
		$this->db->insert('memorials', $_SESSION['add_memo']);
		$memo_id = $this->db->insert_id();
		$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "response"=>"skip_payment", "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
		
		
		// update activity_log
		$description = $username." created a memorial for ".$memorial_name.'. Amount paid is '.$price.' KES.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log 
		
		
		if(isset($_SESSION['coupon'])){
			$coupon_code = $_SESSION['coupon']['coupon_code'];
			$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
			
			//reduce coupon frequency
			$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
			if($res){
				$new_freq = $res->frequency - 1;
				if($new_freq >= 0){ 
					$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
				}
			}	
		}
		
		unset($_SESSION['add_memo']);
		unset($_SESSION['coupon']);
		
		//Creating invoices in allinvoices table.
		//$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_detail->nextprice,"status"=>"paid", "memorialid"=>$memo_id);
		$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id);
		$newinvoiceid=$this->createaninvoice($invoiceparams);


		// Send Confirmation email on creation
		$from=$this->config->config['admin_email'];
		if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
		$from_name="SafiriSalama Memorials";
		$subject="Memorial Confirmation Email";
		$vlink = base_url().'memorial/invoice/'.$newinvoiceid;
		
		// send to page admin
		$to=$remailadd;				
		$to_name=$username;				
		
		$content='<p> Memorial for the '.$memorial_name.' has been created successfully. click <a href="'.$vlink.'">link</a> to view detail.</br><p></p>';
		$title = 'Memorial Confirmation Email';
		
		$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
		$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		//end	


		// Payment Receipt
		$subject="Memorial Creation Payment Receipt";
		
		// send to page admin
		$to=$remailadd;				
		$to_name=$username;				
		
		$content='';
		$title = 'Memorial Creation Payment Receipt';
		
		$message_body = $this->emailtemp->payment_template_callback($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
		$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		//end payment receipt	
	
		// delete pending memorial from table
		$this->db->where("createdby", $this->ID)->delete('pending_memorials'); 

		echo 'skip_payment'; 
		exit;
		//redirect('memorial/payment_success?redirect=mymemorials'); 
		//redirect('memorial/mymemorials ');
		
	}
		
	public function skip_payment_process_upgrade(){
		
			$memo_id = $_SESSION['update_memo']['memo_id'];
			$user_id = $_SESSION['update_memo']['user_id'];
			$pakage = $_SESSION['update_memo']['pakage'];
			$price = $_SESSION['update_memo']['price'];
			$package_start_date = $_SESSION['update_memo']['package_start_date'];
			$package_end_date = $_SESSION['update_memo']['package_end_date']; 
			
			$mm = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
			$memorial_name = $mm->fname.' '.$mm->lname;
			
			$pakage_price = $this->db->query("select price from payment_plan where title = '$pakage'")->row()->price;
			
			//update memorial 
			$this->db->where('ID', $memo_id)->update("memorials", array("pakage"=>$pakage,"amount_paid"=>$price, "package_start_date"=>$package_start_date, "package_end_date"=>$package_end_date, "createdAt"=>date('Y-m-d H:i:s')));
			$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "action"=>"upgrade", "response"=>"skip_payment", "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
			
			//update allinvoices
			//$invoiceparams=array("wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s',strtotime($row->willexpireat)), "recursiveat"=>date('Y-m-d', strtotime($row->willexpireat.'+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime($row->willexpireat.'+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$row->details,"subtotal"=>$row->nextprice,"discount"=>0,"totalprice"=>$row->nextprice,"nextprice"=>$row->nextprice,"status"=>"payable", "memorialid"=>$row->memorialid);
			//$invoiceparams=array("wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid");
			//$this->db->where('memorialid', $memo_id)->update("allinvoices", $invoiceparams);  // update that invoice
			
					
			$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>$this->get_invoice_id($memo_id),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id);
			$newinvoiceid=$this->createaninvoice($invoiceparams);
			
			if(isset($_SESSION['coupon'])){
				
				$usr = $this->db->query("select fname, lname from users where ID = ".$user_id)->row();
				$username = $usr->fname.' '.$usr->lname;
				$coupon_code = $_SESSION['coupon']['coupon_code'];
				$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
				
				//reduce coupon frequency
				$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
				if($res){
					$new_freq = $res->frequency - 1;
					if($new_freq >= 0){ 
						$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					}
				}	
			}
			
			unset($_SESSION['add_memo']);
			unset($_SESSION['coupon']);
			
			
			
			unset($_SESSION['update_memo']);
			
			
			// Payment Receipt
			$subject="Memorial Payment Receipt";
			
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='';
			$title = 'Memorial Payment Receipt';
			
			$message_body = $this->emailtemp->payment_email_upgrade($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end payment receipt	
			
			echo 'skip_payment'; 
			exit;
			
			//redirect('memorial/payment_success?redirect=manage_memorial&memo_id='.$memo_id); 
			//redirect('memorial/manage_memorial/'.$memo_id); 
		
	}
	
	public function payment_callback(){
		
		$val = "gt14"; //assigned iPay Vendor ID... hard code it here.
		/*
		these values below are picked from the incoming URL and assigned to variables that we
		will use in our security check URL
		*/
		
		
		$url = 'https://safirisalama.com/memorial/payment_callback?txncd=205371543&qwh=1745241575&afd=179156368&poi=77678423&uyt=1054437273&ifd=240122267&agt=&id=1558352089360&status=aei7p7yrx4ae34&ivm=1558352089360&mc=2&p1=1&p2=19&p3=david&p4=mark&msisdn_id=david%20mark&msisdn_idnum=256712375678&channel=Credit_Card&tokenid=demo&tokenemail=davidmark0772@gmail.com&card_mask=444444xxxxxx4444';
		
		$val1 = $_GET["id"];  // order id
		$val2 = $_GET["ivm"];  //
		$val3 = $_GET["qwh"];
		$val4 = $_GET["afd"];
		$val5 = $_GET["poi"];
		$val6 = $_GET["uyt"];
		$val7 = $_GET["ifd"];

		$ipnurl = "https://www.ipayafrica.com/ipn/?vendor=".$val."&id=".$val1."&ivm=".
		$val2."&qwh=".$val3."&afd=".$val4."&poi=".$val5."&uyt=".$val6."&ifd=".$val7;
		
		
		$fp = fopen($ipnurl, "rb");
		$status = stream_get_contents($fp, -1, -1);
		
		//if($status==$_GET['status'])
		//echo $ipnurl;
		//echo $status; exit;
		//echo $status.'_________'.$_GET['status'];
		
		//if($status==$_GET['status'])
		if($_GET['status'] == 'aei7p7yrx4ae34')
		{
			$pakage = $_SESSION['add_memo']['pakage'];
			$price = $_SESSION['add_memo']['amount_paid'];
			$webaddress = $_SESSION['add_memo']['webaddress'];
			$remailadd = $_SESSION['add_memo']['remailadd'];
			$memorial_name = $_SESSION['add_memo']['fname'].' '.$_SESSION['add_memo']['lname'];
			
			$pakage_detail = $this->db->query("select price from payment_plan where title = '$pakage'")->row();
			$pakage_price=$pakage_detail->price;
			$username = $_SESSION['add_memo']['rfname'].' '.$_SESSION['add_memo']['rsurname'];
		
			//insert query 			
			$this->db->insert('memorials', $_SESSION['add_memo']);
			$memo_id = $this->db->insert_id();
			$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "response"=>$ipnurl, "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
						
			// update activity_log
			$description = $username." created a memorial for ".$memorial_name.'. Amount paid is '.$price.' KES.';
			$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
			// end update activity_log 
			
			
			if(isset($_SESSION['coupon'])){
				$coupon_code = $_SESSION['coupon']['coupon_code'];
				$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
				
				//reduce coupon frequency
				$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
				if($res){
					$new_freq = $res->frequency - 1;
					if($new_freq >= 0){ 
						$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					}
				}	
			}
			
			unset($_SESSION['add_memo']);
			unset($_SESSION['coupon']);
			
			//Creating invoices in allinvoices table.
			//$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_detail->nextprice,"status"=>"paid", "memorialid"=>$memo_id);
			$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id);
			$newinvoiceid=$this->createaninvoice($invoiceparams);


			// Send Confirmation email on creation
			$from=$this->config->config['admin_email'];
			if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
			$from_name="SafiriSalama Memorials";
			$subject="Memorial Confirmation Email";
			$vlink = base_url().'memorial/invoice/'.$newinvoiceid;
			
				// send to page admin
				$to=$remailadd;				
				$to_name=$username;				
				
				$content='<p> Memorial for the '.$memorial_name.' has been created successfully. click <a href="'.$vlink.'">link</a> to view detail.</br><p></p>';
				$title = 'Memorial Confirmation Email';
				
				$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
				$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
				//end	


				// Payment Receipt
				$subject="Memorial Creation Payment Receipt";
				
				// send to page admin
				$to=$remailadd;				
				$to_name=$username;				
				
				$content='';
				$title = 'Memorial Creation Payment Receipt';
				
				$message_body = $this->emailtemp->payment_template_callback($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
				$this->smtp_email->send($from, $from_name, $to, $subject, $message_body); 
				//end payment receipt	
			
				// delete pending memorial from table
				$this->db->where("createdby", $this->ID)->delete('pending_memorials'); 
		
				redirect('memorial/payment_success?redirect=mymemorials'); 
				//redirect('memorial/mymemorials ');
		}
		else
		{
			$this->session->set_flashdata('error', 'Your payment fail');
			redirect('memorial/create_memorial');
		}
		
		fclose($fp);
		
	}
	
	public function upgrade_callback(){
		
		$val = "gt14"; //assigned iPay Vendor ID... hard code it here.
		/*
		these values below are picked from the incoming URL and assigned to variables that we
		will use in our security check URL
		*/
		$val1 = $_GET["id"];  // order id
		$val2 = $_GET["ivm"];  //
		$val3 = $_GET["qwh"];
		$val4 = $_GET["afd"];
		$val5 = $_GET["poi"];
		$val6 = $_GET["uyt"];
		$val7 = $_GET["ifd"];

		$ipnurl = "https://www.ipayafrica.com/ipn/?vendor=".$val."&id=".$val1."&ivm=".
		$val2."&qwh=".$val3."&afd=".$val4."&poi=".$val5."&uyt=".$val6."&ifd=".$val7;
		$fp = fopen($ipnurl, "rb");
		$status = stream_get_contents($fp, -1, -1);
		//if($status==$_GET['status']) 
		if($_GET['status'] == 'aei7p7yrx4ae34')
		{
			$memo_id = $_SESSION['update_memo']['memo_id'];
			$user_id = $_SESSION['update_memo']['user_id'];
			$pakage = $_SESSION['update_memo']['pakage'];
			$price = $_SESSION['update_memo']['price'];
			$package_start_date = $_SESSION['update_memo']['package_start_date'];
			$package_end_date = $_SESSION['update_memo']['package_end_date']; 
			
			$mm = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
			$memorial_name = $mm->fname.' '.$mm->lname;
			
			$pakage_price = $this->db->query("select price from payment_plan where title = '$pakage'")->row()->price;
			
			//update memorial 
			$this->db->where('ID', $memo_id)->update("memorials", array("pakage"=>$pakage,"amount_paid"=>$price, "status"=>"Active", "package_start_date"=>$package_start_date, "package_end_date"=>$package_end_date, "createdAt"=>date('Y-m-d H:i:s')));
			$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "action"=>"upgrade", "response"=>$ipnurl, "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
			
			//update allinvoices
			/* $invoiceparams=array("wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid");
			$this->db->where('memorialid', $memo_id)->update("allinvoices", $invoiceparams);  // update that invoice
			$invoice_id = $this->db->query("select id from allinvoices where memorialid = '$memo_id' order by id desc")->row()->id; //where("memorialid", $memo_id)
			 */
			$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>$this->get_invoice_id($memo_id),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id);
			$newinvoiceid=$this->createaninvoice($invoiceparams);
			
			
			$usr = $this->db->query("select fname, lname, email from users where ID = ".$user_id)->row();
			$username = $usr->fname.' '.$usr->lname;
			
			if(isset($_SESSION['coupon'])){
				
				$coupon_code = $_SESSION['coupon']['coupon_code'];
				$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
				
				//reduce coupon frequency
				$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
				if($res){
					$new_freq = $res->frequency - 1;
					if($new_freq >= 0){ 
						$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					}
				}	
			}
			
			//unset($_SESSION['add_memo']);
			unset($_SESSION['coupon']);
			
			unset($_SESSION['update_memo']);
			
			
			// send to page admin
			
				$from=$this->config->config['admin_email'];
				if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
				$from_name="SafiriSalama Memorials";
				$subject="Memorial Upgradation";
				
				$to=$usr->email;				
				$to_name=$username;				
				
				$content='';
				$title = 'Memorial Payment Receipt';
				$vlink = base_url().'memorial/invoice/'.$newinvoiceid;
				
				$message_body = $this->emailtemp->payment_template_callback($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
				$this->smtp_email->send($from, $from_name, $to, $subject, $message_body); 
			
			//end payment receipt	
					
			redirect('memorial/payment_success?redirect=manage_memorial&memo_id='.$memo_id); 
			//redirect('memorial/manage_memorial/'.$memo_id); 
		}
		else
		{
			$this->session->set_flashdata('error', 'Your payment fail');
			redirect('memorial/proceed_to_upgrade/'.$_SESSION['update_memo']['memo_id'].'/'.$_SESSION['update_memo']['plan_id']);
		}
		
		fclose($fp);
		
	}
	
	public function apply_coupon(){
		
		$coupon = $this->input->post('coupon');
		$price = $this->input->post('price');
		
		$res = $this->db->query("select * from coupons where coupon_code = '$coupon' and frequency > 0")->row();
		if($res){
			$discount_percent = $res->coupon_discount_percentage;
			
			$discount = intval($discount_percent/100*$price);
			$new_price = $price - $discount;
			echo $new_price;
			
		}else{
			echo "invalid";
		}
		
	}
	
	public function crop()
	{		
		$this->data['title'] = "Dashboard";
        $this->load->view("crop", $this->data);
        
	}
	
	function upload_crop_image(){
				 		 
		 if(isset($_POST["image"])){
			 
			$data = $_POST["image"];

			////data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAC0CAIAAAA1l+0PAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABk3SURBVHhe7Z1/iF5VesfnjyJM/3hLWJyhL13WQWGlKETYLVKEsF0WFTrSTZBaRIpdVrZZ2SZl0W4bdV3sgtsqMSC1RurWZNsSXImQqFXYCOLG/CErWUhapdHJTDNm4saEDJhJ0qTfc+/JzZ1zfz3Puee+73tevx++SJz3nvOee997vu9znvvc+05cIoSQSKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhETI+nF+Yv7h8xv7POELDImR8WFw/e/KJx+3/jCM0LELGhM8O7P9oYmJuzQTiLPunsYOGRciYsHDzTUdnpuemJo9vvM/+aeygYREyDpze+QLCq6PX9iH8A9GWfWG8oGEREj0XlpaMVc1Mp4aFfyDasq+NFzQsQqLnk8cenVtjw6tUCLIQc9mXxwgaFiFxs3L4ULYYvKKZafxx/EocaFiExM3Hd995tN9zDevaPmKuEw9+3240LtCwCImY5Tf3lYRXlwXPOnvwPbvpWEDDIiRijDFlufai+r3F9bN20yaWF04tvn2kpS6urNjuuoGGRUisnNr+TE14lQobnNn9km1Qzb5v/+z5iQdaatfarba7zqBhERIlF5aW5qYm68KrVMkG9dn3V29/GnazY+KhNkIPpz44YXvsDBoWIVFy4pEtTilDlbDZJ489apsVeH/Xu0Hc6sDDe22PXULDIiQ+Vg4fMm7VGF5dFhaGpTcYXlxZcazHW8sLp2ynXULDIiQ+qkoZKtXvoYltnANhUZDw6tDz79geO4aGRUhkLL+ypzHXXhSaLL+5z3aRgJiovVtBL9+yreuLgxk0LEJiAtYwv/Z6+WLwipIbDPPO8tqG7UHCq8W3j9geu4eGRUhMSEoZqoSGaJ72A5cJ4lZwvbTDwUDDIiQaLi6fMW7lEV6lQsN+78LSErpyrMdPMKzB5NozaFiERINf9iqv9AbDQ8+/EyS8GkwpQx4aFiHRcHrnC8LaqyrNTU3+719896cTP3Tcx08Dy7Vn0LAIiYazB99rFWHNTM+vvf6tu//J8R0PIbx6f9e7dlgDhIZFSEyoK7ByQnR25L6HgoRXL9+yzQ5osNCwCImJ8wvznkFWUtbw6u1PO9bjIYRXgyxlyEPDIiQyfrNtq4dnzU1NHv7uk+3DK7jVvm//zA5l4NCwCImMiysr6S96OZZUp+TBWLvWbnXcx0MwrAGXMuShYRESH/UPGi1qbs1EqFz74EsZ8tCwCImS4xvvM8/DKnhTUWFLGezbDwkaFiFRYh7gJ3nCTFLKECrXPpRShjw0LEJiJf2154+u7tcI4VWoUoa9f/Kv9o2HBw2LkGGw/yeXXt/cXmd/PN2o3Tf8yLEeR//8O3+N6Mn5oyNY3qfb73fe3dWRN+zedQYNi5BhcPy9S89MXHqutf5lqk47rvrvv/rzmvAKL/3N7215rv/1/V/8Zv1mv/rOJvTmvrsj7NHr37E72A00LEKGBOY2JrljMUF1/tkv19jQ36/5u80zD/7gS5tenLrh6LX9+iALXTmdlwue9atn7Q52AA2LkCFx7tNuDWvHVb/csLnUsFKrSrXzd9cZw5qZPnjN10o3xh8Rppnwyum/Stgp7Fo30LAIGR4IRhCSOBM+kJa3frVoQPgL1oCZWz1147devvq6NMI62u/t+cLGYpPdN/xI4VYQDKuzhSENi5ChYR7P8h9/4E74INpx1d6v/K1jPfnAKhWsKlVaAPHBdTc6TeBfS//4pzrDguDCxzv5iXwaFiFD5cgb4YOsHVedfOCufKxUtCooC6+sYSVB1i+m78k3fOO2h9RuBSHIghF3AA2LkGGze13YZNb5Z7+cZdDhPo5PZcrCqyuGlZShZm6FtlhXOp1L1U32nYZFyLBJSxycCe+tHVf9cvbONErKp6scPdf/eolhJUHWr2/YgOaQLWVw+perg+w7DYuQEeD1zaGCrDTXXroGzPSDL23KFoOuYSWe9fPJTfC7tvk1uPD+n9gdDAQNi5ARIFyJA8Krp278Vr3SUoZKw0pKHP7nHx5uFV6lwk4Fzb7TsAgZDUKVOKCTv5y49Md1+q+v9OsMK3nAw2f/+e8BrmDCsIKWONCwCBkZApY4wLbu/C3Hp/KqWxJCM9OL62fNgq593IeRhLvHkIZFyMgQusTh7I+nq2zr0z/q1RlW9jPRQTw0XIkDDYuQEeL/XrorVDLrir73245bpTrw+9fUGBaCrPm11wcLsgKVONCwCBklwpY4ZEKfhcRWPshy3SrR3NTkiUe2mDIxpzcPYQAhShxoWISMGK9v7sSzIHS7eoWYZd8dq8qEheHZvU8GCLJM9n2z3cEW0LAIGTEQiXRkWImcxNaVm59LlWTfwyxUsVOtSxxoWISMHl0+xcHqcmIrXRi6PpVXv2eeNRqiJssYXztoWISMJLvXde5Z6D+xLQRZrknllfxkdLAgq12JAw2LkFElvUKHSV4lvIrAp6WemTj9Z72Pri74VF793skH7ko3bivYVovsOw2LkFi5uLLyb1dva9Svb9gw/9U/rBfCKNekVmtuavLswfeM17RXC2hYhMTKW5tebPy1G+jnk5uMH9Wr4FCu+r2P777TvvHwoGEREiWnPjghcSvopxM/RJAFx3E9SKmPJiaW39xn335I0LAIiRLtjzk77uMjBGL9nn37IUHDIiQ+3t/1rjC8SoUg6xfT97QPsubWTJx84nE7iGFAwyIkMi6urDh+JBE864PrbhSlq2o0M42F4fmFeTuUgUPDIiQyDjy8VxVepYJh7fnCxgBB1tTk4r332KEMHBoWITGxvHDKw61SwbMOXvO1tkHWULPvNCxCYuK1Ddu9DQuyJQ4FD9Kp3zv2jXXmRxUHDg2LkGhYfPtIG7eCEGTt/+I3g5Q4fHZgvx3WAKFhERINu9ZudQzIT/Nrr28ZZ8Gwzux+yQ5rgNCwCImDQ8+/0zK8gtAD+ll+cx8cx/EgldDc3KkzcGhYhETA8sIpx3r8hBgtzT0dm73Vf2HY76F5OrABQ8MiJAKEtw3WCz28v+vdtMNzHx7xDrKGlcACNCxCRh35bYM1Qg+vbdhue0w48ciWuTVqz5qbmlzadL/tYuDQsAgZdV6+ZZvjPh6CYcH4bI8JF5aWjAepsu8z0/A4VroTQsrR3jZYKvRw4OG9tsccp3e+oFoY8l5CQkglfrcNlqq0zhN/PPaNddLsexJe2ZZDgoZFyOjid9ugI/SQ5dqLfHZgvzDIwmbLr+yxzYYEDYuQESXNtbcxrLT5W5tetD1WsHiv4Mkz/d7i+lnbYHjQsAgZURBevXr7069t2O4tWFVNbJVxfmG+McjCBkOpFHWgYRFCLn3y2KM1JQ546cSD37ebDhUaFiHEZN9NfUNpiUPy9wtLS3bToULDIoQYzux+qXRhiPDq1PZn7EbDhoZFCLEs3nGbm32fmZ5fe719eQSgYRFCLMUSB1PKMOyf9spDwyKEXOH4xvvmpiatYY3Gj6fmoWERQq5wYWnJXC5Msu8Ir859eMS+MBrQsAghqzj51JPwLOiTxx61fxoZAhjWb7ZtRdy4eO89jcJmp3e+YJu1AJ3I39HjAoe8/1R+V3xPPvG4009RfuNP+ezAfsleYJvSu8zCcn5hXnVI82o5vJXDhzze2qPsCNPb6aRRbT7f7jAlDknq/eLyGfunkSGAYeGjNYk67GGTsBnM2zZrAaa6/B1PPLLFNhODQQr7N6v9fs/DsM4efE++C34Vxojq0+HVC/3jK8e26YyFm28yC43CuzfKfIKtSxZhDcKjnQnbq5LN9nl4hX5qhE8Hx2SIj2qp4czul0bQSUEYw7qSpasVPp5QhmXO/kL/RWFgHoaFCSzsP13qexgWJnDatln9HjbWRhnmQxHvAmZap2WBisGUCcNr+XsH+byMVMkvWdn2AlblqmXCfgVZcHyuoGGV0LVhVVXoVQkbq46b/P77VDhKmG+2cWiWX9mjGkyJklCxZfYX8YJ2GNhe+HAChMBqR1YaIkmhYZXQqWGZBMHlhnJh8ggXhuhf8YSjy8L+dnFrK46M6V+5syUK8agA9W9bzUwjtrWNa/ELr4b1WPSooWGV0Klh4QhIO89L/IWM/n0imm4qbsxM9tjZMmGnWmZVtIEnhO0bgyybjiy0rRFOyyE+Fj1qaFgldGdYJuJAE6+IA7OiMZXT8qdQwtY0t//xO0foTfXdUEQdCiWflG1cwaLkYVJ5JUnDEbwAFwU0rBK6Myz5yEuUnui12XdESdrF4BWF/rE5c1Z4WXOV0GHLXJv5wsDx0YwKx7zme8IjvKrvkNRDwyqhI8NqE16lwqhqavk8ljyO0DxUYsUjyS0R+myZa1MPLMlkVX1PLK6f1X1DDO8nSMcDGlYJHRkWpoq02yrV/siSolSiSoGmE9Y7ZiQtB1Oq1rk2WI/2QMHgSusP/MKrLi5ufH6gYZXQhWGZi4Mh5jAGhgNuO80RoHogETpZOXzIduqL4gDqhRG2DAPVoWhFkKUNr6o+OyKHhlVCF4alrb2qVBJkFYuSAoRXqfq9xXvvsZ16YcIr9KMajGrjECUO2uw7PjsnyPJwPfyXufaW0LBK6MKw1EVA1cLYnJ0KeT2udtUpQZ0kSoMUzcFB/y2DLHxkZpDyN02eY5e3G214hbdjrr09NKwSghuWR7KjTknld/5NW10cLKjNx6TOECX1ZfBHM35Nq5ZhINAaKzbOgiz1N4S4jI7UQ8MqIbhhLW26X3iIhMpPnpXDh0K6IVSIJuRoU2nYOE2ZeTRsebMOvFV3S0ASe6aZrJJHCdcq203SEhpWCWENq5NLZrm7RuRHQy5MML8i0mOzt8pnMoadr9JQVWA6bf3Q5qHwpojLtPEyWnmchKQUGlYJYQ1LnW5Hn4J5iz7TEEO4BFOFeNjY494RXayXmHj+0GmNABsXr9xpMcGv3O5nphGUwSgVBzP5aJhrDwUNq4SwhqVLMM1ML66flTTBCLEqPC/4zV7IHHlVIJa4iXaa4VDLZzIGUwyRVMcKO94+jY3PzoxZ4PhW2FL+aQYaJMmgYZUQ0LDwkjm/xfMB5zfWKaJbAvs9RAeS1A/2BZ8RBmNSNpqRNN7362BGIuy/ojhDl8wOVOaqvqwp1+j9iEPs0LBKCGhYuvVgLjNl1h31Y0hS4/Cshi/83K22qsGYQ6epctR6TdVMVtV/4BAFeWKn6U38pnLhgDDXHhYaVgkBDUuVSMb5nS0fMA/NGOpnEV5t6twc8yceT/uEbSnsQHCvdR7tnlYl9VU3MGFLbG9btiBw3UkijK39ZQHiQMMqIZRhmeuD8vUgNludNvJ4LFxRmIf5GKQ5cMsJbYX1mWbliybiPcUYbMsCwqycVbh7iXXZ90bhUPR77a8JEAcaVgmhDEt11Rzv6AxVe9G9RIUCS6xQ5NNS/nmpqqjwodRfglSVnmKQLQuyUmCUim+XJuFoaDOARAINq4RQhiUfJ4RT3Aln8P2sSugUVTptFKn3JE1mm9ViwhNxMGhGVVvkpV0Vlj5KwQP0I7fdOoW425GUQsMqIZRhKaoo0+cBFMoIMHi5EbjC2Mp+gkxlB5jAxVE56FJjaZ+1ayVdPVeI23RSMCpd7XuFMHjm2juChlVCEMPC2S+fdVXjbLUqrLgSp1oV4t0bS951GWuZv5gtxWFgqS/7gQMuPzilMif55ascJDg0rBKCGJbKa6p8QeV6jrALVVfQzAYyO0AnjZe6FIcr6VCygpOfVBAOkbOaboP2MQyrVBEpk1DQsEoIYliKlVd1J0B1d15eeHfEPraX1ShSToLHDGjL0yXLJV3JWLig5tyHR8y+yNy8REnDIKVhpBQaVglBDEtRl1R7bV4Vv1wRBjYzXZUqUlzUS3awJmTAS2Z4whmejkoQgGiLG0IluVXmWyqccqFyaqQIDauEIIZljolsGpvDUh0g6CrIM2EO33Gb7aKAyg4wvJqYqIsEFoCpqYobMIb2C7FQVwnRSWPij/hBwyqhvWHhL/JTv/78xjz0mEWNh9rsoMwOzPCqS4q0JQjyE+D4xvvkwQ4G2TKNFbIOK+mk/koo8YOGVUJ7w1Jl3PFe9aWPHtVY9SYI5CtW86lVB4BaW5GHHjhVpJ9CMsiqKwxCcECEp7FEGA/vy+mCKA1LbijGsPS/U9LesBQ54+R5x7ZZBYp02GXh3etz24rIqHYdp4pKMCp5Qlq3FhYvNktRVeoLhQ6DlOCTPFEalmqyefxWcHvDQlgn/boW3A0njygzYbbYxhWo8u5VFwpVK1/0o7rkj9mu2Ova+xPrsYtuZQzbrHCXAkhGlIaliF9qc89VtDcseS2PiQGbFq2nd76gMyyMqt+zjSuAHcgNC/8t7iPQBkGqh0PBR7QF9KWDbCTITealwpCyx2+QIERpWIoMEU736qv7VbQ3LPkVLrxR4zmtyogZCb7bVbl8DLJ0daNyUpwk9fc8F1HVoGF3PPLu6sVgcka5f6xSi5/zIKVEaViK6CA5j7WFfC0NC/+bvdQoDK8xD60rSkqsoTFqg4nLXRXvXuoFqtgEh1SbF1dl9NG/NpzxCOLwFvJdhjAqZt8DEqVhqRzB4zxuaViq5AvmQGPlN/o3HYrnFTaWWIO8SBIdlt5Poy2Vkl8iTFE9vcvYtPICi0k1ivtP9xSttEtIyUdMhERpWEBxw1d1zriKloalqqXElo1LBm0ggMFLPFr3wZVVNpijpDGsqluFqsBeyI8kzgfVB61daGPj1HB1tbKQcmCkhlgNq6MnZ6a0NCxVWgRb2mbVYPmGM15lDTWlnhm6crZC8KK9iod5q12b6zxF/PQugEOq+g7A4PNpQY8gS3LLN2lk0IYFL7DN2qG7PpWcyo6t1NDSsBRVF0lz26wWRUQpNuiWpVgebqJNP2MlpfVE4aesWwwWDqkdmNzv0l+NZva9NQM1LJxPS5vuxwcPu/GQfb8EfEOaDjXfkAs331R6qatIS8NSFGHlfianHlXtKGaXZPGlWHAly2rnYqvu+pp4T/MgIjNHUvwp41OTZIu0azqMoejX5rTXWB421l4kJUUGa1hQv4dzxU/OhNF+SeKtsT1GWz+Z4T6KPSozLMW1LXFtofYZxBJrxncAtnTalqssPlLYOqTPJAK8oznCYsPC7jSGljiLzLUCsftD2M2iD+JDV5kpJBkeqWfghuWromHhjJHOt0zJfTBoBR2bvRXOAiOA8A8YB+Zk+pJid8oMS/GIEnEhfheGpVjTla1oVEMy1ux164wq04TdabzacPKJx6V7nQj7iDPcNl7NSc3djkZJ9t05jYmKiA0LqIOsvDANEHNNTZrBw18gzbelVcGwMEhT7ijrqmYyOGgNS5LeRtQgn7rY0t+Xkz3FLtiWGlSVEzgZau7TBurcU9mOZ+Cz9uhNW4xG8sRtWNolQ3glb73KsDQPcjKGJXuYhOog41hJDEtVj4otnWWR6sIlJrZf/aSq2N0cz9ovAFVvUOOw1Y/QSmLVKgckjcRtWEBXqhNcBcPCv7O/N0o+jUfBsPL5F3wW2tjHL7JQXW3AljULTwxAd6rkfuW/hnTLVQ1rhc/Rb3VMQPSGBRCk6E7EgCozLHM0xIYlLEwbBcPK13apfBlCc0ktaxF1pqziIoZqZ1Nhe0nxlPpuxPRgKov+Sco4GBb+/vHdd2LyO00GoYJhqSYGxiwsTOvIsOQHDVvmZy/amsBHY1iSWtYiihoRqKz8ImXxjttUi0F0hRCy6pRzUFXJGaVXXWWdkzzjYFgp+CqWO0UwFQzrnObG7OEalgkGNYaVX9PZ7HVhsyoZw/KKKVS3NFiXKSzi1JmmZMDykFBxvfWysFOh7vr4XDE+hgVskkL1XddSSW1X3rBURYloG5Fh5YequsIIYWM/w9JWeyFyyX8cQBsMGunv/lNdMzVKsu/CSmaSMVaGBTCRcKqZuaQ6QbW6XM+F6YFv7/zAFNWYsRlWvmJAWyyOjf0MC19CKsOCnH1f1D+s3WO0WgeHMCpm37WMm2GlLL+yB0sDcwJpv1prlEwGTJ70vMQKtLRqWZWCjcmwVldgaBdB2Lj0cDWiNSyMM7/vPheRlU9GzdDeEQ1hbH6pvc8tAQwrTR4NQHLDSsGpkJ5DaGtOeg/zwvbJoi/t4djsrZi0+O51Fh150nSJXPWFjhnYEadhvSSGhW2cVvXKhwOpL6vkbVhOP43K1lnaHczkVJwJwVnh9CMRzqvGygmSEcCw8OniXByA7PspwWkEi4Ev4Gszu/kmO1fyyr+UCp4Fp8CcwbujH4ljYpLkx9woYRZDe5AlQ8U2Tqt65e/BxNFwXm2U37TUHk8oeyOPtqnS5h7gEDldSVTz/UccAhhWXGCWwiNgYVgsQAiIoPTfiBqM3tyHcwjnujagI4R0zefOsAgh8ULDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREwqVL/w8KSyqJVFfZUwAAAABJRU5ErkJggg==

			$image_array_1 = explode(";", $data);

			//base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAC0CAIAAAA1l+0PAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABk3SURBVHhe7Z1/iF5VesfnjyJM/3hLWJyhL13WQWGlKETYLVKEsF0WFTrSTZBaRIpdVrZZ2SZl0W4bdV3sgtsqMSC1RurWZNsSXImQqFXYCOLG/CErWUhapdHJTDNm4saEDJhJ0qTfc+/JzZ1zfz3Puee+73tevx++SJz3nvOee997vu9znvvc+05cIoSQSKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhETI+nF+Yv7h8xv7POELDImR8WFw/e/KJx+3/jCM0LELGhM8O7P9oYmJuzQTiLPunsYOGRciYsHDzTUdnpuemJo9vvM/+aeygYREyDpze+QLCq6PX9iH8A9GWfWG8oGEREj0XlpaMVc1Mp4aFfyDasq+NFzQsQqLnk8cenVtjw6tUCLIQc9mXxwgaFiFxs3L4ULYYvKKZafxx/EocaFiExM3Hd995tN9zDevaPmKuEw9+3240LtCwCImY5Tf3lYRXlwXPOnvwPbvpWEDDIiRijDFlufai+r3F9bN20yaWF04tvn2kpS6urNjuuoGGRUisnNr+TE14lQobnNn9km1Qzb5v/+z5iQdaatfarba7zqBhERIlF5aW5qYm68KrVMkG9dn3V29/GnazY+KhNkIPpz44YXvsDBoWIVFy4pEtTilDlbDZJ489apsVeH/Xu0Hc6sDDe22PXULDIiQ+Vg4fMm7VGF5dFhaGpTcYXlxZcazHW8sLp2ynXULDIiQ+qkoZKtXvoYltnANhUZDw6tDz79geO4aGRUhkLL+ypzHXXhSaLL+5z3aRgJiovVtBL9+yreuLgxk0LEJiAtYwv/Z6+WLwipIbDPPO8tqG7UHCq8W3j9geu4eGRUhMSEoZqoSGaJ72A5cJ4lZwvbTDwUDDIiQaLi6fMW7lEV6lQsN+78LSErpyrMdPMKzB5NozaFiERINf9iqv9AbDQ8+/EyS8GkwpQx4aFiHRcHrnC8LaqyrNTU3+719896cTP3Tcx08Dy7Vn0LAIiYazB99rFWHNTM+vvf6tu//J8R0PIbx6f9e7dlgDhIZFSEyoK7ByQnR25L6HgoRXL9+yzQ5osNCwCImJ8wvznkFWUtbw6u1PO9bjIYRXgyxlyEPDIiQyfrNtq4dnzU1NHv7uk+3DK7jVvm//zA5l4NCwCImMiysr6S96OZZUp+TBWLvWbnXcx0MwrAGXMuShYRESH/UPGi1qbs1EqFz74EsZ8tCwCImS4xvvM8/DKnhTUWFLGezbDwkaFiFRYh7gJ3nCTFLKECrXPpRShjw0LEJiJf2154+u7tcI4VWoUoa9f/Kv9o2HBw2LkGGw/yeXXt/cXmd/PN2o3Tf8yLEeR//8O3+N6Mn5oyNY3qfb73fe3dWRN+zedQYNi5BhcPy9S89MXHqutf5lqk47rvrvv/rzmvAKL/3N7215rv/1/V/8Zv1mv/rOJvTmvrsj7NHr37E72A00LEKGBOY2JrljMUF1/tkv19jQ36/5u80zD/7gS5tenLrh6LX9+iALXTmdlwue9atn7Q52AA2LkCFx7tNuDWvHVb/csLnUsFKrSrXzd9cZw5qZPnjN10o3xh8Rppnwyum/Stgp7Fo30LAIGR4IRhCSOBM+kJa3frVoQPgL1oCZWz1147devvq6NMI62u/t+cLGYpPdN/xI4VYQDKuzhSENi5ChYR7P8h9/4E74INpx1d6v/K1jPfnAKhWsKlVaAPHBdTc6TeBfS//4pzrDguDCxzv5iXwaFiFD5cgb4YOsHVedfOCufKxUtCooC6+sYSVB1i+m78k3fOO2h9RuBSHIghF3AA2LkGGze13YZNb5Z7+cZdDhPo5PZcrCqyuGlZShZm6FtlhXOp1L1U32nYZFyLBJSxycCe+tHVf9cvbONErKp6scPdf/eolhJUHWr2/YgOaQLWVw+perg+w7DYuQEeD1zaGCrDTXXroGzPSDL23KFoOuYSWe9fPJTfC7tvk1uPD+n9gdDAQNi5ARIFyJA8Krp278Vr3SUoZKw0pKHP7nHx5uFV6lwk4Fzb7TsAgZDUKVOKCTv5y49Md1+q+v9OsMK3nAw2f/+e8BrmDCsIKWONCwCBkZApY4wLbu/C3Hp/KqWxJCM9OL62fNgq593IeRhLvHkIZFyMgQusTh7I+nq2zr0z/q1RlW9jPRQTw0XIkDDYuQEeL/XrorVDLrir73245bpTrw+9fUGBaCrPm11wcLsgKVONCwCBklwpY4ZEKfhcRWPshy3SrR3NTkiUe2mDIxpzcPYQAhShxoWISMGK9v7sSzIHS7eoWYZd8dq8qEheHZvU8GCLJM9n2z3cEW0LAIGTEQiXRkWImcxNaVm59LlWTfwyxUsVOtSxxoWISMHl0+xcHqcmIrXRi6PpVXv2eeNRqiJssYXztoWISMJLvXde5Z6D+xLQRZrknllfxkdLAgq12JAw2LkFElvUKHSV4lvIrAp6WemTj9Z72Pri74VF793skH7ko3bivYVovsOw2LkFi5uLLyb1dva9Svb9gw/9U/rBfCKNekVmtuavLswfeM17RXC2hYhMTKW5tebPy1G+jnk5uMH9Wr4FCu+r2P777TvvHwoGEREiWnPjghcSvopxM/RJAFx3E9SKmPJiaW39xn335I0LAIiRLtjzk77uMjBGL9nn37IUHDIiQ+3t/1rjC8SoUg6xfT97QPsubWTJx84nE7iGFAwyIkMi6urDh+JBE864PrbhSlq2o0M42F4fmFeTuUgUPDIiQyDjy8VxVepYJh7fnCxgBB1tTk4r332KEMHBoWITGxvHDKw61SwbMOXvO1tkHWULPvNCxCYuK1Ddu9DQuyJQ4FD9Kp3zv2jXXmRxUHDg2LkGhYfPtIG7eCEGTt/+I3g5Q4fHZgvx3WAKFhERINu9ZudQzIT/Nrr28ZZ8Gwzux+yQ5rgNCwCImDQ8+/0zK8gtAD+ll+cx8cx/EgldDc3KkzcGhYhETA8sIpx3r8hBgtzT0dm73Vf2HY76F5OrABQ8MiJAKEtw3WCz28v+vdtMNzHx7xDrKGlcACNCxCRh35bYM1Qg+vbdhue0w48ciWuTVqz5qbmlzadL/tYuDQsAgZdV6+ZZvjPh6CYcH4bI8JF5aWjAepsu8z0/A4VroTQsrR3jZYKvRw4OG9tsccp3e+oFoY8l5CQkglfrcNlqq0zhN/PPaNddLsexJe2ZZDgoZFyOjid9ugI/SQ5dqLfHZgvzDIwmbLr+yxzYYEDYuQESXNtbcxrLT5W5tetD1WsHiv4Mkz/d7i+lnbYHjQsAgZURBevXr7069t2O4tWFVNbJVxfmG+McjCBkOpFHWgYRFCLn3y2KM1JQ546cSD37ebDhUaFiHEZN9NfUNpiUPy9wtLS3bToULDIoQYzux+qXRhiPDq1PZn7EbDhoZFCLEs3nGbm32fmZ5fe719eQSgYRFCLMUSB1PKMOyf9spDwyKEXOH4xvvmpiatYY3Gj6fmoWERQq5wYWnJXC5Msu8Ir859eMS+MBrQsAghqzj51JPwLOiTxx61fxoZAhjWb7ZtRdy4eO89jcJmp3e+YJu1AJ3I39HjAoe8/1R+V3xPPvG4009RfuNP+ezAfsleYJvSu8zCcn5hXnVI82o5vJXDhzze2qPsCNPb6aRRbT7f7jAlDknq/eLyGfunkSGAYeGjNYk67GGTsBnM2zZrAaa6/B1PPLLFNhODQQr7N6v9fs/DsM4efE++C34Vxojq0+HVC/3jK8e26YyFm28yC43CuzfKfIKtSxZhDcKjnQnbq5LN9nl4hX5qhE8Hx2SIj2qp4czul0bQSUEYw7qSpasVPp5QhmXO/kL/RWFgHoaFCSzsP13qexgWJnDatln9HjbWRhnmQxHvAmZap2WBisGUCcNr+XsH+byMVMkvWdn2AlblqmXCfgVZcHyuoGGV0LVhVVXoVQkbq46b/P77VDhKmG+2cWiWX9mjGkyJklCxZfYX8YJ2GNhe+HAChMBqR1YaIkmhYZXQqWGZBMHlhnJh8ggXhuhf8YSjy8L+dnFrK46M6V+5syUK8agA9W9bzUwjtrWNa/ELr4b1WPSooWGV0Klh4QhIO89L/IWM/n0imm4qbsxM9tjZMmGnWmZVtIEnhO0bgyybjiy0rRFOyyE+Fj1qaFgldGdYJuJAE6+IA7OiMZXT8qdQwtY0t//xO0foTfXdUEQdCiWflG1cwaLkYVJ5JUnDEbwAFwU0rBK6Myz5yEuUnui12XdESdrF4BWF/rE5c1Z4WXOV0GHLXJv5wsDx0YwKx7zme8IjvKrvkNRDwyqhI8NqE16lwqhqavk8ljyO0DxUYsUjyS0R+myZa1MPLMlkVX1PLK6f1X1DDO8nSMcDGlYJHRkWpoq02yrV/siSolSiSoGmE9Y7ZiQtB1Oq1rk2WI/2QMHgSusP/MKrLi5ufH6gYZXQhWGZi4Mh5jAGhgNuO80RoHogETpZOXzIduqL4gDqhRG2DAPVoWhFkKUNr6o+OyKHhlVCF4alrb2qVBJkFYuSAoRXqfq9xXvvsZ16YcIr9KMajGrjECUO2uw7PjsnyPJwPfyXufaW0LBK6MKw1EVA1cLYnJ0KeT2udtUpQZ0kSoMUzcFB/y2DLHxkZpDyN02eY5e3G214hbdjrr09NKwSghuWR7KjTknld/5NW10cLKjNx6TOECX1ZfBHM35Nq5ZhINAaKzbOgiz1N4S4jI7UQ8MqIbhhLW26X3iIhMpPnpXDh0K6IVSIJuRoU2nYOE2ZeTRsebMOvFV3S0ASe6aZrJJHCdcq203SEhpWCWENq5NLZrm7RuRHQy5MML8i0mOzt8pnMoadr9JQVWA6bf3Q5qHwpojLtPEyWnmchKQUGlYJYQ1LnW5Hn4J5iz7TEEO4BFOFeNjY494RXayXmHj+0GmNABsXr9xpMcGv3O5nphGUwSgVBzP5aJhrDwUNq4SwhqVLMM1ML66flTTBCLEqPC/4zV7IHHlVIJa4iXaa4VDLZzIGUwyRVMcKO94+jY3PzoxZ4PhW2FL+aQYaJMmgYZUQ0LDwkjm/xfMB5zfWKaJbAvs9RAeS1A/2BZ8RBmNSNpqRNN7362BGIuy/ojhDl8wOVOaqvqwp1+j9iEPs0LBKCGhYuvVgLjNl1h31Y0hS4/Cshi/83K22qsGYQ6epctR6TdVMVtV/4BAFeWKn6U38pnLhgDDXHhYaVgkBDUuVSMb5nS0fMA/NGOpnEV5t6twc8yceT/uEbSnsQHCvdR7tnlYl9VU3MGFLbG9btiBw3UkijK39ZQHiQMMqIZRhmeuD8vUgNludNvJ4LFxRmIf5GKQ5cMsJbYX1mWbliybiPcUYbMsCwqycVbh7iXXZ90bhUPR77a8JEAcaVgmhDEt11Rzv6AxVe9G9RIUCS6xQ5NNS/nmpqqjwodRfglSVnmKQLQuyUmCUim+XJuFoaDOARAINq4RQhiUfJ4RT3Aln8P2sSugUVTptFKn3JE1mm9ViwhNxMGhGVVvkpV0Vlj5KwQP0I7fdOoW425GUQsMqIZRhKaoo0+cBFMoIMHi5EbjC2Mp+gkxlB5jAxVE56FJjaZ+1ayVdPVeI23RSMCpd7XuFMHjm2juChlVCEMPC2S+fdVXjbLUqrLgSp1oV4t0bS951GWuZv5gtxWFgqS/7gQMuPzilMif55ascJDg0rBKCGJbKa6p8QeV6jrALVVfQzAYyO0AnjZe6FIcr6VCygpOfVBAOkbOaboP2MQyrVBEpk1DQsEoIYliKlVd1J0B1d15eeHfEPraX1ShSToLHDGjL0yXLJV3JWLig5tyHR8y+yNy8REnDIKVhpBQaVglBDEtRl1R7bV4Vv1wRBjYzXZUqUlzUS3awJmTAS2Z4whmejkoQgGiLG0IluVXmWyqccqFyaqQIDauEIIZljolsGpvDUh0g6CrIM2EO33Gb7aKAyg4wvJqYqIsEFoCpqYobMIb2C7FQVwnRSWPij/hBwyqhvWHhL/JTv/78xjz0mEWNh9rsoMwOzPCqS4q0JQjyE+D4xvvkwQ4G2TKNFbIOK+mk/koo8YOGVUJ7w1Jl3PFe9aWPHtVY9SYI5CtW86lVB4BaW5GHHjhVpJ9CMsiqKwxCcECEp7FEGA/vy+mCKA1LbijGsPS/U9LesBQ54+R5x7ZZBYp02GXh3etz24rIqHYdp4pKMCp5Qlq3FhYvNktRVeoLhQ6DlOCTPFEalmqyefxWcHvDQlgn/boW3A0njygzYbbYxhWo8u5VFwpVK1/0o7rkj9mu2Ova+xPrsYtuZQzbrHCXAkhGlIaliF9qc89VtDcseS2PiQGbFq2nd76gMyyMqt+zjSuAHcgNC/8t7iPQBkGqh0PBR7QF9KWDbCTITealwpCyx2+QIERpWIoMEU736qv7VbQ3LPkVLrxR4zmtyogZCb7bVbl8DLJ0daNyUpwk9fc8F1HVoGF3PPLu6sVgcka5f6xSi5/zIKVEaViK6CA5j7WFfC0NC/+bvdQoDK8xD60rSkqsoTFqg4nLXRXvXuoFqtgEh1SbF1dl9NG/NpzxCOLwFvJdhjAqZt8DEqVhqRzB4zxuaViq5AvmQGPlN/o3HYrnFTaWWIO8SBIdlt5Poy2Vkl8iTFE9vcvYtPICi0k1ivtP9xSttEtIyUdMhERpWEBxw1d1zriKloalqqXElo1LBm0ggMFLPFr3wZVVNpijpDGsqluFqsBeyI8kzgfVB61daGPj1HB1tbKQcmCkhlgNq6MnZ6a0NCxVWgRb2mbVYPmGM15lDTWlnhm6crZC8KK9iod5q12b6zxF/PQugEOq+g7A4PNpQY8gS3LLN2lk0IYFL7DN2qG7PpWcyo6t1NDSsBRVF0lz26wWRUQpNuiWpVgebqJNP2MlpfVE4aesWwwWDqkdmNzv0l+NZva9NQM1LJxPS5vuxwcPu/GQfb8EfEOaDjXfkAs331R6qatIS8NSFGHlfianHlXtKGaXZPGlWHAly2rnYqvu+pp4T/MgIjNHUvwp41OTZIu0azqMoejX5rTXWB421l4kJUUGa1hQv4dzxU/OhNF+SeKtsT1GWz+Z4T6KPSozLMW1LXFtofYZxBJrxncAtnTalqssPlLYOqTPJAK8oznCYsPC7jSGljiLzLUCsftD2M2iD+JDV5kpJBkeqWfghuWromHhjJHOt0zJfTBoBR2bvRXOAiOA8A8YB+Zk+pJid8oMS/GIEnEhfheGpVjTla1oVEMy1ux164wq04TdabzacPKJx6V7nQj7iDPcNl7NSc3djkZJ9t05jYmKiA0LqIOsvDANEHNNTZrBw18gzbelVcGwMEhT7ijrqmYyOGgNS5LeRtQgn7rY0t+Xkz3FLtiWGlSVEzgZau7TBurcU9mOZ+Cz9uhNW4xG8sRtWNolQ3glb73KsDQPcjKGJXuYhOog41hJDEtVj4otnWWR6sIlJrZf/aSq2N0cz9ovAFVvUOOw1Y/QSmLVKgckjcRtWEBXqhNcBcPCv7O/N0o+jUfBsPL5F3wW2tjHL7JQXW3AljULTwxAd6rkfuW/hnTLVQ1rhc/Rb3VMQPSGBRCk6E7EgCozLHM0xIYlLEwbBcPK13apfBlCc0ktaxF1pqziIoZqZ1Nhe0nxlPpuxPRgKov+Sco4GBb+/vHdd2LyO00GoYJhqSYGxiwsTOvIsOQHDVvmZy/amsBHY1iSWtYiihoRqKz8ImXxjttUi0F0hRCy6pRzUFXJGaVXXWWdkzzjYFgp+CqWO0UwFQzrnObG7OEalgkGNYaVX9PZ7HVhsyoZw/KKKVS3NFiXKSzi1JmmZMDykFBxvfWysFOh7vr4XDE+hgVskkL1XddSSW1X3rBURYloG5Fh5YequsIIYWM/w9JWeyFyyX8cQBsMGunv/lNdMzVKsu/CSmaSMVaGBTCRcKqZuaQ6QbW6XM+F6YFv7/zAFNWYsRlWvmJAWyyOjf0MC19CKsOCnH1f1D+s3WO0WgeHMCpm37WMm2GlLL+yB0sDcwJpv1prlEwGTJ70vMQKtLRqWZWCjcmwVldgaBdB2Lj0cDWiNSyMM7/vPheRlU9GzdDeEQ1hbH6pvc8tAQwrTR4NQHLDSsGpkJ5DaGtOeg/zwvbJoi/t4djsrZi0+O51Fh150nSJXPWFjhnYEadhvSSGhW2cVvXKhwOpL6vkbVhOP43K1lnaHczkVJwJwVnh9CMRzqvGygmSEcCw8OniXByA7PspwWkEi4Ev4Gszu/kmO1fyyr+UCp4Fp8CcwbujH4ljYpLkx9woYRZDe5AlQ8U2Tqt65e/BxNFwXm2U37TUHk8oeyOPtqnS5h7gEDldSVTz/UccAhhWXGCWwiNgYVgsQAiIoPTfiBqM3tyHcwjnujagI4R0zefOsAgh8ULDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREwqVL/w8KSyqJVFfZUwAAAABJRU5ErkJggg==

			$image_array_2 = explode(",", $image_array_1[1]);

			//iVBORw0KGgoAAAANSUhEUgAAAZAAAAC0CAIAAAA1l+0PAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABk3SURBVHhe7Z1/iF5VesfnjyJM/3hLWJyhL13WQWGlKETYLVKEsF0WFTrSTZBaRIpdVrZZ2SZl0W4bdV3sgtsqMSC1RurWZNsSXImQqFXYCOLG/CErWUhapdHJTDNm4saEDJhJ0qTfc+/JzZ1zfz3Puee+73tevx++SJz3nvOee997vu9znvvc+05cIoSQSKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhEUKigYZFCIkGGhYhJBpoWISQaKBhETI+nF+Yv7h8xv7POELDImR8WFw/e/KJx+3/jCM0LELGhM8O7P9oYmJuzQTiLPunsYOGRciYsHDzTUdnpuemJo9vvM/+aeygYREyDpze+QLCq6PX9iH8A9GWfWG8oGEREj0XlpaMVc1Mp4aFfyDasq+NFzQsQqLnk8cenVtjw6tUCLIQc9mXxwgaFiFxs3L4ULYYvKKZafxx/EocaFiExM3Hd995tN9zDevaPmKuEw9+3240LtCwCImY5Tf3lYRXlwXPOnvwPbvpWEDDIiRijDFlufai+r3F9bN20yaWF04tvn2kpS6urNjuuoGGRUisnNr+TE14lQobnNn9km1Qzb5v/+z5iQdaatfarba7zqBhERIlF5aW5qYm68KrVMkG9dn3V29/GnazY+KhNkIPpz44YXvsDBoWIVFy4pEtTilDlbDZJ489apsVeH/Xu0Hc6sDDe22PXULDIiQ+Vg4fMm7VGF5dFhaGpTcYXlxZcazHW8sLp2ynXULDIiQ+qkoZKtXvoYltnANhUZDw6tDz79geO4aGRUhkLL+ypzHXXhSaLL+5z3aRgJiovVtBL9+yreuLgxk0LEJiAtYwv/Z6+WLwipIbDPPO8tqG7UHCq8W3j9geu4eGRUhMSEoZqoSGaJ72A5cJ4lZwvbTDwUDDIiQaLi6fMW7lEV6lQsN+78LSErpyrMdPMKzB5NozaFiERINf9iqv9AbDQ8+/EyS8GkwpQx4aFiHRcHrnC8LaqyrNTU3+719896cTP3Tcx08Dy7Vn0LAIiYazB99rFWHNTM+vvf6tu//J8R0PIbx6f9e7dlgDhIZFSEyoK7ByQnR25L6HgoRXL9+yzQ5osNCwCImJ8wvznkFWUtbw6u1PO9bjIYRXgyxlyEPDIiQyfrNtq4dnzU1NHv7uk+3DK7jVvm//zA5l4NCwCImMiysr6S96OZZUp+TBWLvWbnXcx0MwrAGXMuShYRESH/UPGi1qbs1EqFz74EsZ8tCwCImS4xvvM8/DKnhTUWFLGezbDwkaFiFRYh7gJ3nCTFLKECrXPpRShjw0LEJiJf2154+u7tcI4VWoUoa9f/Kv9o2HBw2LkGGw/yeXXt/cXmd/PN2o3Tf8yLEeR//8O3+N6Mn5oyNY3qfb73fe3dWRN+zedQYNi5BhcPy9S89MXHqutf5lqk47rvrvv/rzmvAKL/3N7215rv/1/V/8Zv1mv/rOJvTmvrsj7NHr37E72A00LEKGBOY2JrljMUF1/tkv19jQ36/5u80zD/7gS5tenLrh6LX9+iALXTmdlwue9atn7Q52AA2LkCFx7tNuDWvHVb/csLnUsFKrSrXzd9cZw5qZPnjN10o3xh8Rppnwyum/Stgp7Fo30LAIGR4IRhCSOBM+kJa3frVoQPgL1oCZWz1147devvq6NMI62u/t+cLGYpPdN/xI4VYQDKuzhSENi5ChYR7P8h9/4E74INpx1d6v/K1jPfnAKhWsKlVaAPHBdTc6TeBfS//4pzrDguDCxzv5iXwaFiFD5cgb4YOsHVedfOCufKxUtCooC6+sYSVB1i+m78k3fOO2h9RuBSHIghF3AA2LkGGze13YZNb5Z7+cZdDhPo5PZcrCqyuGlZShZm6FtlhXOp1L1U32nYZFyLBJSxycCe+tHVf9cvbONErKp6scPdf/eolhJUHWr2/YgOaQLWVw+perg+w7DYuQEeD1zaGCrDTXXroGzPSDL23KFoOuYSWe9fPJTfC7tvk1uPD+n9gdDAQNi5ARIFyJA8Krp278Vr3SUoZKw0pKHP7nHx5uFV6lwk4Fzb7TsAgZDUKVOKCTv5y49Md1+q+v9OsMK3nAw2f/+e8BrmDCsIKWONCwCBkZApY4wLbu/C3Hp/KqWxJCM9OL62fNgq593IeRhLvHkIZFyMgQusTh7I+nq2zr0z/q1RlW9jPRQTw0XIkDDYuQEeL/XrorVDLrir73245bpTrw+9fUGBaCrPm11wcLsgKVONCwCBklwpY4ZEKfhcRWPshy3SrR3NTkiUe2mDIxpzcPYQAhShxoWISMGK9v7sSzIHS7eoWYZd8dq8qEheHZvU8GCLJM9n2z3cEW0LAIGTEQiXRkWImcxNaVm59LlWTfwyxUsVOtSxxoWISMHl0+xcHqcmIrXRi6PpVXv2eeNRqiJssYXztoWISMJLvXde5Z6D+xLQRZrknllfxkdLAgq12JAw2LkFElvUKHSV4lvIrAp6WemTj9Z72Pri74VF793skH7ko3bivYVovsOw2LkFi5uLLyb1dva9Svb9gw/9U/rBfCKNekVmtuavLswfeM17RXC2hYhMTKW5tebPy1G+jnk5uMH9Wr4FCu+r2P777TvvHwoGEREiWnPjghcSvopxM/RJAFx3E9SKmPJiaW39xn335I0LAIiRLtjzk77uMjBGL9nn37IUHDIiQ+3t/1rjC8SoUg6xfT97QPsubWTJx84nE7iGFAwyIkMi6urDh+JBE864PrbhSlq2o0M42F4fmFeTuUgUPDIiQyDjy8VxVepYJh7fnCxgBB1tTk4r332KEMHBoWITGxvHDKw61SwbMOXvO1tkHWULPvNCxCYuK1Ddu9DQuyJQ4FD9Kp3zv2jXXmRxUHDg2LkGhYfPtIG7eCEGTt/+I3g5Q4fHZgvx3WAKFhERINu9ZudQzIT/Nrr28ZZ8Gwzux+yQ5rgNCwCImDQ8+/0zK8gtAD+ll+cx8cx/EgldDc3KkzcGhYhETA8sIpx3r8hBgtzT0dm73Vf2HY76F5OrABQ8MiJAKEtw3WCz28v+vdtMNzHx7xDrKGlcACNCxCRh35bYM1Qg+vbdhue0w48ciWuTVqz5qbmlzadL/tYuDQsAgZdV6+ZZvjPh6CYcH4bI8JF5aWjAepsu8z0/A4VroTQsrR3jZYKvRw4OG9tsccp3e+oFoY8l5CQkglfrcNlqq0zhN/PPaNddLsexJe2ZZDgoZFyOjid9ugI/SQ5dqLfHZgvzDIwmbLr+yxzYYEDYuQESXNtbcxrLT5W5tetD1WsHiv4Mkz/d7i+lnbYHjQsAgZURBevXr7069t2O4tWFVNbJVxfmG+McjCBkOpFHWgYRFCLn3y2KM1JQ546cSD37ebDhUaFiHEZN9NfUNpiUPy9wtLS3bToULDIoQYzux+qXRhiPDq1PZn7EbDhoZFCLEs3nGbm32fmZ5fe719eQSgYRFCLMUSB1PKMOyf9spDwyKEXOH4xvvmpiatYY3Gj6fmoWERQq5wYWnJXC5Msu8Ir859eMS+MBrQsAghqzj51JPwLOiTxx61fxoZAhjWb7ZtRdy4eO89jcJmp3e+YJu1AJ3I39HjAoe8/1R+V3xPPvG4009RfuNP+ezAfsleYJvSu8zCcn5hXnVI82o5vJXDhzze2qPsCNPb6aRRbT7f7jAlDknq/eLyGfunkSGAYeGjNYk67GGTsBnM2zZrAaa6/B1PPLLFNhODQQr7N6v9fs/DsM4efE++C34Vxojq0+HVC/3jK8e26YyFm28yC43CuzfKfIKtSxZhDcKjnQnbq5LN9nl4hX5qhE8Hx2SIj2qp4czul0bQSUEYw7qSpasVPp5QhmXO/kL/RWFgHoaFCSzsP13qexgWJnDatln9HjbWRhnmQxHvAmZap2WBisGUCcNr+XsH+byMVMkvWdn2AlblqmXCfgVZcHyuoGGV0LVhVVXoVQkbq46b/P77VDhKmG+2cWiWX9mjGkyJklCxZfYX8YJ2GNhe+HAChMBqR1YaIkmhYZXQqWGZBMHlhnJh8ggXhuhf8YSjy8L+dnFrK46M6V+5syUK8agA9W9bzUwjtrWNa/ELr4b1WPSooWGV0Klh4QhIO89L/IWM/n0imm4qbsxM9tjZMmGnWmZVtIEnhO0bgyybjiy0rRFOyyE+Fj1qaFgldGdYJuJAE6+IA7OiMZXT8qdQwtY0t//xO0foTfXdUEQdCiWflG1cwaLkYVJ5JUnDEbwAFwU0rBK6Myz5yEuUnui12XdESdrF4BWF/rE5c1Z4WXOV0GHLXJv5wsDx0YwKx7zme8IjvKrvkNRDwyqhI8NqE16lwqhqavk8ljyO0DxUYsUjyS0R+myZa1MPLMlkVX1PLK6f1X1DDO8nSMcDGlYJHRkWpoq02yrV/siSolSiSoGmE9Y7ZiQtB1Oq1rk2WI/2QMHgSusP/MKrLi5ufH6gYZXQhWGZi4Mh5jAGhgNuO80RoHogETpZOXzIduqL4gDqhRG2DAPVoWhFkKUNr6o+OyKHhlVCF4alrb2qVBJkFYuSAoRXqfq9xXvvsZ16YcIr9KMajGrjECUO2uw7PjsnyPJwPfyXufaW0LBK6MKw1EVA1cLYnJ0KeT2udtUpQZ0kSoMUzcFB/y2DLHxkZpDyN02eY5e3G214hbdjrr09NKwSghuWR7KjTknld/5NW10cLKjNx6TOECX1ZfBHM35Nq5ZhINAaKzbOgiz1N4S4jI7UQ8MqIbhhLW26X3iIhMpPnpXDh0K6IVSIJuRoU2nYOE2ZeTRsebMOvFV3S0ASe6aZrJJHCdcq203SEhpWCWENq5NLZrm7RuRHQy5MML8i0mOzt8pnMoadr9JQVWA6bf3Q5qHwpojLtPEyWnmchKQUGlYJYQ1LnW5Hn4J5iz7TEEO4BFOFeNjY494RXayXmHj+0GmNABsXr9xpMcGv3O5nphGUwSgVBzP5aJhrDwUNq4SwhqVLMM1ML66flTTBCLEqPC/4zV7IHHlVIJa4iXaa4VDLZzIGUwyRVMcKO94+jY3PzoxZ4PhW2FL+aQYaJMmgYZUQ0LDwkjm/xfMB5zfWKaJbAvs9RAeS1A/2BZ8RBmNSNpqRNN7362BGIuy/ojhDl8wOVOaqvqwp1+j9iEPs0LBKCGhYuvVgLjNl1h31Y0hS4/Cshi/83K22qsGYQ6epctR6TdVMVtV/4BAFeWKn6U38pnLhgDDXHhYaVgkBDUuVSMb5nS0fMA/NGOpnEV5t6twc8yceT/uEbSnsQHCvdR7tnlYl9VU3MGFLbG9btiBw3UkijK39ZQHiQMMqIZRhmeuD8vUgNludNvJ4LFxRmIf5GKQ5cMsJbYX1mWbliybiPcUYbMsCwqycVbh7iXXZ90bhUPR77a8JEAcaVgmhDEt11Rzv6AxVe9G9RIUCS6xQ5NNS/nmpqqjwodRfglSVnmKQLQuyUmCUim+XJuFoaDOARAINq4RQhiUfJ4RT3Aln8P2sSugUVTptFKn3JE1mm9ViwhNxMGhGVVvkpV0Vlj5KwQP0I7fdOoW425GUQsMqIZRhKaoo0+cBFMoIMHi5EbjC2Mp+gkxlB5jAxVE56FJjaZ+1ayVdPVeI23RSMCpd7XuFMHjm2juChlVCEMPC2S+fdVXjbLUqrLgSp1oV4t0bS951GWuZv5gtxWFgqS/7gQMuPzilMif55ascJDg0rBKCGJbKa6p8QeV6jrALVVfQzAYyO0AnjZe6FIcr6VCygpOfVBAOkbOaboP2MQyrVBEpk1DQsEoIYliKlVd1J0B1d15eeHfEPraX1ShSToLHDGjL0yXLJV3JWLig5tyHR8y+yNy8REnDIKVhpBQaVglBDEtRl1R7bV4Vv1wRBjYzXZUqUlzUS3awJmTAS2Z4whmejkoQgGiLG0IluVXmWyqccqFyaqQIDauEIIZljolsGpvDUh0g6CrIM2EO33Gb7aKAyg4wvJqYqIsEFoCpqYobMIb2C7FQVwnRSWPij/hBwyqhvWHhL/JTv/78xjz0mEWNh9rsoMwOzPCqS4q0JQjyE+D4xvvkwQ4G2TKNFbIOK+mk/koo8YOGVUJ7w1Jl3PFe9aWPHtVY9SYI5CtW86lVB4BaW5GHHjhVpJ9CMsiqKwxCcECEp7FEGA/vy+mCKA1LbijGsPS/U9LesBQ54+R5x7ZZBYp02GXh3etz24rIqHYdp4pKMCp5Qlq3FhYvNktRVeoLhQ6DlOCTPFEalmqyefxWcHvDQlgn/boW3A0njygzYbbYxhWo8u5VFwpVK1/0o7rkj9mu2Ova+xPrsYtuZQzbrHCXAkhGlIaliF9qc89VtDcseS2PiQGbFq2nd76gMyyMqt+zjSuAHcgNC/8t7iPQBkGqh0PBR7QF9KWDbCTITealwpCyx2+QIERpWIoMEU736qv7VbQ3LPkVLrxR4zmtyogZCb7bVbl8DLJ0daNyUpwk9fc8F1HVoGF3PPLu6sVgcka5f6xSi5/zIKVEaViK6CA5j7WFfC0NC/+bvdQoDK8xD60rSkqsoTFqg4nLXRXvXuoFqtgEh1SbF1dl9NG/NpzxCOLwFvJdhjAqZt8DEqVhqRzB4zxuaViq5AvmQGPlN/o3HYrnFTaWWIO8SBIdlt5Poy2Vkl8iTFE9vcvYtPICi0k1ivtP9xSttEtIyUdMhERpWEBxw1d1zriKloalqqXElo1LBm0ggMFLPFr3wZVVNpijpDGsqluFqsBeyI8kzgfVB61daGPj1HB1tbKQcmCkhlgNq6MnZ6a0NCxVWgRb2mbVYPmGM15lDTWlnhm6crZC8KK9iod5q12b6zxF/PQugEOq+g7A4PNpQY8gS3LLN2lk0IYFL7DN2qG7PpWcyo6t1NDSsBRVF0lz26wWRUQpNuiWpVgebqJNP2MlpfVE4aesWwwWDqkdmNzv0l+NZva9NQM1LJxPS5vuxwcPu/GQfb8EfEOaDjXfkAs331R6qatIS8NSFGHlfianHlXtKGaXZPGlWHAly2rnYqvu+pp4T/MgIjNHUvwp41OTZIu0azqMoejX5rTXWB421l4kJUUGa1hQv4dzxU/OhNF+SeKtsT1GWz+Z4T6KPSozLMW1LXFtofYZxBJrxncAtnTalqssPlLYOqTPJAK8oznCYsPC7jSGljiLzLUCsftD2M2iD+JDV5kpJBkeqWfghuWromHhjJHOt0zJfTBoBR2bvRXOAiOA8A8YB+Zk+pJid8oMS/GIEnEhfheGpVjTla1oVEMy1ux164wq04TdabzacPKJx6V7nQj7iDPcNl7NSc3djkZJ9t05jYmKiA0LqIOsvDANEHNNTZrBw18gzbelVcGwMEhT7ijrqmYyOGgNS5LeRtQgn7rY0t+Xkz3FLtiWGlSVEzgZau7TBurcU9mOZ+Cz9uhNW4xG8sRtWNolQ3glb73KsDQPcjKGJXuYhOog41hJDEtVj4otnWWR6sIlJrZf/aSq2N0cz9ovAFVvUOOw1Y/QSmLVKgckjcRtWEBXqhNcBcPCv7O/N0o+jUfBsPL5F3wW2tjHL7JQXW3AljULTwxAd6rkfuW/hnTLVQ1rhc/Rb3VMQPSGBRCk6E7EgCozLHM0xIYlLEwbBcPK13apfBlCc0ktaxF1pqziIoZqZ1Nhe0nxlPpuxPRgKov+Sco4GBb+/vHdd2LyO00GoYJhqSYGxiwsTOvIsOQHDVvmZy/amsBHY1iSWtYiihoRqKz8ImXxjttUi0F0hRCy6pRzUFXJGaVXXWWdkzzjYFgp+CqWO0UwFQzrnObG7OEalgkGNYaVX9PZ7HVhsyoZw/KKKVS3NFiXKSzi1JmmZMDykFBxvfWysFOh7vr4XDE+hgVskkL1XddSSW1X3rBURYloG5Fh5YequsIIYWM/w9JWeyFyyX8cQBsMGunv/lNdMzVKsu/CSmaSMVaGBTCRcKqZuaQ6QbW6XM+F6YFv7/zAFNWYsRlWvmJAWyyOjf0MC19CKsOCnH1f1D+s3WO0WgeHMCpm37WMm2GlLL+yB0sDcwJpv1prlEwGTJ70vMQKtLRqWZWCjcmwVldgaBdB2Lj0cDWiNSyMM7/vPheRlU9GzdDeEQ1hbH6pvc8tAQwrTR4NQHLDSsGpkJ5DaGtOeg/zwvbJoi/t4djsrZi0+O51Fh150nSJXPWFjhnYEadhvSSGhW2cVvXKhwOpL6vkbVhOP43K1lnaHczkVJwJwVnh9CMRzqvGygmSEcCw8OniXByA7PspwWkEi4Ev4Gszu/kmO1fyyr+UCp4Fp8CcwbujH4ljYpLkx9woYRZDe5AlQ8U2Tqt65e/BxNFwXm2U37TUHk8oeyOPtqnS5h7gEDldSVTz/UccAhhWXGCWwiNgYVgsQAiIoPTfiBqM3tyHcwjnujagI4R0zefOsAgh8ULDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREAw2LEBINNCxCSDTQsAgh0UDDIoREwqVL/w8KSyqJVFfZUwAAAABJRU5ErkJggg==

			$data = base64_decode($image_array_2[1]);

			$imageName = time() . '.png';
			
			 $this->db->insert('crop_images', array("Image"=>$imageName));
			//$id = $this->db->insert_id();
			 
			 $dir = "./uploads";
			file_put_contents("$dir/$imageName", $data);
			
			//echo '<img src="'.base_url().'uploads/'.$imageName.'" width="130" height="130" class="thumbnail" />';	
			
			echo $imageName;  
			exit;
			
			//$resp['cropped_image'] = $imageName;
			//$resp['text'] = '<img src="'.base_url().'uploads/'.$imageName.'" width="130" height="130" class="thumbnail" />';
			//echo json_encode($resp); 
			

		}
		 
		
	}
	
	function get_timeago($ptime){
		
		$estimate_time = time() - $ptime;
		if( $estimate_time < 1 ){
			return 'less than 1 second ago';
		}
		$condition = array( 
					12 * 30 * 24 * 60 * 60  =>  'year',
					30 * 24 * 60 * 60       =>  'month',
					24 * 60 * 60            =>  'day',
					60 * 60                 =>  'hour',
					60                      =>  'minute',
					1                       =>  'second'
		);

		foreach( $condition as $secs => $str )
		{
			$d = $estimate_time / $secs;
			if( $d >= 1 ){
				$r = round( $d );
				return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
			}
		}
	}
		
	public function is_pakage_expired(){
		
		$memo_id = $this->input->post('memo_id');
		$name_limit = $this->input->post('name_limit');
		$pakage = $this->db->query("select pakage from memorials where ID = ".$memo_id)->row()->pakage;
		
		
		$limit = $this->db->query("select ".$name_limit." as lmt from payment_plan where title = 'BASIC'")->row()->lmt;
		if($pakage != 'BASIC'){
			$limit = $this->db->query("select ".$name_limit." as lmt from payment_plan where title = 'PREMIUM'")->row()->lmt;
		}
		
		
		$added_limit = 0;
		
		if($name_limit == 'photo_limit'){
			$added_limit = $this->db->query("select count(*) as cnt from memo_pic where memo_id = ".$memo_id)->row()->cnt;
		}elseif($name_limit == 'video_limit'){
			$added_limit = $this->db->query("select count(*) as cnt from memo_video where memo_id = ".$memo_id)->row()->cnt;
		}elseif($name_limit == 'audio_limit'){
			$added_limit = $this->db->query("select count(*) as cnt from memo_audio where memo_id = ".$memo_id)->row()->cnt;
		}elseif($name_limit == 'page_managers'){
			$added_limit = $this->db->query("select count(*) as cnt from memorial_admin where memo_id = ".$memo_id)->row()->cnt;
		}
		
		if($added_limit >= $limit){
			$this->send_notification_email_to_super_admin_to_update_pakage($memo_id, $name_limit);
			echo "yes";
			exit;
		}else{
			echo "no";
			exit; 
		}
	}
	
	public function send_notification_email_to_super_admin_to_update_pakage($memo_id, $name_limit){
		
		$is_sent_last_3_days = $this->db->query("select * from limit_exceed_notification_email where memo_id = '$memo_id' and createdAt >= DATE_SUB(NOW(),INTERVAL 3 day)")->result();
		if(count($is_sent_last_3_days) > 0){
			//notification already sent in last 3 days
			return;
		}else{
			
			$limit_title = "";
			if($name_limit == 'photo_limit'){
				$limit_title = "Photos";
			}elseif($name_limit == 'video_limit'){
				$limit_title = "Videos";
			}elseif($name_limit == 'audio_limit'){
				$limit_title = "Audio";
			}elseif($name_limit == 'page_managers'){
				$limit_title = "Page Managers";
			}
						
			$memo = $this->db->query("select ID, fname, lname, createdby from memorials where ID = ".$memo_id)->row();
			$usr = $this->db->query("select fname, lname, email from users where ID = ".$memo->createdby)->row();
					
			$to=$usr->email;
			$from="smtptesting@goalwebs.com";
			$from_name="Safiri Salama - Invoice";
			$subject="Package Limit Exceeded";
			$to_name = $usr->fname.' '.$usr->lname;
			$memorial_name = $memo->fname.' '.$memo->lname;
			$vlink = base_url().'memorial/manage_memorial/'.$memo->ID;
			$content = 'Your subscription limit for '.$limit_title.' has been exceeded for memorial '.$memorial_name.'. Click <a target="_blank" href="'.$vlink.'"><b>Here</b></a> to upgrade your subscription pakage';
			
			$message_body = $this->emailtemp->payment_upgrade_template($content, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			
			$this->db->insert("limit_exceed_notification_email", array("memo_id"=>$memo_id, "createdAt"=>date('Y-m-d')));
			
			return;
		}
	
	}
	
	public function cron_invoice() 
	{
		$whatisdatetoday=strtotime(date('Y-m-d'));
		$queryis="select count(*) as totalinvoices from allinvoices where recursiveunix = ".$whatisdatetoday;
		$howmanyinvoices = $this->db->query($queryis)->row()->totalinvoices;
		echo $howmanyinvoices;
		$foundrenewable = $this->db->query("select * from allinvoices where recursiveunix = ".$whatisdatetoday)->result();
		foreach($foundrenewable as $row)
		{
			
			$invoiceparams=array("userid"=>$row->userid,"useremail"=>$row->useremail, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s',strtotime($row->willexpireat)), "recursiveat"=>date('Y-m-d', strtotime($row->willexpireat.'+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime($row->willexpireat.'+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$row->details,"subtotal"=>$row->nextprice,"discount"=>0,"totalprice"=>$row->nextprice,"nextprice"=>$row->nextprice,"status"=>"payable", "memorialid"=>$row->memorialid);
			echo "<br>".$newinvoiceid=$this->createaninvoice($invoiceparams);
			
			
		}
		
	}
	public function createaninvoice($invoiceparams)
	{
		$this->db->insert('allinvoices',$invoiceparams ); 
		return $this->db->insert_id();
	}
	public function resendinvoice($theid)
	{
		$fetchinvoice=$this->db->query("select * from allinvoices where id = ".$theid)->row();
		
		$usr = $this->db->query("select fname, lname from users where ID = ".$fetchinvoice->userid)->row();
		$memo = $this->db->query("select fname, lname from memorials where ID = ".$fetchinvoice->memorialid)->row();
		
		$to=$fetchinvoice->useremail;
		$from=$this->config->config['admin_email'];
		if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
		
		$from_name="Safiri Salama - Invoice";
		$subject="Invoice Resend Triggered";
		$to_name = $usr->fname.' '.$usr->lname;
		$memorial_name = $memo->fname.' '.$memo->lname;
		$vlink = base_url().'memorial/manage_memorial/'.$fetchinvoice->memorialid;
		$invoice_link = base_url().'memorial/invoice/'.$theid.'?pay='.$fetchinvoice->memorialid;
		$content = 'Your subscription for memorial '.$memorial_name.' is expiring, Click <a target="_blank" href="'.$vlink.'"><b>Here</b></a> to renew your subscription pakage, Click <a target="_blank" href="'.$invoice_link.'"><b>Invoice</b></a> to view your invoice';
		
		$message_body = $this->emailtemp->payment_renewal_template($content, $to_name);
		$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		
	}
	public function cron_invoiceemail()
	{
		$whatisdatetoday=strtotime(date('Y-m-d'));
		$thequery="select * from allinvoices where recursiveunix = ".$whatisdatetoday;
		$foundrenewable = $this->db->query($thequery)->result();
		foreach($foundrenewable as $row)
		{
		$to=$row->useremail;
		$message_body=$row->details."Auto Invoice resend triggered. Will be activated after confirmation of invoice email content.";
		$from="smtptesting@goalwebs.com";
		$from_name="Safiri Salama - Invoice";
		$subject="Auto invoice triggered";
		$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		}
	}
	public function profanitytest($passstring,$command,$texttype,$memoid,$userid)
	{
		$originalstring=$passstring;
		$profanitystatus=0;
		$mainmessage=array();
		$completemesasge=array();
		$testwords = $this->db->query('select word from profanity')->result();
		foreach($testwords as $w){
		$mainmessage=array();
			$txt = $w->word;
			if (stripos($passstring, $txt) !== false){
				$profanitylen=strlen($txt);
				$requiredstars=str_repeat("*",$profanitylen);
				$profanitystatus = 1;
				$passstring=str_ireplace($txt,$requiredstars,$passstring);
				array_push($mainmessage,$originalstring,$profanitystatus,$txt,$passstring);
				array_push($completemesasge,$mainmessage);
				unset($mainmessage);
				$this->db->where('word', $txt);
				$this->db->set('frequency', 'frequency+1', FALSE);
				$this->db->update('profanity');
				$this->contentmodlogs($memoid,$userid,profanity,"Profanity detected: ".$txt,"Original ".$texttype. ": ".$originalstring);

			}
		}
		//print_r($completemesasge);
		if($command=="")
		return $completemesasge;
		if($command=="altered")
			return $passstring;
	}
	
	function contentmodlogs($memoid,$userid,$reporttype,$message,$detail)
	{
		$reportparams=array("memo_id"=>$memoid,"user_id"=>$userid,"reporttype"=>$reporttype,"messagealert"=>$message, "details"=>$detail);
		$this->db->insert('contentmoderation',$reportparams ); 
	}
	
	function get_invoice_id($memo_id){
		
		$invoice_id = $this->db->query("select invoiceid from allinvoices where memorialid = '$memo_id' order by id desc")->row()->invoiceid;
		return $invoice_id;
		
	}
		
}
