<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact extends CI_Controller {
	function __construct()
	{
		parent::__construct();				
		$sess_data=$this->session->userdata('user');					
		$this->ID = 0; 		
		$this->username = ""; 				
		if($this->session->userdata('user')){			
			$this->ID = $sess_data->ID; 			
			$this->username = $sess_data->username; 					
		} 
 	}
	public function index()
	{		//echo $this->ID; exit;		//$this->data['login_id'] = $this->ID;
        
		$data['title'] = "Contact Us | Safiri Salama";
		$data['meta_description'] = "Duplex Apartments, Lower Hill Road, Upperhill Nairobi";
		$data['meta_keywords'] = "Contact Us - Safiri Salama";
		$data['login_id'] = $this->ID;
		$this->load->view("contact", $data);
 	}
	public function Testimonials()
	{
		//$string = "This is p1 Testimonials and p2 teyu";
		//echo profanity_search($string);
		//exit;
		$data['title'] = "Testimonials | Safiri Salama";
		$data['meta_description'] = "Testimonials - Safiri Salama";
		$data['meta_keywords'] = "Testimonials - Safiri Salama";
		
		$data['results'] = array();
		$this->db->select('*');
		$this->db->from('testimonials');
		$this->db->order_by("id", "desc");
 		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$data['results'] = $query->result();
		}
		$this->load->helper('date');
        $this->load->view("testimonials", $data);
 	}
	
	public function markinappropriate(){ 
		
		//$this->session->unset_userdata('inappID2');
		//$this->session->unset_userdata('inappIP2');
		//exit;
		
		$ip = $this->input->ip_address();
		$id = $this->input->post('id');
		if($id!=$this->session->userdata('inappID2'))
		{
		$this->db->where('id', $id);
		$this->db->set('flaggedcontent', 'flaggedcontent+1', FALSE);
		$this->db->update('testimonials');
		echo "Thank you. Your request has been submitted.";
		$this->session->set_userdata('inappIP2', $ip);
		$this->session->set_userdata('inappID2', $id);
		}
		else
		{
			echo "Error";
			
		}
		
	}
	
	public function Check()
	{
		return $this->check_contact_us();
	}
	
	protected function check_contact_us()
	{		//echo "<pre>"; print_r($_POST); exit;
		$this->form_validation->set_error_delimiters('<li>', '</li>');		
		if(!empty($_POST['type'])){			
			$type = $this->input->post('type');			
				if($type=='testimonial'){				
					if(empty($this->session->userdata('user'))){					
						echo "Please login";					
						exit;				
					}			
				}					
		}				
		$this->form_validation->set_rules('type', '', 'trim|required|callback_check_contact_type',
			array('required' => 'Please select type')
		);
		$this->form_validation->set_rules('name', '', 'trim|required',
			array('required' => 'Please enter name')
		);
		$this->form_validation->set_rules('email', '', 'trim|required|valid_email',
			array(
					'required' => 'Please enter email',
					'valid_email' => 'Please enter valid email'
				 )
		);
		$this->form_validation->set_rules('message', '', 'trim|required',
			array('required' => 'Please enter message')
		);	 	
		
		$this->form_validation->set_rules('g-recaptcha-response', '', 'trim|required',	array('required' => 'Please verify captcha'));						
 		 
		if($this->form_validation->run() == FALSE)
		{
			echo validation_errors();
		}
		else
		{
			$type = $this->input->post('type');
			if($type=='testimonial')
			{
				$this->form_validation->set_rules('rating', '', 'trim|required|callback_check_rating',
					array('required' => 'Please select rating')
				);
			}
			else
			{
				if(!empty($_POST['phone']))
				{
					$this->form_validation->set_rules('phone', '', 'numeric',
						array('required' => 'Please enter valid phone')
					);
				}
			}
			if($this->form_validation->run() == FALSE)
			{
				echo validation_errors();
 			}
			else
			{
				if($_POST['type']=='testimonial'){									
				
				$profanity_status = 0;					
				$comment = $this->input->post('message');										
				$words = $this->db->query('select word from profanity')->result();										
				foreach($words as $w){												
					$txt = $w->word;						
					$pos = stripos($comment, $txt);												
					if ($pos !== false){							
						$profanity_status = 1;							
						break;						
					}																					
				} 
				
				$add_data = array(
					'user_id' => $this->ID,
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'rating' => $this->input->post('rating'),
					'message' => profanity_search($this->input->post('message')),												
					'actual_text' => $this->input->post('message'),												
					'profanity_status' => $profanity_status,
					'add_date' => time(),
				);
					$this->db->set($add_data);
					$this->db->insert('testimonials');
					
					// update activity_log
					$name= $this->input->post('name');
					$description = $name.' left a testimonial. <p>'.$this->input->post('message').'</p>';
					$this->db->insert("activity_log", array("description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
					// end update activity_log 
					
					if($profanity_status == 1){
						// update activity_log
						$name= $this->input->post('name');
						$description = $name.' used a profanity word in testimonial. <p>'.$this->input->post('message').'</p>';
						$this->db->insert("activity_log", array("description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
						// end update activity_log 
					}
					
 				}else{
					// send mail to admin //										$from=$this->config->config['admin_email'];					$from_name='Admin';					$name='Admin'; 					$to=$this->input->post('email');					$subject='New message from '.$this->input->post('name');										$vlink = base_url();										$email_data['title']='User Message';					$email_data['content']= $this->input->post('message');                 					$message=$this->load->view('emails/general',$email_data,true); 					$this->smtp_email->send($from,$from_name,$to,$subject,$message);															// send mail to user //					$from=$this->config->config['admin_email'];					$from_name='Admin';					$name=$this->input->post('name'); 					$to=$this->input->post('email');					$subject='Message Sent Successfully';										$vlink = base_url();					$content='Your message has been sent successfully, One of our team member will contact you soon.</br>';					$email_data['title']='';					$email_data['content']= $content;                 					$message=$this->load->view('emails/general',$email_data,true); 					$this->smtp_email->send($from,$from_name,$to,$subject,$message);
				}
				echo 'done';
			}
		}
	}
	public function check_contact_type()
	{
		if(!empty($_POST['type']))
		{
			$type = trim($this->input->post('type'));
			$possible_values = array('contact', 'testimonial');
			if(!in_array($type, $possible_values))
			{
				$this->form_validation->set_message('check_contact_type', 'Please select valid type');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			return TRUE;
		}
	}
	
	public function check_rating()
	{
		if(!empty($_POST['rating']))
		{
			$rating = $this->input->post('rating');
			$possible_values = array(1,2,3,4,5);
			if(!in_array($rating, $possible_values))
			{
				$this->form_validation->set_message('check_rating', 'Please select valid rating');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			return TRUE;
		}
	}
}
