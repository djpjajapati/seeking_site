<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
		
		//$this->load->model("general_model");
		$this->load->library("Google");
		$this->load->library("Facebook");
		
		$this->load->library("Smtp_email");	 
		$this->load->library("Emailtemp");
		
		
        $sess_data=$this->session->userdata('user');
				
		$this->ID = 0; 
		$this->username = ""; 
		//$this->email = ""; 
		//if(isset($sess_data->ID))
		if($this->session->userdata('user')) 
		{
			$this->ID = $sess_data->ID; 
			$this->username = $sess_data->username; 
			//$this->email = $sess_data->email; 
		} 
    
    }
    public function index()
    {
		$sess_data=$this->session->userdata('user');
        if(isset($sess_data->email))
        {
            redirect('home');
        }
		
		if($_GET['redirect'] == 'contact'){
			$this->load->library('session');
			$this->session->set_userdata('redirect', 'contact');
		}
		
		$this->data['gmail_url'] = $this->google->getLoginUrl();
		$this->data['fb_url'] = $this->facebook->login_url(); 
        
		$this->data['page_title'] = "Login";
		$this->load->view('login', $this->data);
        
    }
	
	public function register(){
		$sess_data=$this->session->userdata('user');
        if(isset($sess_data->email))
        {
            redirect('home');
        }
		$this->data['page_title'] = "Register";
		$this->load->view('register', $this->data);
	}
	
	public function forgot()
    {
		$this->data['page_title']="Forgot - Password";
		$this->load->view('forgot_password', $this->data);
	}
	
	public function check_email_company(){
		
		if(isset($_GET['email']))
        {
            $email=$_GET['email'];
			
			$cnt = $this->db->query("select count(*) as cnt from users where Email = '$email'")->row()->cnt;
			if($cnt > 0){
				echo 'false';
			}else{
				echo 'true';
			}
		}
	}
	
	public function add_access(){
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
		 
		$redirect = $this->input->post('redirect');
		
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('login/register');
			}
		  else
			{
				 $fname = $this->input->post('fname');
				 $email = $this->input->post('email');
				 $cnt = $this->db->query("select count(*) as cnt from users where Email = '$email'")->row()->cnt;
				 
				 if($cnt > 0){ 
					 $this->session->set_flashdata('error', "Account already register with this email, Please login");  
					 redirect('login');
				 }
				 
			     $data['fname'] = $this->input->post('fname');
			     $data['lname'] = $this->input->post('lname');
				 $data['email'] = $this->input->post('email');
			     $data['password'] = md5($this->input->post('password'));

                 $EmailVerification = md5($fname.time());
				 $data['EmailVerification'] = $EmailVerification;
                 $createdAt = date('Y-m-d H:i:s');
                 $data['LastLogin'] = $createdAt;
                 $data['createdAt'] = $createdAt;
				
               /////////////////////
				
				
                /*$config =  array(
                  'upload_path'     => './uploads/companies/',
                  'allowed_types'   => "gif|jpg|png|jpeg",
                  'max_size'        => "0",
                  'max_height'      => "1768",
                  'max_width'       => "2048"  
                );
					
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload('UserPhoto'))
				{
					$filed = $this->upload->data();
					$data['Logo'] = $filed["file_name"];	
				}
				else
				{
					
				}*/
				
				///////////////////////////			 
          
         	     $this->db->insert('users', $data);
                 $id=$this->db->insert_id();
                  // send activation link //
					$from=$this->config->config['admin_email'];
					$from_name='Admin';
					$name=$this->input->post('fname')." ".$this->input->post('lname'); 
					$to=$this->input->post('email');
					$subject='Account Creation';
					
					$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
					$content='Your account has been created successfully. You need to verify your email address. Please click the link below to verify your email.</br><br>
					<table width="100%" style="margin-left: 39%;" cellspacing="0" cellpadding="0">
					  <tr>
						  <td>
							  <table cellspacing="0" cellpadding="0">
								  <tr>
									  <td style="border-radius: 2px;" bgcolor="#f1ad48">
										  <a href="'.$vlink.'" target="_blank" style="padding: 8px 12px; border: 1px solid #f1ad48;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
											  Click to verify            
										  </a>
									  </td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					</table>
					';
					$email_data['title']='<span>Verification<span> Email';
					$email_data['content']= $content;                 
					$message=$this->load->view('emails/general',$email_data,true); 
					$this->smtp_email->send($from,$from_name,$to,$subject,$message);
				
			        $this->session->set_flashdata('success', 'Registered successfully, Please check your email');
					
					// login after registration
					
					   $email = $this->input->post('email'); 
					   $password = $this->input->post('password');       
					   $this -> db -> select('ID,fname,lname,email');
					   $this -> db -> from('users');
					   $this -> db -> where('email', $email);
					   $this -> db -> where('password', md5($password));
					   $this -> db -> limit(1);
					   $query = $this->db->get(); 
					   if($query -> num_rows() >= 1)
					   {
							$sess_data = $query->result();
							$sess_data = $sess_data[0];	
							$this->session->set_userdata(array('user'=>$sess_data));
							redirect('home');//redirect('CompanyProfile/'.$sess_data->ContactButton);
					   }
					   else
					   {
						//echo "No match found"; exit;
						$this->session->set_flashdata('error', 'Incorrect Email Or Password');	
						redirect('login');
					   }
					
					//login after registration 
					
			}
	}
	
	public function create_account_with_memorial(){ 
		
		 $fname = $this->input->post('fname');
		 $email = $this->input->post('email');
		 $cnt = $this->db->query("select count(*) as cnt from users where Email = '$email'")->row()->cnt;
		 
		 if($cnt > 0){ 
			 echo "aleady_exists";  
			 exit;
		 }
		 
		 $data['fname'] = $this->input->post('fname');
		 $data['lname'] = $this->input->post('lname');
		 $data['email'] = $this->input->post('email');
		 $data['password'] = md5($this->input->post('password'));

		 $EmailVerification = md5($fname.time());
		 $data['EmailVerification'] = $EmailVerification;
		 $createdAt = date('Y-m-d H:i:s');
		 $data['LastLogin'] = $createdAt;
		 $data['createdAt'] = $createdAt;
			 
  
		 $this->db->insert('users', $data);
		 $id=$this->db->insert_id();
		 
		 
		  // send activation link //
			$from=$this->config->config['admin_email'];
			$from_name='SafiriSalama Memorials';
			$to_name=$this->input->post('fname')." ".$this->input->post('lname'); 
			$to=$this->input->post('email');
			$subject='Account Creation';
			
			$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
			$content='Welcome to Safiri Salama. Your account has been created successfully. You need to verify your email address.Please click the button below to verify your email.</br><br>							<table width="100%" style="margin-left: 39%;" cellspacing="0" cellpadding="0">							  <tr>								  <td>									  <table cellspacing="0" cellpadding="0">										  <tr>											  <td style="border-radius: 2px;" bgcolor="#f1ad48">												  <a href="'.$vlink.'" target="_blank" style="padding: 8px 12px; border: 1px solid #f1ad48;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">													  Click to verify            												  </a>											  </td>										  </tr>									  </table>								  </td>							  </tr>							 							</table>																				<p style="color: #500050;" >							If the verify button is not visible just copy link '.$vlink.' and paste as url to verify 						</p> 			';			$title='<span>Verification<span> Email';          					$message_body = $this->emailtemp->account_creation_template($title, $content, $vlink, $to_name);			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);		
			$this->session->set_flashdata('success', 'Registered successfully, Please check your email');
			
			// login after registration
			   $email = $this->input->post('email'); 
			   $password = $this->input->post('password');       
			   $this -> db -> select('ID,fname,lname,email');
			   $this -> db -> from('users');
			   $this -> db -> where('email', $email);
			   $this -> db -> where('password', md5($password));
			   $this -> db -> limit(1);
			   $query = $this->db->get(); 
			   if($query -> num_rows() >= 1)
			   {
					$sess_data = $query->result();
					$sess_data = $sess_data[0];	
					$this->session->set_userdata(array('user'=>$sess_data));
					echo $id; 
					exit;
			   }
			   else
			   {
				echo "error";
				exit;
			   }
					
			  //login after registration 
		
	}
	
	public function do_login_existing_account(){ 
		
		 $email = $this->input->post('email');
		 $password = $this->input->post('password');
		 
		 $this -> db -> select('ID,fname,lname,email');
		 $this -> db -> from('users');
		 $this -> db -> where('email', $email);
		 $this -> db -> where('password', md5($password));
		 $this -> db -> where('status', 'Active');
		 $this -> db -> limit(1);
		 $query = $this->db->get(); 
		 if($query -> num_rows() >= 1){
			 
			$sess_data = $query->result();
			$sess_data = $sess_data[0];	
			$this->session->set_userdata(array('user'=>$sess_data));
			
			echo $sess_data->ID;
			exit;
		   }
		   else
		   {
			echo "error";
			exit;
		   }
				
		  //reeturn id
	}
	
	public function send_invitation(){
		
		$createdBy = $this->input->post('createdBy');
		$memo_id = $this->input->post('memo_id');
		$webaddress = $this->input->post('webaddress');
		$memory_of = $this->input->post('memory_of');
		$email = $this->input->post('email');
			
		$res = $this->db->query("select ID, lname, fname, count(*) as cnt from users where email = '$email'")->row();
		 
		$username = "";
		$password = "";
		$invited_user_id = 0;  // we don not know if users is already registered or not with this email
		if($res->cnt > 0){ 
			 //echo "aleady_exists";  
			$invited_user_id = $res->ID;
			$username = $res->fname.' '.$res->lname;
		}else{   // create an account send invitation and return registered id
			
			$password = mt_rand(10000000, 99999999);
			$username = explode("@", $email)[0];
			
			$data['fname'] = $username;
			$data['lname'] = "";
			$data['username'] = $username;
			$data['email'] = $email;
			$data['password'] = md5($password);

			$EmailVerification = md5($username.time());
			$data['EmailVerification'] = $EmailVerification;
			$data['createdAt'] = date('Y-m-d H:i:s');
				 
	  
			$this->db->insert('users', $data);
			$invited_user_id=$this->db->insert_id();
			
		}
		
		//echo $invited_user_id.'   '; echo $this->ID; exit; 
		 
		
		$is_sent_cnt = $this->db->query("select count(*) as cnt from memorial_admin where memo_id = '$memo_id' and user_id = '$invited_user_id'")->row()->cnt;
		if($is_sent_cnt > 0){
			echo 'already_sent';
			exit;
		}
		$this->db->insert("memorial_admin", array("memo_id"=>$memo_id, "user_id"=>$invited_user_id, "createdBy"=>$createdBy, "createdAt"=>date('Y-m-d H:i:s')));
		 
		 
		// update activity_log
		$usr = $this->db->query("select fname, lname from users where ID = ".$createdBy)->row();
		$memo = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
		$description = $usr->fname.' '.$usr->lname." invited ".$email." to manage memorial for ".$memo->fname.' '.$memo->lname.'.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "user_id"=>$this->ID, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log 
		 
		 
	  // send activation link //
		$from=$this->config->config['admin_email'];
		$from_name='SafiriSalama Memorials';
		$to_name = $username; 
		$to=$email;
		//$subject='Invitation In The Memory Of '.$memory_of;
		$subject="You have been invited to manage ".$memory_of."'s memorial page";
		
		//$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
		$accept_link = base_url().'login/accept_invitation/'.$memo_id.'/'.$invited_user_id;
		$reject_link = base_url().'login/reject_invitation/'.$memo_id.'/'.$invited_user_id;
		$memo_link = base_url().'memorial/tribute/'.$webaddress;
		$vlink = $accept_link;
		 
		//$content='<p class="text-left">You have been invited to manage <a target="_blank" href="'.$memo_link.'">'.strtoupper($memory_of).' </a> &apos;s memorial page.</p> '.$new_user_text.' <p class="text-left"> <a style="text-decoration: none; cursor: pointer" target="_blank" href="'.$accept_link.'"><button style="cursor: pointer; background-color: #794e5e; text-decoration:none; color:#f9f9f9; display:block; padding:9px 8px; border: none" class="btn btn-danger">Accept Invitation</button></a>  </br> <a style="text-decoration: none; cursor: pointer: margin-left: 10px; cursor: pointer" target="_blank" href="'.$reject_link.'"><button style="cursor: pointer; background-color: #794e5e;; text-decoration:none; color:#f9f9f9; display:block; padding:9px 23px; border: none" class="btn btn-danger">Reject</button></a></p>   <p> if Accept Invitation button is not visible just copy link '.$vlink.' and paste as url to verify! </br>';
		
		$new_user_text = '';
		if($res->cnt <= 0){ // new user
			$new_user_text = '<p> You can use '.$email.' to login. Your password is <b>'.$password.'</b></p>';
		}
	
		$title='<span>Invitation<span> Email';
		$message_body = $this->emailtemp->memorial_invitation_template($title, $to_name, $memo_link, $memory_of, $new_user_text, $accept_link, $reject_link);
		$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
				
		
		//$this->smtp_email->send($from,$from_name,$to,$subject,$message);
		
		echo "success";
					
	}
	
	public function accept_invitation($memo_id="", $invited_id=""){   
	
		$this->db->where('memo_id', $memo_id);
		$this->db->where('user_id', $invited_id);
		$this->db->update('memorial_admin', array("status"=>'Approved'));
		$this->session->set_flashdata('success', 'Invitation Accepted! Please login to manage Tribute');
		redirect('login');
	}
	
	public function reject_invitation($memo_id="", $invited_id=""){   
	
		$this->db->where('memo_id', $memo_id);
		$this->db->where('user_id', $invited_id);
		$this->db->delete('memorial_admin');
		$this->session->set_flashdata('success', 'Invitation Rejected!');
		redirect('login'); 	
	}
	
	public function sign_up_notification(){
		
		$memo_id = $this->input->post('memo_id');
		$email = $this->input->post('email');
		
		$cnt = $this->db->query("select count(*) as cnt from notification_emails where email = '$email' and memo_id = '$memo_id'")->row()->cnt;
		if($cnt > 0){
			echo "Already Subscribed to this tribute";
		}else{
			$this->db->insert("notification_emails", array("email"=>$email, "memo_id"=>$memo_id, "createdAt"=>date('Y-m-d H:i:s')));
			echo "You have successfully signup for tribute notification";
		}
		
		exit;
	}
	
	public function update_views(){
		
		$memo_id = $this->input->post('memo_id');
		$views = $this->input->post('views');
		if(!empty($this->session->userdata('user'))){
			$user_id = $this->session->userdata('user')->ID;
			if($this->is_page_admin($memo_id, $user_id)){
				echo '1';
				exit;
			}else{
				$this->db->where("ID", $memo_id)->update("memorials", array("views"=>$views));
			}
		}else{
			$this->db->where("ID", $memo_id)->update("memorials", array("views"=>$views));
		}
		
		echo '1';
	}
	
	public function update_featured_views(){
		
		$memo_id = $this->input->post('memo_id');
		$views = $this->input->post('views');
		
		$this->db->where("ID", $memo_id)->update("featured_memorials", array("views"=>$views));
		
		echo '1';
		
	}
	
	public function verify_eamil($email, $token){
		
		$usr = $this->db->query("select * from users where email = '$email'")->row();
		
		$to_name = $usr->fname.' '.$usr->lname;
		
		$from=$this->config->config['admin_email'];
		if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
		$from_name="SafiriSalama Memorials";
		$to = $email;
		$subject="Verification Email";
		$vlink = base_url().'login/EmailVerification/'.$token;
		
		$content='Please click the link below to verify your email.</br><br>
					<table width="100%" style="margin-left: 39%;" cellspacing="0" cellpadding="0">
					  <tr>
						  <td>
							  <table cellspacing="0" cellpadding="0">
								  <tr>
									  <td style="border-radius: 2px;" bgcolor="#f1ad48">
										  <a href="'.$vlink.'" target="_blank" style="padding: 8px 12px; border: 1px solid #f1ad48;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
											  Click to verify            
										  </a>
									  </td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					</table>
';
		$title='<span>Verification<span> Email';          
		
		$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
		$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
		
		$this->session->set_flashdata('success', "Verification link has been sent to your email, Please check your email to verify your account");  
		redirect('memorial/mymemorials');
	}
	
	public function EmailVerification($token=""){
		
		$cnt = $this->db->query("select count(*) as cnt from users where EmailVerification = '$token'")->row()->cnt;
		if($cnt > 0){
			$this->db->where('EmailVerification', $token);
			$this->db->update('users', array('EmailVerification'=>1));
			$this->session->set_flashdata('success', 'Email verified successfully, Please login');
		}	
		redirect('login');	
	}
		
		
    public function login_access()
    {
        $this->load->library('form_validation');
		$this->data['page_title']="login - User"; 
        $this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		$redirect = $this->input->post('redirect');
		
		if ($this->form_validation->run() == FALSE)
		{	
			$error=validation_errors();
			$this->session->set_flashdata('error', $error);
		}
		else
		{ 
			   $email = $this->input->post('email'); 
			   $password = $this->input->post('password');       
			   $this -> db -> select('ID,fname,lname,email, status');
			   $this -> db -> from('users');
			   $this -> db -> where('email', $email);
			   $this -> db -> where('password', md5($password));
			   //$this -> db -> where('status', 'Active');
			   $this -> db -> limit(1);
			   $query = $this->db->get(); 
			   if($query -> num_rows() >= 1)
			   {
					//echo "match found"; exit;
					$sess_data = $query->result();
					$sess_data = $sess_data[0];	
					
					if($sess_data->status != 'Active'){
						
						$this->session->set_flashdata('error', "Your account has been blocked by Admin");
						redirect('login');
					}
					
					//update last login
					$this->db->set('LastLogin', date('Y-m-d H:i:s'));
					$this->db->where('email',$email);
					$this->db->update('users'); 
					
					$this->session->set_userdata(array('user'=>$sess_data));
					
					if($this->session->userdata('redirect') == 'contact'){
						$this->session->unset_userdata('redirect');
						redirect('contact');
						exit;
					}
					
					redirect('memorial/mymemorials');
			   }
			   else
			   {
				$this->session->set_flashdata('error', 'Incorrect Email Or Password');	
				redirect('login');
				exit;           

			   }
		}

    }
	
	
	public function forgot_password()
    {      
        $this->data['page_title']="Forgot - Password";
		$this->form_validation->set_rules('email', 'Email', 'required');
		
		if($this->form_validation->run() == FALSE)
 	     {
			 $error=validation_errors();
			 $this->session->set_flashdata('error', $error);
			 redirect('login/forgot');
         }
		else
		{
			$email=$this->input->post('email');
			$ResetPassword = "";
				$query = $this->db->query("SELECT * FROM users WHERE email='".$email."'");
				if($query->num_rows()>0)
                {
					$ResetPassword = time().(uniqid());
					$this->db->set('ResetPassword', $ResetPassword);
					$this->db->where('email',$email);
					$this->db->update('users'); 
				}else{
					$this->session->set_flashdata('error', 'Email not found');
					redirect('login/forgot');
				}				$usr = $query->row();
    				$from = $this->config->config['admin_email'];				if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  				$from_name="SafiriSalama Memorials";				$title='Reset Password';				$vlink = base_url().'login/update_password/'.$ResetPassword;				$to_name= $usr->fname.' '.$usr->lname; 				$to=$email;								$content='<p>Your New Password Renewal Link is '.base_url().'login/update_password/'.$ResetPassword.'</p>';   				$subject='Reset Password';								$message_body = $this->emailtemp->general_email_template($title, $content, $vlink, $to_name);				$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);					
				 $this->session->set_flashdata('success', 'Reset link sent. Please check your email');
				 redirect('login/forgot');
		}
        
    }
    public function update_password($ResetPassword)
    {  
        	
		$this->data['page_title']="Update - Password";
		$this->data['ResetPassword']= $ResetPassword;
		
			$res = $this->db->query("select * from users where ResetPassword = '".$ResetPassword."'")->row();
			if(count($res) > 0){
				$this->load->view('reset_password', $this->data);
			}else{
				$this->session->set_flashdata('error', 'Reset link not found');
				 redirect('login/forgot');
			} 

	}
	
	public function change_password() {
		
		//$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[8]');
		$this->form_validation->set_rules('c-password', 'Password Confirmation', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			  $error=validation_errors();
		      $this->session->set_flashdata('error', $error);             
			  redirect("login/update_password/".$this->input->post('ResetPassword'));
		}
		else
		{						
			//$password=md5($this->input->post('password'));
			$new_password=$this->input->post('new_password');
			$c_password=$this->input->post('c-password');
			
			$ResetPassword = $this->input->post('ResetPassword');
			
			if($new_password != $c_password){
				$this->session->set_flashdata('error', 'Password does not match');
				redirect("login/update_password/".$ResetPassword);
			}else{
				$this->db->set('Password', md5($new_password));
				$this -> db -> where('ResetPassword', $ResetPassword);
				$this->db->update('users');
				
				$this->db->set('ResetPassword', '');
				$this -> db -> where('Password', md5($new_password));
				$this->db->update('users');
				
				$this->session->set_flashdata('success', 'The password changed successfully, Please Login');
			    redirect("login");
				
			}
			
		}
	}
	
	public function update_password_code() 
    {
        $this->form_validation->set_rules('password', 'Password', 'required|matches[c-password]|min_length[8]');
		$this->form_validation->set_rules('c-password', 'Password Confirmation', 'required');
		$pass_code=$this->input->post('pass_code');
		if ($this->form_validation->run() == FALSE)
		{
		  $error=validation_errors();
		  $this->session->set_flashdata('error', $error);
	       redirect('admin/login/update_password/'.$pass_code);
           exit();
		}
		else
		{
           $this -> db -> select('admin_id');
	       $this -> db -> from('admin');
	       $this -> db -> where('c-password',$pass_code);
	       $this -> db -> limit(1);
	       $query = $this->db->get(); 
	       if($query -> num_rows() == 0)
            {
                redirect('admin/login');
                exit();       
            } 
			$password=md5($this->input->post('password'));
			$this->db->set('password', $password);
			$this->db->where('c-password', $pass_code);
			$this->db->update('admin');			
            $this->db->set('c-password', 0);
			$this->db->where('c-password', $pass_code);
			$this->db->update('admin');
			$this->session->set_flashdata('success', 'The password changed successfully');
			redirect('admin/login');
            exit();

		}

	} 

	public function send_link(){
		// send activation link //
		$from=$this->config->config['admin_email'];
		$from_name='SafiriSalama Memorials';
		$name= $_GET['name']; //$this->input->post('name'); //$this->input->post('companyname');
		$to=$_GET['email']; //$this->input->post('email');
		$subject='Email verification';
		$token = $this->db->query("select EmailVerification from users where Email = '$to'")->row()->EmailVerification;
		//echo $token; exit;
		$vlink = base_url().'login/EmailVerification/'.$token;
		$content='Welcome to VX HIRE, Please click the <a href="'.$vlink.'">link</a> to complete your profile.';
		$email_data['title']='<span>Email<span> Verification';
		$email_data['content']= $content;                 
		$message=$this->load->view('emails/general',$email_data,true); 
		$this->smtp_email->send($from,$from_name,$to,$subject,$message);
		
		echo 'true';
	}	
	public function google_callback() {
		$this->google->setAccessToken();
        $userData = $this->google->getUserInfo();
		$google_username="";
        if (empty($userData['name'])) {
            $email_parts = explode("@", $userData['email']);
            $google_username = $email_parts[0];
        } else {
			//$google_username = preg_replace('/\s+/', ' ', $userData['name']);
            $google_username = $userData['name'];
        }

        // echo '<pre>'; print_r($userData); echo '</pre>'; exit('Exit');
        $password = mt_rand(10000000, 99999999);
        //$result = $this->common_model->get_table_data('users', '*', array('email' => $userData['email'], 'user_type' => 'client'));
		
		$this -> db -> select('ID,fname,lname,email');
	    $this -> db -> from('users');
	    $this -> db -> where('email', $userData['email']);
	    //$this -> db -> where('password', md5($password));
	    $this -> db -> limit(1);
	    $query = $this->db->get(); 

	    if($query -> num_rows() >= 1){

			$sess_data = $query->result();
			$sess_data = $sess_data[0];	
			$this->session->set_userdata(array('user'=>$sess_data));
			$this->db->where("email", $userData['email'])->update("users", array("LastLogin"=>date('Y-m-d H:i:s'))); // update last login
			redirect('home');			

	    }else{

		$pieces = explode(" ", $google_username);		
		$insert_data = array(
			
                    'fname' => $pieces[0],
                    'lname' => $pieces[1],
                    'email' => $userData['email'],
                    'password' => md5($password),
                    'EmailVerification' => 1,
                    //'signup_method' => 'gmail',
                    //'gmail_id' => $userData['id'],
                    'createdAt' => date('Y-m-d H:i:s')

            );

            $insert_result = $this->db->insert('users', $insert_data);

            if (!empty($userData['email'])) {					$from=$this->config->config['admin_email'];					$from_name='SafiriSalama Memorials';					$to_name=$google_username; 					$to=$userData['email'];					$subject='Account Creation';					$vlink = base_url().'home';					$content='Welcome to Safiri Salama. Your account has been created successfully. Please use your email and password to login. Your account password is <b>'.$password.'</b>.</br><br>';										$title='<span>Account Creation<span> Email';          					$message_body = $this->emailtemp->account_creation_with_social_media_template($title, $content, $vlink, $to_name);					$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);					
					$this -> db -> select('ID,fname,lname,email');
					$this -> db -> from('users');
					$this -> db -> where('email', $userData['email']);
					//$this -> db -> where('password', md5($password));
					$this -> db -> limit(1);
					$query = $this->db->get(); 
					if($query -> num_rows() >= 1){

						$sess_data = $query->result();
						$sess_data = $sess_data[0];	
						$this->session->set_userdata(array('user'=>$sess_data));
						$this->db->where("email", $userData['email'])->update("users", array("LastLogin"=>date('Y-m-d H:i:s'))); // update last login
						redirect('home');						

					}			

            }else{

				$this->session->set_flashdata('error1', 'Unable to process, Please try again later!'); 	
				redirect('login');

			}
	   }  

    }

	
	public function fb_callback() {

		/* if (!session_id()) {
            session_start();
        } */
		//echo "here are we";

		//session_destroy();
        $userProfile = $this->facebook->request('get', '/me?fields=id,name,picture,email,gender');

		//echo "<pre>"; print_r($userProfile); exit;
		
        $fb_username="";
		if(!isset($userProfile['error']) || !empty($userProfile['email'])) {

            $password = mt_rand(10000000, 99999999);
			if (empty($userProfile['name'])) {

				$email_parts = explode("@", $userProfile['email']);
				$fb_username = $email_parts[0];

			} else {

				// $fb_username = preg_replace('/\s+/', '_', $userProfile['name']);
				$fb_username = $userProfile['name'];

			}
			
			$connect = "coach";
			if(isset($_COOKIE["connect"])) {
				$connect = $_COOKIE["connect"];
			}

			$this -> db -> select('ID,fname,lname,email');
			$this -> db -> from('users');
			$this -> db -> where('email', $userProfile['email']);
			//$this -> db -> where('password', md5($password));
			$this -> db -> limit(1);
			$query = $this->db->get(); 
			if($query -> num_rows() >= 1){

				$sess_data = $query->result();
				$sess_data = $sess_data[0];	
				$this->session->set_userdata(array('user'=>$sess_data));
				$this->db->where("email", $userData['email'])->update("users", array("LastLogin"=>date('Y-m-d H:i:s'))); // update last login
				redirect('home');
			
			}else{				$pieces = explode(" ", $fb_username);
				$insert_data = array(

						'fname' => $pieces[0],
						'lname' => $pieces[1],
						'email' => $userProfile['email'],
						'password' => md5($password),
						'EmailVerification' => 1,
						//'signup_method' => 'gmail',
						//'fb_id' => $userProfile['id'],
						'createdAt' => date('Y-m-d H:i:s')

				);
				$insert_result = $this->db->insert('users', $insert_data);
				/* $from=$this->config->config['admin_email'];
				$from_name='SafiriSalama Memorials';
				$name=$fb_username; 
				$to=$userProfile['email'];
				$subject='Account Creation';
				$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
				$content='<p>Welcome to Safari Salama, Your account has been created successfully, You need to verify your email address, Please click the button below to confirm your email address </p> <p><a style="text-decoration: none; cursor: pointer" target="_blank" href="'.$vlink.'"><button style="cursor: pointer; background-color: #e32318; text-decoration:none; color:#f9f9f9; display:block; padding:15px 30px; border: none" class="btn btn-danger">Verify Now</button></a></p>   <p> if verify button is not visible just copy link '.$vlink.' and paste as url to verify! </br>';																					
				$email_data['title']='<span>Verification Email<span>';
				$email_data['content']= $content;                 
				$content=$this->load->view('emails/general',$email_data,true);
				$this->smtp_email->send($from,$from_name,$to,$subject,$content); */				$from=$this->config->config['admin_email'];				$from_name='SafiriSalama Memorials';				$to_name=$fb_username; 				$to=$userProfile['email'];				$subject='Account Creation';				$vlink = base_url().'home';				$content='Welcome to Safiri Salama. Your account has been created successfully. Please use your email and password to login. Your account password is <b>'.$password.'</b>.</br><br>';								$title='<span>Account Creation<span> Email';          				$message_body = $this->emailtemp->account_creation_with_social_media_template($title, $content, $vlink, $to_name);				$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
				//$this->smtp_email->send('support@mailresumes.com', 'Mail Resumes', $userProfile['email'], 'Welcome to Mail Resumes!', $this->email_temp->email_content('Welcome to Mail Resumes!', 'Your mail resumes account is now active. You can learn more about our services at <a href="https://mailresumes.com/welcome/services" style="color: #0c7cd5">mailresumes.com</a>. You can also click on the "Start the Process" tab here [<a href="https://mailresumes.com/job_criteria" style="color: #0c7cd5">Start the Process tab</a>] to begin your search for a new job today.', '', ''));
				$this -> db -> select('ID,fname,lname,email');
				$this -> db -> from('users');
				$this -> db -> where('email', $userProfile['email']);
				//$this -> db -> where('password', md5($password));
				$this -> db -> limit(1);
				$query = $this->db->get(); 

				if($query -> num_rows() >= 1){

					$sess_data = $query->result();
					$sess_data = $sess_data[0];	
					$this->session->set_userdata(array('user'=>$sess_data));
					$this->db->where("email", $userData['email'])->update("users", array("LastLogin"=>date('Y-m-d H:i:s'))); // update last login
					redirect('home');
				}					
		   }   

		} else {

            $this->session->set_flashdata('error', 'Unable to process, Please try again later!'); 	
			redirect('login');        
		}

	   //////////////////

    }
	
	public function login_twitter(){
 
		require_once APPPATH.'vendor/autoload.php';

		$server = new League\OAuth1\Client\Server\Twitter(array(

				'identifier' => 'vrvflSGErWnLGve8WNUdcoA4i', //'9eQxjtaARL3TchcUOeLl29akK',

				'secret' => '6gCoRzaDYVDurS0svpoGWRDW2hWzUiXNKVxuHmROhbOM9ncmsz', //'Tc09Or2s9uW31FuDWkXmXbwkoEW74cuxvEBKcBuXWXEZXjAjwn',

				//'callback_uri' => "https://safirisalama.com/login/twitter_callback",
				'callback_uri' => base_url()."login/twitter_callback",
		));

		// Retrieve temporary credentials

		$temporaryCredentials = $server->getTemporaryCredentials();

		//echo "<pre>"; print_r($temporaryCredentials); exit;

		// Store credentials in the session, we'll need them later

		$_SESSION['temporary_credentials'] = serialize($temporaryCredentials);

		session_write_close();

	

		// Second part of OAuth 1.0 authentication is to redirect the

		// resource owner to the login screen on the server.

		$server->authorize($temporaryCredentials);

	

		

	}

	

	public function twitter_callback(){

		require_once APPPATH.'vendor/autoload.php';

		$server = new League\OAuth1\Client\Server\Twitter(array(

				'identifier' => 'vrvflSGErWnLGve8WNUdcoA4i',

				'secret' => '6gCoRzaDYVDurS0svpoGWRDW2hWzUiXNKVxuHmROhbOM9ncmsz',

				//'callback_uri' => "http://goalwebs.com/memo_site/login/twitter_callback",
				'callback_uri' => base_url()."login/twitter_callback",

		));

		

		if (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) {

			// Retrieve the temporary credentials we saved before

			$temporaryCredentials = unserialize($_SESSION['temporary_credentials']);

			

			// We will now retrieve token credentials from the server

			$tokenCredentials = $server->getTokenCredentials($temporaryCredentials, $_GET['oauth_token'], $_GET['oauth_verifier']);

			

			unset($_SESSION['temporary_credentials']);

			$_SESSION['token_credentials'] = serialize($tokenCredentials);

			//session_write_close();

			

			$user = $server->getUserDetails($tokenCredentials);

			//echo '<pre>'; print_r($user); echo '</pre>'; exit('Exit');	

			

			$password = mt_rand(10000000, 99999999);

			

			$this -> db -> select('ID,fname,lname,email');

			$this -> db -> from('users');

			$this -> db -> where('email', $user->email);

			$this -> db -> limit(1);

			$query = $this->db->get(); 

			if($query -> num_rows() >= 1)

			{

				$sess_data = $query->result();

				$sess_data = $sess_data[0];	

				$this->session->set_userdata(array('user'=>$sess_data));

				

				//$sess_data=$this->session->userdata('user');

				//echo "<pre>"; print_r($sess_data); exit;

				

				redirect('home');

				

			}

			else

			{				

				if(!empty($user->email) && !empty($user->name)) {

					

					if (empty($user->name)) {

						$email_parts = explode("@", $user->email);

						$user->name = $email_parts[0];

					}

				

					/* $cnt = $this->db->query("select count(*) as cnt from users where UserName = '$user->name'")->row()->cnt;

					if($cnt > 0){

						$user->name = $user->name.mt_rand(10,100);

					} */

					

					//$EmailVerification = md5($user->name.time());
					$pieces = explode(" ", $user->name);					
					$insert_data = array(

							'fname' => $pieces[0],
							
							'lname' => $pieces[1],

							'email' => $user->email,

							'password' => md5($password),

							'EmailVerification' => 1,

							//'signup_method' => 'twitter',

							//'twitter_id' => $user->uid,

							'createdAt' => date('Y-m-d H:i:s'),
							
							'LastLogin' => date('Y-m-d H:i:s')

					);



					//$insert_result = $this->common_model->insert_table('users', $insert_data);

					$insert_result = $this->db->insert('users', $insert_data);
					/* $from=$this->config->config['admin_email'];
					$from_name='SafiriSalama Memorials';
					$name=$user->name; 
					$to=$user->email;
					$subject='Account Creation';
					$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
										$content='<p>Welcome to Safiri Salama, Your account has been created successfully, You need to verify your email address, Please click the button below to confirm your email address </p> <p><a style="text-decoration: none; cursor: pointer" target="_blank" href="'.$vlink.'"><button style="cursor: pointer; background-color: #e32318; text-decoration:none; color:#f9f9f9; display:block; padding:15px 30px; border: none" class="btn btn-danger">Verify Now</button></a></p>   <p> if verify button is not visible just copy link '.$vlink.' and paste as url to verify! </br>';
					$email_data['title']='<span>Verification Email<span>';
					$email_data['content']= $content;                 
					$content=$this->load->view('emails/general',$email_data,true);
					//$this->general_model->send_email($from, $to, $subject, $content, $from_name);
					$this->smtp_email->send($from,$from_name,$to,$subject,$content); */										$from=$this->config->config['admin_email'];					$from_name='SafiriSalama Memorials';					$to_name=$user->name; 					$to=$user->email;					$subject='Account Creation';					$vlink = base_url().'home';					$content='Welcome to Safiri Salama. Your account has been created successfully. Please use your email and password to login. Your account password is <b>'.$password.'</b>.</br><br>';										$title='<span>Account Creation<span> Email';          					$message_body = $this->emailtemp->account_creation_with_social_media_template($title, $content, $vlink, $to_name);					$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
					

					$this -> db -> select('ID,fname,lname,email');
					$this -> db -> from('users');
					$this -> db -> where('email', $user->email);
					$this -> db -> limit(1);
					$query = $this->db->get(); 

					if($query->num_rows() >= 1)
					{
						$sess_data = $query->result();
						$sess_data = $sess_data[0];	
						$this->session->set_userdata(array('user'=>$sess_data));
						redirect('home');
					}

				}else{

					$this->session->set_flashdata('error', 'Unable to process, Please try again later!'); 	

					redirect('login');

				}	

		   }			

		}else{

			$this->session->set_flashdata('error', 'Unable to process, Please try again later!'); 	

			redirect('login');

		} 

		

	}
	
	public function login_linkedin(){

		$provider = new League\OAuth2\Client\Provider\LinkedIn([
				'clientId'          => '78jk0d7vquml8i',
				'clientSecret'      => 'Jn7PdZmmjfTbuRxI',
				'redirectUri'       => base_url().'login/linkedin_callback',
		]);
		
		if (!isset($_GET['code'])) {
		
			// If we don't have an authorization code then get one
			$authUrl = $provider->getAuthorizationUrl();
			$_SESSION['oauth2state'] = $provider->getState();
			header('Location: '.$authUrl);
			exit;
		
			// Check given state against previously stored one to mitigate CSRF attack
		}
		
		
	}

	

	public function linkedin_callback() {
    
		$provider = new League\OAuth2\Client\Provider\LinkedIn([
				'clientId'          => '78jk0d7vquml8i',
				'clientSecret'      => 'Jn7PdZmmjfTbuRxI',
				'redirectUri'       => base_url().'login/linkedin_callback',
		]);
	
		$token = $provider->getAccessToken('authorization_code', [
				'code' => $_GET['code']
		]);
			// We got an access token, let's now get the user's details
			$user = $provider->getResourceOwner($token);
	
		//echo '<pre>'; print_r($user->toArray()); echo '</pre>'; exit('Exit');
	
		// Use this to interact with an API on the users behalf
		//echo $token->getToken();
		
		$userProfile = $user->toArray();
		
		$username="";
		if(!empty($userProfile['emailAddress'])) {

            $password = mt_rand(10000000, 99999999);
			if (empty($userProfile['firstName'])) {

				$email_parts = explode("@", $userProfile['emailAddress']);
				$username = $email_parts[0];

			} else {

				// $username = preg_replace('/\s+/', '_', $userProfile['name']);
				$username = $userProfile['firstName']." ".$userProfile['lastName'];

			}
			
			$connect = "coach";
			if(isset($_COOKIE["connect"])) {
				$connect = $_COOKIE["connect"];
			}

			$this -> db -> select('ID,Name,Email,EmailVerification,Role,Status');
			$this -> db -> from('users');
			$this -> db -> where('Email', $userProfile['emailAddress']);
			//$this -> db -> where('password', md5($password));
			$this -> db -> limit(1);
			$query = $this->db->get(); 
			if($query -> num_rows() >= 1){

				$sess_data = $query->result();
				$sess_data = $sess_data[0];	
				$this->session->set_userdata(array('user'=>$sess_data));
				$this->db->where("Email", $userProfile['emailAddress'])->update("users", array("LastLogin"=>date('Y-m-d H:i:s'))); // update last login
				redirect('home');
			
			}else{

				$EmailVerification = md5($username.time());
				$insert_data = array(

						'Name' => $username,
						'Email' => $userProfile['emailAddress'],
						'Password' => md5($password),
						'EmailVerification' => $EmailVerification,
						'Role' => $connect,
						//'signup_method' => 'gmail',
						//'fb_id' => $userProfile['id'],
						'createdAt' => date('Y-m-d H:i:s')

				);

				//$insert_result = $this->common_model->insert_table('users', $insert_data);
				$insert_result = $this->db->insert('users', $insert_data);
				// send activation link //
				$from=$this->config->config['admin_email'];
				$from_name='SafiriSalama Memorials';
				$name=$username; 
				$to=$userProfile['emailAddress'];
				$subject='Account Creation';

				$vlink = base_url().'login/EmailVerification/'.$EmailVerification;
				$content='<p>Welcome to Coaches System, Your account has been created successfully, You need to verify your email address, Please click the button below to confirm your email address </p> <p><a style="text-decoration: none; cursor: pointer" target="_blank" href="'.$vlink.'"><button style="cursor: pointer; background-color: #e32318; text-decoration:none; color:#f9f9f9; display:block; padding:15px 30px; border: none" class="btn btn-danger">Verify Now</button></a></p>   <p> if verify button is not visible just copy link '.$vlink.' and paste as url to verify! </br>';																					
				$email_data['title']='<span>Verification Email<span>';
				$email_data['content']= $content;                 
				$content=$this->load->view('emails/general',$email_data,true);
				//$this->general_model->send_email($from, $to, $subject, $content, $from_name);
				$this->smtp_email->send($from,$from_name,$to,$subject,$content);
				//$this->smtp_email->send('support@mailresumes.com', 'Mail Resumes', $userProfile['emailAddress'], 'Welcome to Mail Resumes!', $this->email_temp->email_content('Welcome to Mail Resumes!', 'Your mail resumes account is now active. You can learn more about our services at <a href="https://mailresumes.com/welcome/services" style="color: #0c7cd5">mailresumes.com</a>. You can also click on the "Start the Process" tab here [<a href="https://mailresumes.com/job_criteria" style="color: #0c7cd5">Start the Process tab</a>] to begin your search for a new job today.', '', ''));
				$this -> db -> select('ID,Name,Role,Email,EmailVerification,Status');
				$this -> db -> from('users');
				$this -> db -> where('Email', $userProfile['emailAddress']);
				//$this -> db -> where('password', md5($password));
				$this -> db -> limit(1);
				$query = $this->db->get(); 

				if($query -> num_rows() >= 1){

					$sess_data = $query->result();
					$sess_data = $sess_data[0];	
					$this->session->set_userdata(array('user'=>$sess_data));
					$this->db->where("Email", $userProfile['emailAddress'])->update("users", array("LastLogin"=>date('Y-m-d H:i:s'))); // update last login
					redirect('home');
				}					
		   }   

		} else {

            $this->session->set_flashdata('error1', 'Unable to process, Please try again later!'); 	
			redirect('login');        
		}
	}
	
	public function is_page_admin($memo_id, $user_id){
			
		$cnt = $this->db->query("select count(*) as cnt from memorials where ID='$memo_id' and createdby = '$user_id'")->row()->cnt;
		if($cnt > 0){
			return true;
			exit;
		}else{
			$cnt = $this->db->query("select count(*) as cnt from memorial_admin where memo_id = '$memo_id' and user_id = '$user_id'")->row()->cnt;
			if($cnt > 0){
				return true;
				exit;
			}else{
				return false;
				exit;
			}
		}		
	}
	
	
	
}
