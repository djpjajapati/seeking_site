<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct(){
			parent::__construct();
			
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			$this->email = ""; 
			if(isset($sess_data->username))
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				$this->email = $sess_data->email; 
			}      
	}

	public function index()
	{		
		$this->data['title'] = "About us | Safiri Salama";
		
		$this->data['meta_description'] = "About us - Safiri Salama";
		$this->data['meta_keywords'] = "About us - Safiri Salama";
		
		$this->data['description'] = "About us - Safiri Salama";
		
		$this->data['total_memorial'] = $this->db->query("select count(*) as cnt from memorials")->row()->cnt;
		$this->data['total_visitor'] = $this->db->query("SELECT sum(views) as cnt FROM memorials")->row()->cnt;
		$this->data['total_tributes'] = $this->db->query("select count(*) as cnt from memo_tribute")->row()->cnt + $this->db->query("select count(*) as cnt from featured_memo_tribute")->row()->cnt;
		$this->data['picture_uploaded'] = $this->db->query("select count(*) as cnt from memo_pic")->row()->cnt;
		$this->data['total_picture_uploaded'] = $this->data['picture_uploaded'] + $this->data['total_memorial'];
		
		$testimonials = $this->db->query("select * from testimonials where user_id > 0 order by id desc")->result();
		foreach($testimonials as $t){
			$t->user_pic = $this->db->query("select pic from users where ID = ".$t->user_id)->row()->pic;
		}
		
		//echo "<pre>"; print_r($testimonials); exit;
		
		$this->data['testimonials'] = $testimonials;
        $this->load->view("about", $this->data);
        
	}
}
