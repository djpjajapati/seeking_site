<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	function __construct(){
			parent::__construct();
            //$this->load->library('cart');
			$this->load->library("pagination");
			$sess_data=$this->session->userdata('user');

			$this->ID = 0;
			$this->username = "";
			$this->email = ""; 
			if(isset($sess_data->email))
			{
				$this->ID = $sess_data->ID;
				$this->username = $sess_data->username;
				$this->email = $sess_data->email; 
			}      
	}

	public function index($page_num = '')
	{
        $min = 0;
        $limit = 8;
        if($page_num > 1){
            $min = $limit*($page_num - 1);
        }

        $this->data['keywords'] = "";

        $result = $this->db->query("select * from categories where Image!=''")->result();
        $res = $this->db->query("select * from categories where Image!='' order by ID asc limit ".$min.", ".$limit)->result();

        $config = array();
        $config["base_url"] = base_url().'categories/index';
        $total_row = count($result);
        $config["total_rows"] = $total_row;
        $config["per_page"] = 8;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '&nbsp;<a class="active" style="background: #c49b63; color: #fff; border: 1px solid transparent;">';
        //$config['cur_tag_open'] = '&nbsp;<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        if($this->uri->segment(1)){
            $page = ($this->uri->segment(1)) ;
        }
        else{
            $page = 1;
        }

        $str_links = $this->pagination->create_links();
        $links = explode('&nbsp;',$str_links );
        //echo "<pre>"; print_r($links); exit;
        $this->data['links'] = $links;

        //$categories = $this->db->query("select * from categories where Image!='' order by ID desc limit 8")->result();
		$this->data['categories'] = $res;
        $this->load->view("categories", $this->data);
        
	}

	public function search($page_num = ''){

	    $keywords = $this->input->post('keywords');
	    if(strlen(trim($keywords)) == 0){
	        redirect('categories');
        }
        $this->data['keywords'] = $keywords;
        $res = $this->db->query("select * from categories where Image!='' AND Name like '%".$keywords."%' order by ID desc")->result();
        $this->data['categories'] = $res;
        $this->load->view("categories", $this->data);

    }

    public function view($id, $page_num=''){

        $min = 0;
        $limit = 4;
        if($page_num > 1){
            $min = $limit*($page_num - 1);
        }

        $keywords = "";
        if($this->input->post('keywords')){
            $keywords = $this->input->post('keywords');
        }

        if(strlen($keywords) > 0){   // No need of pagination on search

            $this->data['category_name'] = $this->db->query("select Name from categories where ID = ".$id)->row()->Name;
            $this->data['keywords'] = $keywords;

            $result = $this->db->query("select * from products WHERE FIND_IN_SET('$id', Category) AND Title like '%".$keywords."%' ")->result();
            foreach ($result as $p){
                $p->price = $this->db->query("select price from product_weight_price where pid = ".$p->ID." limit 1")->row()->price;
            }

            $this->data['products'] = $result;
            $this->load->view("category_products", $this->data);

        }else{

            if($page_num > 0) {
                $this->data['category_name'] = $this->db->query("select Name from categories where ID = " . $id)->row()->Name;
                $this->data['keywords'] = $keywords;

                $result = $this->db->query("select * from products WHERE FIND_IN_SET('$id', Category) AND Title like '%" . $keywords . "%' ")->result();
                $res = $this->db->query("select * from products WHERE FIND_IN_SET('$id', Category) AND Title like '%" . $keywords . "%' order by ID desc limit " . $min . ", " . $limit)->result();

                $config = array();
                $config["base_url"] = base_url() . 'categories/view/' . $id;
                $total_row = count($result);
                $config["total_rows"] = $total_row;
                $config["per_page"] = 4;
                $config['use_page_numbers'] = TRUE;
                $config['num_links'] = $total_row;
                $config['cur_tag_open'] = '&nbsp;<a class="active" style="background: #c49b63; color: #fff; border: 1px solid transparent;">';
                //$config['cur_tag_open'] = '&nbsp;<a>';
                $config['cur_tag_close'] = '</a>';
                $config['next_link'] = '>';
                $config['prev_link'] = '<';

                $this->pagination->initialize($config);
                if ($this->uri->segment(1)) {
                    $page = ($this->uri->segment(1));
                } else {
                    $page = 1;
                }

                $str_links = $this->pagination->create_links();
                $links = explode('&nbsp;', $str_links);
                //echo "<pre>"; print_r($links); exit;
                $this->data['links'] = $links;

                foreach ($res as $p) {
                    $p->price = $this->db->query("select price from product_weight_price where pid = " . $p->ID . " limit 1")->row()->price;
                }

                //echo '<pre>'; print_r($res); echo '</pre>'; exit('Exit');
                $this->data['products'] = $res;
                $this->load->view("category_products", $this->data);

            } else {
                $this->data['keywords'] = $keywords;
                redirect('categories/view/' . $id . '/1', $this->data);
            }
        }
    }
	
	public function get_custom_coffee_price(){
		
		$weight = $this->input->post('weight');
		
		$bean1 = $this->input->post('bean1');
		$bean2 = $this->input->post('bean2');
		$bean3 = $this->input->post('bean3');
		$bean4 = $this->input->post('bean4');
		
		$coffee1 = $this->input->post('coffee1');
		$coffee2 = $this->input->post('coffee2');
		$coffee3 = $this->input->post('coffee3');
		$coffee4 = $this->input->post('coffee4');
		
		$price = 0;
		if($bean2 == 0){ // One bean selected
			
			$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
			$price = $b1->PricePerGram * $weight;			
			//echo $price;
			//exit;
			
		}else{
			
			if($bean3 == 0){ // 2 beans selected
				
				$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
				$b2 = $this->db->query("select PricePerGram from beans where ID = ".$bean2)->row();
				
				// Get weight according to percentage
				$w1 = $weight * ($coffee1/100);
				$w2 = $weight * ($coffee2/100);
				
				// Get prices according to percentage
				$p1 = $b1->PricePerGram * $w1;
				$p2 = $b2->PricePerGram * $w2;
				
				$price = $p1 + $p2;
				//echo $price;
				//exit;
				
			}else{
				
				if($bean4 == 0){ // 3 beans selected
					//echo "here are we"; exit;
					$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
					$b2 = $this->db->query("select PricePerGram from beans where ID = ".$bean2)->row();
					$b3 = $this->db->query("select PricePerGram from beans where ID = ".$bean3)->row();
					
					// Get weight according to percentage
					$w1 = $weight * ($coffee1/100);
					$w2 = $weight * ($coffee2/100);
					$w3 = $weight * ($coffee3/100);
					
					// Get prices according to percentage
					$p1 = $b1->PricePerGram * $w1;
					$p2 = $b2->PricePerGram * $w2;
					$p3 = $b3->PricePerGram * $w3;
					$price = $p1 + $p2 + $p3;
					//echo $price;
					//exit;
					
				}else{ // all 4 beans selected
					
					$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
					$b2 = $this->db->query("select PricePerGram from beans where ID = ".$bean2)->row();
					$b3 = $this->db->query("select PricePerGram from beans where ID = ".$bean3)->row();
					$b4 = $this->db->query("select PricePerGram from beans where ID = ".$bean4)->row();
					
					// Get weight according to percentage
					$w1 = $weight * ($coffee1/100);
					$w2 = $weight * ($coffee2/100);
					$w3 = $weight * ($coffee3/100);
					$w4 = $weight * ($coffee4/100);
					
					// Get prices according to percentage
					$p1 = $b1->PricePerGram * $w1;
					$p2 = $b2->PricePerGram * $w2;
					$p3 = $b3->PricePerGram * $w3;
					$p4 = $b4->PricePerGram * $w4;
					
					$price = $p1 + $p2 + $p3 + $p4;
					//echo $price;
					//exit;
					
				}
				
			}
			
		}
		
		$price = number_format((float)$price, 2, '.', '');   // round to 2 decimal places
		
		$cnt = $this->db->query("select count(*) as cnt from custom_coffee")->row()->cnt+1;
		$data['title'] = "My coffee ".$cnt;
		
		$data['bean1'] = $bean1;
		$data['bean2'] = $bean2;
		$data['bean3'] = $bean3;
		$data['bean4'] = $bean4;
		
		$data['percent1'] = $coffee1;
		$data['percent2'] = $coffee2;
		$data['percent3'] = $coffee3;
		$data['percent4'] = $coffee4;
		
		$data['price'] = $price;
		$data['createdBy'] = 1;
		$data['createdAt'] = date("Y-m-d H:i:s");
		
		$this->db->insert("custom_coffee", $data);
		echo $price;
		
	}

}
