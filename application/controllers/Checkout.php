<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	function __construct(){
			parent::__construct();
			$sess_data=$this->session->userdata('user');
			$this->ID = 0; 
			$this->username = ""; 
			$this->email = ""; 
			if(!isset($sess_data->username)){
				redirect('home');
			}
			$this->ID = $sess_data->ID; 
			$this->username = $sess_data->username; 
			$this->email = $sess_data->email; 			
			
			
			
			$this->merchant_email='davidmark0772@gmail.com';
		    $this->secret_key='AaWVOQ3KeGLwPYePMi02N0FcuSakJpDkAzgoqk9E1UaQTpGGIGIwHnIbgTv0j9lTJCh9WqNYlYPfovW7cFDT2m4jfAjyJErshJCd';
		    $this->merchant_id='10040642';

		    $this->params=array('merchant_email'=>$this->merchant_email,
						 'merchant_id'=>$this->merchant_id,
						 'secret_key'=>$this->secret_key);
						 
		    $this->load->library('paytabs',$this->params);
			
	}
	
	public function index(){
		
		$this->data['title'] = 'Checkout';
		$cart = $cart = $this->cart->contents();
		$this->data['shipping'] = $this->db->query("select * from shipping")->result();
        $this->load->view("checkout", $this->data);
		
	}

	public function pay_now()
	{
		
		   //echo "<pre>"; print_r($_POST); exit;		   
		
		   $reference_no = 1231231;   //Invoice reference number in your system (may be product ID or user ID) or any app record
		   
		   //Customer's Personal Information
		   $customer_name=$this->username; //"Johnny Depp";
		   $customer_fname=$this->input->post('fname'); //"Johnny";
		   $customer_lname=$this->input->post('lname'); //"Depp";
		   $customer_email=$this->input->post('email'); //"johny123@gmail.com";
		   $customer_phone=$this->input->post('phone');
		   
		   
		   $shipping_id=$this->input->post('shipping_id');
		   
		   //End Customer's Personal Information
		   
		   $product_title = 'title'; //Product title of the product. If multiple products then add “||” separator 
		   $amount = $this->input->post('total_price'); //200;
		   $discount = 0;
		   
		   // Billing Address
		   $b_address = $this->input->post('address1'); //"Juffair, Manama, Bahrain";
		   $b_city = $this->input->post('city');  //"Manama";
		   $b_state = $this->input->post('city'); //"Capital";
		   $b_postal_code = $this->input->post('zip');;
		   $b_shipping_contry = "BHR";
		   // End Billing Address
		   
		   // Shipping Address
		   $s_address = $this->input->post('address2'); //"Muharaq, Manama, Bahrain";
		   $s_city = $this->input->post('city2'); //"Manama";
		   $s_state = $this->input->post('city2'); //"Capital";
		   $s_postal_code = $this->input->post('zip2'); //97300;
		   $s_shipping_contry = "BHR";
		   // End Shipping Address
		   
		   // Create Paypage request
		   
		   $request_data=  array(
			//PayTabs Merchant Account Details
			//Customer's Personal Information
			'title' => $customer_name, 			// Customer's Name on the invoice
			'cc_first_name' => $customer_fname, 		//This will be prefilled as Credit Card First Name
			'cc_last_name' => $customer_lname, 		//This will be prefilled as Credit Card Last Name
			'email' => $customer_email,
			'cc_phone_number' => $customer_phone,
			'phone_number' => $customer_phone,
			
			//Customer's Billing Address (All fields are mandatory)
			//When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected. 
			//For other countries, the state can be a string of up to 32 characters.
			'billing_address' => $b_address,
			'city' => $b_city,
			'state' => $b_state,
			'postal_code' => $b_postal_code,
			'country' => $b_shipping_contry,
		   
			//Customer's Shipping Address (All fields are mandatory)
			
			'address_shipping' => $s_address,
			'city_shipping' => $s_city,
			'state_shipping' => $s_state,
			'postal_code_shipping' => $s_postal_code,
			'country_shipping' => $s_shipping_contry,
			//Product Information
			"products_per_title"=>$product_title, //Product title of the product. If multiple products then add “||” separator
			'currency' => "USD", //Currency of the amount stated. 3 character ISO currency code
			"unit_price"=>$amount, //Unit price of the product. If multiple products then add “||” separator.
			'quantity' => "1",  //Quantity of products. If multiple products then add “||” separator
			'other_charges' => "0",	//Additional charges. e.g.: shipping charges, taxes, VAT, etc.
			'amount' =>$amount,  
			'discount'=>$discount, 
			"msg_lang" => "english", //Language of the PayPage to be created.  default - English
			"reference_no" => $reference_no,//Invoice reference number in your system
			"site_url" => "http://qualityend.com", //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
			'return_url' => "http://qualityend.com/coffee_shop/checkout/callback",
			"cms_with_version" => "API USING PHP"
		);
   
		   // End create Paypage request


		$result = $this->paytabs->create_pay_page($request_data);
	
		//echo "FOLLOWING IS THE RESPONSE: <br />";
		//print_r ($result);
		$request_data['shipping_id'] = $shipping_id;  // shipping id later store with order
		$_SESSION['paytabs_request_data'] = $request_data;
		
		echo $result->payment_url; exit;
		
		/* echo '<script type="text/javascript">
		            window.location = "'.$result->payment_url.'"
		       </script>';
		 */
		//$_SESSION['paytabs_api_key'] = $result->secret_key;
	}
	
	public function callback(){
		
		$resp = $this->paytabs->verify_payment($_POST['payment_reference']);	
		$req = $_SESSION['paytabs_request_data'];
		
		if($resp->response_code == '100'){
			
			//response data
			$data['result'] = $resp->result;
			$data['response_code'] = $resp->response_code;
			$data['pt_invoice_id'] = $resp->pt_invoice_id;
			$data['amount'] = $resp->amount;
			$data['currency'] = $resp->currency;
			$data['reference_no'] = $resp->reference_no;
			$data['transaction_id'] = $resp->transaction_id;
			
			
			// request data
			$data['fname'] = $req['cc_first_name'];
			$data['lname'] = $req['cc_last_name'];
			$data['email'] = $req['email'];
			$data['phone'] = $req['phone_number'];
			$data['billing_address'] = $req['billing_address'];
			$data['city'] = $req['city'];
			$data['state'] = $req['state'];
			$data['postal_code'] = $req['postal_code'];
			$data['country'] = $req['country'];
			$data['address_shipping'] = $req['address_shipping'];
			$data['city_shipping'] = $req['city_shipping'];
			$data['state_shipping'] = $req['state_shipping'];
			$data['postal_code_shipping'] = $req['postal_code_shipping'];
			$data['country_shipping'] = $req['country_shipping'];
			$data['shipping_id'] = $req['shipping_id'];
			$data['currency'] = $req['currency'];
			$data['amount'] = $req['amount'];
			$data['discount'] = $req['discount'];
			$data['reference_no'] = $req['reference_no'];
			$data['customer_id'] = $this->ID;
			$createdAt = date("Y-m-d H:i:s");
			$data['createdAt'] = $createdAt;
			
			
			$this->db->insert("orders", $data);
			$orderId = $this->db->insert_id();
			
			

			
			$cart = $this->cart->contents();
			if($this->cart->total_items() > 0){

				foreach($cart as $item){
					
					$product['oid'] = $orderId;
					$product['pid'] = $item['id'];
					$product['name'] = $item['name'];
					$product['price'] = $item['price'];
					$product['quantity'] = $item['qty'];
					$product['subtotal'] = $item['subtotal'];
					$product['createdAt'] = $createdAt; 
						
					$this->db->insert("order_products", $product);				
				}
					
			}
			
			$this->session->unset_userdata('paytabs_request_data');	
			$this->cart->destroy();
			
		}else{
			//echo "Request Fail: ";
			//echo $resp->result;
			$this->session->set_flashdata('error', "RESPONSE CODE: ".$resp->response_code."  ".$resp->result);
			redirect('checkout');
		}
		
		$this->session->set_flashdata('success', 'Your order is placed successfully');
		redirect('checkout');
	}
	
}
