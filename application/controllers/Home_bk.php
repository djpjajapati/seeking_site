<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
			parent::__construct();
			
	}

	public function index()
	{

		$this->data['beans'] = $this->db->query("select * from beans")->result();
        $this->load->view("custom_coffee", $this->data);
		
	}
	
	public function get_custom_coffee_price(){
		
		$weight = $this->input->post('weight');
		
		$bean1 = $this->input->post('bean1');
		$bean2 = $this->input->post('bean2');
		$bean3 = $this->input->post('bean3');
		$bean4 = $this->input->post('bean4');
		
		$coffee1 = $this->input->post('coffee1');
		$coffee2 = $this->input->post('coffee2');
		$coffee3 = $this->input->post('coffee3');
		$coffee4 = $this->input->post('coffee4');
		
		$price = 0;
		if($bean2 == 0){ // One bean selected
			
			$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
			$price = $b1->PricePerGram * $weight;			
			//echo $price;
			//exit;
			
		}else{
			
			if($bean3 == 0){ // 2 beans selected
				
				$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
				$b2 = $this->db->query("select PricePerGram from beans where ID = ".$bean2)->row();
				
				// Get weight according to percentage
				$w1 = $weight * ($coffee1/100);
				$w2 = $weight * ($coffee2/100);
				
				// Get prices according to percentage
				$p1 = $b1->PricePerGram * $w1;
				$p2 = $b2->PricePerGram * $w2;
				
				$price = $p1 + $p2;
				//echo $price;
				//exit;
				
			}else{
				
				if($bean4 == 0){ // 3 beans selected
					//echo "here are we"; exit;
					$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
					$b2 = $this->db->query("select PricePerGram from beans where ID = ".$bean2)->row();
					$b3 = $this->db->query("select PricePerGram from beans where ID = ".$bean3)->row();
					
					// Get weight according to percentage
					$w1 = $weight * ($coffee1/100);
					$w2 = $weight * ($coffee2/100);
					$w3 = $weight * ($coffee3/100);
					
					// Get prices according to percentage
					$p1 = $b1->PricePerGram * $w1;
					$p2 = $b2->PricePerGram * $w2;
					$p3 = $b3->PricePerGram * $w3;
					$price = $p1 + $p2 + $p3;
					//echo $price;
					//exit;
					
				}else{ // all 4 beans selected
					
					$b1 = $this->db->query("select PricePerGram from beans where ID = ".$bean1)->row();
					$b2 = $this->db->query("select PricePerGram from beans where ID = ".$bean2)->row();
					$b3 = $this->db->query("select PricePerGram from beans where ID = ".$bean3)->row();
					$b4 = $this->db->query("select PricePerGram from beans where ID = ".$bean4)->row();
					
					// Get weight according to percentage
					$w1 = $weight * ($coffee1/100);
					$w2 = $weight * ($coffee2/100);
					$w3 = $weight * ($coffee3/100);
					$w4 = $weight * ($coffee4/100);
					
					// Get prices according to percentage
					$p1 = $b1->PricePerGram * $w1;
					$p2 = $b2->PricePerGram * $w2;
					$p3 = $b3->PricePerGram * $w3;
					$p4 = $b4->PricePerGram * $w4;
					
					$price = $p1 + $p2 + $p3 + $p4;
					//echo $price;
					//exit;
					
				}
				
			}
			
		}
		
		$price = number_format((float)$price, 2, '.', '');   // round to 2 decimal places
		
		$cnt = $this->db->query("select count(*) as cnt from custom_coffee")->row()->cnt+1;
		$data['title'] = "My coffee ".$cnt;
		
		$data['bean1'] = $bean1;
		$data['bean2'] = $bean2;
		$data['bean3'] = $bean3;
		$data['bean4'] = $bean4;
		
		$data['percent1'] = $coffee1;
		$data['percent2'] = $coffee2;
		$data['percent3'] = $coffee3;
		$data['percent4'] = $coffee4;
		
		$data['price'] = $price;
		$data['createdBy'] = 1;
		$data['createdAt'] = date("Y-m-d H:i:s");
		
		$this->db->insert("custom_coffee", $data);
		echo $price;
		
	}

}
