<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lnme extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library("Smtp_email");	 
			$this->load->library("Emailtemp");
 					
			$this->load->library('session'); 
			$this->load->helper('form'); 
			$sess_data=$this->session->userdata('user');
			
			$this->ID = 0; 
			$this->username = ""; 
			//$this->email = ""; 
			//if(isset($sess_data->ID))
			if($this->session->userdata('user')) 
			{
				$this->ID = $sess_data->ID; 
				$this->username = $sess_data->username; 
				//$this->email = $sess_data->email; 
			}       
			
			/* $this->merchant_email='davidmark0772@gmail.com';
		    $this->secret_key='AaWVOQ3KeGLwPYePMi02N0FcuSakJpDkAzgoqk9E1UaQTpGGIGIwHnIbgTv0j9lTJCh9WqNYlYPfovW7cFDT2m4jfAjyJErshJCd';
		    $this->merchant_id='10040642';

		    $this->params=array('merchant_email'=>$this->merchant_email,
						 'merchant_id'=>$this->merchant_id,
						 'secret_key'=>$this->secret_key);
						 
		    $this->load->library('paytabs',$this->params); */
			
	}

	public function index()
	{
		
			// mpesa response
			/* $res = '{"Body":{"stkCallback":{"MerchantRequestID":"28042-1031179-1","CheckoutRequestID":"ws_CO_DMZ_463810850_01052019170708481","ResultCode":0,"ResultDesc":"The service request is processed successfully.",
			"CallbackMetadata":{"Item":[{"Name":"Amount","Value":3.00},{"Name":"MpesaReceiptNumber","Value":"NE18M7MYHQ"},
			{"Name":"Balance"},{"Name":"TransactionDate","Value":20190501170802},{"Name":"PhoneNumber","Value":254724756011}]}}}}';
			 
			$response =json_decode($res);
			
			if($response->Body->stkCallback->ResultCode == 0){
				
				$this->db->insert('mpesa_response', array("response"=>$res, "paid_by"=>$this->ID)); 
			
				$data['merchant_id'] = $response->Body->stkCallback->MerchantRequestID;
				$data['CheckoutRequestID'] = $response->Body->stkCallback->CheckoutRequestID;
				$data['ResultCode'] = $response->Body->stkCallback->ResultCode;
				$$data['ResultDesc'] = $response->Body->stkCallback->ResultDesc;
				
				$CallbackMetadata = $response->Body->stkCallback->CallbackMetadata;
				$data['amount'] = $CallbackMetadata->Item[0]->Value;
				$data['MpesaReceiptNumber'] = $CallbackMetadata->Item[1]->Value;
				$data['TransactionDate']=$CallbackMetadata->Item[3]->Value;
				$data['PhoneNumber']=$CallbackMetadata->Item[4]->Value;
				
				//echo '<pre>'; print_r($data);	
			} */
	}
	
	//public function LipaNaMpesa(Request $request)
	public function LipaNaMpesa()
	{	
		$amount = $this->input->post('ttl');
		$phone_mpesa = $this->input->post('phone_mpesa');
		
		$MERCHANT_ID = "925888";
		$passkey = "f6dcc01d4b5513c6c0372ef723679d132fa60f25427a6089617fb02a35c6fb59";


		$TIMESTAMP = date('YmdHis');

		$PASSWORD = base64_encode( $MERCHANT_ID . $passkey . $TIMESTAMP );

		$headers = array("MERCHANT_ID"=>$MERCHANT_ID,
			"PASSWORD"=>$PASSWORD,
			"TIMESTAMP"=>date('YmdHis'));

		$TRX_ID =  date('U');
		$TransactionDesc =  'testing payment'; //$request->TransactionDesc;
		$reference = 'asd123456'; //$request->reference_id;
		$amount = $amount; //$request->amount;
		//$msisdn = '254' . substr($request->phone,-9,9);
		$msisdn = '254' . substr($phone_mpesa,-9,9);
		$m_trx_id = $msisdn."_".$TRX_ID;
		$encParams = $m_trx_id;

		//"https://buyticketsonline.co.uk/call_back.php" ,
		
		$call_back_url = base_url()."lnme/mpesa_callback";
		$realjson = '{ "BusinessShortCode" : "'.$MERCHANT_ID .'",
				"Password" :"'.$PASSWORD.'",
				"Timestamp" :"'.$TIMESTAMP.'",
				"TransactionType" :"CustomerPayBillOnline" ,
				"Amount" :"'.$amount.'",
				"PartyA":"'.$msisdn.'",
				"PartyB" :"'.$MERCHANT_ID .'",
				"PhoneNumber" :"'.$msisdn.'",
				"CallBackURL" :"'.$call_back_url.'", 
				"AccountReference" :"'.$reference.'",
				"BillRefNumber" :"reference",
				"TransactionDesc":"'.$reference.'" }';


			$url = "https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest";

			//request token for using on header auth

			$response = json_decode($this->getAuoth('I4I1Lqp3AYqTmDdAcRkLFHUIuVJX7ki3','ssK1mbVtH13u3XUf'));

			//return json_encode($response);

			$token = $response->message;
			// return $token;
			$responseCode = $response->status;

			// echo $responseCode .'</br>';

			if ($responseCode) {
			//
			
			$pakage = $this->db->query("select title from payment_plan where ID = ".$_POST['p1'])->row()->title;
			//session_start();
			$_SESSION['add_memo']['pakage'] = $pakage;
			$_SESSION['add_memo']['amount_paid'] = $_POST['ttl'];
			
			
			$ref_no = time('His').rand(100, 1000);
			$_SESSION['mpesa_ref_no'] = $ref_no; 
			$this->db->insert("mpesa_response", array("ref_no"=>$ref_no, "ResultCode"=>1));
			
			//print_r($_SESSION['add_memo']); exit;		
			
			if($_POST['ttl'] <= 0){  // skip payment in case of 0 or free memorial
				$this->skip_payment(); 
				exit;  
			}
			
			//////////////////REMOVE THIS TO TEST LIVE/////////////
			//$this->mpesa_callback(); exit;  
			//////////////////REMOVE THIS TO TEST LIVE/////////////
				

			return $this -> testCurl( $url, $token, $realjson );

			} else {
			return "Please try again";
			}
	}
	
	public function check_mpesa_response(){
		
		$ref_no = $_SESSION['mpesa_ref_no'];
		$resp = $this->db->query("select * from mpesa_response where ref_no = '$ref_no' and response IS NOT NULL and response != '' ")->row();
		
		if($resp){
			if($resp->ResultCode === '0'){
				echo 'success';
				exit;
			}else{
				echo $resp->ResultDesc;
				exit;
			}
		}
		
	}
	
	public function test(){
		
		//1557986930216
		//echo $_SESSION['mpesa_ref_no'];
		
		/* $ref_no = $_SESSION['mpesa_ref_no'];
		$resp = $this->db->query("select * from mpesa_response where ref_no = '$ref_no' and response IS NOT NULL and response != '' ")->row();
		 */
		
		/* $ref_no = time('His').rand(100, 1000);
		$_SESSION['mpesa_ref_no'] = $ref_no; 
		$this->db->insert("mpesa_response", array("ref_no"=>$ref_no));
		echo $ref_no;  */
		
	}
	
	public function LipaNaMpesa_upgrade(){
		
		$pakage_title = $this->input->post('pakage_title');
		$amount = $this->input->post('amount');
		$phone_mpesa = $this->input->post('phone_mpesa');
		
		$MERCHANT_ID = "925888";
		$passkey = "f6dcc01d4b5513c6c0372ef723679d132fa60f25427a6089617fb02a35c6fb59";


		$TIMESTAMP = date('YmdHis');

		$PASSWORD = base64_encode( $MERCHANT_ID . $passkey . $TIMESTAMP );

		$headers = array("MERCHANT_ID"=>$MERCHANT_ID,
			"PASSWORD"=>$PASSWORD,
			"TIMESTAMP"=>date('YmdHis'));

		$TRX_ID =  date('U');
		$TransactionDesc =  'testing payment'; //$request->TransactionDesc;
		$reference = 'asd123456'; //$request->reference_id;
		$amount = $amount; //$request->amount;
		//$msisdn = '254' . substr($request->phone,-9,9);
		$msisdn = '254' . substr($phone_mpesa,-9,9);
		$m_trx_id = $msisdn."_".$TRX_ID;
		$encParams = $m_trx_id;

		//"https://buyticketsonline.co.uk/call_back.php" ,
		
		$call_back_url = base_url()."lnme/mpesa_callback_upgrade";
		$realjson = '{ "BusinessShortCode" : "'.$MERCHANT_ID .'",
				"Password" :"'.$PASSWORD.'",
				"Timestamp" :"'.$TIMESTAMP.'",
				"TransactionType" :"CustomerPayBillOnline" ,
				"Amount" :"'.$amount.'",
				"PartyA":"'.$msisdn.'",
				"PartyB" :"'.$MERCHANT_ID .'",
				"PhoneNumber" :"'.$msisdn.'",
				"CallBackURL" :"'.$call_back_url.'", 
				"AccountReference" :"'.$reference.'",
				"BillRefNumber" :"reference",
				"TransactionDesc":"'.$reference.'" }';


			$url = "https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest";

			//request token for using on header auth

			$response = json_decode($this->getAuoth('I4I1Lqp3AYqTmDdAcRkLFHUIuVJX7ki3','ssK1mbVtH13u3XUf'));

			//return json_encode($response);

			$token = $response->message;
			// return $token;
			$responseCode = $response->status;

			//echo $responseCode .'</br>'; exit;

			if ($responseCode) {
				
				
			$pakage = $this->db->query("select title from payment_plan where ID = ".$_POST['p1'])->row()->title;
				
			$update_memo['plan_id'] = $_POST['p1'];
			$update_memo['user_id'] = $_POST['p2'];
			$update_memo['memo_id'] = $_POST['p3'];
			$update_memo['price'] = $_POST['ttl'];
			$update_memo['pakage'] = $pakage;
			$update_memo['package_start_date'] = date('Y-m-d');
			$update_memo['package_end_date'] = date('Y-m-d', strtotime('+1 year'));	
			
			$_SESSION['update_memo'] = $update_memo;
			
			$ref_no = time('His').rand(100, 1000);
			$_SESSION['mpesa_ref_no'] = $ref_no; 
			$this->db->insert("mpesa_response", array("ref_no"=>$ref_no, "ResultCode"=>1));
			
			if($_POST['ttl'] <= 0){
				$this->skip_payment_upgrade(); 
				exit;  // skip payment
			}
			
			//$this->mpesa_callback_upgrade(); exit;
			return $this -> testCurl( $url, $token, $realjson );

			} else {
				return "Please try again";
			}
	}
	
	public function skip_payment(){
		
		$pakage = $_SESSION['add_memo']['pakage'];
		$price = $_SESSION['add_memo']['amount_paid'];
		
		
		$webaddress = $_SESSION['add_memo']['webaddress'];
		$remailadd = $_SESSION['add_memo']['remailadd'];
		$memorial_name = $_SESSION['add_memo']['fname'].' '.$_SESSION['add_memo']['lname'];
		
		$pakage_detail = $this->db->query("select price from payment_plan where title = '$pakage'")->row();
		$pakage_price=$pakage_detail->price;
		$username = $_SESSION['add_memo']['rfname'].' '.$_SESSION['add_memo']['rsurname'];

		//echo "insert"; print_r($_SESSION['add_memo']); exit;	
		//insert query 			
		$this->db->insert('memorials', $_SESSION['add_memo']); 
		$memo_id = $this->db->insert_id();
		//$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "response"=>$ipnurl, "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
		
		
		// update activity_log
		$description = $username." created a memorial for ".$memorial_name.'. Amount paid is '.$price.' KES.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log 
		
		
		if(isset($_SESSION['coupon'])){
			$coupon_code = $_SESSION['coupon']['coupon_code'];
			$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
			
			//reduce coupon frequency
			$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
			if($res){
				$new_freq = $res->frequency - 1;
				if($new_freq >= 0){ 
					$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
				}
			}	
		}
		
		unset($_SESSION['add_memo']); 
		unset($_SESSION['coupon']);
		
		//Creating invoices in allinvoices table.
		//$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_detail->nextprice,"status"=>"paid", "memorialid"=>$memo_id);
		$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id);
		$newinvoiceid=$this->createaninvoice($invoiceparams);


		// Send Confirmation email on creation
		$from=$this->config->config['admin_email'];
		if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
		$from_name="SafiriSalama Memorials";
		$subject="Memorial Confirmation Email";
		$vlink = base_url().'memorial/invoice/'.$newinvoiceid;
		
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='<p> Memorial for the '.$memorial_name.' has been created successfully. click <a href="'.$vlink.'">link</a> to view detail.</br><p></p>';
			$title = 'Memorial Confirmation Email';
			
			$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end	


			// Payment Receipt
			$subject="Memorial Payment Receipt";
			
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='';
			$title = 'Memorial Payment Receipt';
			
			$message_body = $this->emailtemp->payment_template_callback($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end payment receipt	
		
			// delete pending memorial from table
			$this->db->where("createdby", $this->ID)->delete('pending_memorials');
			
			redirect('memorial/payment_success?redirect=mymemorials'); 
		
	}
	
	public function skip_payment_upgrade(){
		
			$memo_id = $_SESSION['update_memo']['memo_id'];
			$user_id = $_SESSION['update_memo']['user_id'];
			$pakage = $_SESSION['update_memo']['pakage'];
			$price = $_SESSION['update_memo']['price'];
			$package_start_date = $_SESSION['update_memo']['package_start_date'];
			$package_end_date = $_SESSION['update_memo']['package_end_date']; 
			
			$mm = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
			$memorial_name = $mm->fname.' '.$mm->lname;
			
			$pakage_price = $this->db->query("select price from payment_plan where title = '$pakage'")->row()->price;
			
			//update memorial 
			$this->db->where('ID', $memo_id)->update("memorials", array("pakage"=>$pakage,"amount_paid"=>$price, "package_start_date"=>$package_start_date, "package_end_date"=>$package_end_date, "createdAt"=>date('Y-m-d H:i:s')));
			//$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "action"=>"upgrade", "response"=>$ipnurl, "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
			
			
			if(isset($_SESSION['coupon'])){
				
				$usr = $this->db->query("select fname, lname from users where ID = ".$user_id)->row();
				$username = $usr->fname.' '.$usr->lname;
				$coupon_code = $_SESSION['coupon']['coupon_code'];
				$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
				
				//reduce coupon frequency
				$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
				if($res){
					$new_freq = $res->frequency - 1;
					if($new_freq >= 0){ 
						$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					}
				}	
			}
			
			unset($_SESSION['add_memo']);
			unset($_SESSION['coupon']);		
			unset($_SESSION['update_memo']);
	
			redirect('memorial/payment_success?redirect=manage_memorial&memo_id='.$memo_id); 
		
	}
	
	public function test1(){
		
		/* $prefix = 'bla_';
		$str = 'bla_string_bla_bla_bla';
		$str = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $str);
		
		echo $str;  */
		
	}
	
	public function mpesa_callback_test_file()
	{
	    	//$json_string =  serialize($_POST);

			// For info re: JSON in PHP:
			// http://php.net/manual/en/function.json-encode.php
			//$json_string2 =  serialize($_GET);
			//$json_string3 =  serialize($_REQUEST);
			// For info re: writing files in PHP:
			// http://php.net/manual/en/function.fwrite.php
			$json_string44= serialize(file_get_contents('php://input'));
			   
			$res = substr($json_string44, strpos($json_string44, ':"') + 2); 
			$res = substr($res, 0, -2); 
			
			$myfile = fopen("filesss/response_file".date('l jS \of F Y h:i:s A').".txt", "w") or die("Unable to open file!");
			fwrite($myfile, $res);
			fclose($myfile);

	}
	
	public function mpesa_callback(){  
	
	
		$json_string44= serialize(file_get_contents('php://input'));
		 
		$myfile = fopen("filesss/response_file".date('l jS \of F Y h:i:s A').".txt", "w") or die("Unable to open file!");
		fwrite($myfile, $json_string44);
		fclose($myfile); 
		 
		$res = substr($json_string44, strpos($json_string44, ':"') + 2); 
		$res = substr($res, 0, -2);
	
		
	
		$response =json_decode($res);

		/* $res = '{"Body":{"stkCallback":{"MerchantRequestID":"28042-1031179-1","CheckoutRequestID":"ws_CO_DMZ_463810850_01052019170708481","ResultCode":0,"ResultDesc":"The service request is processed successfully.",
				"CallbackMetadata":{"Item":[{"Name":"Amount","Value":3.00},{"Name":"MpesaReceiptNumber","Value":"NE18M7MYHQ"},
				{"Name":"Balance"},{"Name":"TransactionDate","Value":20190501170802},{"Name":"PhoneNumber","Value":254724756011}]}}}}';
				  
		$response =json_decode($res); */
		
		if($response->Body->stkCallback->ResultCode == 0){
			
			$data['response'] = $res;
			$data['merchant_id'] = $response->Body->stkCallback->MerchantRequestID;
			$data['CheckoutRequestID'] = $response->Body->stkCallback->CheckoutRequestID;
			$data['ResultCode'] = $response->Body->stkCallback->ResultCode;
			$data['ResultDesc'] = $response->Body->stkCallback->ResultDesc;
			
			$CallbackMetadata = $response->Body->stkCallback->CallbackMetadata;
			$data['amount'] = $CallbackMetadata->Item[0]->Value;
			$data['MpesaReceiptNumber'] = $CallbackMetadata->Item[1]->Value;
			$data['TransactionDate']=$CallbackMetadata->Item[3]->Value;
			$data['PhoneNumber']=$CallbackMetadata->Item[4]->Value;
			$data['createdby']=$this->ID;
			$data['createdAt']=date('Y-m-d H:i:s');
			
			// insert with memo_id
			//$this->db->insert('mpesa_response', $data); 
			//echo '<pre>'; print_r($data);	
		}
		
		
		$pakage = $_SESSION['add_memo']['pakage'];
		$price = $_SESSION['add_memo']['amount_paid'];
		
		
		$webaddress = $_SESSION['add_memo']['webaddress'];
		$remailadd = $_SESSION['add_memo']['remailadd'];
		$memorial_name = $_SESSION['add_memo']['fname'].' '.$_SESSION['add_memo']['lname'];
		
		$pakage_detail = $this->db->query("select price from payment_plan where title = '$pakage'")->row();
		$pakage_price=$pakage_detail->price;
		$username = $_SESSION['add_memo']['rfname'].' '.$_SESSION['add_memo']['rsurname'];

		//echo "insert"; print_r($_SESSION['add_memo']); exit;	
		//insert query 			
		$this->db->insert('memorials', $_SESSION['add_memo']); 
		$memo_id = $this->db->insert_id();
		
		$data['memo_id']=$memo_id;

		// check previous request by ref_no
		
			$ref_no = $_SESSION['mpesa_ref_no'];
			$ref = $this->db->query("select * from mpesa_response where ref_no = '$ref_no' order by ID desc")->row();
			if($ref){
				$this->db->where("ref_no", $ref_no)->update("mpesa_response", $data);
			}
			
			//$this->db->insert('mpesa_response', $data); 
		//end request check
		
		$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "response"=>$ref_no, "paid_by"=>$this->ID, "payment_method"=>"mpesa", "createdAt"=>date('Y-m-d H:i:s')));
		
		
		// update activity_log
		$description = $username." created a memorial for ".$memorial_name.'. Amount paid is '.$price.' KES.';
		$this->db->insert("activity_log", array("memo_id"=>$memo_id, "description"=>$description, "createdAt"=>date("Y-m-d H:i:s")));
		// end update activity_log 
		
		
		if(isset($_SESSION['coupon'])){
			$coupon_code = $_SESSION['coupon']['coupon_code'];
			$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
			
			//reduce coupon frequency
			$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
			if($res){
				$new_freq = $res->frequency - 1;
				if($new_freq >= 0){ 
					$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
				}
			}	
		}
		
		unset($_SESSION['add_memo']); 
		unset($_SESSION['coupon']);
		
		//Creating invoices in allinvoices table.
		//$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_detail->nextprice,"status"=>"paid", "memorialid"=>$memo_id);
		$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id, "payment_method"=>"mpesa");
		$newinvoiceid=$this->createaninvoice($invoiceparams);


		// Send Confirmation email on creation
		$from=$this->config->config['admin_email'];
		if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
		$from_name="SafiriSalama Memorials";
		$subject="Memorial Confirmation Email";
		$vlink = base_url().'memorial/invoice/'.$newinvoiceid;
		
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='<p> Memorial for the '.$memorial_name.' has been created successfully. click <a href="'.$vlink.'">link</a> to view detail.</br><p></p>';
			$title = 'Memorial Confirmation Email';
			
			$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end	


			// Payment Receipt
			$subject="Memorial Payment Receipt";
			
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='';
			$title = 'Memorial Payment Receipt';
			
			$message_body = $this->emailtemp->payment_template_callback($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end payment receipt	
		
			// delete pending memorial from table
			$this->db->where("createdby", $this->ID)->delete('pending_memorials');
			
			redirect('memorial/payment_success?redirect=mymemorials'); 
	}
	
	public function ref_no(){
		//echo $_SESSION['mpesa_ref_no'];
	}
	
	
	public function mpesa_callback_upgrade(){
		
			//$json_string =  serialize($_POST);

			// For info re: JSON in PHP:
			// http://php.net/manual/en/function.json-encode.php
			//$json_string2 =  serialize($_GET);
			//$json_string3 =  serialize($_REQUEST);
			// For info re: writing files in PHP:
			// http://php.net/manual/en/function.fwrite.php
			
			$json_string44= serialize(file_get_contents('php://input'));
			
			$response =json_decode($json_string44);
			
			//$myfile = fopen("filesss/response_file".date('l jS \of F Y h:i:s A').".txt", "w") or die("Unable to open file!");
			//$txt = $json_string."\n";
			//fwrite($myfile, $txt);
			//$txt = $json_string2."\n";
			//fwrite($myfile, $txt);
			//$txt = $json_string3."\n"; 
			//fwrite($myfile, $txt);
			//$txt = $_SERVER['HTTP_REFERER'].$_SERVER['REMOTE_HOST']." - End";
			//fwrite($myfile, $txt);
			//fwrite($myfile, $response);
			//fclose($myfile); */

			
			/* $res = '{"Body":{"stkCallback":{"MerchantRequestID":"28042-1031179-1","CheckoutRequestID":"ws_CO_DMZ_463810850_01052019170708481","ResultCode":0,"ResultDesc":"The service request is processed successfully.",
					"CallbackMetadata":{"Item":[{"Name":"Amount","Value":3.00},{"Name":"MpesaReceiptNumber","Value":"NE18M7MYHQ"},
					{"Name":"Balance"},{"Name":"TransactionDate","Value":20190501170802},{"Name":"PhoneNumber","Value":254724756011}]}}}}';
					  
			$response =json_decode($res); */
			
			if($response->Body->stkCallback->ResultCode == 0){
				
				$data['response'] = $res;
				$data['merchant_id'] = $response->Body->stkCallback->MerchantRequestID;
				$data['CheckoutRequestID'] = $response->Body->stkCallback->CheckoutRequestID;
				$data['ResultCode'] = $response->Body->stkCallback->ResultCode;
				$data['ResultDesc'] = $response->Body->stkCallback->ResultDesc;
				
				$CallbackMetadata = $response->Body->stkCallback->CallbackMetadata;
				$data['amount'] = $CallbackMetadata->Item[0]->Value;
				$data['MpesaReceiptNumber'] = $CallbackMetadata->Item[1]->Value;
				$data['TransactionDate']=$CallbackMetadata->Item[3]->Value;
				$data['PhoneNumber']=$CallbackMetadata->Item[4]->Value;
				$data['createdby']=$this->ID;
				$data['createdAt']=date('Y-m-d H:i:s');
				
				$memo_id = $_SESSION['update_memo']['memo_id'];
				$this->db->where("memo_id", $memo_id)->update("mpesa_response", $data);
			}
				
			$memo_id = $_SESSION['update_memo']['memo_id'];
			$user_id = $_SESSION['update_memo']['user_id'];
			$pakage = $_SESSION['update_memo']['pakage'];
			$price = $_SESSION['update_memo']['price'];
			$package_start_date = $_SESSION['update_memo']['package_start_date'];
			$package_end_date = $_SESSION['update_memo']['package_end_date']; 
			
			$mm = $this->db->query("select fname, lname from memorials where ID = ".$memo_id)->row();
			$memorial_name = $mm->fname.' '.$mm->lname;
			
			$pakage_price = $this->db->query("select price from payment_plan where title = '$pakage'")->row()->price;
			
			//update memorial 
			$this->db->where('ID', $memo_id)->update("memorials", array("pakage"=>$pakage,"amount_paid"=>$price, "package_start_date"=>$package_start_date, "package_end_date"=>$package_end_date, "createdAt"=>date('Y-m-d H:i:s')));
			//$this->db->insert('ipay_response', array("memo_id"=>$memo_id, "pakage"=>$pakage, "action"=>"upgrade", "response"=>$ipnurl, "paid_by"=>$this->ID, "createdAt"=>date('Y-m-d H:i:s')));
			
			
			if(isset($_SESSION['coupon'])){
				
				$usr = $this->db->query("select fname, lname from users where ID = ".$user_id)->row();
				$username = $usr->fname.' '.$usr->lname;
				$coupon_code = $_SESSION['coupon']['coupon_code'];
				$this->db->insert("coupon_log", array("coupon_code"=>$coupon_code, "memo_id"=>$memo_id, "pakage"=>$pakage, "username"=>$username, "createdAt"=>date('Y-m-d H:i:s')));
				
				//reduce coupon frequency
				$res = $this->db->query("select frequency from coupons where coupon_code = '$coupon_code'")->row();
				if($res){
					$new_freq = $res->frequency - 1;
					if($new_freq >= 0){ 
						$this->db->where("coupon_code", $coupon_code)->update("coupons", array("frequency"=>$new_freq));
					}
				}	
			}
			
			
			unset($_SESSION['coupon']);
			unset($_SESSION['update_memo']);
			
			
			
			//Update allinvoices
			//$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_detail->nextprice,"status"=>"paid", "memorialid"=>$memo_id);
			$invoiceparams=array("userid"=>$this->session->userdata('user')->ID,"useremail"=>$this->session->userdata('user')->email, "wasgenerated"=>date('Y-m-d H:i:s'), "willexpireat"=>date('Y-m-d H:i:s'), "recursiveat"=>date('Y-m-d', strtotime('+1 years')), "recursiveunix"=>strtotime(date('Y-m-d', strtotime('+1 years'))), "invoiceid"=>rand(999,999999),"details"=>$pakage,"subtotal"=>$price,"discount"=>0,"totalprice"=>$price,"nextprice"=>$pakage_price,"status"=>"paid", "memorialid"=>$memo_id, "payment_method"=>"mpesa");
			$this->db->where('memorialid', $memo_id)->update("allinvoices", $invoiceparams);  // update that invoice

			// Send Confirmation email on creation
			$from=$this->config->config['admin_email'];
			if($from == "alexrobbio860@gmail.com") $from = "smtptesting@goalwebs.com";  
			$from_name="SafiriSalama Memorials";
			$subject="Memorial Confirmation Email";
			$vlink = base_url().'memorial/invoice/'.$newinvoiceid;
		
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='<p> Memorial for the '.$memorial_name.' has been upgraded successfully. click <a href="'.$vlink.'">link</a> to view detail.</br><p></p>';
			$title = 'Memorial Confirmation Email';
			
			$message_body = $this->emailtemp->att_attribute_email_template($title, $content, $vlink, $to_name);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end	


			// Payment Receipt
			$subject="Memorial Payment Receipt";
			
			// send to page admin
			$to=$remailadd;				
			$to_name=$username;				
			
			$content='';
			$title = 'Memorial Payment Receipt';
			
			$message_body = $this->emailtemp->payment_template_callback($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body);
			//end payment receipt	
		
			// delete pending memorial from table
			$this->db->where("createdby", $this->ID)->delete('pending_memorials');
				
			
			// Payment Receipt
			//$subject="Memorial Payment Receipt";
				
			// send to page admin
			/* $to=$remailadd;				
			$to_name=$username;				
			
			$content='';
			$title = 'Memorial Payment Receipt';
			
			$message_body = $this->emailtemp->payment_email_upgrade($title, $content, $vlink, $to_name, $memorial_name, $pakage, $pakage_price, $price);
			$this->smtp_email->send($from, $from_name, $to, $subject, $message_body); */
			//end payment receipt	
			
			
			
			redirect('memorial/payment_success?redirect=manage_memorial&memo_id='.$memo_id); 
			//redirect('memorial/manage_memorial/'.$memo_id); 
			
		
	}
	  public function sendCurlz($url, $data)
		{
		   /* echo json_encode($data);
			exit;*/
			$request = curl_init(); 
			curl_setopt( $request, CURLOPT_URL, $url );
			//curl_setopt( $request, CURLOPT_PORT , 7790 );
			curl_setopt( $request, CURLOPT_CUSTOMREQUEST, "POST" ); 
			curl_setopt( $request, CURLOPT_POSTFIELDS, json_encode($data)); 
			curl_setopt( $request, CURLOPT_FOLLOWLOCATION, 1 ); 
			curl_setopt( $request, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $request, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Content-Type: application/json', 'Content-Length: ' . strlen( json_encode($data) ) ) );       
			//curl_setopt( $request, CURLOPT_TIMEOUT, 5 );
			//curl_setopt( $request, CURLOPT_CONNECTTIMEOUT, 5 );
						
			$response = curl_exec( $request );
			
			
			if(!curl_errno($request)){
			  $info = curl_getinfo($request);
			  echo 'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'];
			} else {
			  echo 'Curl error: ' . curl_error($request);
			} 
			
			
			
			
			
			curl_close( $request );
			
			//$result = json_decode( $response );
			print_r($response);
			exit;
			return $response;

		}

		public function testCurl( $url, $token ,$fields)
		{
			//https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest
			//IO02Oae16InaqGApWnYgsKe3HCEF
			
			/*{ "BusinessShortCode" : "925888", "Password" :"OTI1ODg4MTQ2YmFiYjU2NWZiYmY0YzViYzUxZmU5NGFiNjIyNmVhMTYwMTRlMTM0YWY3ZmQzNDJmOWM0MTZhNWU5Mzg2MTIwMTkwNTAxMDgzODM0", "Timestamp" :"20190501083834", "TransactionType" :"CustomerPayBillOnline" , "Amount" :"3", "PartyA":"254724756011", "PartyB" :"925888", "PhoneNumber" :"254724756011", "CallBackURL" :"https://agizapap.com/KaruraResponse" ,//callback url must be https "AccountReference" :"testing payment", "BillRefNumber" :"reference", "TransactionDesc":"testing payment" }*/
			
			//echo $fields; exit;
			
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $fields,
		  CURLOPT_HTTPHEADER => array(
			"authorization: Bearer ".$token,
			"cache-control: no-cache",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
		
		exit;
	}

	public function getAuoth($key,$secret)
	{
		//echo "in get Auth"; exit;
		$curl = curl_init();
		$credentials = base64_encode($key.':'.$secret);

		//return $credentials;

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"authorization: Basic ".$credentials
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		//echo $response;

		if ($err) {
		  echo "cURL Error #:" . $err;
		  $resp = ['status' => 0,
					 'message' => $err];
					 return json_encode($resp);
		} else {
			$json = json_decode($response);

		//var_dump($json);
		//exit();
			$access_token = $json->access_token;
			$resp = ['status' => 1,
					 'message' => $access_token];
		  return json_encode($resp);
		}
	}
		
	/* public function mpesa_callback(){
		
		$resp = $_POST; 
		$this->db->insert("mpesa_response", array("response"=>$resp, "paid_by"=>123));
	} */
	
	public function createaninvoice($invoiceparams)
	{
		echo $this->db->insert('allinvoices',$invoiceparams ); 
		return $this->db->insert_id();
	}
	
	
}
