<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Featured extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $sess_data=$this->session->userdata('admin');
        if(!isset($sess_data->admin_email))
        {
            redirect('admin/login');
        }      
        $this->admin_id = $sess_data->admin_id;       
        $this->admin_name = $sess_data->admin_name;       
    }
    public function index()
    {
		$this->data['page_title'] = "Featured Memorials";
		//echo "Featured Memorials Goes here";
		
		$this->db->update("featured_memo_tribute" , array("read_status"=>1));
		$memorials = $this->db->query("select * from featured_memorials order by ID desc")->result();

		foreach($memorials as $m){
			$m->tributes = $this->db->query("select count(*) as cnt from featured_memo_tribute where memo_id = ".$m->ID)->row()->cnt;
		}
		
		$this->data['memorials'] = $memorials;
		$this->load->view('admin/all_featured_view', $this->data);
        
    }
	
	public function add_featured_memorial(){ 
		
		$this->data['page_title'] = "Add Featured Memorial";
		$this->load->view('admin/add_featured_memorial', $this->data);
	}
	
	public function delete_memo(){
		
		$memo_id = $this->input->post('id');
		
		$this->db->where("featured_id", $memo_id)->delete("featured_images");
		$this->db->where("memo_id", $memo_id)->delete("featured_memo_tribute");
		
		$this->db->where("ID", $memo_id)->delete('featured_memorials');
		echo "success";
	}
	
	public function delete_tribute(){
		
		$tribute_id = $this->input->post('id');
		$this->db->where("ID", $tribute_id)->delete("featured_memo_tribute");
		echo "success";
	}
	
	public function edit_memo($id){
		
		
		$this->data['page_title'] = "Edit Featured Memorial";
		$this->data['memo'] = $this->db->query("select * from featured_memorials where ID = ".$id)->row();
		$tributes = $this->db->query("select * from featured_memo_tribute where memo_id = ".$id)->result();
		foreach($tributes as $t){
			$res = $this->db->query("select ID, fname, lname from users where ID = '$t->createdby'")->row();
			$t->by = $res->fname.' '.$res->lname;
		}

		//echo "<pre>"; print_r($tributes); exit;
		
		$this->data['tributes'] = $tributes;
		
		$this->data['images'] = $this->db->query("select * from featured_images where featured_id = ".$id)->result();
		
		$this->load->view('admin/edit_featured_memorial', $this->data);
		
	}
	
	public function del_image(){
		
		$id = $this->input->post('id');
		$this->db->where("ID", $id)->delete("featured_images");
		echo 'success';
	}
	
	public function update_current_featured(){
		
		$id=$this->input->post("id");
		$this->db->update('featured_memorials', array("current_featured"=>0));
		$this->db->where("ID", $id)->update('featured_memorials', array("current_featured"=>1));
		echo "success";
	}
	
	public function do_add_featured(){
		
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');
		$this->form_validation->set_rules('dod', 'Date of Death', 'required');
		$this->form_validation->set_rules('placeofdeath', 'Place Of Death', 'required');
		$this->form_validation->set_rules('cropped_image', 'Display Image', 'required');
		
		$this->form_validation->set_rules('about', 'Quote', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		
		
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);
				//echo "dddd ".$error; exit;
                 //redirect('admin/home/add');
                redirect($_SERVER['HTTP_REFERER']);
			}
		  else
			{
				 $data['fname'] = $this->input->post('fname');
				 $data['lname'] = $this->input->post('lname');
				 $data['dob'] = date('Y-m-d', strtotime($this->input->post('dob')));
				 $data['dod'] = date('Y-m-d', strtotime($this->input->post('dod')));
				 $data['placeofdeath'] = $this->input->post('placeofdeath');
				 $data['cropped_image'] = $this->input->post('cropped_image');
				 
			     $data['about'] = $this->input->post('about');
			     $data['description'] = $this->input->post('description');
				 
				 //echo "<pre>"; print_r($data); exit;
				 
               /////////////////////
				
                $config =  array(
                  'upload_path'     => './uploads/music/',
                  'allowed_types'   => "mp3"
                );
					
				$this->load->library('upload', $config);
				
				//upload audio
				if($this->upload->do_upload('audio'))
				{
					$filed = $this->upload->data();
					$data['audio'] = $filed["file_name"];	
				}				
				
				///////////////////////////			 
          
         	     $this->db->insert('featured_memorials', $data);
                 $id=$this->db->insert_id();
				 
				 
				 
				 
				 ///////// Uploade multiple Images//////////	
					
					$this->load->library('upload');
					$dataInfo = array();
					$files = $_FILES;
					$cpt = count($_FILES['userfile']['name']);
					for($i=0; $i<$cpt; $i++)
					{
						$_FILES['userfile']['name']= $files['userfile']['name'][$i];
						$_FILES['userfile']['type']= $files['userfile']['type'][$i];
						$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
						$_FILES['userfile']['error']= $files['userfile']['error'][$i];
						$_FILES['userfile']['size']= $files['userfile']['size'][$i];

						$this->upload->initialize($this->set_upload_options());
						//echo $pid; exit;//check here
						if($this->upload->do_upload('userfile')){

							// Uploaded file data
							$imageData = $this->upload->data();
							$this->db->insert("featured_images", array("featured_id"=>$id, "image"=>$imageData['file_name']));

						}else{
							//print_r($this->upload->display_errors()); exit;
						}
					} 
					
			     $this->session->set_flashdata('success', 'Memorial Added Successfully');
				 redirect('admin/featured/add_featured_memorial');			
			}
		
	}
	
	public function do_update_featured(){
		
		
		$id = $this->input->post('id');
		
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');
		$this->form_validation->set_rules('dod', 'Date of Death', 'required');
		$this->form_validation->set_rules('placeofdeath', 'Place Of Death', 'required');
		$this->form_validation->set_rules('cropped_image', 'Display Image', 'required');
		
		$this->form_validation->set_rules('about', 'Quote', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		
		
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);
				//echo "dddd ".$error; exit;
                 //redirect('admin/home/add');
                redirect($_SERVER['HTTP_REFERER']);
			}
		  else
			{
				 $data['fname'] = $this->input->post('fname');
				 $data['lname'] = $this->input->post('lname');
				 $data['dob'] = date('Y-m-d', strtotime($this->input->post('dob')));
				 $data['dod'] = date('Y-m-d', strtotime($this->input->post('dod')));
				 $data['placeofdeath'] = $this->input->post('placeofdeath');
				 $data['cropped_image'] = $this->input->post('cropped_image');
				 
			     $data['about'] = $this->input->post('about');
			     $data['description'] = $this->input->post('description');
				 
				 //echo "<pre>"; print_r($data); exit;
				 
               /////////////////////
				
                $config =  array(
                  'upload_path'     => './uploads/music/',
                  'allowed_types'   => "mp3"
				  //'max_size'     => '*'
                );
					
				$this->load->library('upload', $config);
				
				//upload audio
				if($this->upload->do_upload('audio'))
				{
					$filed = $this->upload->data();
					$data['audio'] = $filed["file_name"];	
				}
				/* else{
					echo $this->upload->display_errors();
					exit;
				} */			
				
				
				///////////////////////////			 
          
				$this->db->where("ID", $id)->update('featured_memorials', $data);
         	     //$this->db->insert('featured_memorials', $data);
                 //$id=$this->db->insert_id();
				 
				 
				 
				 
				 ///////// Uploade multiple Images//////////	
					
					$this->load->library('upload');
					$dataInfo = array();
					$files = $_FILES;
					$cpt = count($_FILES['userfile']['name']);
					for($i=0; $i<$cpt; $i++)
					{
						$_FILES['userfile']['name']= $files['userfile']['name'][$i];
						$_FILES['userfile']['type']= $files['userfile']['type'][$i];
						$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
						$_FILES['userfile']['error']= $files['userfile']['error'][$i];
						$_FILES['userfile']['size']= $files['userfile']['size'][$i];

						$this->upload->initialize($this->set_upload_options());
						//echo $pid; exit;//check here
						if($this->upload->do_upload('userfile')){

							// Uploaded file data
							$imageData = $this->upload->data();
							$this->db->insert("featured_images", array("featured_id"=>$id, "image"=>$imageData['file_name']));

						}else{
							//print_r($this->upload->display_errors()); exit;
						}
					} 
					
			     $this->session->set_flashdata('success', 'Memorial Updated Successfully');
				 redirect($_SERVER['HTTP_REFERER']);		
			}
		
	}
	
	public function set_upload_options()
	{   
		//upload an image options
		$config = array();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = '0';
		//$config['overwrite']     = FALSE;

		return $config;
	}
    
}
