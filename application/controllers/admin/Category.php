<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $sess_data=$this->session->userdata('admin');
        if(!isset($sess_data->admin_email))
        {
            redirect('admin/login');
        }      
        $this->admin_id = $sess_data->admin_id;       
    }
    public function index()
    {
		$this->data['page_title'] = "List of Categories";
		$this->data['categories'] = $this->db->query("select * from categories order by ID desc")->result_array();
		$this->load->view('admin/list_category_view', $this->data);
        
    }
   		
	public function delete_category(){
		$cat_id = $this->input->post('id');
		$cnt = $this->db->query("select count(*) as cnt from products where Category = ".$cat_id)->row()->cnt;
		if($cnt > 0){
			echo "Category is in use, You cannot delete this category.";
			exit;
		}else{
			$del = $this->db->where('ID', $cat_id)->delete('categories');
			echo "success";
			exit;
		}
		
	}
	
	
     public function add()
    {
        $this->data['page_title'] = "Add Category";
		$this->load->view('admin/add_category', $this->data);
    }
	
	
    function add_category()
    {
	
		$this->form_validation->set_rules('name', 'Title', 'required');
				
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
                 redirect('admin/category/add');
			}
		  else
			{
				
			     $data['Name'] = $this->input->post('name');
			     $data['createdAt'] = date("Y-m-d H:i:s");

                 /////////////////////

                 $config =  array(
                    'upload_path'     => './uploads/categories/',
                    'allowed_types'   => "gif|jpg|png|jpeg",
                    'max_size'        => "0"
                    //'max_height'      => "1768",
                    //'max_width'       => "2048"
                 );

                 $this->load->library('upload', $config);

                 //upload image
                 if($this->upload->do_upload('UserPhoto'))
                 {
                    $filed = $this->upload->data();
                    $data['Image'] = $filed["file_name"];
                 }else{
                     print_r($this->upload->display_errors()); exit;
                 }

                 ///////////////////////////

         	     $this->db->insert('categories', $data);
			     $this->session->set_flashdata('success', 'Category Added Successfully');
				 redirect('admin/category');			
			}
		}
		
	  
	  
      function edit($id)
		{
            $this->data['page_title'] = "Edit Category";
			$r = $this->db->query("select * from categories where ID = ".$id)->row();
           	if ($r == false)
		      {
		     	redirect("admin/category");
    			}
               
            $this->data['category'] =$r;
            $this->load->view('admin/edit_category', $this->data);
		}
		
        function update_category($id)
		{
			$this->form_validation->set_rules('name', 'Title', 'required');
			
			if ($this->form_validation->run() == FALSE)
				{
					 $error=validation_errors();
					 $this->session->set_flashdata('error', $error);    
					 redirect('admin/category/edit/'.$id);
				}
			  else
				{
					 
					 $data['Name'] = $this->input->post('name');

                    /////////////////////

                    $config =  array(
                        'upload_path'     => './uploads/categories/',
                        'allowed_types'   => "gif|jpg|png|jpeg",
                        'max_size'        => "0"
                        //'max_height'      => "1768",
                        //'max_width'       => "2048"
                    );

                    $this->load->library('upload', $config);

                    //upload image
                    if($this->upload->do_upload('UserPhoto'))
                    {
                        $filed = $this->upload->data();
                        $data['Image'] = $filed["file_name"];
                    }else{
                        print_r($this->upload->display_errors()); exit;
                    }

                    ///////////////////////////

					 $this->db->where("ID", $id)->update("categories", $data);
					 $this->session->set_flashdata('success', 'Record Updated Successfully');
					 redirect('admin/category');			
				}
		}
      function view_profile($id)
		{		
    		$this->data['page_title'] = "View Profile";
    		$this->data['user_data'] = $this->db->query("select * from hotels where Id =".$id)->result_array();//$this->member_model->get_single($id);
    		$this->load->view('admin/view_member_profile', $this->data);
		}   
     function customer_email_check($str)
		{
	
		$r = $this->general_model->role_exists('MemberEmail', $str,'members');
		if ($r == false)
			{
			$this->form_validation->set_message('customer_email_check', 'Email Already Exists!!!');
			return false;
			}
		  else
			{
			return true;
			}
		}    
    public function change_status()
    {
        $id=$_POST['id'];
        if($id)
        {
             $query = $this->db->query("SELECT * FROM websites WHERE ID='".$id."'");
             $result= $query->result_array();
			 
             if($result[0]['Status']=='Active')
             {                
                $this->db->set('Status', 'Inactive');             
		   	    $this ->db->where('ID',$id);
			    $this->db->update('websites');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
             else
             {
                $this->db->set('Status', 'Active');
		   	    $this ->db->where('ID',$id);
			    $this->db->update('websites');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
               
        }
        else
        {
            echo json_encode(array("status"=>0,"msg"=>"Status Changing Failed"));
        }
     
    }
	
	public function change_job_status(){
		$id=$_POST['id'];
        if($id)
        {
             $query = $this->db->query("SELECT * FROM jobopenings WHERE id='".$id."'");
             $result= $query->result_array();
			 
             if($result[0]['Status']=='Active')
             {                
                $this->db->set('Status', 'Inactive');             
		   	    $this ->db->where('id',$id);
			    $this->db->update('jobopenings');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
             else
             {
                $this->db->set('Status', 'Active');
		   	    $this ->db->where('id',$id);
			    $this->db->update('jobopenings');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
               
        }
        else
        {
            echo json_encode(array("status"=>0,"msg"=>"Status Changing Failed"));
        }
	}
     public function email_checks()
		{
    	   if(isset($_GET['email']))
           {
                $str=$_GET['email'];
        		$r = $this->general_model->role_exists('MemberEmail', $str,'members');
            		if ($r == false)
            			{            		
            			echo 'false';
            			}
            		  else
            			{
            			echo 'true';
            			}
             }
             else
             {
                echo 'false';
             }   
		}
    public function get_states()
    {        
        $cid = $this->input->post('country_id');
       if($cid)
        {
          $result = $this->general_model->get_provinces($cid);
          
          echo "<option value=''>--Select State--</option>"; 
          
          foreach($result as $row){ 
           
            echo "<option value='".$row['id']."'>".$row['name']."</option>"; 
            
          }
         }
    }
    public function get_cities()
    {        
        $sid = $this->input->post('state_id');
       if($sid)
        {
          $result = $this->general_model->get_cities($sid);
          
          echo "<option value=''>--Select City--</option>"; 
          
          foreach($result as $row){ 
           
            echo "<option value='".$row['id']."'>".$row['name']."</option>"; 
            
          }
         }
    }
	
	public function availability($hotel_id){
		//echo $hotel_id; exit;
		$this->data['page_title'] = "Availability";
        $this->load->view('admin/add_availibility', $this->data);
	}
	
	
	public function delete_job(){
		$del = $this->db->where('id', $this->input->post('id'))->delete('jobopenings');
		echo $del;
	}
	
	public function view_job($id){
		$this->data['job'] = $this->db->query("select * from jobopenings where id = ".$id)->row();
		$this->data['page_title'] = "Job Detail";
        $this->load->view('admin/view_job_detail', $this->data);
	}
    


}
