<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $sess_data=$this->session->userdata('admin');
        if(!isset($sess_data->admin_email))
        {
            redirect('admin/login');
        }      
        $this->admin_id = $sess_data->admin_id;       
    }
    
    public function index()
    {
		/*$this->data['page_title'] = "Coupon";
		$this->data['coupons'] = $this->db->query("select * from coupons order by ID desc")->result();
		$this->load->view('admin/coupon_management', $this->data);*/
        
    }
	
	public function content_management(){ 
		
		$this->data['page_title'] = "Content Management";
		$this->data['pages'] = $this->db->query("select * from content_management")->result();
		$this->load->view('admin/content_management', $this->data); 
		
	}
	
	public function add_static_page(){ 
		
		$this->data['page_title'] = "Add Contents";
		$this->load->view('admin/add_static_page', $this->data);
		
	}
	
	public function add_content(){
		
		$data['title']=$this->input->post('title');
		$data['description']=$this->input->post('basic');  
		
		$this->db->insert("content_management", $data);
		$this->session->set_flashdata('success', "Record Added Successfully");
		redirect('admin/manage/content_management');
		
	}
	
	public function edit_static_page($id){ 
		
		$this->data['page_title'] = "Content Management";
		$this->data['page'] = $this->db->query("select * from content_management where ID = ".$id)->row();
		$this->load->view('admin/edit_static_page', $this->data);
		
	}
	
	public function update_content(){
		
		$id=$this->input->post('id');
		$data['title']=$this->input->post('title');
		$data['description']=$this->input->post('basic');  
		
		
		//echo "<pre>"; print_r($_POST); exit;
		$this->db->where("ID", $id)->update("content_management", $data);
		$this->session->set_flashdata('success', "Record Updated Successfully");
		redirect('admin/manage/content_management');
		
	}
	
	public function view_static_page($id){ 
		
		$this->data['page_title'] = "View Content";
		$this->data['page'] = $this->db->query("select * from content_management where ID = ".$id)->row();
		$this->load->view('admin/view_static_page', $this->data);
		
	}
    
    public function music()
    {
		$this->data['page_title'] = "Manage Music";
		$this->data['music'] = $this->db->query("select * from memorial_music")->result();
		$this->load->view('admin/music_management', $this->data);
        
    }
    
    public function add_music(){
	    $this->data['page_title'] = "Add Music";
		$this->load->view('admin/add_music', $this->data);
	    
	}
	
	public function do_add_music(){
	    
	    $this->form_validation->set_rules('title', 'Title', 'required');
		//$this->form_validation->set_rules('audio', 'Audio File', 'required');
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('admin/manage/add_music');
			}
		  else
			{
			    $data['title'] = $this->input->post('title');
				$config =  array(
                    'upload_path'     => './uploads/music/',
                    'allowed_types'   => "mp3",
                    'max_size'        => "*"
        
                    /*  'allowed_types' => 'avi|mp4|flw|mov',
                    'max_height'      => "1768",
                    'max_width'       => "2048"  */
                );
        
                $this->load->library('upload', $config);
        
                if($this->upload->do_upload("audio"))
                {
                    $filed = $this->upload->data();
                    $data['file'] = $filed["file_name"];
                    $this->db->insert("memorial_music", $data);
                    $this->session->set_flashdata('success', "File Uploade Successfully");
                }
                else
                {
                    //if (!empty($_FILES['userfile']['name'])) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error', $error['error']);
                    //}
                }
                 
                 redirect('admin/manage/add_music');
			}
	    
	}
	
	public function delete_music(){  
		
		$id = $this->input->post('id');
		$aud = $this->input->post('aud');
		
		$this->db->where('ID', $id)->delete('memorial_music');
		$path = FCPATH.'uploads/music/'.$aud;
		unlink($path);
		echo 'success';
		
	}
	
	public function profanity()
    {
		$this->data['page_title'] = "Manage Profanity";
		$this->data['profanity'] = $this->db->query("select * from profanity")->result();
		$this->load->view('admin/profanity_management', $this->data);
        
    }
	
	public function add_profanity(){
        
        //$id = $this->input->post('id');
        $word = $this->input->post('word');
        $cnt = $this->db->query("select count(*) as cnt from profanity where word = '$word'")->row()->cnt;
		if($cnt > 0){
			$this->session->set_flashdata('error', "Word already added");
		}else{
			$this->db->insert("profanity", array("word"=>$word));
			$this->session->set_flashdata('success', "Word Added Successfully");
		}
		
		redirect('admin/manage/profanity');
        
    }
    
    public function update_profanity(){
        
        $id = $this->input->post('id');
        $word = $this->input->post('word');
        
		$this->db->where('ID', $id)->update("profanity", array("word"=>$word));
		echo $word;
        
    }
    
    public function delete_word(){  
		
		$id = $this->input->post('id');
		$this->db->where('ID', $id)->delete('profanity');
		echo 'success';
		
	}
	
	
	public function pakage()
    {
		$this->data['page_title'] = "Manage Pakages";
		$this->data['pakages'] = $this->db->query("select * from payment_plan")->result();
		$this->load->view('admin/pakage_managment', $this->data);
        
    }
    
    public function edit_pakage($id){
        
        $this->data['page_title'] = "Edit Pakages";
		$this->data['pakage'] = $this->db->query("select * from payment_plan where ID = ".$id)->row();
        $this->load->view('admin/edit_pakage', $this->data);
    }
    
    public function update_pakage(){
        
        $id = $this->input->post('id');
        
        $data['title'] = $this->input->post('title');
        $data['photo_limit'] = $this->input->post('photo_limit');
        $data['video_limit'] = $this->input->post('video_limit');
        $data['audio_limit'] = $this->input->post('audio_limit');
        $data['page_managers'] = $this->input->post('page_managers');
        $data['price'] = $this->input->post('price');
        $data['annual_maintenance_fee'] = $this->input->post('annual_maintenance_fee');
        
		$this->db->where('ID', $id)->update("payment_plan", $data);
	    $this->session->set_flashdata('success', "Pakage Updated Successfully");
        redirect('admin/manage/pakage');
    }
    
    public function delete_pakage(){  
	
		$id = $this->input->post('id');
		$this->db->where('ID', $id)->delete('payment_plan');
		echo 'success';
		
	}
	
	public function testimonials(){
		
		$this->db->update("testimonials" , array("read_status"=>1));
	    
	    $this->data['page_title'] = "Manage Testimonials";
		$testimonials = $this->db->query("select * from testimonials where user_id > 0")->result();
		
		foreach($testimonials as $t){
		    
		    $user = $this->db->query("select * from users where ID = ".$t->user_id)->row();
		    if($user){
		        $t->user_status = $user->status;
		    }else{
		        $t->user_status = "";
		    }
		    
		}
		
		$this->data['testimonials'] = $testimonials;
		$this->load->view('admin/testimonials', $this->data);
	    
	}
	
	public function delete_testimonial(){  
	
		$id = $this->input->post('id');
		$this->db->where('ID', $id)->delete('testimonials');
		echo 'success';
		
	}
	
	public function payments(){
	    
	    $this->data['page_title'] = "Manage Payments";
		$memorials = $this->db->query("select * from memorials where remailadd !='' order by ID desc")->result();
		foreach($memorials as $m){
		    $res = $this->db->query("select * from payment_plan where title = '$m->pakage'")->row();
		    if($res){
		        $m->price = $res->price;
				
				if($m->price > $m->amount_paid && $m->amount_paid > 0){
					$m->discount = $m->price - $m->amount_paid;
				}else{
					$m->discount = 0;
				}
		        $m->currency = $res->currency;
				
		    }else{
		        $m->price = 0;
				$m->discount = 0;
		        $m->currency = "";
		    } 
			
			$m->invoice_id = $this->db->query("select id from allinvoices where memorialid = '$m->ID' order by id desc")->row()->id;
			
		}
		
		$this->data['memorials'] = $memorials;
		$this->load->view('admin/payments', $this->data);
	    
	}
	
	public function invoices(){
	    
	    $this->data['page_title'] = "Manage invoices";
		$memorials = $this->db->query("select * from allinvoices order by id desc")->result();
		
		
		$this->data['memorials'] = $memorials;
		$this->load->view('admin/invoices', $this->data);
	    
	}
	
}
