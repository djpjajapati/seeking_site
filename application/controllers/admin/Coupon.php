<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $sess_data=$this->session->userdata('admin');
        if(!isset($sess_data->admin_email))
        {
            redirect('admin/login');
        }      
        $this->admin_id = $sess_data->admin_id;       
    }
    
    public function index()
    {
		$this->db->update("coupon_log" , array("read_status"=>1));
		
		$this->data['page_title'] = "Coupon";
		$this->data['coupons'] = $this->db->query("select * from coupons order by ID desc")->result();
		$coupon_log = $this->db->query("select * from coupon_log order by ID desc")->result();
		foreach($coupon_log as $c){
			$res = $this->db->query("select fname, lname from memorials where ID = ".$c->memo_id)->row();
			$c->memorial = $res->fname.' '.$res->lname;
		}
		$this->data['coupon_log'] = $coupon_log;
		//echo "<pre>"; print_r($this->data['coupon']); exit;
		$this->load->view('admin/coupon_management', $this->data);
        
    }
	
	public function search_coupons(){
		
		$date1 = $this->input->post('date1');
		$date2 = $this->input->post('date2');
		
		$this->data['page_title'] = "Coupon";
		$this->data['coupons'] = $this->db->query("select * from coupons where expiry > '$date1' and expiry < '$date2' order by ID desc")->result();
		$coupon_log = $this->db->query("select * from coupon_log order by ID desc")->result();
		foreach($coupon_log as $c){
			$res = $this->db->query("select fname, lname from memorials where ID = ".$c->memo_id)->row();
			$c->memorial = $res->fname.' '.$res->lname;
		}
		$this->data['coupon_log'] = $coupon_log;
		$this->data['date1'] = $date1;
		$this->data['date2'] = $date2;
		//echo "<pre>"; print_r($this->data['coupon']); exit;
		$this->load->view('admin/coupon_management', $this->data);
		
	}
    
    public function add_coupon(){
	    $this->data['page_title'] = "Add Coupon";
		$this->load->view('admin/add_coupon', $this->data);
	    
	}
	
	public function do_add_coupon(){
	    
	    $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required');
		$this->form_validation->set_rules('coupon_discount_percentage', 'Coupon Discount Percentage', 'required');
		$this->form_validation->set_rules('frequency', 'Frequency', 'required');
		$this->form_validation->set_rules('expiry', 'expiry', 'required');
        
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('admin/coupon/add_coupon');
			}
		  else
			{
				 $coupon_code = $this->input->post('coupon_code');
				 $cnt = $this->db->query("select count(*) as cnt from coupons where coupon_code = '$coupon_code'")->row()->cnt;
				 
				 if($cnt > 0){ 
					 $this->session->set_flashdata('error', "Coupon code already in use, Please use another");  
					 redirect('admin/coupon/add_coupon');
				 }
				 
			     $data['coupon_code'] = $this->input->post('coupon_code');
			     $data['coupon_discount_percentage'] = $this->input->post('coupon_discount_percentage');
				 $data['frequency'] = $this->input->post('frequency');
			     $data['expiry'] = $this->input->post('expiry');
                 $data['createdAt'] = date('Y-m-d H:i:s');
				
               
         	     $this->db->insert('coupons', $data);
                 
                 redirect('admin/coupon');
			}
	    
	}
	
		public function do_add_Bcoupon(){
	    
	    $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required');
		$this->form_validation->set_rules('coupon_discount_percentage', 'Coupon Discount Percentage', 'required');
		$this->form_validation->set_rules('frequency', 'Frequency', 'required');
		$this->form_validation->set_rules('expiry', 'expiry', 'required');
        
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('admin/coupon/add_coupon');
			}
		  else
			{
				$coupon_quantity = $this->input->post('coupon_quantity');
				for($qqq=0; $qqq<$coupon_quantity; $qqq++)
				{
				 $coupon_code = $this->input->post('coupon_code');
				 $permitted_chars = '0123456789ABCDKLMNOXYZ';
				 $randomC=substr(str_shuffle($permitted_chars), 0, 4);
				 $coupon_code =$coupon_code."_".$randomC;
				 $cnt = $this->db->query("select count(*) as cnt from coupons where coupon_code = '$coupon_code'")->row()->cnt;
				 
				 if($cnt > 0){ 
					$qqq--;
					 continue;
				 }
				 
			     $data['coupon_code'] = $coupon_code;
			     $data['coupon_discount_percentage'] = $this->input->post('coupon_discount_percentage');
				 $data['frequency'] = $this->input->post('frequency');
			     $data['expiry'] = $this->input->post('expiry');
                 $data['createdAt'] = date('Y-m-d H:i:s');
				
               
         	     $this->db->insert('coupons', $data);
				}
                 redirect('admin/coupon');
			}
	    
	}
	
	public function edit_coupon($id){
	    
	    $this->data['page_title'] = "Edit Coupon";
		$this->data['coupon'] = $this->db->query("select * from coupons where ID =".$id)->row();
		$this->load->view('admin/edit_coupon', $this->data);
	    
	}
	
	public function do_edit_coupon(){
	    
	    $id = $this->input->post('id');
	    $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required');
		$this->form_validation->set_rules('coupon_discount_percentage', 'Coupon Discount Percentage', 'required');
		$this->form_validation->set_rules('frequency', 'Frequency', 'required');
		$this->form_validation->set_rules('expiry', 'expiry', 'required');
        
		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
				 redirect('admin/coupon/edit_coupon/'.$id);
			}
		  else
			{
				 $coupon_code = $this->input->post('coupon_code');
				 $cnt = $this->db->query("select count(*) as cnt from coupons where coupon_code = '$coupon_code' and ID != '$id'")->row()->cnt;
				 
				 if($cnt > 0){ 
					 $this->session->set_flashdata('error', "Coupon code already in use, Please use another");  
					 redirect('admin/coupon/edit_coupon/'.$id);
				 }
				 
			     $data['coupon_code'] = $this->input->post('coupon_code');
			     $data['coupon_discount_percentage'] = $this->input->post('coupon_discount_percentage');
				 $data['frequency'] = $this->input->post('frequency');
			     $data['expiry'] = $this->input->post('expiry');
                 $data['createdAt'] = date('Y-m-d H:i:s');
				
               
         	     $this->db->where('ID', $id)->update('coupons', $data);
                 
                 redirect('admin/coupon');
			}
	    
	}
	
	public function delete_coupon(){  // from multiple pages
		
		$id=$this->input->post('id');
		$this->db->where('ID', $id)->delete('coupons');
		echo 'success';
		
	}
	
	
	
	/*public function update_featured(){
		
		$memo_id = $this->input->post('memo_id');
		$data['featured'] = $this->input->post('featured');
		$this->db->where('ID', $memo_id)->update("memorials", $data);
		echo "success";
		
	}*/
	
}
