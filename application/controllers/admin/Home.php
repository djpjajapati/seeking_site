<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $sess_data=$this->session->userdata('admin');
        if(!isset($sess_data->admin_email))
        {
            redirect('admin/login');
        }      
        $this->admin_id = $sess_data->admin_id;       
        $this->admin_name = $sess_data->admin_name;       
    }
    public function index()
    {
		$this->data['page_title'] = "Dashboard";
		
		$this->data['total_memorials'] = $this->db->query("select count(*) as cnt from memorials")->row()->cnt;
		$this->data['total_members'] = $this->db->query("SELECT count(*) as cnt FROM users")->row()->cnt;
		$this->data['total_login'] = $this->db->query("SELECT count(*) as cnt FROM users where LastLogin !='0000-00-00 00:00:00'")->row()->cnt;
		$this->data['total_tributes'] = $this->db->query("select count(*) as cnt from memo_tribute")->row()->cnt;
		
		$this->data['latest_memorials'] = $this->db->query("select * from memorials order by ID desc limit 6")->result();
		$this->data['top_memorials'] = $this->db->query("select * from memorials order by views desc limit 5")->result();
		$this->data['activity_log'] = $this->db->query("select * from activity_log order by ID desc limit 4")->result();
		
		$recent_tributes = $this->db->query("select * from memo_tribute order by ID desc limit 6")->result();
		
		foreach($recent_tributes as $t){
			$m = $this->db->query("select fname, lname from memorials where ID = ".$t->memo_id)->row();
			$u = $this->db->query("select fname, lname from users where ID = ".$t->createdby)->row();
			$t->memo_name = $m->fname.' '.$m->lname;
			$t->by = $u->fname.' '.$u->lname;
		}
		$this->data['recent_tributes'] = $recent_tributes;
		
		$this->load->view('admin/dashboard', $this->data);
        
    }
	
	public function activities(){ 
		
		$this->db->update("activity_log" , array("read_status"=>1));
		
		$this->data['title'] = 'All Memorial Activities';
		$this->data['activities'] = $this->db->query("select * from activity_log order by ID desc")->result();
		$this->load->view('admin/all_activities', $this->data);
		
	}
	
	
	
    /* public function add()
    {
        $this->data['page_title'] = "Add product";
        $this->data['categories'] =$this->db->query("select * from categories")->result();
        $this->data['grinds'] =$this->db->query("select * from grinds")->result();
        $this->data['beans'] =$this->db->query("select * from beans")->result();
		$this->load->view('admin/add_product_view', $this->data);
    }
	
    public function add_product()
    {
	
		//$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('weight', 'Weight', 'required');
		$this->form_validation->set_rules('unit', 'Unit', 'required');

		$this->form_validation->set_rules('category[]', 'Category', 'required');
		$this->form_validation->set_rules('grind[]', 'Grind', 'required');
		$this->form_validation->set_rules('bean[]', 'Bean', 'required');
		$this->form_validation->set_rules('roast[]', 'Roast', 'required');

		if ($this->form_validation->run() == FALSE)
			{
		         $error=validation_errors();
                 $this->session->set_flashdata('error', $error);    
                 //redirect('admin/home/add');
                redirect($_SERVER['HTTP_REFERER']);
			}
		  else
			{
				 $data['Title'] = $this->input->post('title');
			     $data['Description'] = $this->input->post('description');
				 
				 $categories = implode(',', $this->input->post('category'));
				 $grinds = implode(',', $this->input->post('grind'));
				 $beans = implode(',', $this->input->post('bean'));
				 $roast = implode(',', $this->input->post('roast'));
			     $data['Category'] = $categories;
			     $data['Grind'] = $grinds;
			     $data['Bean'] = $beans;
			     $data['Roast'] = $roast;

               /////////////////////
				
                $config =  array(
                  'upload_path'     => './uploads/products/',
                  'allowed_types'   => "gif|jpg|png|jpeg",
                  'max_size'        => "0",
                  'max_height'      => "1768",
                  'max_width'       => "2048"  
                );
					
				$this->load->library('upload', $config);
				
				//upload image
				if($this->upload->do_upload('UserPhoto'))
				{
					$filed = $this->upload->data();
					$data['Image'] = $filed["file_name"];	
				}				
				
				///////////////////////////			 
          
         	     $this->db->insert('products', $data);
                 $id=$this->db->insert_id();
				 
				 $size['pid'] = $id;
				 $size['price'] = $this->input->post('price');
				 $size['weight'] = $this->input->post('weight');
				 $size['unit'] = $this->input->post('unit');
				
				 $this->db->insert('product_weight_price', $size);
								 
			     $this->session->set_flashdata('success', 'Product Added Successfully');
				 redirect('admin/home');			
			}
		}
		
		public function delete_product(){
			$del = $this->db->where('ID', $this->input->post('id'))->delete('products');
			$del1 = $this->db->where('pid', $this->input->post('id'))->delete('product_weight_price');
			echo $del;
		}
		
	  
	  
      function edit($id)
		{
            $this->data['page_title'] = "Edit Website";
			$r = $this->db->query("select * from products where ID = ".$id)->row();
           	if ($r == false)
		      {
		     	redirect("admin/home");
    			}
               
            $this->data['product'] =$r;
            $this->data['categories'] =$this->db->query("select * from categories")->result();
			$this->data['grinds'] =$this->db->query("select * from grinds")->result();
			$this->data['beans'] =$this->db->query("select * from beans")->result();
            $this->data['sizes'] =$this->db->query("select * from product_weight_price where pid = ".$id)->result();
            $this->data['product_images'] =$this->db->query("select * from product_images where pid = ".$id)->result();

            //echo  "<pre>"; print_r($this->data['product_images']); exit;

            $this->load->view('admin/edit_product_view', $this->data);
		}
		
        function edit_product($id)
		{
			//$this->form_validation->set_rules('category', 'Category', 'required');
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');

            $this->form_validation->set_rules('category[]', 'Category', 'required');
            $this->form_validation->set_rules('grind[]', 'Grind', 'required');
            $this->form_validation->set_rules('bean[]', 'Bean', 'required');
            $this->form_validation->set_rules('roast[]', 'Roast', 'required');

			if ($this->form_validation->run() == FALSE)
				{
					 $error=validation_errors();
					 $this->session->set_flashdata('error', $error);    
					 redirect('admin/home/edit/'.$id);
				}
			  else
				{
					 $data['Title'] = $this->input->post('title');
					 $data['Description'] = $this->input->post('description');

					 $categories = implode(',', $this->input->post('category'));
					 $grinds = implode(',', $this->input->post('grind'));
					 $beans = implode(',', $this->input->post('bean'));
                     $roast = implode(',', $this->input->post('roast'));
					 $data['Category'] = $categories;
					 $data['Grind'] = $grinds;
					 $data['Bean'] = $beans;
					 $data['Roast'] = $roast;

				   /////////////////////
					
					$config =  array(
					  'upload_path'     => './uploads/products/',
					  'allowed_types'   => "gif|jpg|png|jpeg",
					  'max_size'        => "0",
					  'max_height'      => "1768",
					  'max_width'       => "2048"  
					);
						
					$this->load->library('upload', $config);
					
					//upload image
					if($this->upload->do_upload('UserPhoto'))
					{
						$filed = $this->upload->data();
						$data['Image'] = $filed["file_name"];	
					}

					///////////////////////////			 
					$this->db->where("ID", $id)->update("products", $data);
					$this->session->set_flashdata('success', 'Record Updated Successfully');
					redirect('admin/home');			
				}
		}
		
		public function add_product_weight(){
			
			$size['pid'] = $this->input->post('pid');
			$size['price'] = $this->input->post('price');
			$size['weight'] = $this->input->post('weight');
			$size['unit'] = $this->input->post('unit');
			
			$this->db->insert('product_weight_price', $size); 
			$sizes = $this->db->query("select * from product_weight_price where pid = ".$this->input->post('pid'))->result();
			
			
			$i=1; 
			foreach($sizes as $s){ ?>
			<tr id="company1_<?php echo $s->ID; ?>" class="odd gradeX">
			  
				<td>
				
				<?php echo $i; ?>
				
				</td>
			  
				<td>
					 <?php echo $s->weight; ?>
				</td>
				
				<td>
					<?php echo $s->unit; ?>
				</td>
				
				<td>
					<?php echo $s->price; ?>
				</td>
				
				
				<td class="center" style="width: 14%; padding:5px; vertical-align: middle; text-align: center;">
				  
				  <!--<a href="<?php echo base_url();?>admin/home/edit/<?php echo $s->ID; ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>-->
				  
				  <a  onclick="delete_product_weight(<?php echo $s->ID; ?>)" class="btn btn-primary btn-xs"><i class="fa fa-trash-o"></i></a>
				</td>
			</tr>
			<?php $i++; } 
			
		}
		
		public function delete_product_weight(){
			
			$del = $this->db->where('ID', $this->input->post('id'))->delete('product_weight_price');
			echo $del;
			
		}

        public function delete_product_image(){

            $del = $this->db->where('ID', $this->input->post('id'))->delete('product_images');
            echo $del;

        }

        public function add_product_images(){

            $pid = $this->input->post('product_id');

            $this->load->library('upload');
            $dataInfo = array();
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for($i=0; $i<$cpt; $i++)
            {
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];

                $this->upload->initialize($this->set_upload_options());
                //echo $pid; exit;//check here
                if($this->upload->do_upload('userfile')){

                    // Uploaded file data
                    $imageData = $this->upload->data();
                    $this->db->insert("product_images", array("pid"=>$pid, "image"=>$imageData['file_name']));
                     //$uploadImgData[$i]['image_name'] = $imageData['file_name'];

                }else{

                    //print_r($this->upload->display_errors()); exit;

                }

                //$this->upload->do_upload();
                //$dataInfo[] = $this->upload->data();
            }

            redirect('admin/home/edit/'.$pid);
        }
	
        public function set_upload_options()
        {
            //upload an image options
            $config = array();
            $config['upload_path'] = './uploads/products/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = '0';
            //$config['overwrite']     = FALSE;
            return $config;
        }
	
	public function update_site_details(){
		
		$site_id = $this->input->post('site_id');
		$detail = $this->input->post('detail');
		$this->db->where("ID", $site_id)->update("websites", array("Details"=>$detail));
		
		//echo strlen(trim($detail)); exit;
		
		if(strlen(trim($detail)) > 0){ 
			$this->session->set_flashdata('success', 'Site details updated successfully');
		}
		
		redirect('admin/home/view/'.$site_id);
	}
	
	public function delete_gallory_image(){
		
		$del = $this->db->where('ID', $this->input->post('id'))->delete('website_gallory');
		
		if($del){
			unlink("./uploads/gallory/".$this->input->post('img'));		
			echo 'success';
			exit;			
		}
	}
	
	public function add_edit_site_feature(){
		
		$feature_id = $this->input->post("feature_id");
		$website_id = $this->input->post("site_id");
		
		//$data['name'] = $this->input->post("name");
		$data['rating'] = $this->input->post("rating");
		
		
		$cnt = $this->db->query("select count(*) as cnt from feature_added where website_id = '$website_id' and feature_id = '$feature_id'")->row()->cnt;
		if($cnt > 0){
			$array = array('feature_id' => $feature_id, 'website_id' => $website_id);
			//$this->db->where($array)->update("feature_added", $data); 
			$this->db->where($array);
			$this->db->update("feature_added", $data); 
			echo "success";
			
		}else{
			$data['feature_id'] = $feature_id;
			$data['website_id'] = $website_id;
			$this->db->insert("feature_added", $data); 
			echo "success";
		}
		
		
	}
		
	function view_profile($id)
	{		
		$this->data['page_title'] = "View Profile";
		$this->data['user_data'] = $this->db->query("select * from hotels where Id =".$id)->result_array();//$this->member_model->get_single($id);
		$this->load->view('admin/view_member_profile', $this->data);
	}   
	function customer_email_check($str)
	{

	$r = $this->general_model->role_exists('MemberEmail', $str,'members');
	if ($r == false)
		{
		$this->form_validation->set_message('customer_email_check', 'Email Already Exists!!!');
		return false;
		}
	  else
		{
		return true;
		}
	}    
    public function change_status()
    {
        $id=$_POST['id'];
        if($id)
        {
             $query = $this->db->query("SELECT * FROM websites WHERE ID='".$id."'");
             $result= $query->result_array();
			 
             if($result[0]['Status']=='Active')
             {                
                $this->db->set('Status', 'Inactive');             
		   	    $this ->db->where('ID',$id);
			    $this->db->update('websites');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
             else
             {
                $this->db->set('Status', 'Active');
		   	    $this ->db->where('ID',$id);
			    $this->db->update('websites');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
               
        }
        else
        {
            echo json_encode(array("status"=>0,"msg"=>"Status Changing Failed"));
        }
     
    }
	
	public function change_job_status(){
		$id=$_POST['id'];
        if($id)
        {
             $query = $this->db->query("SELECT * FROM jobopenings WHERE id='".$id."'");
             $result= $query->result_array();
			 
             if($result[0]['Status']=='Active')
             {                
                $this->db->set('Status', 'Inactive');             
		   	    $this ->db->where('id',$id);
			    $this->db->update('jobopenings');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
             else
             {
                $this->db->set('Status', 'Active');
		   	    $this ->db->where('id',$id);
			    $this->db->update('jobopenings');
               echo json_encode(array("status"=>1,"msg"=>"Status Updated Successfully"));
             }
               
        }
        else
        {
            echo json_encode(array("status"=>0,"msg"=>"Status Changing Failed"));
        }
	}
     public function isUrlValid(){
		 
		  $url = $_GET['url'];
		
		  $pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
		  
		  if(preg_match($pattern, $url)){
			echo 'true'; //return true;
		  } else{
			echo 'false'; //return false;
		  }
	} */
    
}
