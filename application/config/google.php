<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Code Igniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		Rick Ellis
 * @copyright	Copyright (c) 2006, pMachine, Inc.
 * @license		http://www.codeignitor.com/user_guide/license.html
 * @link		http://www.codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * PayPal_Lib Controller Class (Paypal IPN Class)
 *
 * This CI library is based on the Paypal PHP class by Micah Carrick
 * See www.micahcarrick.com for the most recent version of this class
 * along with any applicable sample files and other documentaion.
 *
 * This file provides a neat and simple method to interface with paypal and
 * The paypal Instant Payment Notification (IPN) interface.  This file is
 * NOT intended to make the paypal integration "plug 'n' play". It still
 * requires the developer (that should be you) to understand the paypal
 * process and know the variables you want/need to pass to paypal to
 * achieve what you want.  
 *
 * This class handles the submission of an order to paypal as well as the
 * processing an Instant Payment Notification.
 * This class enables you to mark points and calculate the time difference
 * between them.  Memory consumption can also be displayed.
 *
 * The class requires the use of the PayPal_Lib config file.
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Commerce
 * @author      Ran Aroussi <ran@aroussi.com>
 * @copyright   Copyright (c) 2006, http://aroussi.com/ci/
 *
 */

// ------------------------------------------------------------------------



$config['clientId'] = '1070451556508-e3ju64jssuuuk62g9267l88hflm4ueb3.apps.googleusercontent.com';  //'36080850749-96rc7vh5os1elcqdu28v7ts9bgegid7c.apps.googleusercontent.com'; //add your client id
$config['clientSecret'] = 'RVLxFpH3QZlSniEXI5PlJ3bP'; //add your client secret
$config['redirectUri'] = 'https://safirisalama.com/login/google_callback'; //add your redirect uri
//$config['redirectUri'] = 'http://localhost/consultancy/signup/google_callback'; //add your redirect uri
$config['apiKey'] = 'AIzaSyBrmBQO95oYC5YdS3tSwU3iJKbixXKV3Pk';  //'AIzaSyDW8TCsLeF4U7eqpHrh67VglAf0jsVoBUI'; //add your api key here
$config['applicationName'] ='memorial'; //application name for the api


?>