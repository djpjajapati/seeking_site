<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Code Igniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		Rick Ellis
 * @copyright	Copyright (c) 2006, pMachine, Inc.
 * @license		http://www.codeignitor.com/user_guide/license.html
 * @link		http://www.codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * PayPal_Lib Controller Class (Paypal IPN Class)
 *
 * This CI library is based on the Paypal PHP class by Micah Carrick
 * See www.micahcarrick.com for the most recent version of this class
 * along with any applicable sample files and other documentaion.
 *
 * This file provides a neat and simple method to interface with paypal and
 * The paypal Instant Payment Notification (IPN) interface.  This file is
 * NOT intended to make the paypal integration "plug 'n' play". It still
 * requires the developer (that should be you) to understand the paypal
 * process and know the variables you want/need to pass to paypal to
 * achieve what you want.  
 *
 * This class handles the submission of an order to paypal as well as the
 * processing an Instant Payment Notification.
 * This class enables you to mark points and calculate the time difference
 * between them.  Memory consumption can also be displayed.
 *
 * The class requires the use of the PayPal_Lib config file.
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Commerce
 * @author      Ran Aroussi <ran@aroussi.com>
 * @copyright   Copyright (c) 2006, http://aroussi.com/ci/
 *
 */

// ------------------------------------------------------------------------



$config['facebook_app_id']              = '1768491333296754'; //'2078828192417934'; //'354022678794809'; //'329698384462117'; //'2072142406389482';
$config['facebook_app_secret']          = '6a7b9452fd7999e9158ce1962fa53de5'; //'5d799e9413a0bd5ed17c2362ebf7a5b1'; //'0c55093fdfac9e59bfc672a177dde0c7';  //'a98d40b50dd1800bace093b87b08f4ec';  //'a2296cf7afe74470002623be296e6aa3';
$config['facebook_login_type']          = 'web';
//$config['facebook_login_redirect_url']  = 'http://goalwebs.com/rewarding_program/login/fb_callback';
//$config['facebook_login_redirect_url']  = 'https://safirisalama.com/login/fb_callback';
$config['facebook_login_redirect_url']  = 'login/fb_callback'; 
$config['facebook_logout_redirect_url'] = '/login_with_fb/login';
$config['facebook_permissions']         = array('public_profile', 'email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE; 


?>